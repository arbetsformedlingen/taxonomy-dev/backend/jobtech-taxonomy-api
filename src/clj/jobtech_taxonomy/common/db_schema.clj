(ns jobtech-taxonomy.common.db-schema
  (:require [clojure.spec.alpha :as s]))

(def definitions
  "Taxonomy schema definitions                                      
                                        Keys are attributes, values are simplified datomic schema definitions where:
                                          * 1st item defines both value type and its cardinality (where wrapping in a
                                            vector means :db.cardinality/many)
                                          * Optional :db.unique/* keyword can be provided to define uniqueness
                                          * Last item is a :db/doc string"
  {;; Concept
   :concept/id [:db.type/string :db.unique/identity "Unique identifier for concepts"]
   :concept/type [:db.type/string "The concepts main type"]
   :concept/definition [:db.type/string "Text defining the concept, is used for disambiguation."]
   :concept/preferred-label [:db.type/string "What we prefer to call the concept."]
   :concept/alternative-labels [[:db.type/string] "Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept."]
   :concept/deprecated [:db.type/boolean "If a concept is deprecated"]
   :concept/no-esco-relation [:db.type/boolean "If a concept cannot be mapped to esco"]
   :concept/hidden-labels [[:db.type/string] "A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations."]
   :concept/quality-level [:db.type/long "Quality level"]
   :concept/replaced-by [[:db.type/ref] "Refers to other concepts that is replacing this one"]
   :concept/short-description [:db.type/string "A short explanatory description of the concept to be used in GUIs"]
   :concept/sort-order [:db.type/long "Value for display sort order in category"]
                                         ;; Concept type
   :concept-type/id [:db.type/string :db.unique/identity "Unique identifier for concept types"]
   :concept-type/label-en [:db.type/string "English label for concept types"]
   :concept-type/label-sv [:db.type/string "Swedish label for concept types"]
                                         ;; Concept extras
   :concept.external-database.ams-taxonomy-67/id [:db.type/string "ID from legacy Taxonomy version 67"]
   :concept.external-standard/driving-licence-code-2013 [:db.type/string "Driving licence code"]
   :concept.external-standard/esco-uri [:db.type/string :db.unique/identity "ESCO Concept Identifier (URI)"]
   :concept.external-standard/eures-code-2014 [:db.type/string "EURES code"]
   :concept.external-standard/eures-nace-code-2007 [:db.type/string "NACE code according to EURES naming convention"]
   :concept.external-standard/citizenship-code [:db.type/string "Codes representing broad categorizations of countries."]
   :concept.external-standard/glottolog-uri [:db.type/string "URI referring to Glottolog language concept"]
   :concept.external-standard/implicit-driving-licences [[:db.type/ref] "List of 'lower' ranking driving licences included in the licence"]
   :concept.external-standard/isco-code-08 [:db.type/string "ISCO-08 level 4"]
   :concept.external-standard/iso-3166-1-alpha-2-2013 [:db.type/string "Country code 2 letter"]
   :concept.external-standard/iso-3166-1-alpha-3-2013 [:db.type/string "Country code 3 letter"]
   :concept.external-standard/iso-639-1-2002 [:db.type/string "2 letter language code"]
   :concept.external-standard/iso-639-2-1998 [:db.type/string "3 letter language code"]
   :concept.external-standard/iso-639-3-2007 [:db.type/string "3 letter language code"]
   :concept.external-standard/iso-639-3-alpha-2-2007 [:db.type/string "2 letter language code"]
   :concept.external-standard/iso-639-3-alpha-3-2007 [:db.type/string "3 letter language code"]
   :concept.external-standard/iso-uri [:db.type/string "Official ISO URI"]
   :concept.external-standard/lau-2-code-2015 [:db.type/string "Swedish Municipality code"]
   :concept.external-standard/national-nuts-level-3-code-2019 [:db.type/string "Swedish Län code"]
   :concept.external-standard/nuts-level-3-code-2013 [:db.type/string "OLD NUTS level 3 code"]
   :concept.external-standard/nuts-level-3-code-2021 [:db.type/string "NUTS-2021 level 3 code"]
   :concept.external-standard/nuts-level-3-code-2024 [:db.type/string "NUTS-2024 level 3 code"]
   :concept.external-standard/sni-level-code-2007 [:db.type/string "SNI 2007 level code"]
   :concept.external-standard/sni-level-code-2007-official-structure [:db.type/string "SNI 2007 level code Official structure"]
   :concept.external-standard/ssyk-code-2012 [:db.type/string :db.unique/identity "SSYK-2012 type"]
   :concept.external-standard/sun-education-field-code-2000 [:db.type/string "Old SUN education field code, either 1, 2 or 3 digits and a letter"]
   :concept.external-standard/sun-education-field-code-2020 [:db.type/string "SUN education field code, either 1, 2 or 3 digits and a letter"]
   :concept.external-standard/sun-education-level-code-2000 [:db.type/string "Old SUN education level code, either 1, 2 or 3 digits"]
   :concept.external-standard/sun-education-level-code-2020 [:db.type/string "SUN education level code, either 1, 2 or 3 digits"]
   :concept.external-standard/unemployment-fund-code-2017 [:db.type/string :db.unique/identity "Swedish unemployment fund code"]
   :concept.external-standard/unemployment-type-code [:db.type/string :db.unique/identity "Swedish unemployment codes"]
   :concept.external-standard/wikidata-uri [:db.type/string "URI referring to concepts in Wikidata"]
                                         ;; Unused
   :concept.relation/related [[:db.type/ref] "related concepts"]
                                         ;; Daynotes
   :daynote/comment [:db.type/string "Comment entered by the user who modifies a concept or relation. Intended to be added to the transaction."]
   :daynote/ref [:db.type/ref "Optional reference to a concept or relation this tx adds a note about. Intended to be added to the transaction. Needed only when there are no changes to a concept/relation in this transaction"]
                                         ;; Relations
   :relation/id [:db.type/string :db.unique/identity "Unique identifier for relations"]
   :relation/concept-1 [:db.type/ref "The entity ID of the first concept in a relation"]
   :relation/concept-2 [:db.type/ref "The entity ID of the second concept in a relation"]
   :relation/type [:db.type/string "the type of relationship"]
   :relation/description [:db.type/string "Text describing the relation."]
   :relation/substitutability-percentage [:db.type/long "The substitutability percentage, how well the demand for an occupation is satisfied by a similar occupation"]
                                         ;; Scheme
   :scheme/member [[:db.type/ref] "members of the scheme"]
   :scheme/name [:db.type/string "The scheme name."]
                                         ;; User
   :taxonomy-user/id [:db.type/string "The user id. Intended to be added to the transaction."]
                                         ;; Version
   :taxonomy-version/id [:db.type/long :db.unique/identity "The version identifier of the taxonomy, used almost like a tag in Git."]
   :taxonomy-version/tx [:db.type/ref "Transaction id representing as-of time point of the taxonomy version"]
                                         ;; Webhook
   :webhook/key [:db.type/string :db.unique/identity "Webhook key"]
   :webhook/url [:db.type/string "Webhook URL"]})

(defn- qualified-keyword-with? [ns]
  (fn [x]
    (and (qualified-keyword? x)
         (-> x namespace (= ns)))))

(s/def ::definition
  (s/cat :db/ident qualified-keyword-with?
         :def (s/spec
               (s/cat :type
                      (s/or :db.cardinality/one (qualified-keyword-with? "db.type")
                            :db.cardinality/many (s/coll-of (qualified-keyword-with? "db.type")
                                                            :kind vector?
                                                            :count 1))
                      :optional (s/* (s/alt :db/unique (qualified-keyword-with? "db.unique")))
                      :doc string?))))

(defn- ->tx [args]
  (let [ret (s/conform ::definition args)]
    (if (s/invalid? ret)
      (throw (ex-info "Invalid schema definition" (s/explain-data ::definition args)))
      (let [{:keys [db/ident def]} ret
            {:keys [type optional doc]} def
            [cardinality valueType] type]
        (into {:db/ident ident
               :db/valueType (cond-> valueType (vector? valueType) first)
               :db/cardinality cardinality
               :db/doc doc}
              optional)))))

(def schema
  (mapv ->tx definitions))

(ns jobtech-taxonomy.common.concept)

(def id-regex
  (let [chars "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"]
    (re-pattern (format "^[%1$s]{4}_[%1$s]{3}_[%1$s]{3}$" chars))))

(defn id? [x]
  (and (string? x)
       (boolean (re-matches id-regex x))))
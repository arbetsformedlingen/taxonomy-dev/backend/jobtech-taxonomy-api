(ns jobtech-taxonomy.common.build-info
  (:require [jobtech-taxonomy.common.io-utils :as iou]))

(def ^:private build-info-schema
  [:map {:closed true}
   [:build-timestamp {:optional true} [:or :string :nil]]
   [:commit {:optional true} [:or :string :nil]]])

(def ^:private build-info-edn (iou/read-edn-resource build-info-schema "build-info.edn"))

(def commit (:commit build-info-edn "<unknown commit>"))
(def build-timestamp (:build-timestamp build-info-edn "<unknown build timestamp>"))

(def info
  {:build-timestamp build-timestamp
   :commit commit})

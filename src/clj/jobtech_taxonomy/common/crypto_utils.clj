(ns jobtech-taxonomy.common.crypto-utils
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(set! *warn-on-reflection* true)

(def available-sha-3-keys #{:SHA3-224 :SHA3-256 :SHA3-384 :SHA3-512})

(def sha-3-default :SHA3-256)

(def reader-buffer-size-default 8096)

;; MessageDigest functions

(defn digest->get-instance [^String digest-name]
  (java.security.MessageDigest/getInstance digest-name))

(defn digest->digest [digest]
  (.digest ^java.security.MessageDigest digest))

(defn digest->update [^java.security.MessageDigest digest ^"[B" buffer ^Integer buffer-size]
  (.update digest buffer 0 buffer-size))

(defn digest->get-algorithm [^java.security.MessageDigest digest]
  (.getAlgorithm digest))

(defn utf-8->bytes [s]
  (.getBytes ^String s "UTF-8"))

(defn sha-3-digest
  ([] (sha-3-digest sha-3-default))
  ([key]
   (if-let [valid-sha-key (available-sha-3-keys key)]
     (digest->get-instance (name valid-sha-key))
     (throw
      (IllegalArgumentException.
       "Invalid SHA-3 version")))))

(defn digest-input-streamable
  ([input-bytes] (digest-input-streamable input-bytes {}))
  ([input-bytes opts] (digest-input-streamable input-bytes
                                               opts
                                               (sha-3-digest (:sha-key opts
                                                                       sha-3-default))))
  ([input-bytes opts digest]
   (let [buffer-size (:buffer-size opts reader-buffer-size-default)]
     (with-open [input-stream (io/input-stream input-bytes :buffer-size buffer-size)]
       (let [buffer (byte-array buffer-size)]
         (loop []
           (let [bytes-to-read (.read input-stream buffer)]
             (if (pos? bytes-to-read)
               (do
                 (digest->update digest buffer bytes-to-read)
                 (recur))
               digest))))))))

(defn input-streamable->sha-3
  ([input-bytes] (input-streamable->sha-3 input-bytes {}))
  ([input-bytes opts]
   (->> (digest-input-streamable input-bytes opts)
        digest->digest
        vec)))

(defn utf-8->sha-3
  ([utf-8-string] (utf-8->sha-3 utf-8-string {}))
  ([utf-8-string opts]
   (if (string? utf-8-string)
     (input-streamable->sha-3
      (utf-8->bytes utf-8-string)
      (merge {:buffer-size (min reader-buffer-size-default (count utf-8-string))} opts))
     (throw
      (ex-info
       "Input must be a string"
       {:input utf-8-string})))))

(defn bytes->hex-str [data]
  (when data (str/join (map #(format "%02x" (byte %)) data))))

(defn dir->sha-3 [dir-path & opts]
  (->> (file-seq (io/file dir-path))
       (filter #(.isFile ^java.io.File %))
       (reduce (fn [digest file] (digest-input-streamable file opts digest))
               (sha-3-digest (:sha-key opts sha-3-default)))
       digest->digest
       vec))
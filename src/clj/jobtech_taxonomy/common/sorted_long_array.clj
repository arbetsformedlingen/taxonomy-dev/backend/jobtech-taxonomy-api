(ns jobtech-taxonomy.common.sorted-long-array
  "Utilities for working with sorted arrays of longs. They can provide faster lookup than hash maps. Theoretically, we would expect a hash map to provide O(1) lookup and binary search O(log n) lookup. But finding a primitive long in a primitive long array can be faster because (i) it doesn't involve boxing the primitive long to use as a key and (ii) it involves following fewer references through memory: there is just an array of continuous longs and fast operations that operate directly on primitives without indirection."
  (:refer-clojure :exclude [memoize contains?])
  (:import [java.util HashSet Collection Arrays]
           [java.util.concurrent.atomic AtomicInteger]))

(set! *warn-on-reflection* true)

(defn distinct-sorted-long-array
  "Constructs a sorted array of distinct long numbers from the input `elements`."
  ^longs [elements]
  (let [s (if (instance? java.util.Set elements)
            elements
            (HashSet. ^Collection elements))
        arr ^longs (into-array Long/TYPE s)]
    (Arrays/sort arr)
    arr))

(defn lookup-fn
  "Constructs a function from key-value pairs `kv-pairs`. This function can be called either with a key or a key and a default value in order to retrieve a value."
  [kv-pairs]
  (let [kv-pairs (sort-by first kv-pairs)
        ks ^longs (into-array Long/TYPE (map first kv-pairs))
        vs ^objects (into-array Object (map second kv-pairs))]
    (fn lu
      ([k] (lu k nil))
      ([k default-value]
       (if (number? k)
         (let [index (Arrays/binarySearch ks (long k))]
           (if (neg? index)
             default-value
             (aget vs index)))
         default-value)))))

(defn memoize
  "Memoize a function `f` for input arguments from the sorted long array `arr`."
  [f ^longs arr]
  {:pre [(ifn? f)]}
  (let [n (alength arr)
        objs (object-array n)]
    (fn [x]
      (let [index (Arrays/binarySearch arr (long x))
            _ (assert (not (neg? index)))
            obj (aget objs index)]
        (if obj
          obj
          (let [obj (f x)]
            (aset objs index obj)
            obj))))))

(defn binary-search
  "Given a sorted long array `arr` and an element in that array `x` to look for, return the index of that element if it exists, otherwise return `nil`."
  [^longs arr x]
  (let [index (Arrays/binarySearch arr (long x))]
    (when-not (neg? index)
      index)))

(defn contains?
  "Test if a sorted long array `arr` contains element `x`"
  [^longs arr x]
  (not (neg? (Arrays/binarySearch arr (long x)))))

(defn stateful-predicate
  "Given a sorted long array `arr`, return a predicate function that has to be called non-strictly increasing integers, and returns true iff an integer is part of `arr`."
  [^longs arr]
  (let [n (alength arr)]
    (if (zero? n)
      (fn [_] false)
      (let [state (AtomicInteger. 0)]
        (fn [x]
          (loop []
            (let [index (.get state)]
              (if (= index n)
                false
                (let [y (aget arr index)]
                  (cond
                    (< x y) false
                    (= x y) true
                    :else (do (.incrementAndGet state)
                              (recur))))))))))))

(ns jobtech-taxonomy.common.math-utils
  (:require [clojure.math :as math]
            [java-time.api :as jt]))

(defn duration->ms-double [duration]
  (let [{:keys [seconds nanos]} (jt/as-map duration)]
    (+
     (* 1.0e3 seconds)
     (* 1.0e-6 nanos))))

(defn- count-numbers [values]
  (if
   (and (seq values)
        (every? number? values))
    (count values)
    (throw (IllegalArgumentException. "Input must be a list of numbers"))))

(defn mean
  ([values] (mean (count-numbers values) values))
  ([value-count values]
   (/ (apply + values)
      value-count)))

(defn population-variance
  ([values] (let [value-count (count-numbers values)]
              (population-variance value-count (mean value-count values) values)))
  ([value-count mean-value values]
   (/ (transduce
       (map #(math/pow (- % mean-value) 2)) + values)
      value-count)))

(defn population-standard-deviation
  ([values]
   (let [value-count (count-numbers values)]
     (population-standard-deviation value-count (mean value-count values) values)))
  ([value-count mean-value values]
   (math/sqrt (population-variance value-count mean-value values))))

(ns jobtech-taxonomy.common.io-utils
  (:require [babashka.fs :as fs]
            [clojure.data.json :as json]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [jobtech-taxonomy.common.crypto-utils :as cu]
            [malli.core :as m]
            [malli.error :as me]
            [taoensso.timbre :as log])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream]
           [java.util.zip ZipEntry ZipInputStream ZipOutputStream]))

(set! *warn-on-reflection* true)

;; File and resource helpers

(defn raw-resource [resource-name]
  (io/resource resource-name))

(defn is-absolute-path? [path]
  (.isAbsolute ^java.io.File (io/file path)))

(defn is-relative-path? [path]
  (not (is-absolute-path? path)))

(defn get-absolute-path [path]
  (.getAbsolutePath ^java.io.File (io/file path)))

(defn file-exists? [path]
  (.exists ^java.io.File (io/file path)))

(defn existing-file [name]
  (let [file (io/file name)]
    (if (.exists ^java.io.File file)
      file
      (throw
       (ex-info
        "File not found"
        {:file-name name})))))

(defn pattern-file-seq-from
  ([directory pattern]
   (filter #(re-matches pattern (str %))
           (pattern-file-seq-from directory)))
  ([directory]
   (->> (existing-file directory)
        io/file
        file-seq)))

;; Structured data reader helpers

(defn read-edn-string [edn-string]
  (try
    (edn/read-string edn-string)
    (catch Exception e
      (throw
       (ex-info
        (str "EDN: " (ex-message e))
        {:exception e})))))

(defn keyword-from-x [x]
  (cond
    (keyword? x) x
    (string? x) (keyword x)
    (symbol? x) (-> x name keyword)
    (nil? x) nil
    :else (throw (ex-info "Cannot make keyword from x" {:x x}))))

(defn validate-for [schema]
  (let [valid? (m/validator schema)]
    (fn validate [edn-data]
      (if (valid? edn-data)
        edn-data
        (throw (ex-info (str "Invalid EDN data, cause: "
                             (-> (m/explain schema edn-data)
                                 (me/humanize)))
                        {:schema schema}))))))

(defn get-env [name] (System/getenv ^String name))

(defn- get-env-var-value [env-var-name]
  (if-let [env-var-value (get-env env-var-name)]
    (do (log/info :env-var/found env-var-name)
        env-var-value)
    (log/debug :env-var/missing env-var-name)))

(defn- validate-env-value
  [schema value]
  (let [validator (validate-for schema)
        edn-data (read-edn-string value)]
    (validator edn-data)))

(defn get-env-var [schema name]
  (validate-env-value schema (get-env-var-value name)))

;; EDN readers

(defn- read-edn-stream [stream-fn edn-file]
  (try
    (with-open [r (io/reader (stream-fn edn-file))]
      (edn/read (java.io.PushbackReader. r)))
    (catch Exception e
      (throw
       (ex-info
        (str "EDN: " (ex-message e))
        (merge
         {:exception e}
         (condp = stream-fn
           io/file {:file-name edn-file}
           io/resource {:resource-name edn-file}
           {:name edn-file})))))))

(defn- validate-edn-stream [schema stream-fn file-name]
  (let [validate (validate-for schema)
        edn-data (read-edn-stream stream-fn file-name)]
    (validate edn-data)))

(defn read-edn-file [schema file-name]
  (validate-edn-stream schema io/file file-name))

(defn read-edn-resource [schema resource-name]
  (validate-edn-stream schema io/resource resource-name))

;; JSON readers

(defn- read-json-stream [stream-fn json-file]
  (try
    (with-open [r (io/reader (stream-fn json-file))]
      (json/read r))
    (catch Exception e
      (throw
       (ex-info
        (str "JSON: " (ex-message e))
        (merge
         {:exception e}
         (condp = stream-fn
           io/file {:file-name json-file}
           io/resource {:resource-name json-file}
           {:name json-file})))))))

(defn- validate-json-stream [schema stream-fn file-name]
  (let [validate (validate-for schema)
        json (read-json-stream stream-fn file-name)]
    (validate json)))

(defn read-json-file [schema file-name]
  (validate-json-stream schema io/file file-name))

(defn read-json-resource [schema resource-name]
  (validate-json-stream schema io/resource resource-name))

;; Zip helpers

(defn unzip-stream
  "Unzips a byte array input stream into the given destination folder."
  [^ByteArrayInputStream byte-array-input-stream ^java.io.File destination-folder]
  (let [dest-folder (io/file destination-folder)]
    (with-open [zos (ZipInputStream. byte-array-input-stream)]
      (loop []
        (let [entry (.getNextEntry zos)]
          (when entry
            (let [output-file (io/file dest-folder (.getName entry))]
              (if (.isDirectory entry)
                (do
                  (io/make-parents output-file)
                  (recur))
                (do
                  (io/make-parents output-file)
                  (with-open [fos (io/output-stream output-file)]
                    (io/copy zos fos))
                  (recur))))))))))

(defn delete-directory-contents [directory]
  (when-not (fs/directory? directory)
    (throw (ex-info "Cannot delete contents of something that is not a directory"
                    {:directory directory})))
  (doseq [f (fs/list-dir directory)]
    (fs/delete-tree f)))

(defn non-merging-copy-directory-contents [src-path dst-path]
  (if (fs/exists? dst-path)
    (delete-directory-contents dst-path)
    (fs/create-dir dst-path))
  (doseq [sub-src (fs/list-dir src-path)
          :let [sub-dst (fs/path
                         dst-path
                         (fs/relativize
                          src-path
                          sub-src))]]
    (if (fs/directory? sub-src)
      (fs/copy-tree sub-src sub-dst)
      (fs/copy sub-src sub-dst))))

(defn unzip-file
  "Unzips `source-file` into `destination-folder`.

  Options:
    - `:delete-existing?` (default `false`): If `true`, deletes existing files in `destination-folder` before unzipping."
  ([source-file destination-folder] (unzip-file source-file destination-folder {}))
  ([^java.io.File source-file ^java.io.File destination-folder {:keys [delete-existing?] :or {delete-existing? false}}]
   (when (fs/regular-file? destination-folder)
     (throw (ex-info "Cannot unzip into a regular file"
                     {:source-file source-file
                      :destination-folder destination-folder})))
   (when (and delete-existing? (fs/directory? destination-folder))
     (delete-directory-contents destination-folder))
   (with-open [bis (io/input-stream source-file)]
     (unzip-stream bis destination-folder))))

(defn unzip-byte-array
  "Unzips a byte array representing a zip file into the given destination folder."
  {:pre [(fn [zip-byte-array destination-dir-path]
           (and (instance? byte-array zip-byte-array)
                (instance? String destination-dir-path)))]}
  [^"[B" zip-byte-array ^String destination-folder]
  (let [dest-folder (io/file destination-folder)]
    (with-open [bis (ByteArrayInputStream. zip-byte-array)]
      (unzip-stream bis dest-folder)
      (log/info "Unzipped to" destination-folder))))

(defn- zip
  "takes path and return zip as byte array"
  [^String source-dir-path]
  (let [source-folder (io/file source-dir-path)
        baos (ByteArrayOutputStream.)
        zos (ZipOutputStream. baos)]
    (doseq [file (rest (file-seq source-folder))]
      (let [relative-path (.relativize (.toPath ^java.io.File source-folder) (.toPath ^java.io.File file))]
        (if (.isDirectory ^java.io.File file)
          (do
            (.putNextEntry zos (ZipEntry. (str relative-path "/"))) ; Include trailing slash for directories
            (.closeEntry zos))
          (do
            (.putNextEntry zos (ZipEntry. (str relative-path)))
            (io/copy file zos)))
        (.closeEntry zos)))
    (.close zos)
    (.toByteArray baos)))

(defn zip-with-write-check
  "takes path and return zip as byte array, throws an exception if data is written to the db during zipping"
  [^String source-dir-path]
  (let [db-checksum-before (cu/dir->sha-3 source-dir-path)
        zip-bytes (zip source-dir-path)
        db-checksum-after (cu/dir->sha-3 source-dir-path)]
    (if (= db-checksum-before db-checksum-after)
      zip-bytes
      (throw (ex-info "Data was written to the database during the zipping process" {:type :data-written-during-zipping})))))

;; Bytes

(defn write-bytes [filename ^bytes byte-array]
  (with-open [out (io/output-stream (io/file filename))]
    (.write out byte-array)))

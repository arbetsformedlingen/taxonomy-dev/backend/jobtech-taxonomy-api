(ns jobtech-taxonomy.common.malli-utils
  (:require [malli.core :as m]
            [malli.error :as me]
            [malli.transform :as mt]
            [taoensso.timbre :as log]))

(defn coerce-string [schema]
  (let [coercer (m/coercer schema mt/string-transformer)]
    (fn string-coercer [text]
      (coercer text))))

(defn throwing-validator-for [malli-schema]
  (let [validator (m/validator malli-schema)]
    (fn validate-data [schema-data]
      (if (validator schema-data)
        schema-data
        (let [error (m/explain malli-schema schema-data)]
          (log/error :malli-utils/validation-error (me/humanize error))
          (throw (ex-info "Invalid data" error)))))))

(def ^:private config-schema-part-schema
  [:tuple
   :keyword
   [:fn
    {:error/fn
     (fn [{:keys [value]} _]
       (str "`" value "` is not a valid malli schema"))}
    (fn verify-part-schema [part-schema]
      (try (m/schema?
            (m/schema part-schema))
           (catch clojure.lang.ExceptionInfo _e
             false)))]])

(def ^:private valid-schema-part?
  (m/validator config-schema-part-schema))

(defn config-validator-for [config-schema-part]
  (if (valid-schema-part? config-schema-part)
    (let [[part-config-key part-config-schema] config-schema-part
          part-config-valid? (m/validator part-config-schema)]
      (fn validate-config [cfg]
        (when-let [part-config (part-config-key cfg)]
          (if (part-config-valid? part-config)
            part-config
            (throw
             (ex-info
              (str "Invalid configuration under `" part-config-key "`")
              {part-config-key (me/humanize (m/explain part-config-schema part-config))}))))))
    (throw
     (ex-info
      "Failed validating `schema-part`"
      {:error (->> config-schema-part
                   (m/explain config-schema-part-schema)
                   (me/humanize))}))))

(ns jobtech-taxonomy.common.spec-utils
  (:require [spec-tools.core :as st]
            [spec-tools.parse :as st-parse]))

(declare validator)

(defn key-validator [k key->spec on-missing]
  (let [p (validator (key->spec k))]
    (fn [x]
      (let [y (get x k ::missing)]
        (if (= y ::missing)
          on-missing
          (p y))))))

(defn normalize-spec [x]
  (cond
    (map? x) x
    (st/spec? x) x
    (keyword? x) (recur (st/get-spec x))
    :else (st/create-spec {:spec x})))

(defn validator
  "Given argument `src`, returns a function `f` such that `((f src) x)` and `(clojure.spec.alpha/valid? src x)` are equivalent for any `x`."
  [src]
  (let [{:keys [type spec leaf?] :as x} (normalize-spec src)]
    (case (st-parse/type-dispatch-value type)
      :vector (let [item-pred (validator (::st-parse/item x))]
                (fn [x]
                  (and (sequential? x)
                       (every? item-pred x))))
      :map (let [key->spec (::st-parse/key->spec x)
                 preds (vec (for [[on-missing ks] [[false (::st-parse/keys-req x)]
                                                   [true (::st-parse/keys-opt x)]]
                                  k ks]
                              (key-validator k key->spec on-missing)))]
             (fn [x]
               (and (map? x)
                    (every? #(% x) preds))))
      :string string?
      :long int?
      :double number?
      :boolean boolean?
      (if (and leaf? (fn? spec))
        spec
        (throw (AssertionError. "Cannot make validator"))))))




(ns jobtech-taxonomy.common.taxonomy
  (:require [clojure.spec.alpha :as sp]
            [clojure.string :as str]
            [clojure.walk :as w]
            [jobtech-taxonomy.common.spec-utils :as jsu]
            [spec-tools.core :as st]
            [spec-tools.data-spec :as ds]))

(def taxonomy-namespace "taxonomy")

(defn change-namespace-to-taxonomy [k]
  (if (keyword? k)
    (keyword taxonomy-namespace (name k))
    k))

(defn map->nsmap
  "Apply our nice namespace to the supplied structure."
  [m]
  (w/postwalk change-namespace-to-taxonomy m))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Output response types

;; General fundamentals

(defmacro par "Use this to make parameter declarations somewhat tidier." [type desc & kvs]
  `(st/spec ~(merge {:spec type :description desc} (apply hash-map kvs))))

(sp/def :taxonomy/id (st/spec string?))
(sp/def :taxonomy/type (st/spec string?))
(sp/def :taxonomy/deprecated (st/spec boolean?))
(sp/def :taxonomy/no-esco-relation (st/spec boolean?))
(sp/def :taxonomy/preferred-label (st/spec string?))
(sp/def :taxonomy/alternative-labels (sp/coll-of string?))
(sp/def :taxonomy/hidden-labels (sp/coll-of string?))
(sp/def :taxonomy/definition (st/spec string?))
(sp/def :taxonomy/event-type (st/spec string?))
(sp/def :taxonomy/version (st/spec int?))
(sp/def :taxonomy/quality-level
  (par (sp/and int? #{1 2 3}) ;; `int?` is needed to trigger reitit's coercion mechanism
       "Concept quality level"
       :swagger/type :integer
       :swagger/enum [1 2 3]))
(sp/def :taxonomy/deprecated-legacy-id
  (st/spec string? {:description (str "Internal ID that exists only for concepts imported from the old taxonomy database. "
                                      "These IDs are unique per type.")}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(sp/def :taxonomy/concept-shallow
  (ds/spec
   {:name :taxonomy/concept-shallow
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated
                         :taxonomy/alternative-labels
                         :taxonomy/hidden-labels])}))

(sp/def :taxonomy/replaced-by
  (ds/spec
   {:name :taxonomy/replaced-by
    :spec (sp/coll-of :taxonomy/concept-shallow)}))

; ssyk-2012

(sp/def :taxonomy/ssyk-code-2012 (st/spec string?))

(sp/def :taxonomy/concept-ssyk
  (ds/spec
   {:name :taxonomy/concept-ssyk
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/ssyk-code-2012 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-ssyk
  (ds/spec
   {:name :taxonomy/concepts-ssyk
    :spec (sp/coll-of :taxonomy/concept-ssyk)}))

;; municipality

(sp/def :taxonomy/lau-2-code-2015 (st/spec string?))

(sp/def :taxonomy/concept-municipality
  (ds/spec
   {:name :taxonomy/concept-municipality
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/lau-2-code-2015 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-municipality
  (ds/spec
   {:name :taxonomy/concepts-municipality
    :spec (sp/coll-of :taxonomy/concept-municipality)}))

;; eures-code

(sp/def :taxonomy/eures-code-2014 (st/spec string?))

(sp/def :taxonomy/concept-employment-duration
  (ds/spec
   {:name :taxonomy/concept-employment-duration
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/eures-code-2014 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-employment-duration
  (ds/spec
   {:name :taxonomy/concepts-employment-duration
    :spec (sp/coll-of :taxonomy/concept-employment-duration)}))

;; nuts-level-3

(sp/def :taxonomy/nuts-level-3-code-2013 (st/spec string?))
(sp/def :taxonomy/national-nuts-level-3-code-2019 (st/spec string?))

(sp/def :taxonomy/concept-region
  (ds/spec
   {:name :taxonomy/concept-region
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by
                         :taxonomy/national-nuts-level-3-code-2019 :taxonomy/nuts-level-3-code-2013 :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-region
  (ds/spec
   {:name :taxonomy/concepts-region
    :spec (sp/coll-of :taxonomy/concept-region)}))

;; country

(sp/def :taxonomy/iso-3166-1-alpha-3-2013 (st/spec string?))
(sp/def :taxonomy/iso-3166-1-alpha-2-2013 (st/spec string?))

(sp/def :taxonomy/concept-country
  (ds/spec
   {:name :taxonomy/concept-country
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/iso-3166-1-alpha-3-2013 :taxonomy/iso-3166-1-alpha-2-2013 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-country
  (ds/spec
   {:name :taxonomy/concepts-country
    :spec (sp/coll-of :taxonomy/concept-country)}))

;; isco-08

(sp/def :taxonomy/isco-code-08 (st/spec string?))

(sp/def :taxonomy/concept-isco
  (ds/spec
   {:name :taxonomy/concept-isco
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/isco-code-08 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-isco
  (ds/spec
   {:name :taxonomy/concepts-isco
    :spec (sp/coll-of :taxonomy/concept-isco)}))

;; education-field-code-2020

(sp/def :taxonomy/sun-education-field-code-2020 (st/spec string?))
(sp/def :taxonomy/sun-education-field-code-2000 (st/spec string?))

(sp/def :taxonomy/concept-sun-education-field
  (ds/spec
   {:name :taxonomy/concept-sun-education-field
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/sun-education-field-code-2020 :taxonomy/sun-education-field-code-2000 :taxonomy/deprecated
                         :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-sun-education-field
  (ds/spec
   {:name :taxonomy/concepts-sun-education-field
    :spec (sp/coll-of :taxonomy/concept-sun-education-field)}))

;; sun-education-level-code-2020

(sp/def :taxonomy/sun-education-level-code-2020 (st/spec string?))
(sp/def :taxonomy/sun-education-level-code-2000 (st/spec string?))

(sp/def :taxonomy/concept-sun-education-level
  (ds/spec
   {:name :taxonomy/concept-sun-education-level
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/sun-education-level-code-2020 :taxonomy/sun-education-level-code-2000 :taxonomy/deprecated
                         :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-sun-education-level
  (ds/spec
   {:name :taxonomy/concepts-sun-education-level
    :spec (sp/coll-of :taxonomy/concept-sun-education-level)}))

;; sni-level-code

(sp/def :taxonomy/sni-level-code-2007 (st/spec string?))

(sp/def :taxonomy/concept-sni-level
  (ds/spec
   {:name :taxonomy/concept-sni-level
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/sni-level-code-2007 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-sni-level
  (ds/spec
   {:name :taxonomy/concepts-sni-level
    :spec (sp/coll-of :taxonomy/concept-sni-level)}))

;; language

(sp/def :taxonomy/iso-639-1-2002 (st/spec string?))
(sp/def :taxonomy/iso-639-2-1998 (st/spec string?))
(sp/def :taxonomy/iso-639-3-2007 (st/spec string?))

(sp/def :taxonomy/concept-language
  (ds/spec
   {:name :taxonomy/concept-language
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by
                         :taxonomy/iso-639-1-2002 :taxonomy/iso-639-2-1998 :taxonomy/iso-639-3-2007
                         :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-language
  (ds/spec
   {:name :taxonomy/concepts-language
    :spec (sp/coll-of :taxonomy/concept-language)}))

;; unemployment-fund-code-2017

(sp/def :taxonomy/unemployment-fund-code-2017 (st/spec string?))

(sp/def :taxonomy/concept-unemployment-fund
  (ds/spec
   {:name :taxonomy/concept-isco
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/unemployment-fund-code-2017 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-unemployment-fund
  (ds/spec
   {:name :taxonomy/concepts-unemployment-fund
    :spec (sp/coll-of :taxonomy/concept-unemployment-fund)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; /versions

(sp/def :taxonomy/version-object
  (ds/spec
   {:name :taxonomy/version-object
    :spec {:taxonomy/version int?
           :taxonomy/timestamp inst?}}))

(sp/def :taxonomy/versions
  (ds/spec
   {:name :taxonomy/versions
    :spec (sp/coll-of :taxonomy/version-object)}))

(def versions-spec :taxonomy/versions)

;; TODO specify a cleaner better API

;; /changes

(sp/def :taxonomy/attribute (st/spec string?))
(sp/def :taxonomy/old-value (st/spec any?))
(sp/def :taxonomy/new-value (st/spec any?))

(sp/def :taxonomy/new-concept
  (st/spec
   {:name :taxonomy/new-concept
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/definition :taxonomy/preferred-label]
                   :opt [:taxonomy/driving-licence-code-2013
                         :taxonomy/eures-code-2014
                         :taxonomy/isco-code-08
                         :taxonomy/iso-3166-1-alpha-2-2013
                         :taxonomy/iso-3166-1-alpha-3-2013
                         :taxonomy/iso-639-1-2002
                         :taxonomy/iso-639-2-1998
                         :taxonomy/iso-639-3-2007
                         :taxonomy/lau-2-code-2015
                         :taxonomy/national-nuts-level-3-code-2019
                         :taxonomy/nuts-level-3-code-2013
                         :taxonomy/sni-level-code-2007
                         :taxonomy/sun-education-field-code-2000
                         :taxonomy/sun-education-field-code-2020
                         :taxonomy/sun-education-level-code-2000
                         :taxonomy/sun-education-level-code-2020
                         :taxonomy/ssyk-code-2012
                         :taxonomy/alternative-labels
                         :taxonomy/hidden-labels])}))

(sp/def :taxonomy/latest-version-of-concept
  (ds/spec
   {:name :taxonomy/latest-version-of-concept
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated
                         :taxonomy/alternative-labels
                         :taxonomy/hidden-labels])}))

(sp/def :taxonomy/concept-attribute-change
  (st/spec
   {:name :taxonomy/concept-attribute-change
    :spec (sp/keys :req [:taxonomy/attribute :taxonomy/old-value :taxonomy/new-value])}))

(sp/def :taxonomy/concept-attribute-changes
  (ds/spec
   {:name :taxonomy/concept-attribute-changes
    :spec (sp/coll-of :taxonomy/concept-attribute-change)}))

(sp/def :taxonomy/concept-change
  (ds/spec
   {:name :taxonomy/concept-change
    :spec (sp/keys :req [:taxonomy/event-type
                         :taxonomy/version]
                   :opt [:taxonomy/concept-attribute-changes
                         :taxonomy/new-concept
                         :taxonomy/latest-version-of-concept])}))

(sp/def :taxonomy/concept-changes
  (ds/spec
   {:name :taxonomy/concept-changes
    :spec (sp/coll-of :taxonomy/concept-change)}))

;; Pre-compile a validator for faster response validation.
;; This being an opaque function means that the other properties of a spec is lost:
;; in other words, it will only be useful for validation.
(sp/def :taxonomy/concept-changes-validator (jsu/validator :taxonomy/concept-changes))

(def concept-changes-validator-spec :taxonomy/concept-changes-validator)

;; OLD ENDPOINT

(sp/def :taxonomy/changed-concept
  (st/spec
   {:name :taxonomy/changed-concept
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/preferred-label])}))

(sp/def :taxonomy/event
  (ds/spec
   {:name :taxonomy/event
    :spec (sp/keys :req [:taxonomy/event-type
                         :taxonomy/changed-concept]
                   :opt [:taxonomy/version])}))

(sp/def :taxonomy/events
  (ds/spec
   {:name :taxonomy/events
    :spec (sp/coll-of :taxonomy/event)}))

;; /concepts

(sp/def :taxonomy/concepts-shallow
  (ds/spec
   {:name :taxonomy/concepts-shallow
    :spec (sp/coll-of :taxonomy/concept-shallow)}))

(sp/def :taxonomy/concept
  (ds/spec
   {:name :taxonomy/concept
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/no-esco-relation :taxonomy/replaced-by :taxonomy/deprecated-legacy-id
                         :taxonomy/alternative-labels :taxonomy/hidden-labels])}))

(sp/def :taxonomy/concepts
  (ds/spec
   {:name :taxonomy/concepts
    :spec (sp/coll-of :taxonomy/concept)}))

(def concepts-spec :taxonomy/concepts)

(sp/def :taxonomy/concept-private
  (ds/spec
   {:name :taxonomy/concept-private
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/no-esco-relation :taxonomy/replaced-by
                         :taxonomy/deprecated-legacy-id :taxonomy/quality-level
                         :taxonomy/alternative-labels
                         :taxonomy/hidden-labels])}))

(sp/def :taxonomy/concepts-private
  (ds/spec
   {:name :taxonomy/concepts-private
    :spec (sp/coll-of :taxonomy/concept-private)}))

(def concepts-spec-private :taxonomy/concepts-private)

;; /autocomplete

(def autocomplete-spec :taxonomy/concepts-shallow)

;; /replaced-by-changes

(sp/def :taxonomy/replaced-by-change
  (ds/spec
   {:name :taxonomy/replaced-by-change
    :spec (sp/keys :req [:taxonomy/version :taxonomy/concept])}))

(sp/def :taxonomy/replaced-by-changes
  (ds/spec
   {:name :taxonomy/replaced-by-changes
    :spec (sp/coll-of :taxonomy/replaced-by-change)}))

(def replaced-by-changes-spec :taxonomy/replaced-by-changes)

;; /concept/types

(sp/def :taxonomy/concept-types
  (ds/spec
   {:name :taxonomy/concept-types
    :spec (sp/coll-of string?)}))

(def concept-types-spec :taxonomy/concept-types)

;; /relation/types

(sp/def :taxonomy/relation-types
  (ds/spec
   {:name :taxonomy/relation-types
    :spec (sp/coll-of string?)}))

(def relation-types-spec :taxonomy/relation-types)

;; Error message

(sp/def :taxonomy/err (ds/spec {:name "error"
                                :spec {:taxonomy/error (st/spec string?)
                                       (ds/opt :taxonomy/info) (st/spec any?)}}))
(def error-spec :taxonomy/err)

(sp/def :taxonomy/ok (ds/spec {:name "ok"
                               :spec {:taxonomy/message (st/spec string?)}}))
(def ok-spec :taxonomy/ok)

(sp/def :taxonomy/msg (ds/spec {:name "msg"
                                :spec {:taxonomy/message (st/spec string?)}}))
(def msg-spec :taxonomy/msg)

(sp/def :taxonomy/unauthorized (ds/spec {:name "unauthorized"
                                         :spec {:taxonomy/error (st/spec string?)}}))
(def unauthorized-spec :taxonomy/unauthorized)

(sp/def :taxonomy/source (st/spec string?))
(sp/def :taxonomy/target (st/spec string?))
(sp/def :taxonomy/relation-type (st/spec string?))
(sp/def :taxonomy/substitutability-percentage (st/spec int?))

(sp/def :taxonomy/edge (ds/spec {:name "edge"
                                 :spec (sp/keys :req [:taxonomy/source
                                                      :taxonomy/target
                                                      :taxonomy/relation-type]
                                                :opt [:taxonomy/substitutability-percentage])}))

(sp/def :taxonomy/edges (ds/spec {:name "edges"
                                  :spec (sp/coll-of :taxonomy/edge)}))

(sp/def :taxonomy/nodes (ds/spec {:name "nodes"
                                  :spec (sp/coll-of :taxonomy/concept-shallow)}))

(sp/def :taxonomy/graph (ds/spec {:name "graph"
                                  :spec (sp/keys :opt [:taxonomy/edges :taxonomy/nodes])}))

(def graph-spec :taxonomy/graph)

(declare koncept-spec)

(sp/def :concept/id (st/spec string?))
(sp/def :concept/type (st/spec string?))
(sp/def :concept/deprecated (st/spec boolean?))
(sp/def :concept/preferred-label (st/spec string?))
(sp/def :concept/definition (st/spec string?))

(def koncept
  {(ds/req :id) :concept/id
   (ds/req :type) :concept/type
   (ds/req :preferred-label) :concept/preferred-label
   (ds/opt :definition) :concept/definition
   (ds/opt :deprecated) :concept/deprecated
   (ds/opt :replaced-by) koncept-spec})

(def koncept-spec
  (ds/spec
   {:name :taxonomy/Koncept
    :spec koncept}))

;; TODO FIX implicit driving licence spec!!

;; driving-licence

(sp/def :taxonomy/driving-licence-code-2013 (st/spec string?))

(sp/def :taxonomy/concept-driving-licence
  (ds/spec
   {:name :taxonomy/concept-driving-licence
    :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/driving-licence-code-2013 :taxonomy/preferred-label]
                   :opt [:taxonomy/definition :taxonomy/deprecated :taxonomy/replaced-by :taxonomy/deprecated-legacy-id])}))

(sp/def :taxonomy/concepts-driving-licence
  (ds/spec
   {:name :taxonomy/concepts-driving-licence
    :spec (sp/coll-of :taxonomy/concept-driving-licence)}))

;; Legacy

(sp/def :taxonomy/legacy-concept
  (ds/spec {:name :taxonomy/legacy-concept
            :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label :taxonomy/deprecated-legacy-id]
                           :opt [:taxonomy/definition :taxonomy/ssyk-code-2012 :taxonomy/lau-2-code-2015 :taxonomy/national-nuts-level-3-code-2019])}))

(def legacy-concept :taxonomy/legacy-concept)

;; legacy mapping object

(sp/def :taxonomy/ssyk-id (st/spec string?))
(sp/def :taxonomy/ssyk-deprecated-legacy-id (st/spec string?))
(sp/def :taxonomy/occupation-field-id (st/spec string?))
(sp/def :taxonomy/occupation-field-deprecated-legacy-id (st/spec string?))

(sp/def :taxonomy/legacy-mapping-concept
  (ds/spec {:name :taxonomy/legacy-mapping-concept
            :spec (sp/keys :req [:taxonomy/id :taxonomy/type :taxonomy/preferred-label :taxonomy/deprecated-legacy-id]
                           :opt [:taxonomy/definition
                                 :taxonomy/ssyk-id
                                 :taxonomy/ssyk-code-2012
                                 :taxonomy/ssyk-deprecated-legacy-id
                                 :taxonomy/occupation-field-id
                                 :taxonomy/occupation-field-deprecated-legacy-id])}))

(sp/def :taxonomy/legacy-mapping-concepts
  (ds/spec
   {:name :taxonomy/legacy-mapping-concepts
    :spec (sp/coll-of :taxonomy/legacy-mapping-concept)}))

(def legacy-mapping-concepts :taxonomy/legacy-mapping-concepts)

(def detailed-endpoint-configs
  (sort-by :endpoint-name
           [{:endpoint-name "driving-licence"
             :default-type "driving-licence"
             :extra-attributes [{:name :driving-licence-code-2013
                                 :doc "Driving licence code"
                                 :where-field :concept.external-standard/driving-licence-code-2013}]
             :pull [:concept.external-standard/driving-licence-code-2013
                    :concept/short-description
                    {:concept.external-standard/implicit-driving-licences
                     [:concept/id
                      :concept.external-standard/driving-licence-code-2013]}]}

            {:endpoint-name "ssyk"
             :default-type "ssyk-level-1 ssyk-level-2 ssyk-level-3 ssyk-level-4"
             :extra-attributes [{:name :ssyk-code-2012
                                 :doc "SSYK 2012"
                                 :where-field :concept.external-standard/ssyk-code-2012}
                                {:name :type
                                 :doc "SSYK level types (space-separated): ssyk-level-1, ssyk-level-2, ssyk-level-3 or ssyk-level-4"}]
             :pull [:concept.external-standard/ssyk-code-2012]}

            {:endpoint-name "municipality"
             :default-type "municipality"
             :extra-attributes [{:name :lau-2-code-2015
                                 :doc "Swedish municipality code"
                                 :where-field :concept.external-standard/lau-2-code-2015}]
             :pull [:concept.external-standard/lau-2-code-2015]}

            {:endpoint-name "region"
             :default-type "region"
             :extra-attributes [{:name :nuts-level-3-code-2013
                                 :doc "Nuts level 3 code"
                                 :where-field :concept.external-standard/nuts-level-3-code-2013}
                                {:name :national-nuts-level-3-code-2019
                                 :doc "Swedish län code"
                                 :where-field :concept.external-standard/national-nuts-level-3-code-2019}]
             :pull [:concept.external-standard/nuts-level-3-code-2013
                    :concept.external-standard/national-nuts-level-3-code-2019]}

            {:endpoint-name "country"
             :default-type "country"
             :extra-attributes [{:name :iso-3166-1-alpha-2-2013
                                 :doc "Country code 2 letter"
                                 :where-field :concept.external-standard/iso-3166-1-alpha-2-2013}
                                {:name :iso-3166-1-alpha-3-2013
                                 :doc "Country code 3 letter"
                                 :where-field :concept.external-standard/iso-3166-1-alpha-3-2013}]
             :pull [:concept.external-standard/iso-3166-1-alpha-2-2013
                    :concept.external-standard/iso-3166-1-alpha-3-2013]}

            {:endpoint-name "isco"
             :default-type "isco-level-4"
             :extra-attributes [{:name :isco-code-08
                                 :doc "ISCO code 2008"
                                 :where-field :concept.external-standard/isco-code-08}]
             :pull [:concept.external-standard/isco-code-08]}

            {:endpoint-name "sun-education-field"
             :default-type "sun-education-field-1 sun-education-field-2 sun-education-field-3 sun-education-field-4"
             :extra-attributes [{:name :sun-education-field-code-2020
                                 :doc "SUN education field 2020 code, either 1, 2 or 3 digits and a letter"
                                 :where-field :concept.external-standard/sun-education-field-code-2020}

                                {:name :sun-education-field-code-2000
                                 :doc "Old SUN 2000 education field code, either 1, 2 or 3 digits and a letter"
                                 :deprecated true
                                 :where-field :concept.external-standard/sun-education-field-code-2000}

                                {:name :type
                                 :doc "SUN education field types (space-separated): sun-education-field-1, sun-education-field-2, sun-education-field-3 or sun-education-field-4"}]
             :pull [:concept.external-standard/sun-education-field-code-2020
                    :concept.external-standard/sun-education-field-code-2000]}

            {:endpoint-name "sun-education-level"
             :default-type "sun-education-level-1 sun-education-level-2 sun-education-level-3"
             :extra-attributes [{:name :sun-education-level-code-2020
                                 :doc "SUN education level code, either 1, 2 or 3 digits"
                                 :where-field :concept.external-standard/sun-education-level-code-2020}

                                {:name :sun-education-level-code-2000
                                 :doc "SUN 2000 education level code, either 1, 2 or 3 digits"
                                 :deprecated true
                                 :where-field :concept.external-standard/sun-education-level-code-2000}

                                {:name :type
                                 :doc "SUN education level types (space-separated): sun-education-level-1, sun-education-level-2 or sun-education-level-3"}]
             :pull [:concept.external-standard/sun-education-level-code-2020
                    :concept.external-standard/sun-education-level-code-2000]}

            {:endpoint-name "sni-level"
             :default-type "sni-level-1 sni-level-2"
             :extra-attributes [{:name :sni-level-code-2007
                                 :doc "SNI 2007 level code"
                                 :where-field :concept.external-standard/sni-level-code-2007}
                                {:name :type
                                 :doc "SNI level type: sni-level-1 or sni-level-2"}]
             :pull [:concept.external-standard/sni-level-code-2007]}

            {:endpoint-name "language"
             :default-type "language"
             :extra-attributes [{:name :iso-639-3-alpha-2-2007
                                 :doc "2 letter language code"
                                 :deprecated true
                                 :where-field :concept.external-standard/iso-639-3-alpha-2-2007}
                                {:name :iso-639-3-alpha-3-2007
                                 :doc "3 letter language code"
                                 :deprecated true
                                 :where-field :concept.external-standard/iso-639-3-alpha-3-2007}
                                {:name :iso-639-1-2002
                                 :doc "2 letter language code"
                                 :where-field :concept.external-standard/iso-639-1-2002}
                                {:name :iso-639-2-1998
                                 :doc "3 letter language code"
                                 :where-field :concept.external-standard/iso-639-2-1998}
                                {:name :iso-639-3-2007
                                 :doc "3 letter language code"
                                 :where-field :concept.external-standard/iso-639-3-2007}]
             :pull [:concept.external-standard/iso-639-3-alpha-2-2007
                    :concept.external-standard/iso-639-3-alpha-3-2007
                    :concept.external-standard/iso-639-1-2002
                    :concept.external-standard/iso-639-2-1998
                    :concept.external-standard/iso-639-3-2007]}

            {:endpoint-name "unemployment-fund"
             :default-type "unemployment-fund"
             :extra-attributes [{:name :taxonomy/unemployment-fund-code-2017
                                 :doc "Swedish unemployment fund code"
                                 :where-field :concept.external-standard/unemployment-fund-code-2017}]
             :pull [:concept.external-standard/unemployment-fund-code-2017]}

            {:endpoint-name "employment-duration"
             :default-type "employment-duration"
             :extra-attributes [{:name :eures-code-2014
                                 :doc "Eures code"
                                 :where-field :concept.external-standard/eures-code-2014}]

             :pull [:concept.external-standard/eures-code-2014]}]))

(def version-param
  (par
   (sp/or :int int?
         ;; `keyword?` is needed only to trigger reitit's coercion mechanism
          :ref (sp/and keyword? #{:latest :next}))
   "Taxonomy version, either number that indicates the version, \"latest\" for latest release, or \"next\" for unpublished changes (requires admin rights)"
   :swagger/type :string))

;; Response definitions

(defn response200 [spec]
  {200 {:body spec
        :description "OK"}})

(defn response-bad-request [spec]
  {400 {:body spec
        :description "Bad request"}})

(def response401
  {401 {:body unauthorized-spec
        :description "Unauthorized"}})

(def response404
  {404 {:body error-spec
        :description "Not found"}})

(def response406
  {406 {:body error-spec
        :description "Not Acceptable"}})

(def response409
  {409 {:body error-spec
        :description "Conflict"}})

(def response500
  {500 {:body error-spec
        :description "Internal Server Error"}})

;; Detailed endpoint

(def detailed-endpoint-query-base
  {(ds/opt :id) (par string? "ID of concept")
   (ds/opt :preferred-label) (par string? "Textual name of concept")
   (ds/opt :deprecated) (par boolean? "Restrict to deprecation state" :openapi {:deprecated true})
   (ds/opt :include-deprecated) (par boolean? "Include deprecated values")
   (ds/opt :relation) (par #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"} "Relation type")
   (ds/opt :related-ids) (par string? "OR-restrict to these relation IDs (white space separated list)")
   (ds/opt :include-legacy-information) (par boolean? "This parameter will be removed. Include information related to Arbetsförmedlingen's old internal taxonomy"
                                             :openapi {:deprecated true})

   ;;   (ds/opt :code) (par string? name) ;; TODO Create one for each attribute
   (ds/opt :offset) (par nat-int? "Return list offset (from 0)")
   (ds/opt :limit) (par pos-int? "Return list limit")
   (ds/opt :version) version-param})

(defn create-extra-query-spec-field [extra-query-attribute]
  {(ds/opt (:name extra-query-attribute))
   (par string? (:doc extra-query-attribute) :swagger/deprecated (:deprecated extra-query-attribute))})

(defn compose-extra-where-attribute [extra-attribute query-params]
  (let [db-field-name (:where-field extra-attribute)
        field-name (:name extra-attribute)
        value (field-name query-params)]
    [db-field-name value]))

(defn compose-extra-where-attributes [query-params extra-attributes]
  (->> extra-attributes
       (filter :where-field)
       (map #(compose-extra-where-attribute % query-params))
       (filter second)))

(defn build-db-function-args [query-params-map endpoint-config]
  (let [query-params (:query (:parameters query-params-map))
        extra-attributes (:extra-attributes endpoint-config)]
    (-> query-params
        (cond-> (:related-ids query-params)
          (update :related-ids str/split #" "))
        (update :type (fnil str/split (:default-type endpoint-config)) #" ")
        (assoc :extra-pull-fields (:pull endpoint-config))
        (assoc :extra-where-attributes (compose-extra-where-attributes query-params extra-attributes)))))

(defn compose-handler-function [endpoint-config db-function]
  (fn [args]
    {:status 200
     :description "OK"
     :body (vec (map map->nsmap (db-function (build-db-function-args args endpoint-config))))}))

(defn create-detailed-endpoint [endpoint-config db-function]
  (let [endpoint-name (:endpoint-name endpoint-config)
        query (->> endpoint-config
                   :extra-attributes
                   (map create-extra-query-spec-field)
                   (into detailed-endpoint-query-base))]
    [(str "/concepts/" endpoint-name)
     {:summary (str "Get " endpoint-name ". Supply at least one search parameter.")
      :parameters {:query query}
      :get {:responses {200 {:body (keyword taxonomy-namespace (str "concepts-" endpoint-name))
                             :description "OK"}}
            :handler (compose-handler-function endpoint-config db-function)}}]))

(ns jobtech-taxonomy.common.relation
  (:require [clojure.set :as set]
            [jobtech-taxonomy.common.concept :as concept]))

(def persisted-relations
  "Mapping from relations as persisted in the db to labels describing a reverse
  direction for the relation edge"
  {"broader" "narrower"
   "related" "related"
   "possible-combination" "possible-combination"
   "unlikely-combination" "unlikely-combination"
   "substitutability" "substituted-by"
   "broad-match" "narrow-match"
   "exact-match" "exact-match"
   "close-match" "close-match"})

(def relations
  (into persisted-relations (set/map-invert persisted-relations)))

(defn- build-id [c1 t c2]
  (str c1 ":" t ":" c2))

(defn- build-id-ensuring-order-if-symmetrical [c1 t c2]
  (if (and (= t (relations t)) (pos? (compare c1 c2)))
    (build-id c2 t c1)
    (build-id c1 t c2)))

(defn id
  "Calculate relation id for a given concept-1/type/concept-2 tuple"
  [concept-1-id type concept-2-id]
  {:pre [(concept/id? concept-1-id) (relations type) (concept/id? concept-2-id)]}
  (if (persisted-relations type)
    (build-id-ensuring-order-if-symmetrical concept-1-id type concept-2-id)
    (build-id-ensuring-order-if-symmetrical concept-2-id (relations type) concept-1-id)))

(defn edge-tx
  "Create a transaction fact describing an edge between 2 concepts

  Going from `concept-1` in `type` direction will lead to `concept-2`

  By default it is expected that concepts 1 and 2 are already transacted and
  can be referenced using `[:concept/id ...]` attribute ref. If you need to
  create both edge and concepts in the same transaction, you can supply
  `::concept-1-tempid` and `::concept-2-tempid` attrs that will be used instead
  of `[:concept/id ...]` attribute refs.

  Remaining attrs will be transacted as a part of a relation entity"
  ([concept-1-id type concept-2-id]
   (edge-tx concept-1-id type concept-2-id {}))
  ([concept-1-id type concept-2-id {::keys [concept-1-tempid concept-2-tempid] :as attrs}]
   {:pre [(concept/id? concept-1-id) (relations type) (concept/id? concept-2-id)]}
   (if (persisted-relations type)
     (into {:relation/id (id concept-1-id type concept-2-id)
            :relation/concept-1 (or concept-1-tempid
                                    [:concept/id concept-1-id])
            :relation/type type
            :relation/concept-2 (or concept-2-tempid
                                    [:concept/id concept-2-id])}
           (dissoc attrs ::concept-1-tempid ::concept-2-tempid))
     (edge-tx concept-2-id
              (relations type)
              concept-1-id
              (set/rename-keys attrs {::concept-1-tempid ::concept-2-tempid
                                      ::concept-2-tempid ::concept-1-tempid})))))

(def rules
  "Datomic query rules that allow querying for relation edges using `edge` rule

  `edge` rule accepts args similar to `edge-tx`:
  ```
  (edge ?concept-1 ?type ?concept-2 ?rel)
  ```"
  [['(-forward-edge ?from-concept ?type ?to-concept ?relation)
    [(list 'ground (vec (keys persisted-relations))) '[?type ...]]
    '[?relation :relation/concept-1 ?from-concept]
    '[?relation :relation/type ?type]
    '[?relation :relation/concept-2 ?to-concept]]
   ['(-reverse-edge ?from-concept ?type ?to-concept ?relation)
    [(list 'ground (set/map-invert persisted-relations))
     '[[?type ?reverse-type]]]
    '[?relation :relation/concept-2 ?from-concept]
    '[?relation :relation/type ?reverse-type]
    '[?relation :relation/concept-1 ?to-concept]]
   '[(edge ?from-concept ?type ?to-concept ?relation)
     (or (-forward-edge ?from-concept ?type ?to-concept ?relation)
         (-reverse-edge ?from-concept ?type ?to-concept ?relation))]])

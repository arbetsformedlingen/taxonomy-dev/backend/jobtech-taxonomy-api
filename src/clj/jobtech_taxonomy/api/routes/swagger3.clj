(ns jobtech-taxonomy.api.routes.swagger3
  (:require [expound.alpha :as expound]
            [reitit.coercion :as coercion]))

(defn wrap-coercion [coercion]
  (reify coercion/Coercion
    (-get-name [_] (coercion/-get-name coercion))
    (-get-options [_] (coercion/-get-options coercion))
    (-get-model-apidocs [_ specification model options]
      (coercion/-get-model-apidocs coercion specification model options))
    (-get-apidocs [_ specification data]
      (coercion/-get-apidocs coercion specification data))
    (-compile-model [_ model name]
      (coercion/-compile-model coercion

                               ;; Fix while waiting for this PR:
                               ;; https://github.com/metosin/reitit/pull/683
                               (if (and (seq model) (apply = model))
                                 (take 1 model)
                                 model)

                               name))
    (-open-model [_ spec]
      (coercion/-open-model coercion spec))
    (-encode-error [_ error]
      {:taxonomy/error (with-out-str ((expound/custom-printer {:print-specs? false}) (:problems error)))})
    (-request-coercer [_ type spec]
      (coercion/-request-coercer coercion type spec))
    (-response-coercer [_ spec]
      (coercion/-response-coercer coercion spec))))

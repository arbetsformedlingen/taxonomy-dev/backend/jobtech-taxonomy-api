(ns jobtech-taxonomy.api.routes.raw-handler
  (:require [clojure.string :as str]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]
            [muuntaja.format.edn :as edn]
            [muuntaja.format.core :as muuntaja]
            [ring.util.http-response :as http-response]
            [ring.util.response :as response]
            [spec-tools.data-spec :as ds]
            [taoensso.timbre :as log]))

(defn- datomic-raw-zipped-handler [config zip-fn]
  (fn [_]
    (let [type (:type (config/get-backend config))
          storage-dir (config/->backend-local-storage-dir config)]
      (cond
        (not= :datomic type)
        (-> {:error (format "Incompatible database type: %s" (name type))}
            taxonomy/map->nsmap
            http-response/method-not-allowed)
        
        (not storage-dir)
        (-> {:error (format "db backend not running file based datomic-local. Current config is: %s" type)}
            taxonomy/map->nsmap
            http-response/method-not-allowed)

        :else
        (try
          (-> storage-dir
              zip-fn
              response/response
              (response/header "Content-Type" "application/zip")
              (response/header "Content-Disposition"
                               (format "attachment; filename=%s.zip"
                                       (config/->backend-name config))))
          (catch Exception e
            (log/error e "Exception during zipping process")
            (case (:type (ex-data e))
              :data-written-during-zipping
              (-> {:error "Data was written to the database during the zipping process"}
                  taxonomy/map->nsmap
                  http-response/conflict)

              (-> {:error "An unexpected error occurred during the zipping process"}
                  taxonomy/map->nsmap
                  http-response/internal-server-error))))))))

(defn- datom-raw-handler [cfg]
  (fn [request]
    (let [raw-format (-> request :parameters :query :format)]
      (if (nil? raw-format)
        {:status 200
         :body (dc/read-raw cfg {:format :edn})}
        (case (str/lower-case (str/trim raw-format))
          "nippy"
          {:status 200
           :headers {"Content-Type" "multipart/byteranges"}
           :body (dc/read-raw cfg {:format :nippy-bytes})}

          ;; Remove this because is a non-standard approach?
          ;; The standard approach would be the client to include
          ;; a header "Accept: application/edn"
          "edn"
          {:status 200
           :headers {"Content-Type" "application/edn; charset=utf-8"}
           :body (muuntaja/encode-to-bytes (edn/encoder {})
                                           (dc/read-raw cfg {:format :edn})
                                           "utf-8")}

          {:status 405
           :body {:error (str "read-raw not available for format '%s'" format)}})))))

(defn raw-endpoint [cfg]
  ["/raw"
   ["/datoms" {:summary "Get a full copy of the currently running database."
               :parameters {:query {(ds/opt :format) (taxonomy/par string? "Nippy or EDN")}}
               :get {:responses {200 {:body any?
                                      :content {"multipart/byteranges" {}}
                                      :description "OK"}
                                 405 {:body any?
                                      :content {"application/json" {}}
                                      :description "Not implemented"}}
                     :handler
                     (datom-raw-handler cfg)}}]
   ["/datomic-local-file-db"
    {:summary "Get a copy of the currently running database."
     :parameters {:query {}}
     :get {:responses {200 {:body any?
                            :description "OK"}}
           :handler
           (datomic-raw-zipped-handler cfg iou/zip-with-write-check)}}]])

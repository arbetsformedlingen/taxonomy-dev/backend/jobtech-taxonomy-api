(ns jobtech-taxonomy.api.routes.graphql
  (:require
   [clojure.string :as str]
   [jobtech-taxonomy.api.db.graphql :as graphql]
   [jobtech-taxonomy.common.taxonomy :as taxonomy]))

(defn graphql-response-handler [cfg {:keys [query variables operationName api-key]}]
  (try
    (let [response (graphql/execute cfg query variables operationName api-key)]
      (if-let [errors (seq (:errors response))]
        {:status 400
         :body {:taxonomy/error errors}}
        {:status 200
         :body response}))
    (catch Exception ei
      (let [data (ex-data ei)
            cause (ex-message ei)]
        (if (and (some? cause)
                 (str/includes? cause "Not authorized"))
          {:status 401 :body (taxonomy/map->nsmap {:error "Not authorized"})}
          {:status 400 :body (taxonomy/map->nsmap {:error data})})))))

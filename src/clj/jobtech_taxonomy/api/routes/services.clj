(ns jobtech-taxonomy.api.routes.services
  (:require [clojure.data.json :as json]
            [clojure.string :as str]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.concept-types :as concept-types]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.core :as core]
            [jobtech-taxonomy.api.db.daynotes :as daynotes]
            [jobtech-taxonomy.api.db.eures :as eures]
            [jobtech-taxonomy.api.db.events :as events]
            [jobtech-taxonomy.api.db.graph :as graph]
            [jobtech-taxonomy.api.db.graphviz :as graphviz]
            [jobtech-taxonomy.api.db.legacy :as legacy]
            [jobtech-taxonomy.api.db.postalcodes :as postalcodes]
            [jobtech-taxonomy.api.db.search :as search]
            [jobtech-taxonomy.api.db.versions :as v]
            [jobtech-taxonomy.api.middleware.cors :as cors]
            [jobtech-taxonomy.api.middleware.formats :as formats]
            [jobtech-taxonomy.api.routes.graphql :as grh]
            [jobtech-taxonomy.api.routes.parameter-util :as pu]
            [jobtech-taxonomy.api.routes.swagger3 :as swagger3]
            [jobtech-taxonomy.common.build-info :as build-info]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]
            [jobtech-taxonomy.features.private :as private]
            [muuntaja.core :as m]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.openapi :as openapi]
            [ring.util.http-response :as resp]
            [spec-tools.core :as st]
            [spec-tools.data-spec :as ds]
            [taoensso.timbre :as log]))

;; Status:
;;   - fixa replaced-by-modell i /changes, /replaced-by-changes, /search, /private/concept

(defn catch-errors [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception e
        (let [ex-type (:type (ex-data e))
              body {::taxonomy/error (ex-message e)}]
          (case ex-type
            :no-such-db-version {:status 400 :body body}
            (do
              (log/error e "Error while performing request")
              {:status 500 :body body})))))))

(defn json-map? [txt] (try (map? (json/read-str txt)) (catch Exception _e false)))

(defn- main-endpoints [cfg]
  ["/main"
   {:tags ["Main"]}

   ["/versions"
    {:summary "Show the available versions."
     :get {:responses (taxonomy/response200 taxonomy/versions-spec)
           :handler (fn [{{{:keys [_]} :query} :parameters}]
                      {:status 200
                       :body (map taxonomy/map->nsmap (v/get-all-versions cfg))})}}]

   ["/concepts"
    {:summary "Get concepts. Supply at least one search parameter."
     :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept"),
                          (ds/opt :preferred-label) (taxonomy/par string? "Textual name of concept"),
                          (ds/opt :type) (st/spec {:name "types" :spec string? :description "Restrict to concept type"})
                          (ds/opt :deprecated) (taxonomy/par boolean? "Restrict to deprecation state" :openapi {:deprecated true}),
                          (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated values"),
                          (ds/opt :relation) (pu/get-param :relation),
                          (ds/opt :related-ids) (taxonomy/par string? "OR-restrict to these relation IDs (white space separated list). If specified, relation is required"),
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit"),
                          (ds/opt :version) taxonomy/version-param
                          (ds/opt :include-legacy-information) (taxonomy/par boolean? "This parameter will be removed. Include information related to Arbetsförmedlingen's old internal taxonomy"
                                                                             :openapi {:deprecated true})}}
     :middleware [(private/private-version-middleware cfg)]
     :get {:responses (taxonomy/response200 taxonomy/concepts-spec)
           :handler (fn [{{{:keys [id preferred-label type include-deprecated deprecated
                                   relation related-ids offset limit version
                                   include-legacy-information]
                            :or {version :latest relation "related"
                                 include-legacy-information false}} :query} :parameters}]
                      {:status 200
                       :body (mapv taxonomy/map->nsmap (concepts/find-concepts
                                                        cfg
                                                        {:id id
                                                         :preferred-label preferred-label
                                                         :type (when type (str/split type #" "))
                                                         :include-deprecated include-deprecated
                                                         :deprecated deprecated
                                                         :relation relation
                                                         :related-ids (when related-ids (str/split related-ids #" "))
                                                         :offset offset
                                                         :limit limit
                                                         :version version
                                                         :include-legacy-information include-legacy-information}))})}}]

   ["/concept/changes"
    {:summary "Show changes to concepts as a stream of events."
     :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occurred after this version was published."),
                          (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occurred before this version was published and during this version. (default: latest version)"),
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")}}
     :get {:responses (taxonomy/response200 taxonomy/concept-changes-validator-spec)
           :handler (fn [{{{:keys [after-version to-version-inclusive offset limit _unpublished]} :query} :parameters}]
                      {:status 200
                       :body (let [events (daynotes/get-concept-changes-with-pagination-public cfg after-version to-version-inclusive offset limit)]
                               (vec (map taxonomy/map->nsmap events)))})}}]

   ["/graph"
    {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
     :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                          :source-concept-type (taxonomy/par string? "Source nodes concept type")
                          :target-concept-type (taxonomy/par string? "Target nodes concept type")
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)")
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")
                          (ds/opt :version) taxonomy/version-param
                          (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
     :middleware [(private/private-version-middleware cfg)]
     :get {:responses (taxonomy/response200 taxonomy/graph-spec)
           :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                            :or {version :latest}} :query} :parameters}]
                      {:status 200
                       :body (graph/fetch-graph cfg edge-relation-type source-concept-type target-concept-type
                                                include-deprecated offset limit version)})}}]
   ["/replaced-by-changes"
    {:summary "Show the history of concepts being replaced after a given version."
     :parameters {:query {:after-version (taxonomy/par int? "After what taxonomy version did the change occur"),
                          (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occurred before this version was published and during this version. (default: latest version)")}}
     :get {:responses (taxonomy/response200 taxonomy/replaced-by-changes-spec)
           :handler (fn [{{{:keys [after-version to-version-inclusive]} :query} :parameters}]
                      {:status 200
                       :body (vec (map taxonomy/map->nsmap (events/get-deprecated-concepts-replaced-by-from-version cfg after-version to-version-inclusive)))})}}]

   ["/concept/types"
    {:summary "Return a list of all taxonomy types."
     :parameters {:query {(ds/opt :version) taxonomy/version-param}}
     :middleware [(private/private-version-middleware cfg)]
     :get {:responses (taxonomy/response200 taxonomy/concept-types-spec)
           :handler (fn [{{{:keys [version]
                            :or {version :latest}} :query} :parameters}]
                      {:status 200
                       :body (vec (concept-types/get-all-types cfg version))})}}]

   ["/relation/types"
    {:summary "Relation types. Type 'narrower' is not listed here, as it exists as the inverse of 'broader'."
     :get {:responses (taxonomy/response200 taxonomy/relation-types-spec)
           :handler (fn [{{{:keys []} :query} :parameters}]
                      {:status 200
                       :body (vec (core/get-relation-types cfg))})}}]

   ["/relation/changes"
    {:summary "Show changes to the relations as a stream of events."
     :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changed relations that occurred after this version was published."),
                          (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changed relations that occurred before this version was published and during this version. (default: latest version)"),
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")}}
     :get {:responses (taxonomy/response200 [any?])
           :handler (fn [{{{:keys [after-version to-version-inclusive offset limit]} :query} :parameters}]
                      {:status 200
                       :body (let [events (doall (daynotes/get-relation-changes-with-pagination-public cfg after-version to-version-inclusive offset limit))]
                               (vec (map taxonomy/map->nsmap events)))})}}]])

(defn- specific-endpoints [cfg]
  ["/specific"
   {:tags ["Specific Types"]
    :description "Exposes concept with detailed information such as codes from external standards."}
   (map
    #(taxonomy/create-detailed-endpoint % (partial concepts/find-concepts cfg))
    taxonomy/detailed-endpoint-configs)])

(defn- suggesters-endpoints [cfg]
  ["/suggesters"
   {:tags ["Suggesters"]}

   ["/autocomplete"
    {:summary "Autocomplete from query string"
     :description "Help end-users to find relevant concepts from the taxonomy"
     :parameters {:query {:query-string (taxonomy/par string? "String to search for"),
                          (ds/opt :type) (taxonomy/par string? "Type to search for"),
                          (ds/opt :relation) (pu/get-param :relation),
                          (ds/opt :related-ids) (taxonomy/par string? "List of related IDs to search for"),
                          (ds/opt :search-type) (taxonomy/par #{"contains" "starts-with"} "This parameter is ignored" :openapi {:deprecated true})
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")
                          (ds/opt :version) taxonomy/version-param}}
     :get {:responses (taxonomy/response200 taxonomy/autocomplete-spec)
           :handler (fn [{{:keys [query]} :parameters}]
                      {:status 200
                       :body (vec (map taxonomy/map->nsmap
                                       (search/get-concepts-by-search
                                        cfg
                                        (-> query
                                            (update :version #(or % :latest))
                                            (update :relation #(or % "related"))
                                            (update :type #(some-> % (str/split #" ")))
                                            (update :related-ids #(some-> % (str/split #" ")))))))})}}]])

(defn- legacy-endpoints [cfg]
  ["/legacy"
   {:tags ["Legacy"]}

   ["/convert-matching-component-id-to-new-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids from the old matching component to new Taxonomy ids. The old matching component used SSYK codes, municipality codes and region codes as ids so this endpoint implements the same bad behaviour."
     :parameters {:query {:matching-component-id (taxonomy/par string? "An id form the old matching component.")
                          :type legacy/concepts-types}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-matching-component-id-to-new-id-handler cfg)}}]

   ["/convert-old-id-to-new-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids to new Taxonomy ids."
     :parameters {:query {:legacy-id (taxonomy/par string? "A legacy id.")
                          :type legacy/concepts-types}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-old-id-to-new-id-handler cfg)}}]

   ["/convert-new-id-to-old-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between new ids to ikd Taxonomy ids. "
     :parameters {:query {:id (taxonomy/par string? "A taxonomy id")}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-new-id-to-old-id-handler cfg)}}]

   ["/convert-ssyk-2012-code-to-new-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between SSYK 2012 code to new Taxonomy ids. SSYK 2012 is also known as occupation group / yrkesgrupp in older applications."
     :parameters {:query {:code (taxonomy/par string? "The SSYK 2012 code.")}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-ssyk-2012-code-to-new-id-handler cfg)}}]

   ["/convert-municipality-code-to-new-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between municipality code to new Taxonomy ids. "
     :parameters {:query {:code (taxonomy/par string? "The municipality code.")}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-municipality-code-to-new-id-handler cfg)}}]

   ["/convert-swedish-region-code-to-new-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between Swedish län code to new Taxonomy ids. "
     :parameters {:query {:code (taxonomy/par string? "The region code.")}}
     :get {:responses legacy/responses
           :handler (partial legacy/convert-swedish-region-code-to-new-id cfg)}}]

   ["/get-occupation-name-with-relations"
    {:summary "This endpoint is only for legacy applications wanting a translation dump with ids for occupation names and their relations. "
     :get {:responses (taxonomy/response200 taxonomy/legacy-mapping-concepts)
           :handler (fn [{{{:keys [_]} :query} :parameters}]
                      {:status 200
                       :body (map taxonomy/map->nsmap (legacy/get-occupation-name-with-relations cfg))})}}]

   ["/get-old-legacy-education-levels"
    {:summary "This endpoint is only for legacy applications wanting old deprecated legacy education levels. "
     :get {:responses (taxonomy/response200 [{:preferred_label string?,
                                              :id string?,
                                              :deprecated_legacy_id_2 string?,
                                              :definition string?,
                                              :deprecated_legacy_id string?,
                                              :sun_education_level_code_2000 string?}])

           :handler (fn [{{{:keys [_]} :query} :parameters}]
                      {:status 200
                       :body legacy/old-deprecated-legacy-education-levels})}}]

   ["/postalcodes-municipality"
    {:summary "This endpoint is only for legacy applications using postalcodes. "
     :get {:responses (taxonomy/response200 [{:postal_code string?
                                              :locality_preferred_label string?
                                              :municipality_id string?
                                              :national_nuts_level_3_code_2019 string?
                                              :municipality_preferred_label string?
                                              :municipality_deprecated_legacy_id string?
                                              :lau_2_code_2015 string?
                                              :region_id string?
                                              :region_term string?
                                              :region_deprecated_legacy_id string?}])

           :handler (fn [{{{:keys [_]} :query} :parameters}]
                      {:status 200
                       :body (postalcodes/fetch-postalcodes)})}}]])

(defn- eures-endpoints [cfg]
  ["/eures"
   {:tags ["Eures"]}

   ["/convert-occupation-name-concept-id-to-eures-esco-id"
    {:summary "This endpoint is only for legacy applications wanting to convert between occupation names concept id to eures esco id. "
     :parameters {:query {:id (taxonomy/par string? "ID of concept")}}
     :get {:responses eures/responses
           :handler (fn [request] (eures/handler cfg request))}}]])

(defn- status-endpoints [cfg]
  ["/status"
   {:tags ["Status"]}
   ["/build"
    {:swagger {:summary "Some branch and commit information for the build."}
     :get {:responses (taxonomy/response200 any?)
           :handler (fn [_]
                      (resp/ok (assoc build-info/info
                                      :deployment-name
                                      (get-in cfg [:options :deployment-name]))))}}]
   ["/ready"
    {:summary "A readiness health check for the database"
     :get {:responses (taxonomy/response200 any?)
           :handler (fn [_]
                      (let [res (v/get-all-versions cfg)]
                        {:status 200
                         :body {:available (some? res)}}))}}]

   ["/health"
    {:summary "A health check for the API server that returns immediately"
     :get {:responses (taxonomy/response200 any?)
           :handler (fn [_]
                      {:status 200
                       :body {:available true}})}}]])

(defn- deprecate [endpoint] (assoc-in endpoint [1 :openapi :deprecated] true))
(defn- alpha-endpoints [cfg]
  (into ["/alpha"
         {:tags ["Alpha"]
          :summary "These features are NOT stable and will change."
          :description "Features under development."}]
        [(deprecate (:endpoints (config/get-module-data cfg :mappings)))
         ["/graphviz/types"
          {:parameters {:query {(ds/opt :version) taxonomy/version-param}}
           :middleware [(private/private-version-middleware cfg)]
           :get {:responses (merge (taxonomy/response200 any?)
                                   (taxonomy/response-bad-request any?)
                                   taxonomy/response401)
                 :handler (fn [{{{:keys [version]
                                  :or {version :latest}} :query} :parameters}]
                            {:status 200
                             :body (graphviz/edges-for-version cfg version)})}}]]))

(defn openapi-handler [version]
  {:get
   {:handler (openapi/create-openapi-handler)
    :openapi
    {:info
     {:title "Jobtech Taxonomy"
      :version version
      :description "Jobtech Taxonomy Endpoints.\nAlternatively, explore the Taxonomy data with the <a href=\"https://atlas.jobtechdev.se\">JobTech Taxonomy Atlas</a>"}
     :components {:securitySchemes {:api-key {:type "apiKey" :name "api-key" :in "header"}}}
     :security [{:api-key []}]}
    :no-doc true}})

(defn service-routes [cfg]
  ["/v1/taxonomy"
   {:coercion (swagger3/wrap-coercion spec-coercion/coercion)
    :muuntaja formats/instance
    :middleware [cors/cors catch-errors]
    :responses (merge (taxonomy/response-bad-request any?) taxonomy/response401 taxonomy/response500)}

   ["/openapi.json"
    (openapi-handler (get-in cfg [:options :deployment-name]))]

   ["/graphql"
    {:tags ["GraphQL"]
     :summary "GraphQL endpoint for powerful graph queries"
     :description "You should explore this API in [this GraphQL-specific explorer](/v1/taxonomy/graphiql)"
     :middleware [(fn graphql-mw [handler]
                    (fn graphql-run [request]
                      (let [api-key (get-in request [:headers "api-key"])
                            parameters (:parameters request)
                            {:keys [query variables operationName]} (or (:query parameters) (:form parameters) (:body parameters))
                            graphql-parameters {:api-key api-key
                                                :query query
                                                :variables (update (m/decode formats/instance "application/json" variables)
                                                                   :version #(or % :latest))
                                                :operationName operationName}
                            graphql-request (assoc-in request [:parameters :graphql] graphql-parameters)]
                        (handler graphql-request))))]
     :responses (merge (taxonomy/response200 any?)
                       (taxonomy/response-bad-request any?)
                       taxonomy/response401)
     :get {:parameters {:query {:query (taxonomy/par string? "query string")
                                (ds/opt :variables) (taxonomy/par json-map? "json string with query variables")
                                (ds/opt :operationName) (taxonomy/par string? "optional query name that is used when there are multiple query definitions in a single query string")}}
           :handler (fn [{{:keys [graphql]} :parameters}] (grh/graphql-response-handler cfg graphql))}
     :post {:parameters {:form {:query (taxonomy/par string? "query string")
                                (ds/opt :variables) (taxonomy/par json-map? "json string with query variables")
                                (ds/opt :operationName) (taxonomy/par string? "optional query name that is used when there are multiple query definitions in a single query string")}}
            :handler (fn [{{:keys [graphql]} :parameters}] (grh/graphql-response-handler cfg graphql))}}]

   (main-endpoints cfg)
   (specific-endpoints cfg)
   (suggesters-endpoints cfg)
   (private/private-endpoints cfg)
   (private/webhooks-endpoints cfg)
   (legacy-endpoints cfg)
   (eures-endpoints cfg)
   ["" {:tags ["Mappings"]}
    (:endpoints (config/get-module-data cfg :mappings))]
   (alpha-endpoints cfg)
   (status-endpoints cfg)])

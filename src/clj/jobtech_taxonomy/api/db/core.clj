(ns jobtech-taxonomy.api.db.core
  (:require
   [jobtech-taxonomy.api.db.api-util :as api-util]
   [jobtech-taxonomy.api.db.concepts :as concepts]
   [jobtech-taxonomy.api.db.database-connection :as db]
   [taoensso.timbre :as log]))

(defn ^{:auth :editor} retract-concept [cfg user-id id comment]
  (when (concepts/exists? (db/get-db cfg :next) id)
    (let [result (db/transact
                  (db/connect cfg)
                  {:tx-data [(api-util/user-action-tx user-id comment)
                             {:concept/id id
                              :concept/deprecated true}]})]
      (log/info "retract-concept" user-id id comment)
      result)))

(def get-relation-types-query
  '[:find ?v :where [_ :relation/type ?v]])

(defn ^{:auth :editor} get-relation-types [cfg]
  (->> (db/q get-relation-types-query (db/get-db cfg :next))
       (sort-by first)
       (apply concat)))

(defn replace-tx-data [user-id comment old-concept-id new-concept-id]
  {:tx-data [(api-util/user-action-tx user-id comment)
             {:concept/id old-concept-id
              :concept/deprecated true
              :concept/replaced-by [[:concept/id new-concept-id]]}]})

;; TODO append on replaced by listan

(defn ^{:auth :editor} replace-deprecated-concept [cfg user-id old-concept-id new-concept-id comment & [tx-inst]]
  (let [db (db/get-db cfg :next)]
    (log/info "replace-deprecated-concept" user-id old-concept-id new-concept-id comment)
    (when (and (concepts/exists? db old-concept-id)
               (concepts/exists? db new-concept-id))
      (db/transact (db/connect cfg) (-> (replace-tx-data user-id comment old-concept-id new-concept-id)
                                        (update :tx-data api-util/add-tx-instant-if-present tx-inst))))))

(defn ^{:auth :editor} unreplace-deprecated-concept [cfg user-id old-concept-id new-concept-id comment]
  (let [db (db/get-db cfg :next)]
    (when (and (concepts/exists? db old-concept-id)
               (concepts/exists? db new-concept-id))
      (db/transact (db/connect cfg) {:tx-data [(api-util/user-action-tx user-id comment)
                                               [:db/retract [:concept/id old-concept-id] :concept/replaced-by [:concept/id new-concept-id]]]}))))

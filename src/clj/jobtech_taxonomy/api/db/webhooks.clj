(ns jobtech-taxonomy.api.db.webhooks
  (:require
   [jobtech-taxonomy.api.db.database-connection :as db]))

(defn update-webhooks [conn key url]
  (db/transact conn {:tx-data [{:webhook/key key, :webhook/url url}]}))

(defn list-webhooks! [cfg]
  (let [db (db/get-db cfg :next)
        result (db/q {:query '[:find (pull ?e [:webhook/key :webhook/url])
                               :where [?e :webhook/key _]]
                      :args [db]})]
    (apply concat result)))

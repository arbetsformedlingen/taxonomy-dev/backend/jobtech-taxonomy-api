(ns jobtech-taxonomy.api.db.database-connection
  {:clj-kondo/config '{:linters
                       {:unresolved-var
                        {:exclude [datahike.api/as-of
                                   datahike.api/connect
                                   datahike.api/create-database
                                   datahike.api/database-exists?
                                   datahike.api/delete-database
                                   datahike.api/db
                                   datahike.api/history
                                   datahike.api/pull
                                   datahike.api/release
                                   datahike.api/transact
                                   datahike.api/q]}}}}
  (:require [datahike.api :as datahike]
            [datahike.connector :as dc]
            [datahike.datom :as datahike-datom]
            [datomic.client.api :as datomic]
            [jobtech-taxonomy.api.config :as c]
            [jobtech-taxonomy.api.db.database-connection.impl :as impl]
            [taoensso.timbre :as log]
            [wanderung.core :as wanderung])
  (:import [datahike.db
            AsOfDB
            DB
            FilteredDB
            HistoricalDB
            SinceDB]))

(set! *warn-on-reflection* true)

(defn- datomic-db->cache-key [x]
  (try
    (select-keys x [:db-name
                    :database-id
                    :t
                    :next-t
                    :as-of
                    :since
                    :history
                    :next-token])

    (catch Exception _e
      x)))

(defn- datahike-arg->cache-key [x]
  (condp instance? x
    DB {:max-tx (:max-tx x)
        :id (:datahike/id (.-meta ^DB x))}
    AsOfDB {:as-of (.-time_point ^AsOfDB x)
            :db (datahike-arg->cache-key (.-origin_db ^AsOfDB x))}
    SinceDB {:since (.-time_point ^SinceDB x)
             :db (datahike-arg->cache-key (.-origin_db ^SinceDB x))}
    HistoricalDB {:history (datahike-arg->cache-key (.-origin_db ^HistoricalDB x))}
    FilteredDB {:pred (.-pred ^FilteredDB x)
                :db (datahike-arg->cache-key (.-unfiltered_db ^FilteredDB x))}
    x))

(defonce backend-map (atom {DB :datahike
                            AsOfDB :datahike
                            SinceDB :datahike
                            HistoricalDB :datahike
                            FilteredDB :datahike
                            datomic.client.api.protocols.Db :datomic
                            datomic.client.api.protocols.Client :datomic
                            datomic.client.api.protocols.Connection :datomic
                            datomic.core.db.Db :datomic}))

(defn ^:export register-backend! [backend-class backend-keyword]
  (swap! backend-map assoc backend-class backend-keyword))

(defn dispatch-obj [obj]
  (or
   (first
    (for
     [[backend-class backend-keyword] (deref backend-map)
      :when (instance? backend-class obj)]
      backend-keyword))
   (:database-type (meta obj))
   (and (map? obj) (:type (c/get-backend obj)))))

(defn database-backend [obj]
  (if-let [backend (and (map? obj) (:type (c/get-backend obj)))]
    backend
    (cond
      (dc/connection? obj) :datahike
      (instance? clojure.lang.Atom obj) (let [obj-ref (deref obj)]
                                          (database-backend obj-ref))
      :else (dispatch-obj obj))))

(defn- dispatch-map [query & args]
  (let [found-backends (filter
                        identity
                        (flatten
                         (conj
                          (map database-backend args)
                          (map database-backend (:args query)))))]
    (if (and (seq found-backends)
             (apply = found-backends))
      (first found-backends)
      nil)))

(defmulti db->cache-key database-backend)
(defmethod db->cache-key :datomic [db]
  (datomic-db->cache-key db))
(defmethod db->cache-key :datahike [db]
  (datahike-arg->cache-key db))

(defmulti q dispatch-map)
(defmethod q :datahike [query & args]
  (apply datahike/q query args))
(defmethod q :datomic [query & args]
  (apply datomic/q query args))

(defn datoms [db arg-map]
  (case (database-backend db)
    :datomic (datomic/datoms db arg-map)
    :datahike (datahike/datoms db arg-map)))

(defmulti transact (fn [conn _arg-map] (database-backend conn)))
(defmethod transact :datahike [conn arg-map]
  (datahike/transact conn arg-map))
(defmethod transact :datomic [conn arg-map]
  (datomic/transact conn arg-map))

(defmulti pull (fn [db & _args] (database-backend db)))
(defmethod pull :datahike
  ([db arg-map] (datahike/pull db arg-map))
  ([db selector eid] (datahike/pull db selector eid)))
(defmethod pull :datomic
  ([db arg-map] (datomic/pull db arg-map))
  ([db selector eid] (datomic/pull db selector eid)))

(defmulti as-of (fn [db _tx-point] (database-backend db)))
(defmethod as-of :datahike [db tx-point]
  (datahike/as-of db tx-point))
(defmethod as-of :datomic [db tx-point]
  (datomic/as-of db tx-point))

(defmulti db database-backend)
(defmethod db :datahike [conn]
  (datahike/db conn))
(defmethod db :datomic [conn]
  (datomic/db conn))

(defmulti history database-backend)
(defmethod history :datahike [db]
  (datahike/history db))
(defmethod history :datomic [db]
  (datomic/history db))

(defmulti connect database-backend)
(defmethod connect :datahike [cfg]
  (log/info "Connecting to Datahike")
  (let [conn (datahike/connect (:cfg (c/get-backend cfg)))]
    (log/info "Got connection")
    conn))
(defmethod connect :datomic [cfg]
  (log/info "Connecting to Datomic")
  (let [client (impl/get-datomic-client cfg)
        conn (impl/retry (fn retry-connect []
                           (datomic/connect client (:cfg (c/get-backend cfg)))))]
    (log/info "Got connection")
    conn))

(defmulti release database-backend)
(defmethod release :datahike [cfg]
  (when-let [datahike-cfg (:cfg (c/get-backend cfg))]
    (datahike/release datahike-cfg)))
(defmethod release :datomic
  [_cfg] (log/info "No release needed for datomic ..."))

(def create-database impl/create-database)

(defn delete-database [cfg]
  (if (c/->can-delete? cfg)
    (let [backend-cfg (c/->backend-cfg cfg)]
      (condp = (c/->backend-type cfg)
        :datomic (datomic/delete-database (impl/get-datomic-client cfg) backend-cfg)
        :datahike (datahike/delete-database backend-cfg)
        (throw (ex-info "Unknown database backend" {:backend (c/get-backend cfg)})))
      (log/info "`delete-database` successful" (c/->backend-name cfg))
      true)
    (throw (ex-info "`delete-database` was called with `:allow-delete` false"
                    {:backend (c/get-backend cfg)}))))

(def get-database-time-point-by-version-query
  '[:find ?tx
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn get-transaction-id-from-version [db version]
  (ffirst (q get-database-time-point-by-version-query db version)))

(defn get-version+tx-inst
  "Get a vector of maps with version and corresponding transaction instances. Sorted with highest version number first."
  [db]
  (reverse
   (sort-by :version
            (q '[:find ?version ?tx-inst
                 :keys version tx-inst
                 :in $
                 :where
                 [_ :taxonomy-version/id ?version ?tx]
                 [?tx :db/txInstant ?tx-inst]] db))))

(defn get-version-tx-inst
  "Get the transaction instance for any given version. Default to most recent if the requested is not available, same as :last. :next gives nil."
  [db version]
  (if (= :next version)
    nil
    (let [v+tx (get-version+tx-inst db)
          default (first v+tx)
          v+tx (first (filter #(= version (:version %)) v+tx))]
      (:tx-inst
       (or v+tx default)))))

(def get-latest-released-version-query
  '[:find (max ?version)
    :in $
    :where [_ :taxonomy-version/id ?version]])

(defn get-latest-released-version [db]
  (ffirst (q get-latest-released-version-query db)))



(defn- can-connect? [cfg]
  (try
    (let [conn (connect cfg)]
      (release conn)
      true)
    (catch Exception _e
      (log/warn "Existing database not found")
      false)))

(defn init-db [cfg]
  {:pre [(c/valid-config? cfg)]
   :post [(= cfg %)]}
  (let [backend-cfg (c/get-backend cfg)]
    (cond
      ;; Database already exist.
      (and (impl/db-exists? backend-cfg)
           (can-connect? cfg)) (log/info "Database exists")

      (:init-db backend-cfg) (impl/load-from-init-db cfg)

      :else (if (create-database cfg)
              (log/info "Created database")
              (throw (ex-info "Failed to create database"
                              {:cfg cfg}))))
    cfg))

;; This cannot use the conn var, as it will destroy the
;; integration tests (where the conn is made before the
;; tests fill the databases with test data).
(defn get-db
  "Get database, possibly at a point in time of some release.
  Version arg can be:
  - `:next` - the latest database, includes unpublished changes
  - `:latest` - the database at a latest release time point
  - an int - the database at a time point of that release"
  [cfg version]
  (log/info "Connecting to database")
  (let [db (db (connect cfg))]
    (cond
      (= :next version)
      db

      (= :latest version)
      (->> (or (get-latest-released-version db)
               (throw (ex-info "There are no releases yet"
                               {:db db})))
           (get-transaction-id-from-version db)
           (as-of db))

      (int? version)
      (as-of db (or (get-transaction-id-from-version db version)
                    (throw (ex-info "There is no such db version"
                                    {:type :no-such-db-version
                                     :version version
                                     :db db}))))

      :else
      (throw (ex-info "Invalid db version, use :latest, :next or int"
                      {:version version})))))

(defn read-raw
  ([cfg] (read-raw cfg {}))
  ([cfg {:keys [format] :or {format :edn} :as opts}]

   ;; Version has been deprecated!
   {:pre [(not (contains? opts :version))]}
   
   (-> cfg
       c/->backend-cfg
       (impl/wanderung-config-from-db-cfg (c/->backend-type cfg))
       (wanderung/migrate {:wanderung/type format :sink identity}))))

(comment
  (def cfg {:database-backend :datomic
            :datomic {:cfg {:db-name "just-another-db-trying-to-make-it-in-the-world"
                            :server-type :ion
                            :region "eu-central-1"
                            :system "tax-test-v4"
                            :endpoint "https://hcvc9p1nvi.execute-api.eu-central-1.amazonaws.com/"}}})

  (create-database cfg)
  (delete-database cfg)

  (def conn (connect cfg))
  (transact conn [{:db/ident :name :db/valueType :db.type/string
                   :db/cardinality :db.cardinality/one}
                  {:db/ident :age :db/valueType :db.type/long
                   :db/cardinality :db.cardinality/one}])
  'bye)

(comment
  (require '[user])
  (def cfg (deref user/dev-cfg))
  (def conn (connect cfg))
  #_{:clj-kondo/ignore [:clojure-lsp/unused-public-var]}
  (def data
    (->> (q '[:find ?e ?name ?swe ?eng
              :in $
              :where
              [?e :concept-type/id ?name]
              [?e :concept-type/label-sv ?swe]
              [?e :concept-type/label-en ?eng]]

            (db conn))
         #_(group-by first)))

  (def get-all-taxonomy-types-query
    '[:find ?e ?v :where [?e :concept/type ?v]])

  (->> (q '[:find ?pn ?v :where
            [149533581528658 ?p ?v]
            [?p :db/ident ?pn]]
          (get-db cfg :latest)))

  (defn get-all-taxonomy-types "Return a list of taxonomy types." [cfg version]
    (->> (q get-all-taxonomy-types-query (get-db cfg version))
         (sort-by second)))
  (get-all-taxonomy-types cfg :latest)
  'bye)

(comment
  (def cfg {:database-backend :datahike
            :datahike {:cfg {}}})
  (def cfg @user/dev-cfg)
  (create-database cfg)
  (def conn (connect cfg))
  (def datcli (impl/get-datomic-client cfg))
  (->> (datomic/list-databases datcli {})
       #_(clojure.pprint/pprint))
  (transact
   conn
   [{:db/ident :name :db/valueType :db.type/string
     :db/cardinality :db.cardinality/one}
    {:db/ident :age :db/valueType :db.type/long
     :db/cardinality :db.cardinality/one}])
  (transact conn
            [{:name "bob" :age 42} {:name "linda" :age 47}])
  (def mq
    {:query '[:find ?name
              :in [?age] $
              :where
              [?e :name ?name]
              [?e :age ?age]]
     :args [[47] (db conn)]})

  (q mq)

  (q '[:find ?name
       :in [?age] $
       :where
       [?e :name ?name]
       [?e :age ?age]] [47] (db conn))

  'bye)

(def private-version-set #{:next})

(def private-version? private-version-set)

(defn datom->e [x]
  (cond
    (instance? datomic.core.db.Datum x) (.e ^datomic.core.db.Datum x)
    (instance? datahike.datom.Datom x) (.-e ^datahike.datom.Datom x)
    :else (throw (AssertionError. "Unknown datom type"))))

(defn datom->a [x]
  (cond
    (instance? datomic.core.db.Datum x) (.a ^datomic.core.db.Datum x)
    (instance? datahike.datom.Datom x) (.-a ^datahike.datom.Datom x)
    :else (throw (AssertionError. "Unknown datom type"))))

(defn datom->v [x]
  (cond
    (instance? datomic.core.db.Datum x) (.v ^datomic.core.db.Datum x)
    (instance? datahike.datom.Datom x) (.-v ^datahike.datom.Datom x)
    :else (throw (AssertionError. "Unknown datom type"))))

(defn datom->tx [x]
  (cond
    (instance? datomic.core.db.Datum x) (.tx ^datomic.core.db.Datum x)
    (instance? datahike.datom.Datom x) (datahike-datom/datom-tx
                                        ^datahike.datom.Datom x)
    :else (throw (AssertionError. "Unknown datom type"))))

(defn datom->added [x]
  (cond
    (instance? datomic.core.db.Datum x) (.added ^datomic.core.db.Datum x)
    (instance? datahike.datom.Datom x) (datahike-datom/datom-added
                                        ^datahike.datom.Datom x)
    :else (throw (AssertionError. "Unknown datom type"))))

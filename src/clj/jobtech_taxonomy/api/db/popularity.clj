(ns jobtech-taxonomy.api.db.popularity
  (:require [jobtech-taxonomy.common.io-utils :as iou]))

(def ^:private popularity-delay
  (delay
    (iou/read-json-resource [:map-of :string :int] "popularity/freq_data.json")))

(defn fetch []
  @popularity-delay)

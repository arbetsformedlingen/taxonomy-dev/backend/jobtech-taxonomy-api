(ns jobtech-taxonomy.api.db.eures
  (:require
   [clojure.spec.alpha :as sp]
   [jobtech-taxonomy.api.cache :as cache]
   [jobtech-taxonomy.api.db.database-connection :as db]
   [jobtech-taxonomy.common.relation :as relation]
   [jobtech-taxonomy.common.taxonomy :as types]
   [ring.util.http-response :as http]))

(defn get-occupation-exact-esco-match [db]
  (db/q
   '[:find (pull ?oc [:concept/id
                      :concept/type
                      :concept/preferred-label])
     (pull ?esco [:concept/id
                  :concept/type
                  :concept/preferred-label
                  :concept.external-standard/esco-uri])
     :in $ %
     :where
     [?oc :concept/type "occupation-name"]
     (edge ?oc "exact-match" ?esco ?r1)]
   db
   relation/rules))

(defn get-occupation-broad-esco-match [db]
  (db/q
   '[:find (pull ?oc [:concept/id
                      :concept/type
                      :concept/preferred-label])
     (pull ?esco [:concept/id
                  :concept/type
                  :concept/preferred-label
                  :concept.external-standard/esco-uri])
     :in $ %
     :where
     [?oc :concept/type "occupation-name"]
     (edge ?oc "broad-match" ?esco ?r1)]
   db
   relation/rules))

(defn get-occupation-close-match [db]
  (db/q
   '[:find (pull ?oc [:concept/id
                      :concept/type
                      :concept/preferred-label])
     (pull ?esco [:concept/id
                  :concept/type
                  :concept/preferred-label
                  :concept.external-standard/esco-uri])
     :in $ %
     :where
     [?oc :concept/type "occupation-name"]
     (edge ?oc "close-match" ?esco ?r1)]
   db
   relation/rules))

(defn get-occupation-narrow-match [db]
  (db/q
   '[:find (pull ?oc [:concept/id
                      :concept/type
                      :concept/preferred-label])
     (pull ?esco [:concept/id
                  :concept/type
                  :concept/preferred-label
                  :concept.external-standard/esco-uri])
     :in $ %
     :where
     [?oc :concept/type "occupation-name"]

     (edge ?oc "narrow-match" ?esco ?r1)]
   db
   relation/rules))

(defn get-occupation-isco [db]
  (db/q
   '[:find (pull ?oc [:concept/id
                      :concept/type
                      :concept/preferred-label])
     (pull ?esco [:concept/id
                  :concept/type
                  :concept/preferred-label
                  :concept.external-standard/esco-uri])
     :in $ %
     :where
     [?oc :concept/type "occupation-name"]
     (edge ?oc "broader" ?esco ?r1)
     [?esco :concept/type "isco-level-4"]]
   db
   relation/rules))

(defn build-concept-relation-map [accumulation-map type-key concept-relations]
  (reduce
   (fn [acc e]
     (update-in acc
                [(:concept/id (first e)) type-key]
                conj
                (:concept.external-standard/esco-uri (second e))))
   accumulation-map
   concept-relations))

(defn build-concept-relation-map-full [db]
  (-> {}

      (build-concept-relation-map :broad-match (get-occupation-broad-esco-match db)) (build-concept-relation-map :narrow-match (get-occupation-narrow-match db))
      (build-concept-relation-map :close-match (get-occupation-close-match db))
      (build-concept-relation-map :exact-match (get-occupation-exact-esco-match db))
      (build-concept-relation-map :isco (get-occupation-isco db))))

(defn convert-concept-to-esco-ids-2 [concept-with-relations]
  (let [relations (second concept-with-relations)
        broad-match (:broad-match relations)
        close-match (:close-match relations)
        exact-match (:exact-match relations)
        narrow-match (:narrow-match relations)
        isco-match (:isco relations)

        broad-match-count (count broad-match)
        close-match-count (count close-match)
        exact-match-count (count exact-match)
        narrow-match-count (count narrow-match)

        total-match-count (+ broad-match-count close-match-count exact-match-count narrow-match-count)
        taxonomy-concept (first concept-with-relations)

        eures-concepts (if (< 1 total-match-count)
                         (if (#{1 2 3} exact-match-count)
                           exact-match
                           (if (< 3 total-match-count)
                             isco-match
                             (if (not-empty narrow-match)
                               isco-match
                               (concat broad-match close-match exact-match narrow-match))))
                         (concat broad-match close-match exact-match narrow-match))]
    (if (empty? eures-concepts)
      [taxonomy-concept isco-match]
      [taxonomy-concept eures-concepts])))

;; "S8DR_6EK_m92"

(defn occupation-esco-id-lookup [db]
  (into {} (map convert-concept-to-esco-ids-2 (build-concept-relation-map-full db))))

(def taxonomy->eures-map-cache
  (cache/make occupation-esco-id-lookup
              :key-fn db/db->cache-key
              :maximum-size 1))

(defn lookup-occupation-name-id-to-esco-ids [cfg concept-id]
  (let [db (db/get-db cfg :latest)]
    (get (taxonomy->eures-map-cache db) concept-id)))

(defn handler [cfg {{{:keys [id]} :query} :parameters}]
  (if-let [response (lookup-occupation-name-id-to-esco-ids cfg id)]
    (http/ok response)
    (http/not-found {:taxonomy/error "ID not found." :taxonomy/info {:id id}})))

(def responses
  (merge (types/response200 (sp/coll-of string?)) types/response404))

(comment
  (require '[user])
  (time (handler @user/dev-cfg {:parameters {:query {:id "mzjA_bXv_Rkp"}}})))

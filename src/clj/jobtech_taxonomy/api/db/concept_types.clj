(ns jobtech-taxonomy.api.db.concept-types
  (:require [jobtech-taxonomy.api.db.api-util :as api-util]
            [jobtech-taxonomy.api.db.database-connection :as db]))

(defn assert-type [cfg {:keys [concept-type label-sv label-en user-id tx-inst]}]
  (db/transact (db/connect cfg)
               {:tx-data (-> [(api-util/user-action-tx user-id nil)
                              (cond-> {:concept-type/id concept-type}
                                label-sv
                                (assoc :concept-type/label-sv label-sv)
                                label-en
                                (assoc :concept-type/label-en label-en))]
                             (api-util/add-tx-instant-if-present tx-inst))})
  {:success true})

(defn type-exists? [cfg id version]
  (seq (db/q '[:find ?x
               :in $ ?id
               :where [?x :concept-type/id ?id]]
             (db/get-db cfg version)
             id)))

(defn get-all-types
  "Return a list of concept types referred to by concept-types using :concept-type/id.

Also see `base.jobtech-taxonomy.api.concept-types-test` for a test related to this."
  [cfg version]
  (let [type-query '[:find ?v :where [_ :concept-type/id ?v]]]
    (sort (map (comp name first) (db/q type-query (db/get-db cfg version))))))

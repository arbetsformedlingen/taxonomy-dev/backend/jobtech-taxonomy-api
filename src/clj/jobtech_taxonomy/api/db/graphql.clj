(ns jobtech-taxonomy.api.db.graphql
  (:require [com.walmartlabs.lacinia :as l]
            [com.walmartlabs.lacinia.schema :as l.schema]
            [io.github.vlaaad.plusinia :as p]
            [jobtech-taxonomy.api.db.graphql.changelog :as graphql.changelog]
            [jobtech-taxonomy.api.db.graphql.concept :as graphql.concept]
            [jobtech-taxonomy.api.db.graphql.concept-type :as graphql.concept-type]
            [jobtech-taxonomy.api.db.graphql.impl :as graphql.impl]
            [jobtech-taxonomy.api.db.graphql.relation :as graphql.relation]
            [jobtech-taxonomy.api.db.graphql.total-count :as graphql.total-count]
            [jobtech-taxonomy.api.db.graphql.version :as graphql.version]))

(defn schema [cfg]
  (l.schema/compile
   (p/wrap-schema
    (graphql.impl/deep-merge
     graphql.impl/schema
     graphql.concept/schema
     graphql.relation/schema
     graphql.version/schema
     graphql.concept-type/schema
     graphql.changelog/schema
     graphql.total-count/schema)
    (graphql.impl/deep-merge
     (graphql.concept/fetchers cfg)
     (graphql.relation/fetchers cfg)
     (graphql.version/fetchers cfg)
     (graphql.concept-type/fetchers cfg)
     (graphql.changelog/fetchers cfg)
     (graphql.total-count/fetchers cfg)))))

(defn execute [cfg query variables operation-name api-key]
  (l/execute (schema cfg)
             query
             variables
             {:api-key api-key}
             {:operation-name operation-name}))
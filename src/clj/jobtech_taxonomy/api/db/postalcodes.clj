(ns jobtech-taxonomy.api.db.postalcodes
  (:require [jobtech-taxonomy.common.io-utils :as iou]
            [taoensso.timbre :as log]))

(def postal-code-schema
  [:vector
   [:map {:closed true}
    [:postal_code :string]
    [:locality_preferred_label :string]
    [:municipality_id :string]
    [:national_nuts_level_3_code_2019 :string]
    [:municipality_preferred_label :string]
    [:municipality_deprecated_legacy_id :string]
    [:lau_2_code_2015 :string]
    [:region_id :string]
    [:region_term :string]
    [:region_deprecated_legacy_id :string]]])

(def postal-codes
  (let [resource-file "postalcodes.edn"]
    (log/debug "Loading postal codes from" resource-file)
    (iou/read-edn-resource postal-code-schema resource-file)))

(defn fetch-postalcodes [] postal-codes)

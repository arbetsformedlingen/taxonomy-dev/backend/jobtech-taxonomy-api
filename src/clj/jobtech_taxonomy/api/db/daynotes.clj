(ns jobtech-taxonomy.api.db.daynotes
  (:refer-clojure :exclude [type])
  (:require
   [jobtech-taxonomy.api.db.api-util :as api-util]
   [jobtech-taxonomy.api.db.concepts :as concepts]
   [jobtech-taxonomy.api.db.database-connection :as db :refer [datom->e datom->a datom->v datom->tx datom->added]]
   [jobtech-taxonomy.common.sorted-long-array :as sla])
  (:import [java.util Date]
           [java.util HashSet HashMap ArrayList]))

(set! *warn-on-reflection* true)
(defn- hash-set-distinct
  "A transducer that filters distinct elements. Slightly faster than `clojure.core/distinct`."
  []
  (let [s (HashSet.)]
    (filter #(.add s %))))

(defn conj-in [m & path+vals]
  (->> path+vals
       (partition 2)
       (reduce #(update-in %1 (first %2) conj (second %2)) m)))

(def ^:private show-version-instance-ids-query
  '[:find ?tx ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn- get-tx-version-map [db]
  (into (sorted-map) (db/q show-version-instance-ids-query db)))

(defn- convert-transaction-id-to-version-id
  "Return the version that TRANSACTION-ID belongs to.
    If an unpublished transaction, return -1."
  [tx-id->version transaction-id]
  (or (second (first (subseq tx-id->version >= transaction-id)))
      -1))

(defn- build-fetch-relation-txs-query [hist-db concept-id
                                       from-version to-version]
  (-> {:query '{:find [?tx ?inst ?user-id ?comment
                       ?r ?substitutability_percentage]
                :keys [tx timestamp user-id comment
                       relation substitutability_percentage]
                :in [$]
                :where [[?c :concept/id ?input-concept-id]
                        (or [?r :relation/concept-1 ?c]
                            [?r :relation/concept-2 ?c])
                        (or [?r _ _ ?tx]
                            [?tx :daynote/ref ?r])
                        [(get-else $ ?r :relation/substitutability-percentage 0) ?substitutability_percentage]
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                        [(get-else $ ?tx :daynote/comment false) ?comment]
                        [?tx :db/txInstant ?inst]]}
       :args [hist-db]}
      (cond->
       concept-id
        (conj-in [:query :in] '?input-concept-id
                 [:args] concept-id)

        (inst? from-version)
        (conj-in [:query :in] '?from-version
                 [:query :where] '[(<= ?from-version ?inst)]
                 [:args] from-version)

        (int? from-version)
        (conj-in [:query :in] '?from-version
                 [:query :where] '[_ :taxonomy-version/id ?from-version ?from-tx]
                 [:query :where] '[?from-tx :db/txInstant ?from-inst]
                 [:query :where] '[(<= ?from-inst ?inst)]
                 [:args] from-version)

        (inst? to-version)
        (conj-in [:query :in] '?to-version
                 [:query :where] '[(< ?inst ?to-version)]
                 [:args] to-version)

        (int? to-version)
        (conj-in [:query :in] '?to-version
                 [:query :where] '[_ :taxonomy-version/id ?to-version ?to-tx]
                 [:query :where] '[?to-tx :db/txInstant ?to-inst]
                 [:query :where] '[(< ?inst ?to-inst)]
                 [:args] to-version))))

(defn- fetch-relation-txs [hist-db concept-id from-timestamp to-timestamp]
  (-> (build-fetch-relation-txs-query hist-db concept-id from-timestamp to-timestamp)
      db/q))

(defn- build-relation-changes-query
  [hist-db db tx+relations]
  {:query '[:find ?tx ?r ?t ?added
            (pull $latest ?c1 [:concept/id
                               :concept/type
                               :concept/preferred-label])
            (pull $latest ?c2 [:concept/id
                               :concept/type
                               :concept/preferred-label])
            :keys tx relation relation-type added source target
            :in $history $latest [[?tx ?r] ...]
            :where
            [$history ?r :relation/type ?t ?tx ?added]
            [$history ?r :relation/concept-1 ?c1]
            [$history ?r :relation/concept-2 ?c2]]
   :args [hist-db db tx+relations]})

(defn- fetch-relation-changes [hist-db db tx+relations]
  (group-by (juxt :tx :relation)
            (db/q (build-relation-changes-query
                   hist-db db tx+relations))))

(defn- build-relation-events [db
                              tx+relation->changes
                              tx-id->version
                              show-unpublished
                              relation+txs]
  (reduce
   (fn [acc {:keys [tx timestamp user-id comment
                    relation substitutability_percentage]}]
     (let [change (first (tx+relation->changes [tx relation]))
           state (or (not-empty (select-keys change [:relation-type
                                                     :source
                                                     :target]))

                     ;; Issue #784: Under what conditions will this
                     ;; code run?
                     (get-in acc [:relation->latest-state relation])

                     ;; Issue #784: Under what conditions will this
                     ;; code run?
                     (db/pull (db/as-of db tx)
                              '[(:relation/type :as :relation-type)
                                {(:relation/concept-1 :as :source)
                                 [:concept/id :concept/type
                                  :concept/preferred-label]
                                 (:relation/concept-2 :as :target)
                                 [:concept/id :concept/type
                                  :concept/preferred-label]}]
                              relation))
           event (cond-> {:event-type (cond
                                        (nil? change) "COMMENTED"
                                        (:added change) "CREATED"
                                        :else "DEPRECATED")
                          :timestamp timestamp
                          :user-id user-id
                          :relation state
                          :version (convert-transaction-id-to-version-id
                                    tx-id->version tx)}
                   comment (assoc :comment comment)
                   substitutability_percentage
                   (assoc :substitutability-percentage substitutability_percentage)
                   show-unpublished (assoc :transaction-id tx))]
       (-> acc
           (update :events conj event)
           (assoc-in [:relation->latest-state relation] state))))
   {:relation->latest-state {}
    :events []}
   relation+txs))

(defn get-for-relation [db concept-id from-timestamp
                        to-timestamp offset limit show-unpublished]
  (let [; fetch all pairs [tx version] where tx is related
        ;; to the version

        tx-id->version (get-tx-version-map db)
                                        ; get a db with all changes
        hist-db (db/history db)

        relation+txs (fetch-relation-txs hist-db
                                         concept-id
                                         from-timestamp
                                         to-timestamp)
        tx+relation->changes (fetch-relation-changes
                              hist-db
                              db
                              (map (juxt :tx :relation) relation+txs))]
    (->> relation+txs
         (sort-by :tx)
         (filter #(or show-unpublished
                      (not= (convert-transaction-id-to-version-id
                             tx-id->version (:tx %)) -1)))
         (api-util/pagination offset limit)
         (build-relation-events db
                                tx+relation->changes
                                tx-id->version
                                show-unpublished)
         :events)))

(defn create-for-relation [cfg user-id comment concept-1 concept-2 relation-type]
  (when-let [e (concepts/fetch-relation-entity-id-from-concept-ids-and-relation-type cfg concept-1 concept-2 relation-type)]
    (db/transact (db/connect cfg) {:tx-data [(api-util/user-action-tx user-id comment)
                                             {:db/id "datomic.tx"
                                              :daynote/ref e}]})))

(defn- changes->deprecated-event [changes]
  (when (some #(and (:added %)
                    (let [a (:a %)]
                      (or (= :concept/deprecated a)
                          (= :concept/replaced-by a)))
                    (:v %))
              changes)
    {:event-type "DEPRECATED"}))

(defn- changes->commented-event [changes]
  (when (empty? changes)
    {:event-type "COMMENTED"}))

(defn- changes-to-new-concept [changes]
  (reduce
   (fn [acc {attr :a val :v}]
     (case attr
       :concept/alternative-labels (update acc attr conj val)
       :concept/hidden-labels (update acc attr conj val)
       (assoc acc attr val)))
   {}
   changes))

(defn- changes->created-event [changes]
  (when (some #(and (:added %) (= :concept/id (:a %))) changes)
    {:event-type "CREATED"
     :new-concept (changes-to-new-concept changes)}))

(defn- get-concept-id [db eid]
  (ffirst (db/q {:query '[:find ?cid
                          :in $ ?eid
                          :where
                          [?eid :concept/id ?cid]]
                 :args [db eid]})))

(defn- changes->updated-event [changes db]
  {:event-type "UPDATED"
   :concept-attribute-changes
   (->> changes
        (group-by :a)
        (sort-by key)
        (map (fn [[attr changes]]
               (let [added (->> changes (filter :added) (map :v))
                     removed (->> changes (remove :added) (map :v))
                     old-value (first removed)
                     new-value (first added)]
                 (if (= attr :concept/replaced-by)

                   ;; Issue #784: This code might never be
                   ;; reached because it is handled by
                   ;; `changes->deprecated-event`
                   {:attribute (name attr)
                    :old-value (some->> old-value (get-concept-id db))
                    :new-value (some->> new-value (get-concept-id db))
                    :removed (map #(get-concept-id db %) removed)
                    :added (map #(get-concept-id db %) added)}

                   {:attribute (name attr)
                    :old-value old-value
                    :new-value new-value
                    :removed removed
                    :added added})))))})

(defn- changes->event [changes db]
  (or (changes->commented-event changes)
      (changes->deprecated-event changes)
      (changes->created-event changes)
      (changes->updated-event changes db)))

(defn- transaction-data
  "Retrieves data about the transactions filtered using `from-timestamp` and `to-timestamp`."
  [hist-db from-timestamp to-timestamp]
  (let [version->inst (fn [version]
                        (when-let [tx-id (-> hist-db
                                             (db/datoms
                                              {:index :avet
                                               :limit 1
                                               :components [:taxonomy-version/id version]})
                                             first
                                             :tx)]
                          (-> hist-db
                              (db/datoms
                               {:index :eavt
                                :limit 1
                                :components [tx-id :db/txInstant]})
                              first
                              :v)))
        normalize-inst (fn [x] (cond
                                 (inst? x) x
                                 (int? x) (version->inst x)
                                 :else nil))
        xform-from-inst (fn [cmp x]
                          (if x
                            (filter (fn [datom] (cmp (.getTime ^Date (:v datom))
                                                     (.getTime ^Date x))))
                            identity))
        tx-instant-datoms (->> {:index :aevt
                                :limit -1
                                :components [:db/txInstant]}
                               (db/datoms hist-db)
                               (into [] (comp (xform-from-inst >= (normalize-inst from-timestamp))
                                              (xform-from-inst < (normalize-inst to-timestamp))))
                               (sort-by #(.getTime ^Date (datom->v %)))
                               object-array)]
    {:tx-instant-datoms tx-instant-datoms
     :tx-id-long-array (sla/distinct-sorted-long-array (into #{} (map :tx) tx-instant-datoms))}))

(defn- one-field [k s]
  {:init nil
   :acc (fn [new-value] new-value)
   :assoc (fn [dst] `(assoc! ~dst ~k ~s))})

(defn- many-field [k s]
  {:init `(transient [])
   :acc (fn [new-value] `(conj! ~s ~new-value))
   :assoc (fn [dst]
            `(let [dst# ~dst
                   ~s (persistent! ~s)]
               (if (seq ~s)
                 (assoc! dst# ~k ~s)
                 dst#)))})

(defmacro concept-from-datoms [src-datoms
                               attrib-ident-map
                               & field-spec]
  {:pre [(symbol? attrib-ident-map)]}
  (let [fields (mapv (fn [[field-type k]]
                       (let [s (gensym)]
                         (merge
                          {:key k :symbol s}
                          ((case field-type
                             :one one-field
                             :many many-field) k s))))
                     field-spec)
        datoms (gensym)
        datom (gensym)]
    `(loop [~datoms (seq ~src-datoms)
            ~@(mapcat (juxt :symbol :init) fields)]
       (if ~datoms
         (let [~datom (first ~datoms)
               ~datoms (next ~datoms)]
           (case (-> ~datom datom->a ~attrib-ident-map)
             ~@(mapcat
                (fn [f]
                  (let [k (:key f)]
                    [k `(recur
                         ~datoms
                         ~@(for [{:keys [key acc symbol]} fields]
                             (if (= k key)
                               (acc `(datom->v ~datom))
                               symbol)))]))
                fields)
             (recur ~datoms ~@(map :symbol fields))))
         (persistent!
          ~(reduce (fn [dst f] ((:assoc f) dst))
                   `(transient {})
                   fields))))))

(defn- fetch-concept
  "Fetch data about a concept with a certain entity id. Internally, it iterates over all datoms of that entity and filters out the ones that it should keep. This has been observed to be faster than a pull query."
  [db attrib-ident-map entity-id]
  (concept-from-datoms
   (db/datoms db {:index :eavt
                  :limit -1
                  :components [entity-id]})
   attrib-ident-map
   [:one :concept/id]
   [:one :concept/type]
   [:one :concept/definition]
   [:one :concept/preferred-label]
   [:many :concept/alternative-labels]
   [:many :concept/hidden-labels]))

(defn- push-daynote-ref-events
  "Push all events from `daynote-ref-event-map` onto `result` using the function `step`."
  [result step daynote-ref-event-map]
  (reduce (fn [dst [_ v]]
            (step dst v))
          result
          (sort-by first daynote-ref-event-map)))

(defn- build-transaction-datom-groups
  "Returns an object-array where every element is an ArrayList of datoms, sorted by entity id,
corresponding to a transaction id in `tx-id-long-array`. Only include datoms with entity ids that 
exist in `concept-eid-array`. Both `tx-id-long-array` and `concept-eid-array` are primitive arrays
of sorted longs. And `src-datoms` *must* be sorted by entity id."
  [^longs tx-id-long-array ^longs concept-eid-array src-datoms]
  (let [STATEFUL-concept-eid-predicate (sla/stateful-predicate concept-eid-array)
        n (alength tx-id-long-array)
        tx-datoms (object-array n)]
    (dotimes [i n]
      (aset tx-datoms i (ArrayList.)))
    (doseq [x src-datoms
            :when (STATEFUL-concept-eid-predicate (datom->e x))
            :let [tx (datom->tx x)
                  index (sla/binary-search tx-id-long-array tx)]
            :when index
            :let [^ArrayList acc (aget tx-datoms index)]]
      (.add acc x))
    tx-datoms))

(defn- get-concept-entity-id-set
  "Returns the set of all concept entity ids that will potentially be included."
  [hist-db selected-concept-id]
  (let [concept-eids (HashSet.)]
    (doseq [datom (db/datoms hist-db
                             {:index :avet
                              :limit -1
                              :components (if selected-concept-id
                                            [:concept/id selected-concept-id]
                                            [:concept/id])})]
      (.add concept-eids (datom->e datom)))
    concept-eids))

(defn- get-attrib-ident-map
  "Construct a map from entity id to ident"
  [hist-db]
  (sla/lookup-fn
   (into {}
         (map (juxt datom->e datom->v))
         (db/datoms hist-db
                    {:index :aevt
                     :limit -1
                     :components [:db/ident]}))))

(defn- datom->change [attrib-ident-map datom]
  (let [e (datom->e datom)
        a (datom->a datom)
        v (datom->v datom)
        tx (datom->tx datom)
        added (datom->added datom)
        attrib (attrib-ident-map a a)]
    (assert (keyword? attrib))
    {:concept e
     :a attrib
     :v v
     :added added
     :tx tx}))

(defn- get-entity-attrib-value [hist-db eid attrib default]
  (if-let [datom (first (db/datoms
                         hist-db
                         {:index :aevt
                          :limit 1
                          :components [attrib eid]}))]
    (datom->v datom)
    default))

(defn- make-latest-concept-lookup [db attrib-ident-map concept-eid-array]
  (sla/memoize (fn latest-concept [entity-id]
                 (fetch-concept db attrib-ident-map entity-id))
               concept-eid-array))

(defn- get-datoms-of-interest [hist-db concept-eids]
  (db/datoms
   hist-db
   {:index :eavt
    :limit -1
    :components (if (= 1 (count concept-eids))
                  [(first concept-eids)]
                  [])}))

(defn- push-events-for-transaction
  [result ^ArrayList datoms-sorted-by-e push-event
   ^HashMap daynote-ref-event-map
   attrib-ident-map make-event]
  (if (empty? datoms-sorted-by-e)
    (push-daynote-ref-events result push-event daynote-ref-event-map)
    (let [local-push (fn [result eid changes]
                       (.remove daynote-ref-event-map eid)
                       (push-event result (make-event changes eid)))
          datom-iter (.iterator datoms-sorted-by-e)
          datom0 (.next datom-iter)]
      (loop [changes (transient [(datom->change attrib-ident-map datom0)])
             last-eid (datom->e datom0)
             result result]
        (if (.hasNext datom-iter)
          (let [datom (.next datom-iter)
                eid (datom->e datom)]
            (assert (<= last-eid eid))
            (if (= eid last-eid)
              (recur (conj! changes (datom->change attrib-ident-map datom))
                     last-eid
                     result)
              (recur (transient [(datom->change attrib-ident-map datom)])
                     eid
                     (local-push result last-eid changes))))
          (-> result
              (local-push last-eid changes)
              (push-daynote-ref-events push-event daynote-ref-event-map)))))))

(def commented-event-prototype (changes->commented-event []))

(defn- get-initial-commented-events [hist-db concept-eid-array tx-id decorate-event]
  (let [event-map ^HashMap (HashMap.)
        daynote-ref-datoms (db/datoms
                            hist-db
                            {:index :eavt
                             :limit -1
                             :components
                             [tx-id :daynote/ref]})]
    (doseq [datom daynote-ref-datoms
            :let [e (datom->v datom)]
            :when (sla/contains? concept-eid-array e)]
      (.put event-map e (decorate-event commented-event-prototype e)))
    event-map))

(defn- complete-result! [result]
  (if (reduced? result)
    (persistent! (deref result))
    (persistent! result)))

(defn get-for-concept [db concept-id from-timestamp to-timestamp offset
                       limit show-unpublished]
  (let [hist-db (db/history db)
        tx-id->version (get-tx-version-map db)
        concept-eids (get-concept-entity-id-set hist-db concept-id)
        concept-eid-array (sla/distinct-sorted-long-array concept-eids)
        attrib-ident-map (get-attrib-ident-map hist-db)
        tdata (transaction-data hist-db from-timestamp to-timestamp)
        tx-id-long-array (:tx-id-long-array tdata)
        ^objects tx-instant-datoms (:tx-instant-datoms tdata)
        datoms-of-interest (get-datoms-of-interest hist-db concept-eids)
        ^objects transaction-datom-groups (build-transaction-datom-groups
                                           tx-id-long-array
                                           concept-eid-array
                                           datoms-of-interest)
        get-latest-concept (make-latest-concept-lookup db attrib-ident-map
                                                       concept-eid-array)
        push-event ((api-util/pagination offset limit) conj!)
        tx-count (alength transaction-datom-groups)]
    (assert (= tx-count (count tx-instant-datoms)))
    (loop [tx-index 0
           result (transient [])]
      (if (<= tx-count tx-index)
        (complete-result! result)
        (let [tdatom (aget tx-instant-datoms tx-index)
              tx-id (datom->e tdatom)
              inst (datom->v tdatom)
              datoms-sorted-by-e (aget transaction-datom-groups tx-index)
              version (convert-transaction-id-to-version-id tx-id->version tx-id)]
          (if (or show-unpublished (not= -1 version))
            (let [comment (get-entity-attrib-value hist-db tx-id
                                                   :daynote/comment nil)
                  user (get-entity-attrib-value hist-db tx-id
                                                :taxonomy-user/id "@system")
                  decorate-event (fn decorate-event [event entity-id]
                                   (-> event
                                       (assoc
                                        :user-id user
                                        :timestamp inst
                                        :version version
                                        :latest-version-of-concept
                                        (get-latest-concept entity-id))
                                       (cond-> comment (assoc :comment comment)
                                               show-unpublished
                                               (assoc :transaction-id tx-id))))
                  make-event (fn make-event [tr-changes entity-id]
                               (let [changes (into []
                                                   (hash-set-distinct)
                                                   (persistent! tr-changes))]
                                 (-> changes
                                     (changes->event db)
                                     (decorate-event entity-id))))
                  daynote-ref-event-map (get-initial-commented-events
                                         hist-db concept-eid-array tx-id
                                         decorate-event)
                  result (push-events-for-transaction result
                                                      datoms-sorted-by-e
                                                      push-event
                                                      daynote-ref-event-map
                                                      attrib-ident-map
                                                      make-event)]
              (recur (inc tx-index) result))
            (recur (inc tx-index) result)))))))

(defn create-for-concept [cfg concept-id user-id comment]
  (db/transact (db/connect cfg)
               {:tx-data [(api-util/user-action-tx user-id comment)
                          {:db/id "datomic.tx"
                           :daynote/ref [:concept/id concept-id]}]}))

(defn- strip-public-concept-changes [event]
  (dissoc event :user-id :comment :timestamp))

(defn ^{:auth :editor} get-concept-changes-with-pagination-private [cfg after-version to-version-inclusive offset limit]
  (let [db (db/get-db cfg :next)]
    (map #(dissoc % :transaction-id) (get-for-concept db nil after-version to-version-inclusive offset limit true))))

(defn get-concept-changes-with-pagination-public [cfg after-version to-version-inclusive offset limit]
  (let [db (db/get-db cfg :next)
        events (get-for-concept db nil after-version to-version-inclusive offset limit false)]
    (map #(dissoc % :transaction-id) (map strip-public-concept-changes events))))

(defn ^{:auth :editor} get-relation-changes-with-pagination-private [cfg after-version to-version-inclusive offset limit]
  (let [db (db/get-db cfg :next)]
    (map #(dissoc % :transaction-id) (get-for-relation db nil after-version to-version-inclusive offset limit true))))

(defn get-relation-changes-with-pagination-public [cfg after-version to-version-inclusive offset limit]
  (let [db (db/get-db cfg :next)
        events (get-for-relation db nil after-version to-version-inclusive offset limit false)]
    (map #(dissoc % :transaction-id) (map strip-public-concept-changes events))))

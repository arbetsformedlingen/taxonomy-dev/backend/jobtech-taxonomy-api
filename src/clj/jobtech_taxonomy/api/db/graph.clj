(ns jobtech-taxonomy.api.db.graph
  (:refer-clojure :exclude [type])
  (:require [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.common.relation :as relation]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]))

(def concept-pull-pattern
  [:concept/id
   :concept/type
   :concept/preferred-label
   :concept/definition
   :concept/deprecated
   :concept/alternative-labels
   :concept/hidden-labels])

(def initial-graph-query
  {:query '{:find [(pull ?r [:relation/type
                             :relation/substitutability-percentage
                             {:relation/concept-1 [:concept/id]
                              :relation/concept-2 [:concept/id]}])
                   (pull ?c1 concept-pull-pattern)
                   (pull ?c2 concept-pull-pattern)]
            :in [concept-pull-pattern $ %]
            :where []}
   :args [concept-pull-pattern]})

(defn- handle-relations [query relation concept-1-type concept-2-type]
  (-> query
      (update-in [:query :in] into '[?relation-type ?c1-type ?c2-type])
      (update :args into [relation concept-1-type concept-2-type])
      (update-in [:query :where] into '[(edge ?c1 ?relation-type ?c2 ?r)
                                        [?c1 :concept/type ?c1-type]
                                        [?c2 :concept/type ?c2-type]])))

(defn- exclude-deprecated [query]
  (update-in query [:query :where] into '[[(get-else $ ?c1 :concept/deprecated false) ?c1-deprecated]
                                          [(ground false) ?c1-deprecated]
                                          [(get-else $ ?c2 :concept/deprecated false) ?c2-deprecated]
                                          [(ground false) ?c2-deprecated]]))

(defn- build-graph-query [cfg relation-type concept-1-type concept-2-type include-deprecated offset limit version]
  (-> initial-graph-query
      (update :args conj (db/get-db cfg version) relation/rules)
      (handle-relations relation-type concept-1-type concept-2-type)
      (cond-> (not include-deprecated) exclude-deprecated)
      (cond-> offset (assoc :offset offset))
      (cond-> limit (assoc :limit limit))))

(def initial-graph-response
  {:taxonomy/graph
   {:taxonomy/nodes #{}
    :taxonomy/edges #{}}})

(defn create-edge [{:relation/keys [concept-1 concept-2 type substitutability-percentage]}]
  (cond-> {:taxonomy/source (:concept/id concept-1)
           :taxonomy/target (:concept/id concept-2)
           :taxonomy/relation-type type}
    substitutability-percentage
    (assoc :taxonomy/substitutability-percentage substitutability-percentage)))

(defn db-graph-response-reducer [acc [relation-data concept-1 concept-2]]
  (-> acc
      (update-in [:taxonomy/graph :taxonomy/edges] conj (create-edge relation-data))
      (update-in [:taxonomy/graph :taxonomy/nodes] conj (taxonomy/map->nsmap concept-1))
      (update-in [:taxonomy/graph :taxonomy/nodes] conj (taxonomy/map->nsmap concept-2))))

(defn fetch-graph [cfg relation-type source-concept-type target-concept-type include-deprecated offset limit version]
  {:pre [(string? relation-type) (string? source-concept-type) (string? target-concept-type)]}
  (reduce db-graph-response-reducer initial-graph-response
          (db/q (build-graph-query cfg relation-type source-concept-type target-concept-type include-deprecated offset limit version))))

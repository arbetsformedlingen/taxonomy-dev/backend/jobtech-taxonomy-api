(ns jobtech-taxonomy.api.db.api-util)

(defn pagination
  ([offset limit]
   (comp (if offset
           (drop offset)
           identity)
         (if limit
           (take limit)
           identity)))
  ([offset limit coll]
   (cond->> coll
     offset (drop offset)
     limit (take limit))))

(def regex-char-esc-str-map
  (let [esc-chars "(){}[]&^%$#!?*.+"]
    (zipmap esc-chars
            (map #(str "\\" %) esc-chars))))

(defn ignore-case [string]
  (str "(?iu:" string ")"))

(defn ignore-case-starts-with [string]
  (str "(?iu:" string ".*)"))

(defn ignore-case-contains [string]
  (str "(?iu:.*" string ".*)"))

(defn str-to-pattern-ignore-case
  [string]
  (->> string
       (replace regex-char-esc-str-map)
       (reduce str)
       ignore-case))

(defn str-to-pattern-starts-with
  [string]
  (->> string
       (replace regex-char-esc-str-map)
       (reduce str)
       ignore-case-starts-with))

(defn str-to-pattern-contains
  [string]
  (->> string
       (replace regex-char-esc-str-map)
       (reduce str)
       ignore-case-contains))

(defn user-action-tx [user-id comment]
  (cond-> {:db/id "datomic.tx"
           :taxonomy-user/id user-id}
    comment (assoc :daynote/comment comment)))

(defn add-tx-instant-if-present [tx tx-inst]
  (if tx-inst
    (conj tx {:db/id "datomic.tx" :db/txInstant tx-inst})
    tx))

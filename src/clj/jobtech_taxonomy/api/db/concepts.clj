(ns jobtech-taxonomy.api.db.concepts
  (:refer-clojure :exclude [type])
  (:require
   [clojure.pprint :as pp]
   [clojure.set :as set]
   [clojure.string :as s]
   [jobtech-taxonomy.api.db.api-util :as api-util]
   [jobtech-taxonomy.api.db.concept-types :refer [type-exists?]]
   [jobtech-taxonomy.api.db.database-connection :as db]
   [jobtech-taxonomy.api.db.nano-id :as nano-id]
   [jobtech-taxonomy.api.routes.parameter-util :as pu]
   [jobtech-taxonomy.common.relation :as relation]
   [jobtech-taxonomy.common.taxonomy :as types]
   [taoensso.timbre :as log]))

;; To understand the idea behind the following code read this blog post
;; https://grishaev.me/en/datomic-query/

(defn- sort-reply
  "Sort a collection of maps according to key :concept/sort-order"
  [reply]
  (if (contains? (first reply) :concept/sort-order)
    (sort-by :concept/sort-order reply)
    reply))

(def ^:private concept-pull-pattern
  [:concept/id
   :concept/type
   :concept/definition
   :concept/preferred-label
   :concept/deprecated
   :concept/no-esco-relation
   :concept/quality-level
   :concept/sort-order
   :concept/alternative-labels
   :concept/hidden-labels
   {:concept/replaced-by [:concept/id
                          :concept/definition
                          :concept/type
                          :concept/preferred-label
                          :concept/deprecated]}])

(def ^:private initial-concept-query
  '{:find [(pull ?c pull-pattern)]
    :keys [concept]
    :in [$ % pull-pattern]
    :args []
    :where []
    :offset 0
    :limit -1})

(defn remap-query
  [{args :args offset :offset limit :limit :as m}]
  {:query (-> m
              (dissoc :args)
              (dissoc :offset)
              (dissoc :limit))
   :args args
   :offset offset
   :limit limit})

(defn- extend-query-field [dst [k v]]
  (condp contains? k

    #{:find :keys :in :args :where}
    (update dst k (fn [dst] (into (vec dst) v)))

    #{:offset :limit}
    (assoc dst k v)))

(defn extend-query
  "Extends query `dst` with `extra` partial query data."
  [dst extra]
  (reduce extend-query-field dst extra))

(defn handle-relations [query relation related-ids]
  ;; support substitutability-{from/to} for compatibility
  (let [relation ({"substitutability-from" "substituted-by"
                   "substitutability-to" "substitutability"} relation relation)]
    (extend-query query
                  {:in ['?relation '[?related-ids ...]]
                   :args [relation related-ids]
                   :where '[[?cr :concept/id ?related-ids]
                            (edge ?cr ?relation ?c ?_)]})))

(defn handle-deprecated [query deprecated include-deprecated]
  (let [inc-dep (if (= include-deprecated nil) deprecated include-deprecated)]
    (if inc-dep
      query
      (extend-query query {:where ['(not [?c :concept/deprecated true])]}))))

(defn make-find-concepts-query [{:keys [id preferred-label type deprecated include-deprecated
                                        relation related-ids
                                        db pull-pattern
                                        extra-where-attributes
                                        include-legacy-information
                                        offset limit]}]
  {:pre [pull-pattern]}
  (cond-> initial-concept-query
    true
    (extend-query {:args [db relation/rules pull-pattern]})

    (and relation related-ids)
    (handle-relations relation related-ids)

    id
    (extend-query {:in ['?id]
                   :args [id]
                   :where ['[?c :concept/id ?id]]})

    preferred-label
    (extend-query {:in ['?case-insensitive-preferred-label]
                   :args [(api-util/str-to-pattern-ignore-case preferred-label)]
                   :where ['[?c :concept/preferred-label ?preferred-label]
                           '[(.matches ^String ?preferred-label ?case-insensitive-preferred-label)]]})

    type
    (extend-query {:in ['[?type ...]]
                   :args [(if (vector? type) type [type])]
                   :where ['[?c :concept/type ?type]]})

    (not-empty extra-where-attributes)
    (extend-query {:where (map #(into ['?c] %) extra-where-attributes)})

    true
    (extend-query {:where ['[?c :concept/id]]})

    true
    (handle-deprecated deprecated include-deprecated)

    include-legacy-information
    (extend-query {:find ['?legacy-ams-id]
                   :keys ['legacy-ams-id]
                   :where ['[(get-else $ ?c :concept.external-database.ams-taxonomy-67/id false)
                             ?legacy-ams-id]]})

    offset
    (extend-query {:offset offset})

    limit
    (extend-query {:limit limit})

    true
    remap-query))

(defn- add-legacy-id-to-concept [{:keys [legacy-ams-id] :as result}]
  (cond-> result legacy-ams-id (assoc-in [:concept :deprecated-legacy-id] legacy-ams-id)))

(defn find-concepts
  "Finds concepts in the database

  `args` is a map with the following keys (everything is optional):
  - `:id` (string) — concept id to search for
  - `:preferred-label` (string) — preferred label of a concept to search for
  - `:type` (coll of strings) — restrict concept's type to any of these
  - `:deprecated` (boolean) — restrict concepts to deprecated status (The deprecated param is deprecated)
  - `:include-deprecated` (boolean) — include deprecated concepts
  - `:related-ids` (coll of strings) and `:relation` (string) — restrict to relation to these ids
  - `:offset` (int) - pagination offset
  - `:limit` (int) — pagination limit
  - `:version` (int, `:latest` or `:next`) — db version to use, as per
    [[jobtech-taxonomy.api.db.database-connection/get-db]]. Defaults to `:latest`
  - `:extra-pull-fields` (pull pattern) - additional concept pull pattern
  - `:extra-where-attributes` (coll of attribute -> expected value entries) — additional query constraints
  - `:include-legacy-information` (boolean) — whether to include `:deprecated-legacy-id` to responses"
  [cfg args]
  (-> (assoc args
             :db (db/get-db cfg (:version args :latest))
             :pull-pattern (cond-> concept-pull-pattern
                             (:extra-pull-fields args) (into (:extra-pull-fields args))))
      make-find-concepts-query
      db/q
      (cond->> (:include-legacy-information args) (map add-legacy-id-to-concept))
      (->> (map :concept))
      sort-reply))

(defn exists? [db id]
  (boolean (seq (db/q '[:find ?e
                        :in $ ?id
                        :where [?e :concept/id ?id]]
                      db id))))

;; TODO FIX get db som innehåller unpublished sätt db innan! i args

(defn get-total-concepts-count [cfg version]
  (-> '[:find (count ?concept-id)
        :where
        [?c :concept/id ?concept-id]
        (not [?c :concept/deprecated true])]
      (db/q (db/get-db cfg (or version :latest)))
      ffirst))

(defn- split-on-pipe [string]
  (->> string
       (#(s/split % #"\|"))
       (remove s/blank?)
       (map #(s/trim %))
       (set)))

(defn- assert-concept-part [cfg user-id {:keys [id type definition preferred-label comment quality-level alternative-labels hidden-labels short-description sort-order tx-inst] :as params}]
  (let [external-standard-prefix ":concept.external"
        external-standard-keys (filter #(clojure.string/starts-with? (str %) external-standard-prefix) (keys params))
        new-concept (cond-> {:concept/id id
                             :concept/type type
                             :concept/definition definition
                             :concept/preferred-label preferred-label}
                      quality-level (assoc :concept/quality-level quality-level)
                      alternative-labels (assoc :concept/alternative-labels (split-on-pipe alternative-labels))
                      hidden-labels (assoc :concept/hidden-labels (split-on-pipe hidden-labels))
                      short-description (assoc :concept/short-description short-description)
                      sort-order (assoc :concept/sort-order sort-order)
                      (seq external-standard-keys) (merge (select-keys params external-standard-keys)))
        tx [(api-util/user-action-tx user-id comment) new-concept]
        result (db/transact (db/connect cfg)
                            {:tx-data (api-util/add-tx-instant-if-present tx tx-inst)})]
    [result new-concept]))

(defn assert-concept
  [cfg user-id {:keys [type preferred-label definition] :as params}]
  (cond
    ;; Concept must not exist
    (seq (find-concepts cfg {:preferred-label preferred-label :type type :definition definition :version :next}))
    [false nil]

    (not (type-exists? cfg type :next))
    [false nil]

    :else (let [[result new-concept] (assert-concept-part cfg
                                                          user-id
                                                          (assoc params :id (nano-id/generate-new-id-with-underscore)))
                _ (assert result)
                timestamp (nth (first (:tx-data result)) 2)]

            (log/debug "new-concept" (with-out-str (pp/pprint new-concept)))
            [result timestamp new-concept])))

(defn assert-concept-handler [cfg request]
  (let [query (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request cfg request)
        [result timestamp new-concept] (assert-concept cfg user-id query)]
    (if result
      {:status 200 :body {:time timestamp :concept new-concept}}
      {:status 409 :body (types/map->nsmap {:error "Can't create new concept since it is in conflict with existing concept."})})))

(defn- ^{:auth :editor} remove-label
  [cfg concept-id label type user-id comment]
  {:pre [concept-id label type]}
  (let [conn (db/connect cfg)]
    (when (exists? (db/get-db cfg :next) concept-id)
      (db/transact
       conn
       {:tx-data [[:db/retract [:concept/id concept-id] type label]
                  (api-util/user-action-tx user-id comment)]}))))

(defn- ^{:auth :editor} remove-label-and-return-updated [cfg concept-id label-type label-value user-id comment]
  (when-let [removed (remove-label cfg concept-id label-value label-type user-id comment)]
    (-> {:id concept-id
         :db (:db-after removed)
         :pull-pattern concept-pull-pattern}
        make-find-concepts-query
        db/q
        first)))

(defn ^{:auth :editor} handle-remove-alternative-label [cfg request]
  (let [user-id (pu/get-user-id-from-request cfg request)
        {:keys [id comment alternative-label]} (pu/get-query-from-request request)]
    (if-let [updated-concept (remove-label-and-return-updated cfg id :concept/alternative-labels alternative-label user-id comment)]
      {:status 200 :body updated-concept}
      {:status 409 :body (types/map->nsmap {:error "Can't remove alternative label."})})))

(defn ^{:auth :editor} handle-remove-hidden-label [cfg request]
  (let [user-id (pu/get-user-id-from-request cfg request)
        {:keys [id comment hidden-label]} (pu/get-query-from-request request)]
    (if-let [updated-concept (remove-label-and-return-updated cfg id :concept/hidden-labels hidden-label user-id comment)]
      {:status 200 :body updated-concept}
      {:status 409 :body (types/map->nsmap {:error "Can't remove hidden label."})})))

;; CREATE RELATION

(defn ^{:auth :editor} fetch-relation-entity-id-from-concept-ids-and-relation-type [cfg concept-1 concept-2 relation-type]
  (ffirst
   (db/q '{:find [?r]
           :in [$ ?id]
           :where [[?r :relation/id ?id]]}
         (db/get-db cfg :next)
         (relation/id concept-1 relation-type concept-2))))

(defn- assert-relation-transaction-data [c1 c2 type desc substitutability-percentage]
  (relation/edge-tx c1 type c2
                    (cond-> {}
                      desc
                      (assoc :relation/description desc)
                      substitutability-percentage
                      (assoc :relation/substitutability-percentage
                             substitutability-percentage))))

(defn- assert-relation-part [cfg user-id params]
  (let [{:keys [comment concept-1 concept-2 type description substitutability-percentage tx-inst]} params
        new-rel (assert-relation-transaction-data concept-1 concept-2 type description substitutability-percentage)
        tx [(api-util/user-action-tx user-id comment) new-rel]
        result (db/transact (db/connect cfg) {:tx-data (api-util/add-tx-instant-if-present tx tx-inst)})]
    [result new-rel]))

(defn assert-relation [cfg user-id {:keys [concept-1 concept-2 type] :as params}]
  (if (fetch-relation-entity-id-from-concept-ids-and-relation-type cfg concept-1 concept-2 type)
    [false nil]
    (assert-relation-part cfg user-id params)))

(defn assert-relation-query-params []
  (pu/build-parameter-map [:relation-type
                           :concept-1
                           :concept-2
                           :definition
                           :substitutability-percentage
                           :comment]))

(defn assert-relation-handler [cfg request]
  (let [query-params (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request cfg request)
        _ (log/info "POST /relation")
        transformed-params (set/rename-keys query-params {:relation-type :type})
        [success _] (assert-relation cfg user-id transformed-params)]

    (if success
        ;; TODO: Possibly add relation information?
      {:status 200 :body (types/map->nsmap {:message "Created relation."})}
      {:status 409 :body (types/map->nsmap {:error "Can't create new relation since it is in conflict with existing relation."})})))

;; UPDATE RELATION

(defn- ^{:auth :editor} does-relation-exist? [cfg relation-id]
  (= 1 (count
        (db/q '{:find [?r]
                :in [$ ?id]
                :where [[?r :relation/id ?id]]}
              (db/get-db cfg :next)
              relation-id))))

(defn- ^{:auth :editor} fetch-relation-data-by-id [cfg relation-id]
  (->> (db/q {:find '[?r ?e ?s ?attrib-name]
              :in '[$ ?id]
              :where '[[?r :relation/id ?id]
                       [?r ?e ?s]
                       [?e :db/ident ?attrib-name]]}
             (db/get-db cfg :next)
             relation-id)
       (map (partial zipmap [:id :attrib-id :attrib-value :attrib-key]))
       (group-by :id)
       (map (fn [[id group]]
              (into {:id id} (map (juxt :attrib-key :attrib-value)) group)))))

(defn fetch-relation-data [cfg concept-1 concept-2 relation-type]
  (->> (relation/id concept-1 relation-type concept-2)
       (fetch-relation-data-by-id cfg)
       first))

(defn- change-relation-type? [relation-type new-relation-type]
  (and (some? new-relation-type)
       (not= relation-type new-relation-type)))

(defn- perform-accumulate-relation-transaction [cfg user-action-fact relation-and-other-facts]
  (let [result (db/transact
                (db/connect cfg)
                {:tx-data (conj relation-and-other-facts user-action-fact)})]
    (when result
      {:time (nth (first (:tx-data result)) 2)
       :relation (first relation-and-other-facts)})))

(defn accumulate-relation
  "Updates a relation."
  [cfg user-id
   {:keys [concept-1 concept-2 relation-type substitutability-percentage description comment new-relation-type]}]

  ;; Todo: Are preconditions a good idea to check the sanity of the data?
  ;; See this issue: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/518
  {:pre [user-id concept-1 concept-2 relation-type comment
         (does-relation-exist? cfg (relation/id concept-1 relation-type concept-2))
         (not (every? nil? [substitutability-percentage description new-relation-type]))
         (not (and (change-relation-type? relation-type new-relation-type)
                   (does-relation-exist? cfg (relation/id concept-1 new-relation-type concept-2))))]}
  (perform-accumulate-relation-transaction
   cfg
   (api-util/user-action-tx user-id comment)
   (if (change-relation-type? relation-type new-relation-type)
     (let [prev-data (fetch-relation-data cfg concept-1 concept-2 relation-type)
           new-rel (assert-relation-transaction-data
                    concept-1 concept-2 new-relation-type
                    (or description (:relation/description prev-data))
                    (or substitutability-percentage
                        (:relation/substitutability-percentage prev-data)))]
       [new-rel [:db/retractEntity (:id prev-data)]])
     [(cond-> {:relation/id (relation/id concept-1 relation-type concept-2)}
        substitutability-percentage
        (assoc :relation/substitutability-percentage substitutability-percentage)
        description
        (assoc :relation/description description))])))

;; DELETE RELATION

(defn- retract-relation [cfg user-id comment concept-1 concept-2 relation-type]
  (let [relation-entity-id (fetch-relation-entity-id-from-concept-ids-and-relation-type
                            cfg
                            concept-1
                            concept-2
                            relation-type)
        result (when relation-entity-id
                 (db/transact (db/connect cfg) {:tx-data [[:db/retractEntity relation-entity-id]
                                                          (api-util/user-action-tx user-id comment)]}))]
    result))

(defn delete-relation-query-params []
  (pu/build-parameter-map [:relation-type :concept-1 :concept-2 :comment]))

(defn delete-relation-handler [cfg request]
  (let [{:keys [comment relation-type concept-1 concept-2]} (pu/get-query-from-request request)
        user-id (pu/get-user-id-from-request cfg request)
        _ (log/info (str "DELETE /relation " user-id " " relation-type " " concept-1 " " concept-2))
        result (retract-relation cfg user-id comment concept-1 concept-2 relation-type)]
    (if result
      {:status 200 :body (types/map->nsmap {:message "Retracted relation."})}
      {:status 400 :body (types/map->nsmap {:error "Relation not found."})})))

(def fetch-simple-concept-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label])
    :in $ ?id
    :where [?e :concept/id ?id]])

(defn ^{:auth :editor} fetch-simple-concept [cfg id]
  (ffirst (db/q fetch-simple-concept-query (db/get-db cfg :next) id)))

;; Todo add case insensitive match unicode

(def duplicate-label-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label
                    :concept/deprecated])
    :in $ ?preferred-label ?type
    :where
    [?e :concept/type ?type]
    [?e :concept/preferred-label ?preferred-label]
    [?e :concept/deprecated false]])

(def duplicate-definition-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label])
    :in $ ?definition ?type
    :where
    [?e :concept/type ?type]
    [?e :concept/definition ?definition]])

(defn duplicate-concept-exists? [cfg {:concept/keys [id preferred-label definition type]}]
  {:pre [id preferred-label definition type]}
  (let [concepts (concat (db/q duplicate-label-query (db/get-db cfg :next) preferred-label type)
                         (db/q duplicate-definition-query (db/get-db cfg :next) definition type))
        duplicates (filter #(not (= id (:concept/id (first %)))) concepts)]
    {:result (not (zero? (count duplicates)))
     :duplicates duplicates}))

(defn accumulate-concept
  "Updates concept.
  Will not permit two concepts with the same definition that already exists"
  [cfg user-id {:keys [id type definition preferred-label
                       comment quality-level deprecated
                       alternative-labels hidden-labels no-esco-relation]}]
  {:pre [id (not (every? nil?
                         [preferred-label definition
                          type quality-level deprecated
                          no-esco-relation
                          alternative-labels hidden-labels]))]}
  (let [old-concept (fetch-simple-concept cfg id)
        concept (cond-> old-concept
                  preferred-label (assoc :concept/preferred-label preferred-label)
                  definition (assoc :concept/definition definition)
                  type (assoc :concept/type type)
                  quality-level (assoc :concept/quality-level quality-level)
                  (some? deprecated) (assoc :concept/deprecated deprecated)
                  (some? no-esco-relation) (assoc :concept/no-esco-relation no-esco-relation)
                  alternative-labels (assoc :concept/alternative-labels
                                            (split-on-pipe alternative-labels))
                  hidden-labels (assoc :concept/hidden-labels
                                       (split-on-pipe hidden-labels)))

        duplicate-exists (duplicate-concept-exists? cfg concept)
        datomic-result (when (not (:result duplicate-exists))
                         (db/transact (db/connect cfg)
                                      {:tx-data [concept
                                                 (api-util/user-action-tx
                                                  user-id comment)]}))]
    (when datomic-result
      {:time (nth (first (:tx-data datomic-result)) 2) :concept concept})))

(comment
  ;; When we want to get typed data, use the dynamic pattern input
  ;; dynamic pattern input
  (def db 'some-database)
  (def led-zeppelin 'some-artist)
  (db/q '[:find [(pull ?e pattern) ...]
          :in $ ?artist pattern
          :where [?e :release/artists ?artist]]
        db
        led-zeppelin
        [:release/name]))
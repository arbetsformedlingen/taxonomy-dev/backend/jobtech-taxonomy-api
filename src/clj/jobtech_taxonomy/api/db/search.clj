(ns jobtech-taxonomy.api.db.search
  (:require
   [clojure.string :as str]
   [jobtech-taxonomy.api.cache :as cache]
   [jobtech-taxonomy.api.db.api-util :as api-util]
   [jobtech-taxonomy.api.db.concepts :as concepts]
   [jobtech-taxonomy.api.db.database-connection :as db]
   [jobtech-taxonomy.api.db.popularity :as pop]
   [jobtech-taxonomy.common.relation :as relation]
   [taoensso.nippy :as nippy])
  (:import
   [clojure.lang RT]
   [java.nio.file Files LinkOption]
   [java.nio.file.attribute FileAttribute]
   [java.util Arrays]
   [java.util.stream Collectors]
   [org.apache.lucene.analysis Analyzer]
   [org.apache.lucene.analysis.custom CustomAnalyzer CustomAnalyzer$Builder]
   [org.apache.lucene.search.suggest InputIterator Lookup Lookup$LookupResult]
   [org.apache.lucene.search.suggest.analyzing AnalyzingInfixSuggester]
   [org.apache.lucene.store Directory MMapDirectory]
   [org.apache.lucene.util BytesRef]))

(set! *warn-on-reflection* true)

(defn build-analyzer []
  (.build
   (doto ^CustomAnalyzer$Builder (CustomAnalyzer/builder)
     (.withTokenizer "standard" ^"[Ljava.lang.String;" (into-array String []))
     (.addTokenFilter "lowercase" ^"[Ljava.lang.String;" (into-array String []))
     (.addTokenFilter "asciiFolding" ^"[Ljava.lang.String;" (into-array String ["preserveOriginal" "true"])))))

(def ^Analyzer analyzer (build-analyzer))

(def ^Directory directory
  (let [dir (Files/createTempDirectory "taxonomy-lucene-index" (into-array FileAttribute []))]
    (.addShutdownHook
     (Runtime/getRuntime)
     (Thread. ^Runnable (fn []
                          (->> dir
                               (tree-seq
                                #(Files/isDirectory % (into-array LinkOption []))
                                #(.collect (Files/list %) (Collectors/toList)))
                               reverse
                               (run! #(Files/delete %))))))
    (MMapDirectory. dir)))

(defn- default-to-1 [_] 1)

(defn- InputIterator-on ^InputIterator
  [seqable & {:keys [text-fn weight-fn payload-fn contexts-fn]
              :or {text-fn str
                   weight-fn default-to-1
                   payload-fn nil
                   contexts-fn nil}}]
  (let [iter (RT/iter seqable)
        current-vol (volatile! nil)]
    (reify InputIterator
      (next [_]
        (when (.hasNext iter)
          (let [current (.next iter)]
            (vreset! current-vol current)
            (BytesRef. (str (text-fn current))))))
      (weight [_]
        (weight-fn @current-vol))
      (hasPayloads [_]
        (some? payload-fn))
      (payload [_]
        (BytesRef. ^bytes (nippy/freeze (payload-fn @current-vol))))
      (hasContexts [_]
        (some? contexts-fn))
      (contexts [_]
        (->> @current-vol contexts-fn (map #(BytesRef. (str %))) set)))))

(defn create-infix-suggester []
  (AnalyzingInfixSuggester.
   directory
   #_index-analyzer analyzer
   #_query-analyzer analyzer
   #_min-prefix-chars 4
   #_commit-on-build true
   #_all-terms-required false
   #_highlight true))

(defn get-documents-from-db [db]
  (map first (db/q
              '[:find (pull ?c [:concept/id
                                :concept/type
                                :concept/preferred-label
                                :concept/alternative-labels])
                :where [?c :concept/id ?id]]
              db)))

(def ^:private build-cached-strict-suggester
  (cache/make
   (fn [db]
     (let [documents (get-documents-from-db db)]
       (doto ^AnalyzingInfixSuggester (create-infix-suggester)
         (.build (InputIterator-on
                  documents
                  :payload-fn identity
                  :contexts-fn (juxt :concept/type :concept/id)
                  :text-fn #(str (:concept/preferred-label %)
                                 " "
                                 (str/join " " (:concept/alternative-labels %))))))))
   :key-fn db/db->cache-key))

(defn find-related-concept-ids
  "Given concept ids `x` in `related-ids`, find concept ids `y` such that `relation(x, y) is true`."
  [db relation related-ids]
  {:pre [db
         (string? relation)
         (every? string? related-ids)]}
  (-> {:find '[?id]
       :args [db relation/rules]
       :where []
       :in '[$ %]
       :offset 0
       :limit -1}

      ;; For performance reasons, the relation clauses must come first.
      (concepts/handle-relations relation related-ids)
      (concepts/extend-query {:where '[[?c :concept/id ?id]]})

      concepts/remap-query
      db/q
      (->> (map first))))

(defn strict-lookup [db query-string type relation related-ids limit]
  (let [^Lookup lookup (build-cached-strict-suggester db)]
    (->> (.lookup lookup
                  query-string
                  (->> (if (and relation (seq related-ids))
                         (find-related-concept-ids db relation related-ids)
                         type)
                       (map #(BytesRef. (str %)))
                       set)
                  true
                  (or limit 100))
         (map #(let [bytes-ref (.-payload ^Lookup$LookupResult %)]
                 (nippy/thaw (Arrays/copyOfRange (.-bytes bytes-ref)
                                                 (.-offset bytes-ref)
                                                 (+ (.-offset bytes-ref)
                                                    (.-length bytes-ref))))))
         (filter (if (and (seq related-ids) relation (seq type))
                   (comp (set type) :concept/type)
                   any?)))))

(defn get-concepts-by-search [cfg {:keys [;;required
                                          query-string
                                          version
                                          ;; optional
                                          type ;; coll of strings
                                          relation
                                          related-ids ;; coll of strings
                                          offset
                                          limit]}]
  (let [db (db/get-db cfg version)
        results (strict-lookup db query-string type relation related-ids limit)

        pop (pop/fetch)

        ;; costs are from 0 to 1, lower is better

        max-pop (transduce
                 (map #(get pop (:concept/id %) 0))
                 max
                 1
                 results)
        popularity-cost #(- 1
                            (-> %
                                :concept/id
                                (as-> $ (get pop $ 0))
                                (/ max-pop)))

        starts-with-pattern (api-util/str-to-pattern-starts-with query-string)
        starts-with-cost #(if (.matches ^String (:concept/preferred-label %)
                                        starts-with-pattern)
                            0
                            1)]
    (->> results
         (sort-by (juxt starts-with-cost popularity-cost))
         (api-util/pagination offset limit))))

(comment

  (def cfg
    {:database-backend :datahike
     :datahike {:cfg {}}})

  (get-concepts-by-search
   cfg
   {:query-string "köp"
    :version :latest})

  (get-concepts-by-search
   cfg
   {:query-string "f"
    :version :latest
    :type ["ssyk-level-3" #_"occupation-field"]
    :relation "broader"
    :related-ids ["Fghp_zje_WA8"]}))

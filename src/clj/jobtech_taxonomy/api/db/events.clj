(ns jobtech-taxonomy.api.db.events
  (:refer-clojure :exclude [type])
  (:require
   [jobtech-taxonomy.api.db.database-connection :as db]))

(def show-version-instance-ids
  '[:find ?tx ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(def show-latest-version-id
  '[:find (max ?version)
    :where [_ :taxonomy-version/id ?version]])

(defn group-by-transaction-and-entity [datoms]
  (group-by (juxt #(nth % 3) #(nth % 0)) datoms))

(defn group-by-attribute [grouped-datoms]
  (map #(group-by second %) grouped-datoms))

(defn keep-after-update [[_ _ _ _ operation]]
  operation)

;; TODO skriv om till updated rör alla fält utom id.  type preferred-label definition
(defn is-event-update-preferred-label? "checks if op is not all true or false"
  [datoms-grouped-by-attribute]
  (if-let [datoms (:concept/preferred-label datoms-grouped-by-attribute)]
    (not (apply = (map #(nth % 4) datoms)))
    false))

(defn is-event-create-concept? [datoms-grouped-by-attribute]
  (if-let [datoms (:concept/id datoms-grouped-by-attribute)]
    (every? true? (map #(nth % 4) datoms))
    false))

(defn is-event-deprecated-concept? [datoms-grouped-by-attribute]
  (if-let [datoms (:concept/deprecated datoms-grouped-by-attribute)]
    (every? true? (map #(nth % 4) datoms))
    false))

(defn create-event-create-concept-from-datom
  "TODO fix potential bug fest, first is a bit sketchy"
  [datoms-grouped-by-attribute]
  (let [[_ _ _ transaction-id _ timestamp concept-id preferred-label type] (first (:concept/preferred-label datoms-grouped-by-attribute))]
    {:event-type "CREATED"
     :transaction-id transaction-id
     :type type
     :timestamp timestamp
     :concept-id concept-id
     :preferred-label preferred-label}))

(defn create-event-deprecated-concept-from-datom [datoms-grouped-by-attribute]
  (let [[_ _ _ transaction-id _ timestamp concept-id preferred-label type] (first (:concept/deprecated datoms-grouped-by-attribute))]
    {:event-type "DEPRECATED"
     :transaction-id transaction-id
     :type type
     :timestamp timestamp
     :concept-id concept-id
     :preferred-label preferred-label
     :deprecated true}))

(defn create-event-updated-preferred-label [datoms-grouped-by-attribute]
  (let [datoms (:concept/preferred-label datoms-grouped-by-attribute)
        datom-after (filter keep-after-update datoms)
        [[_ _ _ transaction-id _ timestamp concept-id preferred-label type]] datom-after]
    {:event-type "UPDATED"
     :transaction-id transaction-id
     :type type
     :timestamp timestamp
     :concept-id concept-id
     :preferred-label preferred-label}))

(defn determine-event-type
  "This function will return nil events when the event is not CREATED, DEPRECATED or UPDATED.
  Like replaced-by will return nil."
  [datoms-by-attribute]
  (let [is-event-create-concept (is-event-create-concept? datoms-by-attribute)
        is-event-deprecated-concept (is-event-deprecated-concept? datoms-by-attribute)
        is-event-update-preferred-label (is-event-update-preferred-label? datoms-by-attribute)]
    (cond
      is-event-create-concept (create-event-create-concept-from-datom datoms-by-attribute)
      is-event-deprecated-concept (create-event-deprecated-concept-from-datom datoms-by-attribute)
      is-event-update-preferred-label (create-event-updated-preferred-label datoms-by-attribute))))

(defn convert-history-to-events [datoms]
  (let [grouped-datoms (map second (group-by-transaction-and-entity datoms))
        datoms-by-attribute (group-by-attribute grouped-datoms)
        events (filter some? (map determine-event-type datoms-by-attribute))]
    events))

;;  (db/q show-version-instance-ids (db/get-db))
;;  [[13194139533328 68] [13194139533330 69] [13194139533326 67]]
;;  stoppa in ditt värde i listan ovan
;;  sortera listan på transactions id:n
;;  ta index för ditt värde ut listan
;;  stega upp ett index för att få nästföljande transaktions-id med tillhörande taxonomy-versions-id
;;
;;  ...
;;  Blås databasen.
;;
;;  Skapa 3 test transaktioner
;;  1 spara version 66 i tom databas !!
;;  2 Spara konceptet gammel-java
;;  3 spara version 67
;;  4 uppdatera gammel-java, sätt den till deprecated
;;  5. spara version 68
;;  6.
;;
;;  1. databas tom
;;  2. skapa första versions tagg i tomma databasen.
;;  3. redaktionen lägger in saker i databasen.
;;  4. Redaktionen skapar en version av all som tidigare funnits i databasen, dvs allt innan versions-transaktionen fram till versionen innan.
;;  5. redaktionen lägger in mer data.
;;  6. skapar ny version.
;;
;;  Hämta transaktioner från (från-version - 1) till transaktioner tidigare än (till-version)

(defn convert-transaction-id-to-version-id [tx-id->version transaction-id]
  (second (first (subseq tx-id->version >= transaction-id))))

(defn convert-events-transaction-ids-to-version-ids [cfg events]
  (let [tx-id->version (into (sorted-map) (db/q show-version-instance-ids (db/get-db cfg :latest)))]
    (map (fn [event]
           (let [version-id (convert-transaction-id-to-version-id tx-id->version (:transaction-id event))
                 event-with-version-id (merge event {:version version-id})]
             event-with-version-id))
         events)))

(def show-deprecated-replaced-by-query
  '[:find (pull ?c
                [:concept/id
                 :concept/definition
                 :concept/type
                 :concept/preferred-label
                 :concept/deprecated
                 {:concept/replaced-by [:concept/id
                                        :concept/definition
                                        :concept/type
                                        :concept/preferred-label
                                        :concept/deprecated]}])

    ?tx
    :in $ ?one-version-before-from-version ?to-version
    :where
    [?c :concept/deprecated true]
    [?c :concept/replaced-by ?rc ?tx]
    [?tx :db/txInstant ?inst]

    [?fv :taxonomy-version/id ?one-version-before-from-version]
    [?fv :taxonomy-version/tx ?one-version-before-from-version-tx]
    [?one-version-before-from-version-tx :db/txInstant ?one-version-before-from-version-inst]
    [(< ?one-version-before-from-version-inst ?inst)]

    [?tv :taxonomy-version/id ?to-version]
    [?tv :taxonomy-version/tx ?to-version-tx]
    [?to-version-tx :db/txInstant ?to-version-inst]
    [(> ?to-version-inst ?inst)]])

(defn transform-deprecated-concept-replaced-by-result [[deprecated-concept transaction-id]]
  (let [{:concept/keys [id preferred-label definition deprecated replaced-by type]} deprecated-concept]
    {:transaction-id transaction-id
     :concept {:id id
               :type type
               :definition definition
               :preferred-label preferred-label
               :deprecated deprecated
               :replaced-by replaced-by}}))

(defn get-deprecated-concepts-replaced-by-from-version [cfg from-version to-version]
  (let [db (db/get-db cfg (if (= to-version :next) :next :latest))
        deprecated-concepts (if to-version
                              (db/q show-deprecated-replaced-by-query db from-version to-version)
                              (db/q show-deprecated-replaced-by-query db from-version (ffirst (db/q show-latest-version-id db))))]
    (->> deprecated-concepts
         (map transform-deprecated-concept-replaced-by-result)
         (convert-events-transaction-ids-to-version-ids cfg)
         (sort-by :transaction-id)
         (map #(dissoc % :transaction-id)))))

(comment

 ;; (db/transact (get-conn) {:tx-data [{:taxonomy-version/id 67}]})

 ;; (db/q '[:find (pull ?v [*])  :in $ :where [?v :taxonomy-version/id]] (db/get-db) )

  (def get-version
    '[:find ?e
      :in $
      :where [?e :taxonomy-version/id 67]])

  (defn get-version-67-entity [cfg]
    (ffirst (db/q get-version (db/get-db cfg :next))))

  (defn ^:export convert [cfg]
    (let [version-db-id (get-version-67-entity cfg)]
      [{:db/id version-db-id
        :taxonomy-version/id 68}]))

  #_(db/transact (get-conn) {:tx-data [{:taxonomy-version/id 67}]}))

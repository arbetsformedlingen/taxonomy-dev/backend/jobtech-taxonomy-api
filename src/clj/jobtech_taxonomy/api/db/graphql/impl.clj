(ns jobtech-taxonomy.api.db.graphql.impl
  (:require [clojure.instant :as inst]
            [jobtech-taxonomy.api.authentication-service :refer [authenticate-admin]]
            [jobtech-taxonomy.api.db.database-connection :as db])
  (:import [java.text SimpleDateFormat]
           [java.util TimeZone]))

(set! *warn-on-reflection* true)

(def ^:private ^ThreadLocal inst-format
  ;; TODO: this should be used everywhere or preferably with some clojure native version.
  ;; SimpleDateFormat is not thread-safe, so we use a ThreadLocal proxy for access.
  ;; http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4228335
  (proxy [ThreadLocal] []
    (initialValue []
      (doto (SimpleDateFormat. "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        (.setTimeZone (TimeZone/getTimeZone "GMT"))))))

(def parse-date inst/read-instant-date)

(defn serialize-date [dt]
  (.format ^SimpleDateFormat (.get inst-format) dt))

(defn parse-version-ref [str]
  (or ({"latest" :latest "next" :next} str)
      (and (string? str) (Integer/parseInt str))
      (and (int? str) str)
      (throw (ex-info "Invalid version ref" {:ref str}))))

(defn serialize-version-ref [version-ref]
  ({:latest "latest" :next "next"} version-ref version-ref))

(defn get-db
  "Resolver middleware that injects specified database version into a context

  Database version is taken from `:version` key in args map. If the version is
  `next`, the api key from context is checked for admin rights before invoking
  the wrapped resolver"
  [cfg api-key version]
  (cond
    (and (= :next version) (not (authenticate-admin cfg api-key)))
    (throw (ex-info "Not authorized" {:api-key api-key}))

    :else
    (db/get-db cfg version)))

(def schema
  {:scalars {:Date {:description "A date.\n\nFormat: `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`"
                    :parse parse-date
                    :serialize serialize-date}
             :VersionRef {:description "Version identifier, either \"latest\", \"next\" (requires admin rights) or a number of a version"
                          :parse parse-version-ref
                          :serialize serialize-version-ref}}})

(defn deep-merge [& maps]
  (apply merge-with deep-merge maps))

(defn merge-into [& xs]
  (condp every? xs
    sequential? (reduce into [] xs)
    (some-fn map? nil?) (apply merge-with merge-into xs)
    (last xs)))

(defn transform-fetcher [f]
  (fn [_ values]
    (into {} (map (juxt identity f)) values)))
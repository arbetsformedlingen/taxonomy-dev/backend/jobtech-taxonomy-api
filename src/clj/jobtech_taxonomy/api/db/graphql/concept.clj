(ns jobtech-taxonomy.api.db.graphql.concept
  (:require [clojure.string :as str]
            [io.github.vlaaad.plusinia :as p]
            [jobtech-taxonomy.api.db.api-util :as api-util]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.graphql.impl :as graphql.impl]))

(defn- sort-on-sort-order
  [l]
  (if (contains? (first l) :concept/sort-order)
    (sort-by :concept/sort-order l)
    l))

(def query-args
  {:id {:type '(list (non-null String))
        :description "Restrict results to these concept IDs"}
   :type {:type '(list (non-null String))
          :description "Restrict results to these concept types"}
   :preferred_label_contains {:type 'String
                              :description "Filter on preferred label containing a string"}
   :limit {:type 'Int
           :description "Pagination: maximum amount of returned concepts"}
   :offset {:type 'Int
            :description "Pagination: skip this many returned concepts"}
   :include_deprecated {:type '(non-null Boolean)
                        :description "Include deprecated concepts"
                        :default-value false}})

(defn query [db {:keys [id type preferred_label_contains include_deprecated
                        limit offset]}]
  (-> {:query '{:find [(pull ?c [:concept/sort-order [:concept/id :as :id]])]
                :in [$]
                :where []}
       :args [db]}
      (cond->
       id
        (graphql.impl/merge-into
         {:query {:where '[[?c :concept/id ?id]]
                  :in '[[?id ...]]}
          :args [id]})

        type
        (graphql.impl/merge-into
         {:query {:where '[[?c :concept/type ?type]]
                  :in '[[?type ...]]}
          :args [(if (vector? type) type [type])]})

        preferred_label_contains
        (graphql.impl/merge-into
         {:query
          {:where '[[?c :concept/preferred-label ?preferred-label]
                    [(.matches ^String ?preferred-label ?preferred_label_contains)]]
           :in '[?preferred_label_contains]}
          :args [(api-util/str-to-pattern-contains preferred_label_contains)]})

        (not include_deprecated)
        (graphql.impl/merge-into
         {:query {:where '[[(ground false) ?deprecated]
                           [(get-else $ ?c :concept/deprecated false) ?deprecated]]}})

        limit
        (assoc :limit limit)

        offset
        (assoc :offset offset))
      (update-in [:query :where] conj '[?c :concept/id ?id])
      db/q
      (->> (map first)
           sort-on-sort-order
           (map #(p/make-node % :context {:db db})))))

(def fetched-attrs
  {:concept/type
   {:field {:type '(non-null String)
            :description "Type of a concept"}}

   :concept/preferred-label
   {:field {:type '(non-null String)
            :description "Textual name of a concept"}}

   :concept/definition
   {:field {:type '(non-null String)
            :description "Concept definition"}}

   :concept/short-description
   {:field {:type 'String
            :description "A short description that is more informative than the preferred label"}}

   :concept/alternative-labels
   {:field {:type '(non-null (list (non-null String)))
            :description "Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept"}
    :default []}

   :concept/hidden-labels
   {:field {:type '(non-null (list (non-null String)))
            :description "A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations"}
    :default []}

   :concept/quality-level
   {:field {:type 'Int
            :description "Quality level of this concept (1, 2 or 3)"}}

   :concept/sort-order
   {:field {:type 'Int
            :description "Concept sort-order"}}

   :concept.external-standard/ssyk-code-2012
   {:field {:type 'String
            :description "SSYK code that can be found on concepts of `ssyk-level-*` types"}}

   :concept.external-standard/isco-code-08
   {:field {:type 'String
            :description "ISCO code that can be found on concepts of `isco-level-4` type"}}

   :concept.external-standard/esco-uri
   {:field {:type 'String
            :description "ESCO identifier URI that can be found on concepts of `isco-level-4` and `esco-occupation` type"}}

   :concept.external-standard/eures-code-2014
   {:field {:type 'String
            :description "EURES code that can be found on concepts of `employment-duration` type"}}

   :concept.external-standard/driving-licence-code-2013
   {:field {:type 'String
            :description "Driver licence code that can be found on concepts of `driving-licence` type"}}

   :concept.external-standard/national-nuts-level-3-code-2019
   {:field {:type 'String
            :description "Swedish Län code that can be found on concepts of `region` type"}}

   :concept.external-standard/nuts-level-3-code-2013
   {:field {:type 'String
            :description "NUTS level 3 code that can be found on concepts of `region` type"}}

   :concept.external-standard/nuts-level-3-code-2021
   {:field {:type 'String
            :description "NUTS level 3 code that can be found on concepts of `region` type"}}
   
   :concept.external-standard/nuts-level-3-code-2024
   {:field {:type 'String
            :description "NUTS level 3 code that can be found on concepts of `region` type"}}

   :concept.external-standard/lau-2-code-2015
   {:field {:type 'String
            :description "Swedish Municipality code that can be found on concepts of `municipality` type"}}

   :concept.external-standard/iso-3166-1-alpha-2-2013
   {:field {:type 'String
            :description "2 letter country code"}}

   :concept.external-standard/iso-3166-1-alpha-3-2013
   {:field {:type 'String
            :description "3 letter country code"}}

   :concept.external-standard/iso-639-3-alpha-2-2007
   {:field {:type 'String
            :description "2 letter language code"
            :deprecated "Incorrect ISO name, use `iso_639_1_2002` instead. It will be removed."}}

   :concept.external-standard/iso-639-3-alpha-3-2007
   {:field {:type 'String
            :description "3 letter language code"
            :deprecated "Incorrect ISO name, use `iso_639_2_1998` instead. It will be removed."}}

   :concept.external-standard/iso-639-1-2002
   {:field {:type 'String
            :description "2 letter language code"}}

   :concept.external-standard/iso-639-2-1998
   {:field {:type 'String
            :description "3 letter language code"}}

   :concept.external-standard/iso-639-3-2007
   {:field {:type 'String
            :description "3 letter language code"}}

   :concept.external-standard/sun-education-field-code-2020
   {:field {:type 'String
            :description "SUN education field code that can be found on concepts of `sun-education-field-*` types"}}

   :concept.external-standard/sun-education-level-code-2020
   {:field {:type 'String
            :description "SUN education level code that can be found on concepts of `sun-education-level-*` types"}}

   :concept.external-standard/sun-education-field-code-2000
   {:field {:type 'String
            :description "Old SUN education field code 2000 that can be found on concepts of `sun-education-field-*` types"}}

   :concept.external-standard/sun-education-level-code-2000
   {:field {:type 'String
            :description "Old SUN education level code that can be found on concepts of `sun-education-level-*` types"}}

   :concept.external-standard/sni-level-code-2007
   {:field {:type 'String
            :description "SNI level code that can be found on concepts of `sni-level-*` types"}}

   :concept.external-standard/unemployment-type-code
   {:field {:type 'String
            :description "Unemployment type code"}}

   :concept.external-standard/unemployment-fund-code-2017
   {:field {:type 'String
            :description "Unemployment fund code"}
    :name :unemployment_fund_code}

   :concept.external-standard/eures-nace-code-2007
   {:field {:type 'String
            :description "NACE code according to EURES naming convention, found on `sni-level-{1,2,3,4}` concepts"}
    :name :eures_nace_code_2007}

   :concept.external-standard/wikidata-uri
   {:field {:type 'String
            :description "URI referring to the concept in Wikidata."}
    :name :wikidata_uri}

   :concept.external-standard/glottolog-uri
   {:field {:type 'String
            :description "URI referring to the languoid concept in glottolog, found on language concepts."}
    :name :glottolog_uri}

   :concept.external-standard/iso-uri
   {:field {:type 'String
            :description "URI referring to the ISO-definition of a concept."}
    :name :iso_uri}

   :concept.external-standard/citizenship-code
   {:field {:type 'String
            :description "Codes representing broad categorizations of countries."}
    :name :citizenship_code}
   
   :concept/deprecated
   {:field {:type '(non-null Boolean)
            :description "Indicator whether the concept is deprecated"}
    :default false}

   :concept/no-esco-relation
   {:field {:type '(non-null Boolean)
            :description "Indicator whether the concept has no esco relation"}
    :default false}

   :concept.external-database.ams-taxonomy-67/id
   {:field {:type 'String
            :description "Concept ID that was imported from the old Arbetsförmedlingen's taxonomy. Uniqueness is per type only"
            :deprecated "This value is needed for Arbetsförmedlingen's internal systems during migration period. It will be removed."}
    :name :deprecated_legacy_id}})

(defn attr->field-id [attr]
  (or (get-in fetched-attrs [attr :name])
      (keyword (str/replace (name attr) \- \_))))

(defn attr->field-label [attr]
  (name (attr->field-id attr)))

(defn- field-definitions []
  (->> fetched-attrs
       (map (juxt #(-> % key attr->field-id)
                  #(-> % val :field)))
       (into {})))

(defn field-fetcher [attr]
  (let [default (get-in fetched-attrs [attr :default])]
    (fn fetch [{:keys [db]} shallow-concepts]
      (let [id->field (into {}
                            (map (juxt first #(-> % second (get attr default))))
                            (db/q '[:find ?id (pull ?c pull-pattern)
                                    :in $ [?id ...] pull-pattern
                                    :where [?c :concept/id ?id]]
                                  db (into #{} (map :id) shallow-concepts) [attr]))]
        (into {}
              (map (juxt identity #(-> % :id id->field)))
              shallow-concepts)))))

(defn- field-fetchers []
  (->> fetched-attrs
       keys
       (map (juxt attr->field-id field-fetcher))
       (into {})))

(defn fetch-last-changed [{:keys [db]} shallow-concepts]
  (let [id->inst (into {}
                       (db/q '[:find ?id (max ?inst)
                               :in $ [?id ...]
                               :where
                               [?c :concept/id ?id]
                               [?c _ _ ?tx]
                               [?tx :db/txInstant ?inst]]
                             db (map :id shallow-concepts)))]
    (into {}
          (map (juxt identity #(-> % :id id->inst)))
          shallow-concepts)))

(defn- fetch-concepts [cfg {:keys [api-key version] :as args} nils]
  (zipmap nils (repeat (query (graphql.impl/get-db cfg api-key version) args))))

(defn- resolve-uri [{:keys [id]}]
  (str "http://data.jobtechdev.se/taxonomy/concept/" id))

(def schema
  {:queries
   {:concepts {:type '(non-null (list (non-null :Concept)))
               :description "Fetch concepts"
               :args (assoc query-args :version
                            {:type '(non-null :VersionRef)
                             :description "Use this taxonomy version"
                             :default-value :latest})}}
   :objects {:Concept {:description "Fundamental taxonomy type"
                       :fields (merge
                                {:id {:type '(non-null String)
                                      :description "ID of a concept"}
                                 :uri {:type '(non-null String)
                                       :description "URI of a concept"}
                                 :last_changed {:type :Date
                                                :description "The date of the latest modification of a concept"}}
                                (field-definitions))}}})

(defn fetchers [cfg]
  {:Query {:concepts (p/make-query-fetcher (partial fetch-concepts cfg) :context-keys #{:api-key})}
   :Concept (merge {:id (graphql.impl/transform-fetcher :id)
                    :uri (graphql.impl/transform-fetcher resolve-uri)
                    :last_changed fetch-last-changed}
                   (field-fetchers))})
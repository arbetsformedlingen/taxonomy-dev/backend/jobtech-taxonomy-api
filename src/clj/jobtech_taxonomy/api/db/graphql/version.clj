(ns jobtech-taxonomy.api.db.graphql.version
  (:require [io.github.vlaaad.plusinia :as p]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.graphql.concept :as graphql.concept]
            [jobtech-taxonomy.api.db.graphql.impl :as graphql.impl]))

(defn- fetch-versions [cfg {:keys [from to] :as args} nils]
  (zipmap
   nils
   (repeat
    (let [db (db/get-db cfg :next)]
      (when (or (= :next from) (= :next to))
        (throw (ex-info "\"next\" is not a released version" args)))
      (-> {:query '{:find [?id ?inst]
                    :keys [:id :timestamp]
                    :in [$]
                    :where [[?v :taxonomy-version/id ?id]
                            [(< 0 ?id)]
                            [?v :taxonomy-version/tx ?tx]
                            [?tx :db/txInstant ?inst]]}
           :args [db]}
          (cond->
           (int? to)
            (graphql.impl/merge-into
             {:query {:where '[[(<= ?id ?to)]]
                      :in '[?to]}
              :args [to]})
            (int? from)
            (graphql.impl/merge-into
             {:query {:where '[[(<= ?from ?id)]]
                      :in '[?from]}
              :args [from]})

            (= :latest from)
            (graphql.impl/merge-into
             {:query {:where '[[(q '[:find (max ?id)
                                     :where [_ :taxonomy-version/id ?id]]
                                   $)
                                [[?from]]]
                               [(<= ?from ?id)]]}}))
          db/q
          (->> (sort-by :id)
               (mapv #(p/make-node % :context {:db (db/as-of db (:timestamp %))}))))))))

(defn resolve-version-concepts [{:keys [db] :as args} values]
  (zipmap values (repeat (graphql.concept/query db args))))

(def schema
  {:queries
   {:versions
    {:type '(non-null (list (non-null :Version)))
     :description "Fetch released versions"
     :args {:from {:type :VersionRef
                   :description "Earliest version to include, defaults to first released version. `\"next\"` is not accepted here since it's not a released version"}
            :to {:type :VersionRef
                 :description "Latest version to include. `\"next\"` is not accepted here since it's not a released version"
                 :default-value :latest}}}}

   :objects
   {:Version
    {:description "Released version"
     :fields {:id {:type '(non-null Int)
                   :description "Incremental identifier of a version"}
              :timestamp {:type '(non-null :Date)
                          :description "The date of a version release"}
              :concepts {:type '(non-null (list (non-null :Concept)))
                         :description "Concepts at a point in time of a release"
                         :args graphql.concept/query-args}}}}})

(defn fetchers [cfg]
  {:Query {:versions (partial fetch-versions cfg)}
   :Version {:id (graphql.impl/transform-fetcher :id)
             :timestamp (graphql.impl/transform-fetcher :timestamp)
             :concepts resolve-version-concepts}})

(ns jobtech-taxonomy.api.db.graphql.total-count
  (:require [jobtech-taxonomy.api.db.concepts :refer [get-total-concepts-count]]))

(def schema '{:queries {:total_concepts_count
                        {:type :TotalCountResult
                         :args {:version {:type :VersionRef
                                          :description "Use this taxonomy version"
                                          :default-value :latest}}}}
              :objects {:TotalCountResult
                        {:fields
                         {:count
                          {:type Int}}}}})

(defn fetchers [cfg]
  {:Query {:total_concepts_count (fn [{:keys [version]} _nils]
                                   {nil (get-total-concepts-count cfg version)})}
   :TotalCountResult {:count (fn [_ count]
                               (zipmap count count))}})
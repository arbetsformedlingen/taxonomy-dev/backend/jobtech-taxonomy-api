(ns jobtech-taxonomy.api.db.graphql.relation
  (:require [clojure.string :as str]
            [jobtech-taxonomy.api.db.api-util :as api-util]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.graphql.impl :as graphql.impl]
            [jobtech-taxonomy.common.relation :as relation]))

(def ^:private field-renames
  {"substitutability" "substitutes"
   "possible-combination" "possible-combinations"
   "unlikely-combination" "unlikely-combinations"})

(def ^:private entity-attrs-to-include
  {"substitutability" #{:relation/substitutability-percentage}})

(defn- type->attrs-to-include [relation-type]
  (or (entity-attrs-to-include relation-type)
      (entity-attrs-to-include (relation/relations relation-type))))

(defn type->field-id [relation-type]
  (keyword (str/replace (field-renames relation-type relation-type) \- \_)))

(defn type->field-label [relation-type]
  (name (type->field-id relation-type)))

(defn- edge-query-transformer [relation-type]
  #(reduce
    (fn [acc attr]
      (let [label (name attr)
            field-var (symbol (str "?" label))]
        (graphql.impl/merge-into
         acc
         {:query {:where [['?r attr field-var]]
                  :find [field-var]
                  :keys [(keyword (str/replace label \- \_))]}})))
    (graphql.impl/merge-into
     %
     {:query {:where '[(edge ?c ?relation-type ?related-c ?r)]
              :in '[?relation-type]}
      :args [relation-type]})
    (type->attrs-to-include relation-type)))

(def field-id->query-transformer
  (->> relation/relations
       keys
       (map (juxt type->field-id edge-query-transformer))
       (into {:replaced_by #(update-in % [:query :where] conj '[?c :concept/replaced-by ?related-c])
              :replaces #(update-in % [:query :where] conj '[?related-c :concept/replaced-by ?c])})))

(def field-args
  {:id {:type '(list (non-null String))
        :description "Restrict results to these concept IDs"}
   :type {:type '(list (non-null String))
          :description "Restrict results to these concept types"}
   :preferred_label_contains {:type 'String
                              :description "Filter on preferred label containing a string"}
   :limit {:type 'Int
           :description "Pagination: maximum amount of returned concepts"}
   :offset {:type 'Int
            :description "Pagination: skip this many returned concepts"}
   :include_deprecated {:type '(non-null Boolean)
                        :description "Include deprecated concepts"
                        :default-value false}})

(defn- query-transformer->fetcher [transformer]
  (fn [{:keys [;; ctx
               db
               ;; args
               id type preferred_label_contains include_deprecated limit offset]}
       shallow-concepts]
    (let [id->concepts
          (-> {:query '{:find [?from-id ?id]
                        :keys [from_id id]
                        :in [$ % [?from-id ...]]
                        :where [[?c :concept/id ?from-id]]}
               :args [db relation/rules (into #{} (map :id) shallow-concepts)]}
              transformer
              (graphql.impl/merge-into
               {:query {:where '[[?related-c :concept/id ?id]]}})
              (cond->
               type
                (graphql.impl/merge-into
                 {:query {:where '[[?related-c :concept/type ?type]]
                          :in '[[?type ...]]}
                  :args [(if (vector? type) type [type])]})

                id
                (graphql.impl/merge-into
                 {:query {:where '[[(= ?id ?id-to)]]
                          :in '[[?id-to ...]]}
                  :args [id]})

                preferred_label_contains
                (graphql.impl/merge-into
                 {:query
                  {:where '[[?related-c :concept/preferred-label ?preferred-label]
                            [(.matches ^String ?preferred-label ?preferred_label_contains)]]
                   :in '[?preferred_label_contains]}
                  :args [(api-util/str-to-pattern-contains preferred_label_contains)]})

                (not include_deprecated)
                (graphql.impl/merge-into
                 {:query
                  {:where '[[(ground false) ?deprecated]
                            [(get-else $ ?related-c :concept/deprecated false) ?deprecated]]}})

                limit
                (assoc :limit limit)

                offset
                (assoc :offset offset))
              db/q
              (->> (group-by :from_id)))]
      (into {}
            (map (juxt identity (fn [{:keys [id]}]
                                  (map #(dissoc % :from_id) (id->concepts id [])))))
            shallow-concepts))))

(def field-id->fetcher
  (update-vals field-id->query-transformer query-transformer->fetcher))

(defn field-definitions []
  (->> relation/relations
       keys
       (map
        (juxt
         type->field-id
         (fn [relation-type]
           (let [attrs (type->attrs-to-include relation-type)]
             {:type '(non-null (list (non-null :Concept)))
              :description (str (str/capitalize (type->field-label relation-type))
                                " concepts"
                                (when attrs
                                  (str
                                   " (results may include "
                                   (->> attrs
                                        (map #(str "`"
                                                   (-> % name (str/replace \- \_))
                                                   "`"))
                                        (str/join ","))
                                   ")")))
              :args field-args}))))
       (into {})))

(def schema
  {:objects
   {:Concept
    {:fields
     (merge
      {:substitutability_percentage
       {:type 'Int
        :description "Substitutability percentage, an int between 0 and 100. Can appear only on concepts inside `substitutes` or `substituted_by` scopes"}

       :replaces
       {:type '(non-null (list (non-null :Concept)))
        :description "Concepts this concept replaces (that are most probably deprecated, so this field includes deprecated concepts by default)"
        :args (assoc-in field-args [:include_deprecated :default-value] true)}

       :replaced_by
       {:type '(non-null (list (non-null :Concept)))
        :description "Concepts that replace this concept (that is most probably deprecated: don't forget to add `include_deprecated` argument when querying it)"
        :args field-args}}
      (field-definitions))}}})

(defn fetchers [_cfg]
  {:Concept (merge {:substitutability_percentage (graphql.impl/transform-fetcher :substitutability_percentage)}
                   field-id->fetcher)})

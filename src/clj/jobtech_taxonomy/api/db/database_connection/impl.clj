(ns jobtech-taxonomy.api.db.database-connection.impl
  (:require [babashka.fs :as fs]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [datahike.api :as datahike]
            [datomic.client.api :as datomic]
            [jobtech-taxonomy.api.config :as c]
            [jobtech-taxonomy.common.io-utils :as iou]
            [malli.core :as malli]
            [taoensso.timbre :as log]
            [wanderung.core :as wanderung]))

(set! *warn-on-reflection* true)

(defn retry
  ([fun] (retry {} fun))
  ([{:keys [delay-ms tries] :or {delay-ms 200 tries 5} :as opts} fun]
   (if (< 0 tries)
     (try
       (fun)
       (catch Exception e
         (let [remaining-tries (dec tries)]
           (log/error tries "failed with" e "retries remaining" remaining-tries)
           (Thread/sleep (-> delay-ms double Math/round int))
           (retry {:delay-ms (+ (rand delay-ms) (* 2.0 delay-ms)) :tries remaining-tries :exception e} fun))))
     (do
       (log/error "Retry at limit, failing")
       (throw (:exception opts))))))

(defn- maybe-unzip-init-db [{:keys [unzip path] :as init-db}]
  (if unzip
    (let [tmp-dir (fs/create-temp-dir)]
      (iou/unzip-file (fs/file path) (fs/file tmp-dir))
      (assoc init-db
             :path (str tmp-dir)
             :unzip false))
    init-db))

(defn- locate-single-file [path]
  (if (fs/regular-file? path)
    path
    (let [candidates (->> path
                          fs/file
                          file-seq
                          (into [] (filter fs/regular-file?)))]
      (when (= 1 (count candidates))
        (first candidates)))))

(defn- guess-db-init-nippy-file-type [path]
  (when-let [path (locate-single-file path)]
    (when-let [ext (fs/extension path)]
      (when (= "nippy" (str/lower-case ext))
        (cond (fs/regular-file? path) :nippy
              (fs/regular-file? (io/resource path)) :nippy-jar
              :else nil)))))

(defn- guess-db-init-type [{:keys [path] :as init-db}]
  (or (guess-db-init-nippy-file-type path)
      (when (and (fs/directory? path)
                 (some #(= "db.log" (fs/file-name %))
                       (file-seq (fs/file path))))
        :datomic)
      (throw (ex-info "Cannot guess db init type" init-db))))

(defn complete-init-db-with-type [init-db]
  (if (:type init-db)
    init-db
    (assoc init-db :type (guess-db-init-type init-db))))

(defn- normalize-init-db [init-db]
  {:type (or (:type init-db)
             (:wanderung/type init-db))
   :path (or (:path init-db)
             (:filename init-db))
   :unzip (:unzip init-db false)})

(defn wanderung-config-from-db-cfg [{:keys [db-name] :as cfg} wtype]
  (merge cfg
         {:wanderung/type wtype}
         (if db-name {:name db-name} {})
         (if (= :datomic wtype)
           {:tx-opts {:max-transaction-size 100000}}
           {})))

(defn infer-datomic-system-and-name [{:keys [type path]}]
  {:pre [(keyword? type)
         (string? path)]}
  (when (= :datomic type)
    (let [infer-name (fn [root-path]
                       (when (and (fs/exists? root-path)
                                  (fs/directory? root-path))
                         (let [file-names (->> root-path
                                               fs/list-dir
                                               (map fs/file-name))]
                           (when (= 1 (count file-names))
                             (first file-names)))))]
      (when-let [system (infer-name path)]
        (when-let [db-name (infer-name (fs/path path system))]
          {:system system
           :name db-name})))))

(defn wanderung-config-from-init-db [{:keys [type path] :as init-db}]
  (case type
    :datomic (let [data (infer-datomic-system-and-name init-db)]
               (when (nil? data)
                 (throw (ex-info "Failed to infer system and name for Datomic db"
                                 {:init-db init-db})))
               (merge data
                      {:wanderung/type :datomic
                       :server-type :datomic-local                       
                       :storage-dir path}))
    :nippy {:wanderung/type :nippy
            :filename (str (or (locate-single-file path)
                               (throw (ex-info "Failed to locate single file"
                                               {:path path}))))}
    :nippy-jar {:wanderung/type :nippy-jar
                :filename path}))

(defn db-exists? [{:keys [type cfg]}]
  {:pre [(keyword? type)
         (map? cfg)]}
  (case type
    :datomic
    (let [db-name (:db-name cfg)
          dbs (-> cfg datomic/client (datomic/list-databases {:limit -1}))]
      (assert (string? db-name))
      (some #{db-name} dbs))

    :datahike
    (datahike/database-exists? cfg)))

(def memoized-datomic-client (memoize datomic/client))

(defn get-datomic-client [cfg]
  (log/info "Acquiring Datomic client" (c/get-backend cfg))
  (let [client (retry (fn retry-client []
                        (memoized-datomic-client (:cfg (c/get-backend cfg)))))]
    (log/info "Succeeded in acquiring datomic client")
    client))

(defn create-database [cfg]
  {:pre [(c/valid-config? cfg)]}
  (if (c/->can-create? cfg)
    (let [backend-cfg (c/->backend-cfg cfg)]
      (case (c/->backend-type cfg)
        :datomic (datomic/create-database (get-datomic-client cfg) backend-cfg)
        :datahike (datahike/create-database backend-cfg)
        (throw (ex-info "Unknown database backend" {:backend (c/get-backend cfg)})))
      (log/info "`create-database` successful" (c/->backend-name cfg))
      true)
    (throw (ex-info "`create-database` was called with `:allow-create` false"
                    {:backend (c/get-backend cfg)}))))

(defn- validate-system-name-map [x]
  (if (malli/validate [:map [:system :string] [:name :string]] x)
    x
    (throw (AssertionError. "Invalid system-name-map"))))

(defn load-from-init-db [cfg]
  (let [backend-cfg (c/get-backend cfg)
        init-db (-> backend-cfg
                    :init-db
                    normalize-init-db
                    maybe-unzip-init-db
                    complete-init-db-with-type)
        dst-type (c/->backend-type cfg)
        dst-storage-dir (c/->backend-local-storage-dir cfg)
        db-cfg (:cfg backend-cfg)
        direct-copy? (and (= :datomic dst-type)
                          (when-let [inferred
                                     (infer-datomic-system-and-name
                                      init-db)]
                            (= (validate-system-name-map inferred)
                               (validate-system-name-map
                                {:system (:system db-cfg)
                                 :name (:db-name db-cfg)})))
                          (string? dst-storage-dir))]
    (assert (malli/validate c/db-init-schema init-db))
    (if direct-copy?
      (iou/non-merging-copy-directory-contents (:path init-db)
                                               dst-storage-dir)
      (let [src-cfg (wanderung-config-from-init-db init-db)
            dst-cfg (wanderung-config-from-db-cfg db-cfg dst-type)]
        (when-not (db-exists? backend-cfg)
          (log/info "Create database for" backend-cfg)
          (create-database cfg))
        (log/info "Wanderung import from" src-cfg "to" dst-cfg)
        (wanderung/migrate src-cfg dst-cfg)
        (log/info "Wanderung import complete")
        cfg))))


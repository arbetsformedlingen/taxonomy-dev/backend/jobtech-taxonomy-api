(ns jobtech-taxonomy.api.db.legacy
  (:require [clojure.set :as set]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.routes.parameter-util :as pu]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.common.taxonomy :as types]))

(def concept-pull-pattern [:concept/id
                           :concept/type
                           :concept/preferred-label
                           :concept/definition
                           :concept.external-database.ams-taxonomy-67/id
                           :concept.external-standard/ssyk-code-2012
                           :concept.external-standard/lau-2-code-2015
                           :concept.external-standard/national-nuts-level-3-code-2019])

(def ^:private occupation-name-with-relations-schema
  [:vector
   [:map {:closed true}
    [:definition :string]
    [:preferred-label :string]
    [:deprecated-legacy-id :string]
    [:type :string]
    [:ssyk-code-2012 :string]
    [:ssyk-deprecated-legacy-id :string]
    [:id :string]
    [:occupation-field-id :string]
    [:ssyk-id :string]
    [:occupation-field-deprecated-legacy-id :string]]])

(defn read-occupation-name-with-relations-from-disk []
  (iou/read-edn-resource occupation-name-with-relations-schema "occupation-name-with-relations.edn"))

(def occupation-name-with-relations
  (memoize read-occupation-name-with-relations-from-disk))

(defn get-occupation-name-with-relations [_cfg]
  (occupation-name-with-relations))

(def get-concept-id-from-ssyk-2012-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?ssyk-2012
    :where
    [?c :concept/type "ssyk-level-4"]
    [?c :concept.external-standard/ssyk-code-2012 ?ssyk-2012]])

(def get-concept-id-from-municipality-code-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?code
    :where
    [?c :concept/type "municipality"]
    [?c :concept.external-standard/lau-2-code-2015 ?code]])

(def get-concept-id-from-swedish-region-code-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?code
    :where
    [?c :concept/type "region"]
    [?c :concept.external-standard/national-nuts-level-3-code-2019 ?code]])

(def get-legacy-concept-from-new-id-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?id
    :where
    [?c :concept/id ?id]])

(defn rename-database-keys [data]
  (set/rename-keys data {:concept.external-database.ams-taxonomy-67/id :concept/deprecated-legacy-id}))

(def ^:private legacy-id-concept-lookup-file-schema
  [:map-of :string
   [:map-of :string
    [:map {:closed true}
     [:concept/id :string]
     [:concept/type :string]
     [:concept/preferred-label :string]
     [:concept/definition :string]
     [:concept.external-database.ams-taxonomy-67/id :string]]]])

(defn read-legacy-id-concept-lookup-from-disk []
  (iou/read-edn-resource legacy-id-concept-lookup-file-schema "legacy-id-concept-lookup.edn"))

(def legacy-id-concept-lookup
  (memoize read-legacy-id-concept-lookup-from-disk))

(defn get-concept-id-from-legacy-id [_cfg legacy-id type]
  (rename-database-keys (get-in (legacy-id-concept-lookup) [type (str legacy-id)])))

(defn get-concept-id-from-ssyk-2012 [cfg ssyk-2012]
  (rename-database-keys (ffirst (db/q get-concept-id-from-ssyk-2012-query (db/get-db cfg 1) concept-pull-pattern ssyk-2012))))

(defn get-concept-id-from-municipality-code [cfg code]
  (rename-database-keys (ffirst (db/q get-concept-id-from-municipality-code-query (db/get-db cfg 1) concept-pull-pattern code))))

(defn get-concept-id-from-swedish-region-code [cfg code]
  (rename-database-keys (ffirst (db/q get-concept-id-from-swedish-region-code-query (db/get-db cfg 1) concept-pull-pattern code))))

(defn get-concept-id-from-matching-component-id [cfg matching-component-id type]
  (case type
    "ssyk-level-4" (get-concept-id-from-ssyk-2012 cfg matching-component-id)
    "occupation-group" (get-concept-id-from-ssyk-2012 cfg matching-component-id)
    "municipality" (get-concept-id-from-municipality-code cfg matching-component-id)
    "region" (get-concept-id-from-swedish-region-code cfg matching-component-id)
    (get-concept-id-from-legacy-id cfg matching-component-id type)))

(defn get-legacy-concept-from-new-id [cfg id]
  (rename-database-keys (ffirst (db/q get-legacy-concept-from-new-id-query (db/get-db cfg 1) concept-pull-pattern id))))

(def responses
  (merge (types/response200 types/legacy-concept) types/response404))

(def concepts-types
  (types/par #{"continent",
               "country",
               "driving-licence",
               "employment-duration",
               "employment-type",
               "language",
               "language-level",
               "municipality",
               "occupation-collection",
               "occupation-field",
               "occupation-name",
               "region",
               "skill",
               "skill-headline",
               "sni-level-1",
               "sni-level-2",
               "ssyk-level-4",
               "wage-type",
               "worktime-extent"}
             "concept type"))

(defn handle-result-with-error [result error-message]
  (if result
    {:status 200 :body (types/map->nsmap result)}
    {:status 404 :body (types/map->nsmap {:error error-message})}))

(defn handle-result
  ([result id]
   (handle-result-with-error result (str "No new id found for " id)))
  ([result id type]
   (handle-result-with-error result (str "No new id found for " id " with type " type))))

(defn convert-matching-component-id-to-new-id-handler [cfg request]
  (let [{:keys [matching-component-id type]} (pu/get-query-from-request request)
        result (get-concept-id-from-matching-component-id cfg matching-component-id type)]
    (handle-result result matching-component-id type)))

(defn convert-old-id-to-new-id-handler [cfg request]
  (let [{:keys [legacy-id type]} (pu/get-query-from-request request)
        result (get-concept-id-from-legacy-id cfg legacy-id type)]
    (handle-result result legacy-id type)))

(defn convert-ssyk-2012-code-to-new-id-handler [cfg request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        result (get-concept-id-from-ssyk-2012 cfg code)]
    (handle-result result code)))

(defn convert-municipality-code-to-new-id-handler [cfg request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        result (get-concept-id-from-municipality-code cfg code)]
    (handle-result result code)))

(defn convert-swedish-region-code-to-new-id [cfg request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        result (get-concept-id-from-swedish-region-code cfg code)]
    (handle-result result code)))

(defn convert-new-id-to-old-id-handler [cfg request]
  (let [{:keys [id]} (pu/get-query-from-request request)
        result (get-legacy-concept-from-new-id cfg id)]
    (handle-result result id)))

(def old-deprecated-legacy-education-levels
  [{:preferred_label "Eftergymnasial utbildning kortare än två år",
    :id "KBjB_gpS_ZJL",
    :deprecated_legacy_id_2 "5",
    :definition "Eftergymnasial utbildning kortare än två år",
    :deprecated_legacy_id "5",
    :sun_education_level_code_2000 "4"}
   {:preferred_label "Eftergymnasial utbildning två år eller längre",
    :id "ygjL_oek_A2F",
    :deprecated_legacy_id_2 "3",
    :definition "Eftergymnasial utbildning två år eller längre",
    :deprecated_legacy_id "6",
    :sun_education_level_code_2000 "5"}
   {:preferred_label "Forskarutbildning",
    :id "Rdyp_7BA_42S",
    :deprecated_legacy_id_2 "4",
    :definition "Forskarutbildning",
    :deprecated_legacy_id "7",
    :sun_education_level_code_2000 "6"}
   {:preferred_label "Förgymnasial utbildning 9 (10) år",
    :id "wyLL_K3a_HRC",
    :deprecated_legacy_id_2 "1",
    :definition "Förgymnasial utbildning 9 (10) år",
    :deprecated_legacy_id "3",
    :sun_education_level_code_2000 "2"}
   {:preferred_label "Förgymnasial utbildning kortare än 9 år",
    :id "m5ZS_gKU_d4v",
    :deprecated_legacy_id_2 "10",
    :definition "Förgymnasial utbildning kortare än 9 år",
    :deprecated_legacy_id "2",
    :sun_education_level_code_2000 "1"}
   {:preferred_label "Gymnasial utbildning",
    :id "GD66_5wp_MSh",
    :deprecated_legacy_id_2 "2",
    :definition "Gymnasial utbildning",
    :deprecated_legacy_id "4",
    :sun_education_level_code_2000 "3"}
   {:preferred_label "Saknar formell, grundläggande utbildning",
    :id "p7d8_c57_h7X",
    :deprecated_legacy_id_2 "9",
    :definition "Saknar formell, grundläggande utbildning",
    :deprecated_legacy_id "1",
    :sun_education_level_code_2000 "0"}])

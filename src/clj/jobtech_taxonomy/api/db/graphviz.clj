(ns jobtech-taxonomy.api.db.graphviz
  (:require [clojure.java.shell :as sh]
            [clojure.string :as str]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.graphql.relation :as graphql.relation]
            [jobtech-taxonomy.common.relation :as relation]))

(defn- sh! [& args]
  (let [ret (apply sh/sh args)]
    (if (zero? (:exit ret))
      (:out ret)
      (throw (ex-info (format "Command '%s' failed:%n%s"
                              (->> args (take-while string?) (str/join " "))
                              (:err ret))
                      (assoc ret :args args))))))

(defn edge-query
  "This query will only give an image of what is currently being used in the db, not all possible relations."
  [cfg version]
  (db/q {:query '{:find [?t1 (distinct ?rt) ?t2]
                  :keys [:from :relations :to]
                  :where [[?r :relation/type ?rt]
                          [?r :relation/concept-1 ?c1]
                          (not [?c1 :concept/deprecated true])
                          [?c1 :concept/type ?t1]
                          [?r :relation/concept-2 ?c2]
                          (not [?c2 :concept/deprecated true])
                          [?c2 :concept/type ?t2]]}
         :args [(db/get-db cfg version)]}))

(defn weird-fn
  "This takes a list of rows with a set of relations each.
   It keeps the first relation set unchanged and then adds
   the rest of them inverted. Not sure why."
  [rows]
  (let [sorted (sort-by #(-> % :relations count -) rows)
        [{:keys [from relations to]} inv] sorted
        relations (into #{}
                        (map graphql.relation/type->field-label)
                        (into relations
                              (map relation/relations)
                              (:relations inv)))]
    {:from from
     :relations relations
     :bi-directional (every? #(= % (relation/relations %)) relations)
     :to to}))

(defn edges-for-version [cfg version]
  (->> (edge-query cfg version)
       (group-by (comp set (juxt :from :to)))
       vals
       (map weird-fn)))

(defn edges->dot-data [edges]
  (->> edges
       (map
        (fn [{:keys [from relations bi-directional to]}]
          (str "  " (pr-str (str from)) " -> " (pr-str (str to))
               " [decorate=true, label="
               (->> relations
                    sort
                    (str/join "\n")
                    pr-str)
               (when bi-directional
                 (str ", dir=none, constraint=false, color=\"#96b0ff\", fontcolor=\"#96b0ff\""))
               "]")))
       (str/join "\n")
       (format "
       digraph TaxonomyTypes {
         label=\"Taxonomy Types and Relations\"
         rankdir=RL
         graph [fontname=\"Arial\",
                labelloc=t,
                fontsize=30,
                fontcolor=\"#00005a\",
                ranksep=0.5]
         node [shape=box,
               style=\"rounded,filled\",
               color=\"#92eb42\",
               fontcolor=\"#00005a\",
               fontname=\"Arial\",
               margin=\"0.1,0.05\",
               height=0.02,
               width=0.02]
         edge [color=\"#00005a\",
               fontcolor=\"#00005a\",
               fontname=\"Arial\"]
       %s
       }")))

(defn types-svg [cfg {:keys [version]}]
  (let [version-edges (edges-for-version cfg version)
        dot-data (edges->dot-data version-edges)]
    (sh! "dot" "-Tsvg" :in dot-data)))
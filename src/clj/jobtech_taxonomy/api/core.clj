(ns jobtech-taxonomy.api.core
  (:require [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.features.mapping.setup :as mapping]
            [ring.adapter.jetty :as raj]
            [taoensso.timbre :as log])
  (:gen-class))

(set! *warn-on-reflection* true)

;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
 (reify Thread$UncaughtExceptionHandler
   (uncaughtException [_ thread ex]
     (log/error {:what :uncaught-exception
                 :exception ex
                 :where (str "Uncaught exception on " (.getName thread))}))))

(defn- host-link [port]
  (format "http://%s:%s" (.getHostName (java.net.InetAddress/getLocalHost)) port))

(defn start-http-server [cfg]
  (let [server-max-threads (config/->server-max-threads cfg)
        port (config/->server-port cfg)
        updated-cfg (-> cfg
                        dc/init-db
                        mapping/init)
        server (raj/run-jetty
                (handler/app updated-cfg)
                {:max-threads server-max-threads
                 :port port
                 :join? false})]
    (log/info "Started" (host-link port))
    server))

(defn ^:export stop-http-server [_cfg server]
  (.stop ^org.eclipse.jetty.server.Server server))

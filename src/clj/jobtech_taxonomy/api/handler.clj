(ns jobtech-taxonomy.api.handler
  (:require [expound.alpha :as expound]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.middleware.formats :as formats]
            [jobtech-taxonomy.api.middleware.misc
             :refer [http400-on-empty-params
                     log-request
                     trim-request-params
                     provide-build-info-response-headers]]
            [jobtech-taxonomy.api.routes.services :refer [service-routes]]
            [jobtech-taxonomy.features.documentation :as doc]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.dev.pretty :as pretty]
            [reitit.openapi :as openapi]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as coercion]
            [reitit.ring.middleware.dev :as dev]
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.multipart :as multipart]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.ring.spec :as spec]
            [spec-tools.spell :as spell]))

(defn handler [exception _request]
  {:status 500
   :description "Internal Server Error"
   :body {:message (str exception)}})

(defn middleware-stack [cfg]
  [;; request logging 
   log-request
   ;; Decorate response with build info
   (provide-build-info-response-headers cfg)
   ;; swagger feature
   openapi/openapi-feature
   ;; query-params & form-params
   parameters/parameters-middleware
   ;; trim input params
   trim-request-params
   ;; content-negotiation
   muuntaja/format-negotiate-middleware
   ;; encoding response body
   muuntaja/format-response-middleware
   ;; return http 400 for all request with empty parameters
   http400-on-empty-params
   ;; exception handling
   (exception/create-exception-middleware
    (merge
     exception/default-handlers
     {::exception/default handler
      ;; print stack-traces for all exceptions except for invalid user input
      ::exception/wrap
      (fn [fn-handler e request]
        (when-not (= :reitit.coercion/request-coercion (:type (ex-data e)))
          (expound/printer (:problems (ex-data e))))
        (fn-handler e request))}))
   ;; decoding request body
   muuntaja/format-request-middleware
   ;; coercing response bodys
   coercion/coerce-response-middleware
   ;; coercing request parameters
   coercion/coerce-request-middleware
   ;; multipart
   multipart/multipart-middleware])

(defn app [cfg]
  {:pre [(config/valid-config? cfg)]}
  (let [routes [(service-routes cfg)
                (doc/routes cfg)]]
    (ring/ring-handler
     (ring/router
      routes
      {:data {:reitit.middleware/transform dev/print-request-diffs ;; pretty diffs
              :validate spec/validate ;; enable spec validation for route data
              :reitit.spec/wrap spell/closed ;; strict top-level validation
              :exception pretty/exception
              :coercion reitit.coercion.spec/coercion
              :muuntaja formats/instance
              :middleware (middleware-stack cfg)}})
     (ring/routes
      (ring/create-resource-handler
       {:path "/"})

      (ring/create-default-handler)))))

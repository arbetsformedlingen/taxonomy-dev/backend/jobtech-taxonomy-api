(ns jobtech-taxonomy.api.config
  (:require [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.common.malli-utils :as mu]
            [malli.core :as malli]
            [taoensso.timbre :as log]))

(def ^:private default-config-file "config.edn")

(def datomic-cfg-schema
  [:map {:closed true}
   [:wanderung/type {:optional true} [:enum :nippy :datahike :nippy-jar]]
   [:db-name :string]
   [:server-type [:enum :ion :peer-server :datomic-local]]
   [:storage-dir {:optional true} [:or [:enum :mem] :string]]
   [:region {:optional true} :string]
   [:system :string]
   [:endpoint {:optional true} :string]
   [:proxy-port {:optional true} [:and :int [:> 0]]]
   [:access-key {:optional true} :string]
   [:secret {:optional true} :string]])

(def datahike-cfg-schema
  [:map {:closed true}
   [:store [:map
            [:backend [:enum :file :mem]]
            [:path {:optional true} :string]
            [:id {:optional true} :string]]]
   [:wanderung/type {:optional true} [:enum :nippy :datahike :nippy-jar]]
   [:attribute-refs? [:enum true]]
   [:index {:optional true} [:enum :datahike.index/persistent-set]]
   [:name :string]
   [:keep-history? {:optional true} :boolean]
   [:keep-history {:optional true} :boolean]
   [:schema-flexibility {:optional true} [:enum :write]]])

(def db-init-types [:enum :datomic :nippy :nippy-jar :datahike])

;; This is the preferred way of specifiying an init-db.
;; At the `:path` key, there can be a path to either a file or a directory.
;; If the `:unzip` key is true, it means that the the value at `:path` is a zip-file that needs to be unzipped.
(def db-init-schema0 [:map {:closed true}
                      [:type db-init-types]
                      [:path :string]
                      [:unzip {:optional true} :boolean]])

;; This is a deprecated way of specifying an init-db from Wanderung.
;; It is supported but not preferred.
(def db-init-schema-wanderung [:map {:closed true}
                               [:wanderung/type :keyword]
                               [:filename :string]])

;; This is a deprecated way of specifying an init-db that is unzipped from a file.
;; It is supported but not preferred.
(def db-init-schema-unknown-type [:map
                                  {:closed true}
                                  [:filename :string]
                                  [:unzip {:optional true} :boolean]])

(def db-init-schema [:or
                     db-init-schema0
                     db-init-schema-wanderung
                     db-init-schema-unknown-type])

(def backend-schema
  [:map {:closed true}
   [:allow-create {:optional true} :boolean]
   [:allow-delete {:optional true} :boolean]
   [:type [:enum :datahike :datomic]]
   [:init-db {:optional true} db-init-schema]
   [:cfg [:or datahike-cfg-schema datomic-cfg-schema]]])

(def backend-schema-with-id (conj backend-schema [:id :keyword]))

(def options-schema
  [:map {:closed true}
   [:port [:and :int [:> 0]]]
   [:jetty-threads {:optional true} [:and :int [:> 0]]]
   [:deployment-name {:optional true} :string]
   [:log-level {:optional true} [:enum :debug :info :warn :error]]])

(def jobtech-taxonomy-api-schema
  [:map
   [:auth-tokens any?]
   [:user-ids any?]
   [:esco-occupation-lookup-resource-filename {:optional true} :string]])

(def taxonomy-base-config-schema
  [:map
   [:path {:optional true} :string]
   [:options {:optional true} options-schema]
   [:jobtech-taxonomy-api {:optional true} jobtech-taxonomy-api-schema]])

(def taxonomy-multi-backend-config-schema
  (conj taxonomy-base-config-schema
        [:database-backend {:optional true} :keyword]
        [:backends [:vector {:min 1} backend-schema-with-id]]))

(def taxonomy-single-backend-config-schema
  (conj taxonomy-base-config-schema
        [:backend backend-schema]))

(def valid-config? (mu/throwing-validator-for taxonomy-single-backend-config-schema))

(defn conform
  "Takes a configuration on the deprecated multi-backend format or the simplified single-backend format and conforms it to the single backend format"
  [x]
  (condp malli/validate x
    taxonomy-single-backend-config-schema x
    taxonomy-multi-backend-config-schema (let [k (:database-backend x)
                                               backends (:backends x)
                                               backend (if k
                                                         (some #(when (= k (:id %)) %) backends)
                                                         (first backends))]
                                           (when (nil? backend)
                                             (throw (ex-info "No backend found in"
                                                             {:config x})))
                                           (-> x
                                               (dissoc :backends :database-backend)
                                               (assoc :backend (dissoc backend :id))))
    (throw (ex-info "Cannot conform config" {:config x}))))

(defn get-backend [cfg]
  (:backend cfg))
(defn ->backend-type [cfg]
  (-> cfg :backend :type))
(defn ->backend-cfg [cfg]
  (-> cfg :backend :cfg))
(defmulti ->backend-name
  "A descriptive name for the active backend generated using the config file."
  ->backend-type)
(defmethod ->backend-name :datomic [cfg]
  (let [backend (->backend-cfg cfg)]
    (format "datomic-%s-%s" (:system backend) (:db-name backend))))
(defmethod ->backend-name :datahike [cfg]
  (let [backend (->backend-cfg cfg)]
    (format "datahike-%s-%s" (:system backend "unknown") (:name backend "unknown"))))

(defn set-backend-storage-dir [cfg storage-dir]
  (update cfg :backend
          (fn [backend]
            (condp = (:type backend)
              :datomic (assoc-in backend [:cfg :storage-dir] storage-dir)
              :datahike (assoc-in backend [:cfg :store :path] storage-dir)
              (throw (ex-info "Unknown backend type" {:backend backend}))))))

(defn update-storage-dir-path [cfg relative-path]
  (let [full-path (iou/get-absolute-path relative-path)
        updated-cfg (set-backend-storage-dir cfg full-path)]
    (log/warn "Updating storage dir path to" full-path)
    updated-cfg))

(defmulti ->backend-local-storage-dir
  "The path to the local database files."
  ->backend-type)
(defmethod ->backend-local-storage-dir :datomic [cfg]
  (let [storage-dir (:storage-dir (->backend-cfg cfg))]
    (when (string? storage-dir)
      storage-dir)))
(defmethod ->backend-local-storage-dir :datahike [cfg]
  (let [storage-dir (get-in (->backend-cfg cfg) [:store :path])]
    (when (string? storage-dir)
      storage-dir)))

(defn ->can-delete?
  "Can this backend safely be deleted?"
  [cfg] (:allow-delete (get-backend cfg) false))

(defn ->can-create?
  "Can this backend safely be created?"
  [cfg] (:allow-create (get-backend cfg) false))

(defn ->server-port [cfg]
  (get-in cfg [:options :port]))

(defn ->server-max-threads [cfg]
  (let [backend (:options cfg)
        thread-count (:jetty-threads backend 16)]
    thread-count))

(defn get-module-data [cfg module-key]
  (get-in cfg [:modules module-key]))

(defn set-module-data [cfg module-key new-data]
  (assoc-in cfg [:modules module-key] new-data))

(defn update-module-data [cfg module-key f & args]
  (apply update-in cfg [:modules module-key] f args))

(defn- cli-flag->key [arg]
  (case arg
    ("--port" "-p") :port
    ("--config" "-c") :config
    arg))

(defn load-config-from-file [config-file]
  (when-let [config (iou/read-edn-file [:or
                                        taxonomy-multi-backend-config-schema
                                        taxonomy-single-backend-config-schema]
                                       config-file)]
    (-> config
        (assoc :path (str config-file))
        conform)))

(def ^:private parse-port (mu/coerce-string [:and :int [:> 0] [:< 65535]]))

(defn- key+cli-value->key+value [[key cli-value]]
  (if (nil? cli-value)
    (throw (ex-info "Missing value for key" {:key key}))
    [key (case key
           :port
           (try
             (parse-port cli-value)
             (catch Exception _e
               (throw (ex-info "Invalid port" {:key key :value cli-value}))))
           :config cli-value
           (throw (ex-info "Unknown key" {:errors {:key key :value cli-value}})))]))

(defn- validate-cli [args]
  (let [invalid-keys (->>
                      (take-nth 2 args)
                      frequencies
                      (filter (fn more-than-one [[_key count]] (> count 1)))
                      (mapv first))]
    (when (not-empty invalid-keys)
      {:errors {:key-count invalid-keys}})))

(defn parse-cli [args]
  (let [key-params (mapv cli-flag->key args)
        invalid-keys (validate-cli key-params)
        options {:options (into {}
                                (comp (partition-all 2)
                                      (map key+cli-value->key+value))
                                key-params)}]
    (merge options invalid-keys)))

(defn- decorate-config [cfg {:keys [server-port auth-tokens user-ids]}]
  (cond-> cfg
    (some? server-port) (assoc-in [:options :port] server-port)
    (coll? auth-tokens) (assoc-in [:jobtech-taxonomy-api :auth-tokens] auth-tokens)
    (coll? user-ids) (assoc-in [:jobtech-taxonomy-api :user-ids] user-ids)))

(defn decorate-config-from-env [cfg]
  (decorate-config
   cfg
   {:server-port (iou/get-env-var [:or :nil [:and :int [:> 0]]] "TAXONOMY_PORT")
    :auth-tokens (iou/get-env-var
                  [:or :nil [:map-of :keyword :keyword]]
                  "JOBTECH_TAXONOMY_API__AUTH_TOKENS")
    :user-ids (iou/get-env-var
               [:or :nil [:map-of :keyword :string]]
               "JOBTECH_TAXONOMY_API__USER_IDS")}))

(defn get-config [args]
  (let [{:keys [config port]} (-> args parse-cli :options)]
    (-> (or config
            (iou/get-env-var [:or :nil :string] "TAXONOMY_CONFIG_PATH")
            default-config-file)
        load-config-from-file
        decorate-config-from-env
        (decorate-config {:server-port port})
        valid-config?)))

(ns jobtech-taxonomy.api.middleware.misc
  (:require
   [clojure.string :as str]
   [metrics.core :refer [new-registry]]
   [metrics.counters :as metrics]
   [ring.util.http-response :as resp]
   [jobtech-taxonomy.api.db.database-connection :as dc]
   [jobtech-taxonomy.api.routes.parameter-util :as pu]
   [jobtech-taxonomy.common.build-info :as build-info]
   [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)

(defn now []
  (new java.util.Date))

(def reg (new-registry))

;; Create a variable that keeps track of the number of parallel requests
(def concurrent-requests
  (metrics/counter reg "concurrent-requests"))

(defn log-request [handler]
  (fn [request]
    (let [timeStart (now)
          connections (metrics/value (metrics/inc! concurrent-requests))
          response (handler request)
          status (get response :status)
          api-key (get-in request [:headers "api-key"])
          referrer (get-in request [:headers "referer"])
          from (get-in request [:headers "from"])
          req-str (get request :query-string)
          req-url (get request :uri)
          _ (metrics/dec! concurrent-requests)
          timeStop (now)
          requestTime (- (.getTime ^java.util.Date timeStop) (.getTime ^java.util.Date timeStart))
          _ (log/info
             (str "http " status ", " requestTime " ms, #" connections ", request: " req-url
                  (when req-str (str "?" req-str))
                  (if referrer (str " referer: " referrer) "")
                  (if from (str " from: " from) "")
                  (if api-key (str " api-key: " api-key) "")))]
      response)))

(defn multi-trim [qp]
  (cond
    (string? qp) (str/trim qp)
    (vector? qp) (mapv str/trim qp)
    :else
    qp))

(defn trim-request-params [handler]
  (fn trim-middleware [request]
    (let [req (reduce
               #(update-in %1 [:query-params %2] multi-trim)
               request
               (keys (get request :query-params)))]
      (handler req))))

(defn http400-on-empty-params [handler]
  (fn [request]
    (let [params (get request :query-params)
          badParams (map first (filter (fn [[_key value]] (empty? value)) params))]
      (if (empty? badParams)
        (handler request)
        (resp/bad-request
         {:error (apply str "Bad Request. The following parameter values cannot be empty; " (interpose ", " badParams))})))))

(defn try-get-latest-version [cfg]
  (-> cfg
      dc/connect
      dc/db
      dc/get-latest-released-version
      str
      (try (catch Exception _ nil))))

(defn provide-build-info-response-headers [cfg]
  (let [extra-header-data {"X-build-info-commit" build-info/commit
                           "X-build-info-timestamp" build-info/build-timestamp}]
    (fn [handler]
      (fn [request]
        (let [include-debug? (pu/read-boolean-header
                              (:headers request)
                              "x-include-response-debug-headers")
              response (handler request)]
          (if include-debug?
            (-> response
                (update :headers merge
                        extra-header-data
                        (if-let [v (try-get-latest-version cfg)]
                          {"X-latest-taxonomy-version" v}
                          {})))
            response))))))

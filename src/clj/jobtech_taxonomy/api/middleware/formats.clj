(ns jobtech-taxonomy.api.middleware.formats
  (:require
   [cognitect.transit :as transit]
   [java-time.api :as jt]
   [muuntaja.core :as m]))

(def ^:private iso-local-time "HH:mm:ss.SSS")
(def ^:private iso-local-date "yyyy-MM-dd")
(def ^:private iso-local-date-time "yyyy-MM-dd'T'HH:mm:ss.SSS")
(def ^:private iso-zoned-date-time "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
(def ^:private iso-year-month "yyyy-MM")

(def ^:private time-deserialization-handlers
  {:handlers
   {"LocalTime" (transit/read-handler #(jt/local-time iso-local-time %))
    "LocalDate" (transit/read-handler #(jt/local-date iso-local-date %))
    "LocalDateTime" (transit/read-handler #(jt/local-date-time iso-local-date-time %))
    "ZonedDateTime" (transit/read-handler #(jt/zoned-date-time iso-zoned-date-time %))
    "YearMonth" (transit/read-handler #(jt/year-month iso-year-month %))}})

(def ^:private time-serialization-handlers
  {:handlers
   {java.time.LocalTime (transit/write-handler
                         (constantly "LocalTime")
                         #(jt/format iso-local-time %))
    java.time.LocalDate (transit/write-handler
                         (constantly "LocalDate")
                         #(jt/format iso-local-date %))
    java.time.LocalDateTime (transit/write-handler
                             (constantly "LocalDateTime")
                             #(jt/format iso-local-date-time %))
    java.time.ZonedDateTime (transit/write-handler
                             (constantly "ZonedDateTime")
                             #(jt/format iso-zoned-date-time %))
    java.time.YearMonth (transit/write-handler
                         (constantly "YearMonth")
                         #(jt/format iso-year-month %))}})

(def instance
  (m/create
   (-> m/default-options
       (update-in
        [:formats "application/transit+json" :decoder-opts]
        (partial merge time-deserialization-handlers))
       (update-in
        [:formats "application/transit+json" :encoder-opts]
        (partial merge time-serialization-handlers))
       (assoc-in
        [:formats "application/json" :encoder-opts :date-format]
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))))

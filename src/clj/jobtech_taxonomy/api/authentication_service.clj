(ns jobtech-taxonomy.api.authentication-service)

(defn get-user-id-from-api-key [cfg api-key]
  (get-in cfg [:jobtech-taxonomy-api :user-ids (keyword api-key)]))

(defn authenticate-admin [cfg api-key]
  (= :admin
     (get-in cfg
             [:jobtech-taxonomy-api
              :auth-tokens
              (keyword api-key)])))

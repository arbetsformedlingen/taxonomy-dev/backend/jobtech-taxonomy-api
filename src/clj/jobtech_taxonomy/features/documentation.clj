(ns jobtech-taxonomy.features.documentation
  (:require [jobtech-taxonomy.api.db.graphviz :as graphviz]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]
            [jobtech-taxonomy.features.private :as private]
            [reitit.swagger-ui :as swagger-ui]
            [ring.util.http-response :as resp]
            [ring.util.response :as response]
            [spec-tools.data-spec :as ds]))

(defn- graphviz-endpoint [cfg]
  ["/graphviz"
   {:tags ["Graphviz"]}
   ["/types.svg"
    {:parameters {:query {(ds/opt :version) taxonomy/version-param}}
     :middleware [(private/private-version-middleware cfg)]
     :get {:no-doc true
           :responses {200 {:body string?
                            :content {"image/svg+xml" {}}
                            :description "OK"}}
           :handler (fn [{{{:keys [version]
                            :or {version :latest}} :query} :parameters}]
                      {:status 200
                       :headers {"Content-Type" "image/svg+xml"}
                       :body (graphviz/types-svg cfg {:version version})})}}]])

(defn- graphiql-endpoint []
  ["/graphiql"
   ["" {:get
        {:no-doc true
         :handler (fn [_] (response/url-response (iou/raw-resource "graphiql.html")))}}]
   [".json" {:get
             {:no-doc true
              :handler (constantly (resp/ok {:url "/v1/taxonomy/graphql"}))}}]])

(defn- swagger-endpoint []
  ["/swagger-ui*"
   {:get {:no-doc true
          :handler (swagger-ui/create-swagger-ui-handler
                    {:url "/v1/taxonomy/openapi.json"
                     :config {:validatorUrl nil
                              :operationsSorter "alpha"}})}}])

(defn routes [cfg]
  ["/v1/taxonomy" [(graphviz-endpoint cfg)
                   (swagger-endpoint)
                   (graphiql-endpoint)]])

(ns jobtech-taxonomy.features.mapping.mapping
  (:require [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.common :as common]
            [jobtech-taxonomy.features.mapping.nodes :as nodes]
            [malli.core :as m]
            [malli.error :as me]
            [taoensso.timbre :as log]))

(def schema
  "A mapping has a mapping type and some other fields
   that can be useful in describing the map."
  (iou/read-edn-resource common/db-schema-schema "jobtech_taxonomy/features/mapping/mapping-schema.edn"))

(defn- to-malli-with-optional [entry]
  (let [ident (:db/ident entry)]
    (if (= ident :mapping.mapping/type)
      (common/db-schema->malli entry)
      (common/db-schema->malli entry [:optional true]))))

(def ^:private mapping+external-schema
  (concat schema nodes/external-schema))

(def malli-schema
  (into [:map {:closed true} [:db/id {:optional true} [:or :int :string]]]
        (map to-malli-with-optional mapping+external-schema)))

(def validate-mapping (m/validator malli-schema))

(defn explain-mapping-error [mapping] (me/humanize (m/explain malli-schema mapping)))

(def malli-query-schema
  (into [:map {:closed true} [:db/id {:optional true} [:or :int :string]]]
        (map #(common/db-schema->malli % [:optional true]) mapping+external-schema)))

(def validate-mapping-query (m/validator malli-query-schema))

(defn explain-mapping-query-error [mapping]
  (me/humanize (m/explain malli-query-schema mapping)))

(defn- drop-namespace [e]
  (let [ident (:db/ident e)]
    [ident (keyword (name ident))]))

(def ^:private long->short-names
  (into {} (map drop-namespace) schema))

(def ^:private short-names
  (into #{} (vals long->short-names)))

(defn mapping->short-names [mapping]
  (into {} (keep (fn [[k v]] [(k long->short-names k) v])) mapping))

(defn mapping->short-names-only [mapping]
  (into {} (keep (fn [[k v]]
                   (when-let [short-k (or (long->short-names k)
                                          (short-names k))]
                     [short-k v]))) mapping))

(def ^:private short->long-names
  (into {} (map (fn [[k v]] [v k])) long->short-names))

(defn mapping->long-names [mapping]
  (into {} (keep (fn [[k v]] [(k short->long-names k) v])) mapping))

(def sort-order
  [:organisation
   :family
   :name
   :version
   :type
   :taxonomy-version])

(defn mapping->sort-order-vector [mapping]
  (let [snm (mapping->short-names mapping)]
    (into [] (keep snm) sort-order)))

(defn validate->query [query]
  (cond
    (not (validate-mapping-query query))
    {:result :error/invalid-query :error (explain-mapping-query-error query)}
    (:db/id query)
    {:result :ok/specific :ns-query query}
    (empty? query)
    {:result :ok/all :ns-query {}}
    :else
    {:result :ok/filter :ns-query query}))

(def ^:private cardinality
  (into {} (map #(vector (:db/ident %) (:db/cardinality %)) schema)))

(defn- cardinality-eq [ident]
  (log/trace :cardinality-eq ident)
  (let [cardinality (cardinality ident)]
    (if (= :db.cardinality/many cardinality)
      (fn cardinality-many-match [source targets]
        (let [source-set (if (coll? source) (set source) #{source})
              target-set (set targets)]
          (every? #(target-set %) source-set)))
      ;; This return the equality function. It is _not_ a smiley face.
      =)))

(defn- query-component-matches [query mapping-data]
  (let [comparison (fn [query-ident value]
                     (condp (cardinality-eq query-ident) (query-ident mapping-data)
                       value :present
                       nil :missing
                       :mismatch))]
    (reduce-kv
     (fn [acc query-ident query-value]
       (update acc (comparison query-ident query-value) #(conj % query-ident)))
     {:present []
      :missing []
      :mismatch []}
     query)))

(defn- mappings-all-raw [db]
  (into #{} cat (dc/q '[:find (pull ?mapping [*]) :where [?mapping :mapping.mapping/type _]] db)))

(defn- mappings-all [db]
  (let [all-mappings (mappings-all-raw db)]
    (log/trace :mappings/all (map mapping->sort-order-vector all-mappings))
    {:result :ok/all
     :mappings all-mappings}))

(defn- mappings-filter [db query]
  (log/debug :mappings/filter :query query)
  (let [filtered-mappings
        (into #{} (filter
                   (fn [mapping]
                     (let [qcmr (query-component-matches query mapping)]
                       (and (empty? (:missing qcmr))
                            (empty? (:mismatch qcmr))))))
              (mappings-all-raw db))]
    (log/debug :mappings/filter (mapping->sort-order-vector query)
               :found (map mapping->sort-order-vector filtered-mappings))
    {:result :ok/filter
     :mappings filtered-mappings}))

(defn- mappings-specific [db query]
  (let [db-id (:db/id query)
        mapping (dc/pull db '[*] db-id)
        match (query-component-matches query mapping)]
    (log/debug :mappings/filter (mapping->sort-order-vector query) :found match)
    (if (and (validate-mapping mapping)
             (empty? (:missing match))
             (empty? (:mismatch match)))
      {:result :ok/specific
       :mappings #{mapping}}
      {:result :error/specific
       :query query})))

(defn mappings
  "Finds and fetches mappings with attached data in the database.
   * `[db]` returns all entities in `db` with the `:mapping.mapping/type` attribute set.
   * `[db {:db/id eid}]` returns the entity with ID `eid`, if it has a `:mapping.mapping/type` attribute.
   * `[db query]` returns all entities with `:mapping.mapping/type` that also has attributes with corresponding values to those in `query`."
  ([db query]
   (let [long-name-query (mapping->long-names query)]
     (if (not (validate-mapping-query long-name-query))
       {:result :error/invalid-query
        :error (explain-mapping-query-error long-name-query)}
       (let [result (cond
                      (:db/id long-name-query)
                      (mappings-specific db long-name-query)
                      (empty? long-name-query)
                      (mappings-all db)
                      :else
                      (mappings-filter db long-name-query))]
         (log/trace :mapping (:result result)
                    (mapping->sort-order-vector query)
                    (map mapping->sort-order-vector (:mappings result)))
         result)))))

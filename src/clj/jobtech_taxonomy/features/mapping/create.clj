(ns jobtech-taxonomy.features.mapping.create
  (:require [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.common :as common]
            [jobtech-taxonomy.features.mapping.config :as config]
            [jobtech-taxonomy.features.mapping.edge :as edge]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [jobtech-taxonomy.features.mapping.nodes :as nodes]
            [malli.core :as m]
            [malli.error :as me]
            [taoensso.timbre :as log]))

(defn result?
  [result-type result]
  (= result-type (:result result)))

(defn error? [result]
  (= "error" (namespace (:result result))))

(defn add-edges [conn config edge-set]
  (let [{:keys [missing found edges]} (nodes/validate (dc/db conn) config edge-set)

        add-all (edge/add-edges conn (config/->mapping-id config) edges)]
    (log/debug :create/add-edges :missing missing)
    (log/debug :create/add-edges :found found)
    add-all))

(defn mapping
  "Creates an empty mapping and returns it with the assigned `:db/id`"
  [conn mapping-description config-description extra-nodes-description]
  (log/debug :mapping/create-mapping :config-description config-description)
  (let [ns-mapping
        (mapping/validate->query
         (mapping/mapping->long-names
          mapping-description))]
    (if (error? ns-mapping)
      ns-mapping
      (let [mapping-data (:ns-query ns-mapping)
            tx-data-config (config/->tx-data
                            config-description
                            extra-nodes-description)
            new-mapping (merge {:mapping.mapping/type :default}
                               tx-data-config
                               mapping-data
                               {:db/id "new-mapping"})
            txr (dc/transact conn {:tx-data [new-mapping]})
            mapping (dc/pull (:db-after txr) '[*] ((:tempids txr) "new-mapping"))
            config (config/mapping->config (:db-after txr) mapping)]
        {:result :ok/created-new
         :config config
         :mapping mapping}))))

(defn- find-or-create-mapping [conn
                               mapping-description
                               config-description
                               extra-nodes-description]
  (let [find-mapping-result (mapping/mappings (dc/db conn) mapping-description)
        first-found-mapping (first (:mappings find-mapping-result))]
    (log/debug :mapping/focm find-mapping-result)
    (cond
      (error? find-mapping-result)
      find-mapping-result

      (result? :ok/specific find-mapping-result)
      {:result :ok/specific
       :config (config/mapping->config (dc/db conn) first-found-mapping)
       :mapping first-found-mapping}

      (and (result? :ok/filter find-mapping-result)
           (empty? (:mappings find-mapping-result)))

      (mapping
       conn
       mapping-description
       config-description
       extra-nodes-description)

      :else
      {:result :error/mappings-found
       :mappings (:mappings find-mapping-result)})))

(defn created->id [create-result]
  (get-in create-result [:mapping :db/id]))

(defn- to-malli-with-optional [entry]
  (let [ident (:db/ident entry)]
    (if (= ident :mapping.mapping/type)
      (common/db-schema->malli entry)
      (common/db-schema->malli entry [:optional true]))))

(def mapping-schema-with-short-ident
  (map #(update % :db/ident common/drop-keyword-ns) mapping/schema))

(def mapping+external-schema
  (concat mapping-schema-with-short-ident nodes/external-schema))

(def mapping-schema
  (into [:map {:closed true}
         [:db/id {:optional true} [:or :int :string]]]
        (map to-malli-with-optional mapping+external-schema)))

(def node-db-temp-id-ref-schema
  [:map {:closed true}
   [:db/id [:or :string pos-int?]]])

(def node-target-resolved-ref-schema
  [:or :string :int])

(def node-ref-schema
  [:or
   node-target-resolved-ref-schema
   node-db-temp-id-ref-schema])

(def node-schema
  [:map {:closed true}
   [:id {:optional true} :keyword]
   [:include {:optional true} [:map]]])

(def config-malli-schema
  [:map {:closed true}
   [:source {:optional true} node-schema]
   [:target {:optional true}
    (into node-schema [[:mapping {:optional true} :keyword]])]])

(def external-malli-schema
  (into
   [:map {:closed true}
    [:db/id [:or :string pos-int?]]]
   (map to-malli-with-optional nodes/external-schema)))

(def mapping-definition-schema
  [:map {:closed true}
   [:mapping mapping-schema]
   [:config {:optional true} config-malli-schema]
   [:data {:optional true}
    [:map
     [:sources {:optional true} [:vector external-malli-schema]]
     [:targets {:optional true} [:vector external-malli-schema]]
     [:edges [:vector [:tuple node-ref-schema node-ref-schema]]]]]])

(def mapping-edn-schema
  [:or mapping-definition-schema
   [:vector mapping-definition-schema]])

(def validate-mapping-edn
  (m/validator mapping-edn-schema))

(defn explain-mapping-edn-error [mapping]
  (me/humanize (m/explain mapping-edn-schema mapping)))

(defn- add-and-report [conn config raw-nodes raw-edges report]
  (let [{:keys [db tempids nodes]} (if (seq raw-nodes)
                                     (nodes/populate conn raw-nodes)
                                     {:db (dc/db conn)})
        edge-set (nodes/apply-tempids config tempids raw-edges)
        {:keys [missing edges]} (nodes/validate db config edge-set)
        {:keys [existing updated created]} (edge/add-edges conn (config/->mapping-id config) edges)]
    (condp = report
      :full
      {:targets {:created nodes}
       :edges {:missing missing
               :created created
               :existing existing
               :updated updated}}
      :condensed
      {:targets {:created (count nodes)}
       :edges {:missing missing
               :created (count created)
               :existing (count existing)
               :updated (count updated)}}
      :else {})))

(defn create
  "Create or update a mapping.
   For update to trigger the mapping data must contain a `:db/id`that
   will match the rest of the provided attributes."
  ([conn mapping-edn] (create conn mapping-edn :condensed))
  ([conn mapping-edn report]
   (if (validate-mapping-edn mapping-edn)
     (let [foc-result (find-or-create-mapping
                       conn
                       (:mapping mapping-edn)
                       (:config mapping-edn)
                       (get-in mapping-edn [:data :targets]))]
       (if (error? foc-result)
         (do (log/error :mapping/create :foc-error (:result foc-result))
             foc-result)
         (conj (dissoc foc-result :config)
               (add-and-report conn (:config foc-result)
                               (get-in mapping-edn [:data :targets])
                               (get-in mapping-edn [:data :edges])
                               report))))
     (explain-mapping-edn-error mapping-edn))))
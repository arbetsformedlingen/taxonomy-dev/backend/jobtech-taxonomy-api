(ns jobtech-taxonomy.features.mapping.edge
  (:require [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.common :as common]
            [jobtech-taxonomy.features.mapping.config :as config]
            [taoensso.timbre :as log]))

(def schema
  "An edge record has a source, a target and
   one or more mappings."
  (iou/read-edn-resource common/db-schema-schema "jobtech_taxonomy/features/mapping/edge-schema.edn"))

(defn- update-edge-names-for [config]
  (let [source-renamer (config/->rename-fn config :source)
        target-renamer (config/->rename-fn config :target)
        mapped-id-fn (config/->mapped-id-fn config)]
    (fn update-names [edge]
      (-> edge
          (assoc :mapped-id (mapped-id-fn (:target edge)))
          (update :source source-renamer)
          (update :target target-renamer)))))

(defn edges-with-source-for-mapping [db config sources]
  (let [source-attr (config/->id-attr config :source)
        query-result (dc/q '[:find (pull ?edge [:db/id
                                                :mapping.edge/source
                                                :mapping.edge/target
                                                :mapping.edge/mapping])
                             (pull ?source source-pull)
                             (pull ?target target-pull)
                             :keys edge source target
                             :in $ ?mapping ?source-attr source-pull target-pull [?source-val ...]
                             :where
                             [?edge :mapping.edge/mapping ?mapping]
                             [?source ?source-attr ?source-val]
                             [?edge :mapping.edge/source ?source]
                             [?edge :mapping.edge/target ?target]]
                           db (config/->mapping-id config)
                           source-attr
                           (config/->pull config :source)
                           (config/->pull config :target)
                           sources)
        found (into #{} (map (comp #(dissoc % :edge) (update-edge-names-for config))) query-result)
        found-sources (into #{} (map (comp source-attr :source)) query-result)
        missing (into #{} (filter (comp not found-sources)) sources)]
    {:found found
     :missing missing}))

(defn- query-mapping [db mapping sources]
  (let [config (config/mapping->config db mapping)
        result (edges-with-source-for-mapping db config sources)]
    (assoc result :mapping mapping)))

(defn query [db mappings sources]
  (let [source-query-fn (fn [mapping] (query-mapping db mapping sources))]
    (map source-query-fn mappings)))

(defn- db-id [e]
  (cond
    (map? e) (:db/id e)
    (coll? e) (into #{} (map db-id) e)
    :else e))

(defn- update-edge-ids [edge]
  (-> (update edge :db/id db-id)
      (update :mapping.edge/source db-id)
      (update :mapping.edge/target db-id)
      (update :mapping.edge/mapping db-id)))

(defn- edge-status-for [mapping-id]
  (fn [{:keys [edge]}]
    (let [mappings (db-id (:mapping.edge/mapping edge))
          mapping-status (if (mappings mapping-id) :member :edge)]
      (-> (update-edge-ids edge)
          (assoc :mapping mapping-status)))))

(defn edge->source-id+target-id [edge]
  [(:mapping.edge/source edge) (:mapping.edge/target edge)])

(defn edges-between
  ([db mapping-id source-id+target-id-set]
   (log/debug :edge/edges-between :mapping-id mapping-id :edges source-id+target-id-set)
   (let [found (into #{}
                     (map (edge-status-for mapping-id))
                     (dc/q '[:find (pull ?edge [:db/id
                                                :mapping.edge/source
                                                :mapping.edge/target
                                                :mapping.edge/mapping])
                             :keys edge
                             :in $ [[?source ?target] ...]
                             :where
                             [?edge :mapping.edge/source ?source]
                             [?edge :mapping.edge/target ?target]]
                           db source-id+target-id-set))

         missing (into #{} (filter (comp not (into #{} (map edge->source-id+target-id) found))
                                   source-id+target-id-set))
         result (group-by :mapping found)]
     {:members (into #{} (map #(dissoc % :mapping)) (result :member))
      :edges (into #{} (map #(dissoc % :mapping)) (result :edge))
      :missing missing})))

(defn- create-edges-set [conn mapping-id source-id+target-id-set]
  (let [edge-records (map (fn edge-tx-data [[source target]]
                            {:db/id (str "edge=" source "->" target ".")
                             :mapping.edge/source source
                             :mapping.edge/target target
                             :mapping.edge/mapping mapping-id})
                          source-id+target-id-set)
        txr (dc/transact conn {:tx-data edge-records})
        tempids (:tempids txr)
        edges (into #{} (map (fn created-with-id [edge]
                               (-> (update edge :db/id #(tempids %))
                                   (update :mapping.edge/mapping (fn [e] #{e})))))
                    edge-records)]
    edges))

(defn- update-edges-set [conn mapping-id edges]
  (let [edge-ids (map :db/id edges)
        db (:db-after
            (dc/transact
             conn
             {:tx-data
              (map (fn [id]
                     {:db/id id
                      :mapping.edge/mapping mapping-id})
                   edge-ids)}))]
    (into #{}
          (map (edge-status-for mapping-id))
          (dc/q '[:find (pull ?edge [:db/id
                                     :mapping.edge/source
                                     :mapping.edge/target
                                     :mapping.edge/mapping])
                  :keys edge
                  :in $ ?mapping [?edge ...]
                  :where
                  [?edge :mapping.edge/mapping ?mapping]]
                db mapping-id edge-ids))))

(defn add-edges [conn mapping-id source-id+target-id-set]
  (let [{:keys [members edges missing]}
        (edges-between (dc/db conn) mapping-id source-id+target-id-set)
        updated-edges (update-edges-set conn mapping-id edges)
        created-edges (create-edges-set conn mapping-id missing)]
    (log/debug :edges/add-edges :mapping-id mapping-id)
    (log/debug :edges/add-edges :members members)
    (log/debug :edges/add-edges :edges edges)
    (log/debug :edges/add-edges :missing missing)
    {:existing (into #{} (map #(dissoc % :mapping)) members)
     :updated (into #{} (map #(dissoc % :mapping)) updated-edges)
     :created created-edges}))

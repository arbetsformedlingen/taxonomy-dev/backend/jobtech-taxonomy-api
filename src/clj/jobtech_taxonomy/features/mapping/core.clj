(ns jobtech-taxonomy.features.mapping.core
  (:require [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.create :as mc]
            [jobtech-taxonomy.features.mapping.edge :as edge]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [jobtech-taxonomy.features.mapping.nodes :as nodes]
            [jobtech-taxonomy.features.mapping.query :as query]
            [taoensso.timbre :as log]))

(defn mappings [db mapping-query]
  (query/mappings db mapping-query))

(defn translations [db mapping-query source-ids]
  (let [mappings-query-result (query/mappings db mapping-query)]
    (if-let [mappings (:mappings mappings-query-result)]
      (query/edges db mappings source-ids)
      mappings-query-result)))

(def schema
  (into [] cat
        [mapping/schema
         edge/schema
         nodes/external-schema]))

(def schema-idents
  (->> schema
       (map :db/ident)
       sort
       (into [])))

(defn- update-if [key]
  (fn [schema-entry]
    (if-let [value (or (:db/ident (schema-entry key))
                       (schema-entry key))]
      (assoc schema-entry key value)
      schema-entry)))

(defn get-installed-schema [cfg]
  (let [db (dc/db (dc/connect cfg))
        schema-result (dc/q '[:find (pull ?mapping
                                          [:db/cardinality
                                           :db/tupleAttrs
                                           :db/tupleTypes
                                           :db/unique
                                           :db/ident
                                           :db/doc
                                           :db/valueType])
                              :keys schema
                              :in $ [?ident ...]
                              :where
                              [?mapping :db/ident ?ident]]
                            db schema-idents)]
    (map (comp
          (update-if :db/unique)
          (update-if :db/valueType)
          (update-if :db/cardinality)
          :schema)
         schema-result)))

(defn schema-present? [cfg]
  (let [defined-schema (sort-by :db/ident schema)
        installed-schema (sort-by :db/ident
                                  (get-installed-schema cfg))]
    (or (= defined-schema installed-schema)
        (log/warn "Mapping schema not detected"))))

(defn install-schema [cfg]
  (dc/transact (dc/connect cfg) {:tx-data schema})
  (log/info :mapping.core/installed schema-idents))

(def mapping-schema
  [:map {:closed true}
   [:db/id {:optional true} [:and :int [:> 0]]]
   [:type :keyword]
   [:organisation {:optional true} :string]
   [:family {:optional true} :string]
   [:name {:optional true} :string]
   [:version {:optional true} :string]
   [:taxonomy-version {:optional true} :string]
   [:description {:optional true} :string]
   [:uri {:optional true} :string]])

(def config-schema
  [:map {:closed true}
   [:source
    [:map
     [:id :keyword]
     [:include {:optional true} [:map-of :keyword :keyword]]]]
   [:target {:optional true}
    [:map
     [:id {:optional true} :keyword]
     [:mapping {:optional true} :keyword]
     [:include {:optional true} [:map-of :keyword :keyword]]]]])

(def db-id-temp-schema
  [:or
   [:and :int [:< 0]]
   :string])

(def db-id-schema
  [:and :int [:< 0]])

(def node-id-schema
  [:or
   [:map {:closed true}
    [:db/id [:or
             db-id-schema
             db-id-temp-schema]]]
   :string])

(def data-schema
  [:map {:closed true}
   [:targets {:optional true} [:vector
                               [:map {:closed true}
                                [:db/id {:optional true}
                                 [:or
                                  db-id-schema
                                  db-id-temp-schema]]
                                [:mapping.external/id {:optional true} :string]
                                [:mapping.external/uri {:optional true} :string]
                                [:mapping.external/key-value-data {:optional true} [:vector [:tuple :keyword :string]]]
                                [:mapping.external/annotations {:optional true} [:vector :string]]]]]
   [:edges {:optional true} [:vector [:tuple node-id-schema node-id-schema]]]])

(def mapping-edn-schema
  [:map {:closed true}
   [:mapping mapping-schema]
   [:config {:optional true} config-schema]
   [:data {:optional true} data-schema]])

(def mapping-file-schema
  [:or mapping-edn-schema [:vector mapping-edn-schema]])

(defn read-mapping-file [path]
  (try
    (iou/read-edn-file mapping-file-schema path)
    (catch Exception e
      (throw (ex-info "Failed reading mapping file" {:path path :exception e})))))

(defn load-mapping-data [cfg mapping-data-collection]
  (let [conn (dc/connect cfg)]
    (doseq [mapping-data mapping-data-collection]
      (log/info :mapping.core/loading (mapping/mapping->sort-order-vector (:mapping mapping-data)))
      (mc/create conn mapping-data))))

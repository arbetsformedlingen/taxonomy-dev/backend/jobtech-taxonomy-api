(ns jobtech-taxonomy.features.mapping.setup
  (:require [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.common.malli-utils :as malli-utils]
            [jobtech-taxonomy.features.mapping.core :as core]
            [jobtech-taxonomy.features.mapping.routes :as mr]
            [taoensso.timbre :as log])
  (:gen-class))

(def ^:private mappings-config-schema
  [:mappings
   [:map {:closed true}
    [:source-directory {:optional true} :string]]])

(def ^:private get-mapping-config
  (malli-utils/config-validator-for mappings-config-schema))

(defn- get-mapping-data [cfg]
  (log/debug cfg)
  (when-let [mapping-data-directory (:source-directory cfg)]
    (log/info :mapping/setup :loading-from mapping-data-directory)
    (let [mapping-data-files (iou/pattern-file-seq-from
                              mapping-data-directory
                              #"^.+\.edn$")
          mapping-edn-data (map core/read-mapping-file mapping-data-files)]
      (log/info :mapping/setup :loading-files (mapv str mapping-data-files))
      (flatten mapping-edn-data))))

(defn- ensure-mapping-schema! [cfg]
  (if (core/schema-present? cfg)
    (log/info "Mapping schema detected")
    (core/install-schema cfg)))

(defn provide-endpoints [cfg]
  (config/update-module-data cfg
                             :mappings
                             assoc
                             :endpoints
                             (mr/endpoints cfg)))

(defn init [cfg]
  (if-let [mapping-config (get-mapping-config cfg)]
    (let [mapping-data (get-mapping-data mapping-config)]
      (ensure-mapping-schema! cfg)
      (core/load-mapping-data cfg mapping-data)
      (log/info :mapping/setup "Enabled mapping backend")
      (provide-endpoints cfg))
    (do
      (log/warn :mappings-setup "Could not start mapping backend")
      cfg)))

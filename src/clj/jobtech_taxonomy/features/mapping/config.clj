(ns jobtech-taxonomy.features.mapping.config
  (:require [clojure.set :as set]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.common :as common]
            [malli.core :as m]
            [taoensso.timbre :as log]))

(defn ->id-attr [config node]
  (-> config node :id-attr))

(defn ->id-attr-type [config node]
  ((:attr-types config {})
   (->id-attr config node)
   [:or :int :string]))

(defn ->mapped-id-attr [config]
  (-> config :mapping :mapped-id))

(defn ->mapped-id-fn [config]
  (let [mapped-id-attr (->mapped-id-attr config)]
    (fn [target]
      (or (mapped-id-attr target)
          (:mapping.external/id target)))))

(defn ->node-fn [config node]
  (let [id-attr (->id-attr config node)
        id-attr-type-schema (->id-attr-type config node)
        id-attr-type? (m/validator id-attr-type-schema)
        get-tempid (common/get-tempid-by id-attr)
        get-id (common/get-id-by id-attr)]
    (log/debug :config/->node-fn node id-attr id-attr-type-schema)
    (fn node-for [value]
      (cond
        (get-id value) value
        (get-tempid value) value
        (id-attr-type? value) {:id-attr id-attr :value value}
        (and (map? value) (:mapping.nodes/tempid value)) {:error value}
        :else {:error [id-attr value]}))))

(defn ->node-fns [config]
  (let [source-fn (->node-fn config :source)
        target-fn (->node-fn config :target)]
    [source-fn target-fn]))

(defn ->pull [config node & include-attribs]
  (let [mapped-id (when (= :target node) (->mapped-id-attr config))]
    (cond-> (into (-> config node :pull) include-attribs)
      mapped-id (conj mapped-id))))

(defn ->rename [config node]
  (-> config node :rename))

(defn ->rename-fn [config node]
  (let [node-renames (->rename config node)]
    (fn [data]
      (set/rename-keys data node-renames))))

(defn- update-attr-types [db attr-name]
  (if (= :db/id attr-name)
    {:db/id [:and :int [:> 0]]}
    (let [attr-schemas (-> (dc/q '[:find (pull ?attr [*])
                                   :in $ ?attr-name
                                   :where
                                   [?attr :db/ident ?attr-name]]
                                 db attr-name)
                           flatten)
          [attr-key malli-schema] (common/db-schema->malli (first attr-schemas))]
      (log/debug :config/update-attr-types attr-name malli-schema attr-schemas)
      {attr-key malli-schema})))

(defn ->mapping-id [config] (get-in config [:mapping :db/id]))

(defn mapping->config [db mapping]
  (let [source-attr (:mapping.external/source-attribute mapping :db/id)
        source-attr-type (update-attr-types db source-attr)
        target-attr (:mapping.external/target-attribute mapping :db/id)
        target-attr-type (update-attr-types db target-attr)
        config (cond->
                (reduce
                 (fn [acc [node attr value]]
                   (log/debug :config/mapping->config :attrs node attr value)
                   (if (= :mapping node)
                     (assoc-in acc [node attr] value)
                     (update-in (if (not= attr value)
                                  (update-in acc [node :rename] #(merge % {attr value}))
                                  acc) [node :pull] conj attr)))
                 {:attr-types (conj {:db/id [:and :int [:> 0]]} source-attr-type target-attr-type)
                  :mapping {:db/id (:db/id mapping)}
                  :source {:pull #{} :rename {} :id-attr source-attr}
                  :target {:pull #{} :rename {} :id-attr target-attr}}
                 (:mapping.external/attribute-mapping mapping []))
                 (not= :db/id source-attr)
                 (update-in [:source :pull] conj source-attr)
                 (not= :db/id target-attr)
                 (update-in [:target :pull] conj target-attr))]
    (log/debug :config/mapping->config :mapping mapping)
    (log/debug :config/mapping->config :config config)
    config))

(defn- config-tx-target-schema [node-data]
  (log/debug :config/config-tx-target-schema node-data)
  (into
   []
   (comp
    (filter map?)
    cat
    (keep
     (fn [[k _v]]
       (cond
         (= :db/id k) nil
         (= "mapping.external" (namespace k)) [:target k k]
         :else [:target k (keyword (name k))]))))
   node-data))

(defn- config-tx-include-for [raw-config node]
  (into []
        (map #(into [node] %))
        (get-in raw-config [node :include])))

(defn ->tx-data [raw-config raw-node-data]
  (let [source-id (:id (:source raw-config) :db/id)
        tx-source {:mapping.external/source-attribute source-id}
        configured-target-id (:id (:target raw-config))
        target-id (or configured-target-id :db/id)
        target-mapped-id (:mapping (:target raw-config) (or configured-target-id :mapping.external/id))
        tx-target {:mapping.external/target-attribute target-id}

        tx-include {:mapping.external/attribute-mapping
                    (concat [[:mapping :mapped-id target-mapped-id]]
                            (config-tx-include-for raw-config :source)
                            (config-tx-include-for raw-config :target)
                            (config-tx-target-schema raw-node-data))}

        tx-data (merge tx-source
                       tx-target
                       tx-include)]
    tx-data))

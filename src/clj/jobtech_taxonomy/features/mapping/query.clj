(ns jobtech-taxonomy.features.mapping.query
  (:require [clojure.set :as set]
            [jobtech-taxonomy.features.mapping.edge :as edge]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [taoensso.timbre :as log]))

(defn mappings
  ([db] (mapping/mappings db {}))
  ([db query] (mapping/mappings db query)))

(defn edges [db mapping-queries sources]
  (let [mappings
        (reduce
         (fn [acc query]
           (let [found (mapping/mappings db query)]
             (if (= "ok" (namespace (:result found)))
               (update acc :ok set/union (:mappings found))
               (update acc :error conj found))))
         {:ok #{} :error []}
         mapping-queries)]
    (log/debug :query/edges :mappings mappings)
    (if (empty? (:error mappings))
      (let [target-finders (edge/query db (:ok mappings) sources)]
        (log/debug :edges :targets (keys (first target-finders)))
        (conj {:result :ok/query}
              (when target-finders {:lookup target-finders})))
      {:result :error/invalid-query
       :error (:error mappings)})))

(defn- into-set [e]
  (cond
    (set? e) e
    (map? e) #{e}
    (coll? e) (into #{} e)
    :else #{e}))

(defn display-targets [db mappings ids]
  (let [found-edges (edges db (into-set mappings) (into-set ids))]
    (log/debug :query/display-targets found-edges)
    (->> found-edges
         :lookup
         (map #(vec (:found %)))
         flatten
         (sort-by :mapped-id))))

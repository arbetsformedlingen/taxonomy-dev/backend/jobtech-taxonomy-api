(ns jobtech-taxonomy.features.mapping.nodes
  (:require [clojure.set :as set]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.common :as common]
            [jobtech-taxonomy.features.mapping.config :as config]
            [taoensso.timbre :as log]))

(def external-schema
  "One or more of the identifier attributes can be attached
   to an entity describing an external identifier."
  (iou/read-edn-resource common/db-schema-schema "jobtech_taxonomy/features/mapping/external-schema.edn"))

(defn- set-node-ns-on-tempid-and-error [node]
  (and (map? node)
       (set/rename-keys node {:tempid :mapping.nodes/tempid :error :mapping.nodes/error})))

(defn- source+target-fn [config]
  (let [[source-fn target-fn] (map #(comp set-node-ns-on-tempid-and-error %)
                                   (config/->node-fns config))]
    (fn source+target->node-ids-fn [acc [source target]]
      (let [source-id (source-fn source)
            target-id (target-fn target)]
        (log/debug :nodes/source+target-fn :source source :id source-id)
        (log/debug :nodes/source+target-fn :target target :id target-id)
        (conj acc source-id target-id)))))

(defn node-ids-from [config source+target-list]
  (let [node-ids (reduce (source+target-fn config) #{} source+target-list)]
    (log/debug :nodes/node-ids-from :source+target-list source+target-list)
    node-ids))

(defn populate [conn nodes]
  (let [txr (dc/transact conn {:tx-data nodes})
        tempids (:tempids txr)]
    {:tempids tempids
     :nodes (into #{} (map (fn [n] (->
                                    (assoc n :mapping.nodes/tempid (:db/id n))
                                    (update :db/id #(tempids %))))) nodes)
     :db (:db-after txr)}))

(defn- validate-direct [db nodes]
  (let [found (into #{} (map :db/id) (dc/q '[:find ?node
                                             :keys db/id
                                             :in $ [?node ...]
                                             :where [?node _ _]]
                                           db (map :db/id nodes)))
        missing (reduce
                 (fn [acc node]
                   (if (not (found (:db/id node)))
                     (conj acc node)
                     acc)) #{} nodes)
        keep-found (set (remove missing nodes))]
    {:found keep-found
     :missing missing}))

(defn- lookup-values [db id-attr values]
  (into #{}
        (map #(assoc % :id-attr id-attr))
        (dc/q '[:find ?node ?value
                :keys db/id value
                :in $ ?attr [?value ...]
                :where [?node ?attr ?value]]
              db id-attr values)))

(defn- validate-value-lookup-by-attr [db id-attr value-set]
  (let [found (lookup-values db id-attr value-set)
        found-values (into #{} (map :value found))
        missing (reduce
                 (fn [acc value]
                   (if (not (found-values value))
                     (conj acc {:id-attr id-attr :value value})
                     acc))
                 #{} value-set)]
    {:found found
     :missing missing}))

(defn- validate-lookup [db nodes]
  (log/debug :nodes/validate-lookup :nodes nodes)
  (reduce-kv
   (fn [acc attr nodes]
     (log/debug :nodes/validate-lookup :attr attr)
     (let [value-set (into #{} (map :value) nodes)
           {:keys [found missing]}
           (validate-value-lookup-by-attr db attr value-set)]
       (cond-> acc
         (seq found) (update :found #(set/union % found))
         (seq missing) (update :missing #(set/union % missing)))))
   {} (group-by :id-attr nodes)))

(defn update-tempid-for [id-attr tempids]
  (let [tempid? (common/get-tempid-by id-attr)]
    (fn update-tempid [n]
      (if-let [tid (tempid? n)]
        (merge {:mapping.nodes/tempid tid}
               (when-let [id (tempids tid)] {:db/id id})
               (when (map? n) (dissoc n :db/id)))
        n))))

(defn update-edges [config tempids]
  (let [source-attr (config/->id-attr config :source)
        source-fn (update-tempid-for source-attr tempids)
        target-attr (config/->id-attr config :target)
        target-fn (update-tempid-for target-attr tempids)]
    (fn update-edge [[s t]]
      (let [source (source-fn s)
            target (target-fn t)]
        [source target]))))

(defn apply-tempids [config tempids source+target-list]
  (into #{} (map (update-edges config tempids)) source+target-list))

(defn- update-edge-ids [config nodes]
  (let [lookup-map (reduce
                    (fn [acc n]
                      (let [id (:db/id n)
                            tempid (:mapping.nodes/tempid n)
                            id-attr (:id-attr n)
                            value (:value n)]
                        (cond-> (assoc acc (dissoc n :db/id) id)
                          tempid (assoc tempid id)
                          id-attr (assoc-in [id-attr value] id)
                          true (assoc n id)))) {} nodes)
        lookup-fn (fn [node] (lookup-map node))
        [source-fn target-fn] (map #(comp lookup-fn %) (config/->node-fns config))]
    (fn source+target->node-ids-fn [[source target]]
      (let [source-id (source-fn source)
            target-id (target-fn target)]
        [source-id target-id]))))

(defn validate [db config source+target-set]
  (let [node-ids (node-ids-from config source+target-set)
        node-id-classes (group-by
                         (fn id-type [node]
                           (cond
                             (pos-int? (:db/id node)) :direct
                             (or (:mapping.nodes/error node)
                                 (:mapping.nodes/tempid node)) :missing
                             :else :lookup))
                         node-ids)
        direct-node-ids (validate-direct db (node-id-classes :direct))
        lookup-node-ids (validate-lookup db (node-id-classes :lookup))
        found-nodes (into #{} (concat
                               (:found direct-node-ids)
                               (:found lookup-node-ids)))
        missing (into #{} (map (fn [n] (if (= (:mapping.nodes/tempid n) (:db/id n))
                                         (dissoc n :db/id) n)))
                      (concat (:missing node-id-classes)
                              (:missing direct-node-ids)
                              (:missing lookup-node-ids)))

        edges (into #{} (filter
                         (fn [[s t]]
                           (and (pos-int? s) (pos-int? t)))
                         (map (update-edge-ids config found-nodes) source+target-set)))]
    {:edges edges
     :found found-nodes
     :missing missing}))


(ns jobtech-taxonomy.features.mapping.common
  (:require [taoensso.timbre :as log]))

(defn- neg-int-or-string? [id]
  (when (or (neg-int? id)
            (string? id))
    id))

(defn- direct-id-node? [n]
  (and (map? n)
       (neg-int-or-string? (:db/id n))))

(defn- id-attr-id-node? [n]
  (or (neg-int-or-string? n)
      (direct-id-node? n)))

(defn get-tempid-by [id-attr]
  (if (= :db/id id-attr)
    id-attr-id-node?
    direct-id-node?))

(defn- is-direct-id? [n]
  (and (map? n)
       (pos-int? (:db/id n))))

(defn- is-id? [n]
  (or (pos-int? n)
      (is-direct-id? n)))

(defn get-id-by [id-attr]
  (if (= :db/id id-attr)
    is-id?
    is-direct-id?))

(defn drop-keyword-ns [kn]
  (if (keyword? kn)
    (keyword (name kn))
    kn))

(defn- db-schema-value-type [db-schema-entry]
  (let [value-type (:db/valueType db-schema-entry)]
    (cond (keyword? value-type) (drop-keyword-ns value-type)
          (map? value-type) (drop-keyword-ns (:db/ident value-type))
          :else
          (log/error :mapping.common/db-schema-value-type :db/valueType db-schema-entry))))

(defn- db-schema-cardinality [db-schema-entry]
  (let [cardinality (:db/cardinality db-schema-entry)]
    (cond (keyword? cardinality) cardinality
          (map? cardinality) (:db/ident cardinality)
          :else
          (log/error :mapping.common/db-schema-cardinality :db/cardinality db-schema-entry))))

(defn db-schema->malli-type [db-schema-entry]
  (let [malli-type (db-schema-value-type db-schema-entry)
        tuple-form #(into [:tuple] (map drop-keyword-ns)
                          (:db/tupleTypes db-schema-entry))]
    (case [(db-schema-cardinality db-schema-entry)
           (case malli-type :tuple :tuple :other)]
      [:db.cardinality/one :other] malli-type
      [:db.cardinality/one :tuple] (tuple-form)
      [:db.cardinality/many :other] [:or malli-type [:vector malli-type]]
      [:db.cardinality/many :tuple] [:or (tuple-form) [:vector (tuple-form)]]
      (log/error :mapping.common/db-schema->malli-type
                 db-schema-entry))))

(defn db-schema->malli [db-schema-entry & opts]
  (let [property-name (:db/ident db-schema-entry)
        malli-type (db-schema->malli-type db-schema-entry)]
    (if (some? opts)
      [property-name (into {} opts) malli-type]
      [property-name malli-type])))

(def ^:private malli-atomic-value-type
  [:enum
   :db.type/keyword
   :db.type/ref
   :db.type/string])

(def db-schema-schema
  [:vector
   [:map {:closed true}
    [:db/ident :keyword]
    [:db/doc :string]
    [:db/unique {:optional true} [:enum :db.unique/identity]]
    [:db/tupleTypes {:optional true} [:vector malli-atomic-value-type]]
    [:db/valueType (conj malli-atomic-value-type :db.type/tuple)]
    [:db/cardinality [:enum :db.cardinality/many :db.cardinality/one]]]])

(ns jobtech-taxonomy.features.mapping.routes
  (:require [clojure.string :as str]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]
            [jobtech-taxonomy.features.mapping.core :as mc]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [spec-tools.data-spec :as ds]
            [taoensso.timbre :as log]))

(def mapping-ok? any?)

(def mapping-bad-request? any?)

(defn- fix-type [params]
  (log/debug :fix-type params)
  (cond-> params
    (:type params) (update :type keyword)))

(defn- mapping-query [request]
  (log/debug :request (:parameters request))
  (let [taxonomy-version (get-in request [:parameters :query :taxonomy-version])
        path-params (fix-type (get-in request [:parameters :path] {}))]
    (cond-> {:query (dissoc path-params :source-ids)}
      taxonomy-version (update :query
                               assoc
                               :taxonomy-version
                               (if (keyword? taxonomy-version)
                                 taxonomy-version
                                 (str taxonomy-version)))
      (:source-ids path-params)
      (assoc :source-ids (into #{} (map str/trim) (str/split (:source-ids path-params) #","))))))

(defn- display-filter [mappings]
  (into #{} (map mapping/mapping->short-names-only) (:mappings mappings)))

(defn- display-all [mappings taxonomy-version]
  (log/debug :display-all taxonomy-version)
  (let [tv-fn (if (nil? taxonomy-version)
                identity
                (fn [m] (= taxonomy-version (:mapping.mapping/taxonomy-version m))))]
    (->> (:mappings mappings)
         (filter tv-fn)
         (mapv :mapping.mapping/organisation)
         frequencies
         (map (fn [[k v]] {:organisation k :mappings v})))))

(defn- display-translation [result]
  (sort-by :mapped-id (map #(update % :target dissoc :db/id) result)))

(defn- display-mapping-translations [results]
  (-> results
      (update :mapping mapping/mapping->short-names-only)
      (update :found display-translation)
      (#(if (empty? (:missing %))
          (dissoc % :missing)
          %))
      (dissoc :result)))

(defn- display-translations [translations]
  (mapv display-mapping-translations
        (:lookup translations)))

(defn- handle-get-with [cfg]
  (fn handle-query [request]
    (let [params (mapping-query request)
          query (:query params)]
      (log/debug :handle-params params)
      (if-let [source-ids (:source-ids params)]
        {:status 200
         :body (display-translations (mc/translations (dc/get-db cfg :next) query source-ids))}
        (let [mappings (mc/mappings
                        (dc/get-db cfg :next)
                        (if (= '(:taxonomy-version)
                               (keys query)) {} query))]
          (case (:result mappings)
            :ok/all
            {:status 200
             :body (display-all mappings (:taxonomy-version query))}
            :ok/filter
            {:status 200
             :body (display-filter mappings)}
            :ok/specific
            {:status 200
             :body (first (:mappings mappings))}
            :error/invalid-query
            {:status 400
             :body {:taxonomy/error {:query query}}}))))))

(defn endpoints [cfg]
  ["/mappings"
   {:swagger {:summary "Mappings from one entity to another."
              :description
              "The Mappings endpoint provides a way of searching for mappings from concept IDs in the taxonomy to some ID.
                                         Lists all organisations that have mappings by default.
                                         Post takes a map and responds with ..."}}
   [""
    {:swagger {:summary "Mappings from one entity to another."
               :description
               "The Mappings endpoint provides a way of searching for mappings from concept IDs in the taxonomy to some ID.
                         Lists all organisations that have mappings by default.
                         Post takes a map and responds with ..."}
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]
   ["/"
    {:summary "Mappings from one entity to another."
     :description
     "The Mappings endpoint provides a way of searching for mappings from concept IDs in the taxonomy to some ID.
                   Lists all organisations that have mappings by default.
                   Post takes a map and responds with ..."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]

   ["/{organisation}"
    {:description "Given an organisation, lists all mapping families."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]
   ["/{organisation}/{family}"
    {:description "Given an organisation and mapping family, lists all mapping names."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?
                               :family string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]

   ["/{organisation}/{family}/{name}"
    {:description "Given an organisation, mapping family and name, lists all mappings with those values."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?
                               :family string?
                               :name string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]
   ["/{organisation}/{family}/{name}/{type}"
    {:description "Lists all mapping versions for a given organisations mapping names type."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?
                               :family string?
                               :name string?
                               :type string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]

   ["/{organisation}/{family}/{name}/{type}/{version}"
    {:description "Lists all mapping versions for a given organisations mapping names type."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?
                               :family string?
                               :name string?
                               :type string?
                               :version string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]

   ["/{organisation}/{family}/{name}/{type}/{version}/{source-ids}"
    {:description "Fetches the mapped (if any) target for the source given in the specified mapping."
     :get {:parameters {:query {(ds/opt :taxonomy-version) taxonomy/version-param}
                        :path {:organisation string?
                               :family string?
                               :name string?
                               :type string?
                               :version string?
                               :source-ids string?}}
           :responses (merge (taxonomy/response200 mapping-ok?) (taxonomy/response-bad-request mapping-bad-request?))
           :handler (handle-get-with cfg)}}]])

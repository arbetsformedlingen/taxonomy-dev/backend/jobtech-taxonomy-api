(ns jobtech-taxonomy.features.private
  (:require [clojure.string :as str]
            [jobtech-taxonomy.api.authentication-service :refer [authenticate-admin]]
            [jobtech-taxonomy.api.db.concept-types :as concept-types]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.core :as core]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.daynotes :as daynotes]
            [jobtech-taxonomy.api.db.graph :as graph]
            [jobtech-taxonomy.api.db.versions :as v]
            [jobtech-taxonomy.api.db.webhooks :as webhooks-db]
            [jobtech-taxonomy.api.routes.parameter-util :as pu]
            [jobtech-taxonomy.api.routes.raw-handler :as raw-handler]
            [jobtech-taxonomy.api.webhooks :as webhooks]
            [jobtech-taxonomy.common.taxonomy :as taxonomy]
            [ring.util.http-response :as resp]
            [spec-tools.data-spec :as ds]))

(defn- authorized-private?
  [cfg handler]
  (fn [request]
    (if (authenticate-admin cfg (get-in request [:headers "api-key"]))
      (handler request)
      (resp/unauthorized (taxonomy/map->nsmap {:error "Not authorized"})))))

(defn private-query-params
  "Creates middleware that ensures admin authentication for certain query values

  Expects a map from query parameter to a predicate that should be satisfied to trigger
  admin authentication check, for example:
  ```
  {:sudo true?
   :version #{:next}
   :raw-sql-query any?}
  ```
  This param->pred map will require admin rights if either:
  - `:sudo` parameter is `true`
  - `:version` parameter is `:next`
  - `:raw-sql-query` is present"
  [cfg param->pred]
  (let [needs-admin-auth? (apply some-fn
                                 (map (fn [[k pred]]
                                        (fn [query-params]
                                          (let [v (get query-params k ::not-found)]
                                            (and (not= v ::not-found) (pred v)))))
                                      param->pred))]
    (fn [handler]
      (fn [request]
        (if (and (needs-admin-auth? (get-in request [:parameters :query]))
                 (not (authenticate-admin cfg (get-in request [:headers "api-key"]))))
          (resp/unauthorized (taxonomy/map->nsmap {:error "Not authorized"}))
          (handler request))))))

(defn private-version-middleware
  "Creates a middleware to make the next version private."
  [cfg]
  (private-query-params cfg {:version dc/private-version?}))

(defn webhooks-endpoints [cfg]
  ["/webhooks"
   {:tags ["Webhooks"]
    :openapi {:deprecated true}
    :middleware [(partial authorized-private? cfg)]}

   ["/register"
    {:summary ""
     :parameters {:query {:api-key (taxonomy/par string? "A unique API-key used as identifier.")
                          :callback-url (taxonomy/par string? "A URL to a webhook callback listener.")}}
     :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response406)
            :handler (fn [{{{:keys [api-key callback-url]} :query} :parameters}]
                       (let [result (webhooks-db/update-webhooks cfg api-key callback-url)]
                         (if result
                           {:status 200 :body (taxonomy/map->nsmap
                                               {:message (format "The webhook callback was registered.")})}
                           {:status 406 :body (taxonomy/map->nsmap {:error (str "Could not create webhook callback with values " api-key " " callback-url)})})))}}]])

(defn private-endpoints [cfg]
  ["/private"
   {:tags ["Private"]
    :middleware [(partial authorized-private? cfg)]}

   ["/concept/changes"
    {:summary "Show changes to the taxonomy as a stream of events. Include unpublished changes."
     :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occurred after this version was published."),
                          (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occurred before this version was published and during this version. (default: latest version)")
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")}}
     :get {:responses (taxonomy/response200 taxonomy/concept-changes-validator-spec)
           :handler (fn [{{{:keys [after-version to-version-inclusive offset limit]} :query} :parameters}]
                      {:status 200
                       :body (let [events (daynotes/get-concept-changes-with-pagination-private cfg after-version to-version-inclusive offset limit)]
                               (vec (map taxonomy/map->nsmap events)))})}}]
   ["/delete-relation"
    {:summary "Retract a relation."
     :parameters {:query (concepts/delete-relation-query-params)}
     :delete {:responses (merge (taxonomy/response200 taxonomy/msg-spec) taxonomy/response409)
              :handler (partial concepts/delete-relation-handler cfg)}}]

   ["/delete-concept"
    {:summary "Retract the concept with the given ID."
     :parameters {:query {:id (taxonomy/par string? "ID of concept")
                          (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
     :delete {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
              :handler (fn [request]
                         (let [{:keys [id comment]} (pu/get-query-from-request request)
                               user-id (pu/get-user-id-from-request cfg request)]
                           (if (core/retract-concept cfg user-id id comment)
                             {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                             {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

   ["/concept"
    {:summary "Assert a new concept."
     :parameters {:query (pu/build-parameter-map [:type :definition :preferred-label :comment :quality-level :alternative-labels :hidden-labels])}
     :post {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
            :handler (partial concepts/assert-concept-handler cfg)}}]

   ["/remove-alternative-label"
    {:summary "Remove alternative label from concept."
     :parameters {:query
                  (pu/build-parameter-map [:id :comment :alternative-label])}

     :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
             :handler (partial concepts/handle-remove-alternative-label cfg)}}]

   ["/remove-hidden-label"
    {:summary "Remove hidden label from concept."
     :parameters {:query
                  (pu/build-parameter-map [:id :comment :hidden-label])}

     :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
             :handler (partial concepts/handle-remove-hidden-label cfg)}}]

   ["/concept-types"
    {:summary "Assert concept types and their localisation"
     :parameters {:query {:concept-type (taxonomy/par string? "Concept type")
                          (ds/opt :label-sv) (taxonomy/par string? "Label in Swedish")
                          (ds/opt :label-en) (taxonomy/par string? "Label in English")}}
     :post {:responses (taxonomy/response200 any?)
            :handler (fn [req]
                       {:status 200
                        :body (concept-types/assert-type
                               cfg
                               (-> req
                                   :parameters
                                   :query
                                   (assoc :user-id (pu/get-user-id-from-request cfg req))))})}}]

   ["/concepts"
    {:summary "Get concepts. Supply at least one search parameter."
     :parameters {:query
                  (pu/build-parameter-map
                   [:id :preferred-label :type :include-deprecated :deprecated
                    :relation :related-ids :offset :limit :version])}
     :get {:responses (taxonomy/response200 taxonomy/concepts-spec-private)
           :handler (fn [{{{:keys [id preferred-label type include-deprecated deprecated
                                   relation related-ids offset limit version]
                            :or {version :next relation "related"}} :query} :parameters}]
                      {:status 200
                       :body (mapv taxonomy/map->nsmap (concepts/find-concepts
                                                        cfg
                                                        {:id id
                                                         :preferred-label preferred-label
                                                         :type (when type (str/split type #" "))
                                                         :include-deprecated include-deprecated
                                                         :deprecated deprecated
                                                         :relation relation
                                                         :related-ids (when related-ids (str/split related-ids #" "))
                                                         :offset offset
                                                         :limit limit
                                                         :version version}))})}}]

   ["/accumulate-concept"
    {:summary "Accumulate data on an existing concept."
     :parameters {:query
                  (pu/build-parameter-map [:id :type :definition :preferred-label :comment :quality-level :no-esco-relation :deprecated :alternative-labels :hidden-labels])}
     :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
             :handler (fn [request]
                        (let [query (pu/get-query-from-request request)
                              user-id (pu/get-user-id-from-request cfg request)
                              result (concepts/accumulate-concept cfg user-id query)]
                          (if result
                            {:status 200 :body result}
                            {:status 409 :body (taxonomy/map->nsmap {:error "Can't update concept since it is in conflict with existing concept."})})))}}]

   ["/relation"
    {:summary "Assert a new relation."
     :parameters {:query (concepts/assert-relation-query-params)}
     :post {:responses (merge (taxonomy/response200 taxonomy/msg-spec) taxonomy/response409)
            :handler (partial concepts/assert-relation-handler cfg)}}]

   ["/accumulate-relation"
    {:summary "Accumulate data on an existing relation."
     :parameters {:query {:concept-1 pu/concept-source
                          :concept-2 pu/concept-target
                          :relation-type pu/relation-type
                          (ds/opt :new-relation-type) pu/relation-type
                          :comment pu/daynote-comment
                          (ds/opt :description) pu/relation-description
                          (ds/opt :substitutability-percentage) pu/substitutability-percentage}}
     :patch {:responses (merge (taxonomy/response200 any?) taxonomy/response409)
             :handler (fn [request]
                        (let [query (pu/get-query-from-request request)
                              user-id (pu/get-user-id-from-request cfg request)
                              result (concepts/accumulate-relation cfg user-id query)]
                          (if result
                            {:status 200 :body result}
                            (let [msg "Can't update relation since it is in conflict with existing relation."]
                              {:status 409 :body (taxonomy/map->nsmap {:error msg})}))))}}]

   ["/relation/changes"
    {:summary "Show changes to the relations as a stream of events."
     :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changed relations that occurred after this version was published."),
                          (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changed relations that occurred before this version was published and during this version. (default: latest version)"),
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)"),
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")}}
     :get {:responses (taxonomy/response200 [any?])
           :handler (fn [{{{:keys [after-version to-version-inclusive offset limit]} :query} :parameters}]
                      {:status 200
                       :body (let [events (doall (daynotes/get-relation-changes-with-pagination-private cfg after-version to-version-inclusive offset limit))]
                               (vec (map taxonomy/map->nsmap events)))})}}]

   ["/graph"
    {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
     :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                          :source-concept-type (taxonomy/par string? "Source nodes concept type")
                          :target-concept-type (taxonomy/par string? "Target nodes concept type")
                          (ds/opt :offset) (taxonomy/par nat-int? "Return list offset (from 0)")
                          (ds/opt :limit) (taxonomy/par pos-int? "Return list limit")
                          (ds/opt :version) taxonomy/version-param
                          (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
     :get {:responses (taxonomy/response200 taxonomy/graph-spec)
           :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                            :or {version :next}} :query} :parameters}]
                      {:status 200
                       :body (graph/fetch-graph cfg edge-relation-type source-concept-type target-concept-type
                                                include-deprecated offset limit version)})}}]

   ["/replace-concept"
    {:summary "Replace old concept with a new concept."
     :parameters {:query {:old-concept-id (taxonomy/par string? "Old concept ID"),
                          :new-concept-id (taxonomy/par string? "New concept ID")
                          (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
     :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
            :handler (fn [{{{:keys [old-concept-id new-concept-id comment]} :query} :parameters :as request}]
                       (let [user-id (pu/get-user-id-from-request cfg request)]
                         (if (core/replace-deprecated-concept cfg user-id old-concept-id new-concept-id comment)
                           {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                           {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

   ["/unreplace-concept"
    {:summary "Undo a replacement of the old concept with a new concept, while keeping it deprecated"
     :parameters {:query {:old-concept-id (taxonomy/par string? "Old concept ID"),
                          :new-concept-id (taxonomy/par string? "New concept ID")
                          (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
     :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
            :handler (fn [{{{:keys [old-concept-id new-concept-id comment]} :query} :parameters :as request}]
                       (let [user-id (pu/get-user-id-from-request cfg request)]
                         (if (core/unreplace-deprecated-concept cfg user-id old-concept-id new-concept-id comment)
                           {:status 200 :body (taxonomy/map->nsmap {:message "ok"})}
                           {:status 404 :body (taxonomy/map->nsmap {:error "not found"})})))}}]

   ["/versions"
    {:summary "Creates a new version tag in the database."
     :parameters {:query {:new-version-id (taxonomy/par int? "New version ID")
                          (ds/opt :new-version-timestamp) (taxonomy/par inst? "New version timestamp (default: now)")}}
     :post {:responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response406)
            :handler (fn [{{{:keys [new-version-id new-version-timestamp]} :query} :parameters}]
                       (case (v/create-new-version cfg new-version-id new-version-timestamp)
                         ::v/incorrect-new-version
                         {:status 406
                          :body (taxonomy/map->nsmap {:error (str new-version-id " is not the next valid version id!")})}

                         ::v/new-version-before-release
                         {:status 406
                          :body (taxonomy/map->nsmap {:error (str new-version-timestamp " is before the latest release!")})}

                         (let [clients (webhooks/get-client-list-from-db! cfg)]
                           (webhooks/notify-all! clients new-version-id)
                           {:status 200
                            :body (taxonomy/map->nsmap {:message "A new version of the Taxonomy was created."})})))}}]

   ["/concept/automatic-daynotes/"
    {:get {:summary "Fetches concept day notes (optionally restricted to a single concept id)"
           :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this point in time (inclusive)")
                                (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this point in time (exclusive)")}}
           :responses (merge (taxonomy/response200 any?) taxonomy/response404)
           :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                      {:status 200
                       :body (daynotes/get-for-concept (dc/get-db cfg :next) id from-timestamp to-timestamp nil nil true)})}
     :post {:summary "Create a concept daynote"
            :parameters {:query {:id (taxonomy/par string? "ID of concept")
                                 :comment (taxonomy/par string? "Comment")}}
            :responses (taxonomy/response200 taxonomy/ok-spec)
            :handler (fn [{{{:keys [id comment]} :query} :parameters :as request}]
                       (daynotes/create-for-concept cfg id (pu/get-user-id-from-request cfg request) comment)
                       {:status 200
                        :body (taxonomy/map->nsmap {:message "ok"})})}}]

   ["/relation/automatic-daynotes/"
    {:get {:summary "Fetches relation day notes (optionally restricted to relations of a particular concept id)"
           :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this date (inclusive)")
                                (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this date (exclusive)")}}
           :responses (merge (taxonomy/response200 [any?]) taxonomy/response404)
           :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                      {:status 200
                       :body (daynotes/get-for-relation (dc/get-db cfg :next) id from-timestamp to-timestamp nil nil true)})}
     :post {:summary "Create a relation daynote"
            :parameters {:query {:concept-1 (taxonomy/par string? "ID of source concept")
                                 :concept-2 (taxonomy/par string? "ID of target concept")
                                 :relation-type (taxonomy/par #{"broader" "related" "substitutability"} "Relation type")
                                 :comment (taxonomy/par string? "Comment")}}
            :responses (merge (taxonomy/response200 taxonomy/ok-spec) taxonomy/response404)
            :handler (fn [{{{:keys [concept-1 concept-2 relation-type comment]} :query} :parameters :as request}]
                       (if (daynotes/create-for-relation
                            cfg
                            (pu/get-user-id-from-request cfg request)
                            comment
                            concept-1
                            concept-2
                            relation-type)
                         {:status 200
                          :body (taxonomy/map->nsmap {:message "ok"})}
                         {:status 404
                          :body (taxonomy/map->nsmap {:error "relation not found"})}))}}]

   (raw-handler/raw-endpoint cfg)])

# JobTech Taxonomy API
  
The JobTech Taxonomy API is a REST API for the JobTech Taxonomy Database. The JobTech Taxonomy Database contains terms or phrases used at the Swedish labour market.

## Documentation

- [For taxonomy developers](https://arbetsformedlingen.gitlab.io/taxonomy-dev/projects/jobtech-taxonomy/development/)
- [For taxonomy users](https://arbetsformedlingen.gitlab.io/taxonomy-dev/projects/jobtech-taxonomy/)
- [Documentation sources](https://gitlab.com/arbetsformedlingen/taxonomy-dev/projects/jobtech-taxonomy)

## Open Source

The JobTech Taxonomy API is provided under the [EPL-2.0](https://www.eclipse.org/legal/epl-2.0/) license.

So, feel free to take a copy!

Copyright © 2019 JobTech

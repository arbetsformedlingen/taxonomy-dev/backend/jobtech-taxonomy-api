(ns build
  (:require [clojure.tools.build.api :as b]
            [java-time.api :as jt]))

(def class-dir "target/classes")
(def basis (b/create-basis {:project "deps.edn"}))

(defn ^:export update-build [_]
  (let [commit (b/git-process {:git-args "rev-parse HEAD"})
        build-info {:build-timestamp (jt/format (jt/zoned-date-time))
                    :commit commit}]
    (spit "resources/build-info.edn" build-info)))

(def uber-file "target/app.jar")

(def copy-src ["src/clj" "env/prod/clj" "env/prod/resources" "resources"])

(defn clean [_]
  (b/delete {:path "target"}))

(defn ^:export uber [_]
  (clean nil)
  (update-build nil)
  (b/copy-dir {:src-dirs copy-src
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :src-dirs ["src"]
                  :ns-compile '[prod]
                  :bindings {#'clojure.core/*warn-on-reflection* true}
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis
           :main 'prod}))

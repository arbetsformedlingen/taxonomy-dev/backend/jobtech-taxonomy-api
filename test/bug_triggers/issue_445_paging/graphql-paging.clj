#!/usr/bin/env bb
(require '[cheshire.core :as json]
         '[clj-http.lite.client :as client]
         '[clojure.tools.cli :refer [parse-opts]])

(set! *warn-on-reflection* true)

;; Script that tries to trigger the bug in https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/445

(def cli-options
  [["-s" "--step STEP" "The limit number in the graphql paging query."
    :default 100
    :parse-fn #(Integer/parseInt %)]
   ["-h" "--help"]])

;; Creates a GraphQL query of the form:
;; query MyQuery {
;;   concepts(offset:99, limit:99) {
;;     id
;;   }
;; }
(defn create-request [offset limit]
  (let [endpoint "https://taxonomy-i1.api.jobtechdev.se/v1/taxonomy/graphql"]
    (str endpoint "?query=query" "%20MyQuery%20%7B%0A%20%20concepts(offset%3A" offset "%2C%20limit%3A" limit ")%20%7B%0A%20%20%20%20id%0A%20%20%7D%0A%7D%0A")))

(defn get-concepts [offset limit]
  (let [resp (client/get (create-request offset limit))
        body (json/parse-string (:body resp))
        concepts (get-in body ["data" "concepts"])]
    (map #(get % "id") concepts)))

(defn id-in-acc? [id acc]
  (some true? (map #(some (fn [x] (= x id)) %) acc)))

(defn is-duplicate? [ids acc]
  (some true? (map #(id-in-acc? % acc) ids)))

;; Loops through a GraphQL query using paging and checks for concepts being
;; returned in two pages, hence an error.
(defn test-graphql-paging [step]
  (loop [offset 0
         acc []]
    (let [concepts (get-concepts offset step)
          _ (println (count acc) " " offset " " concepts)]
      (cond
        (is-duplicate? concepts acc) (println "Found duplicate")
        (empty? concepts) (println "No paging errors found")
        :else (recur (+ offset step) (cons concepts acc))))))

(defn run-it! []
  (let [{:keys [errors options summary]} (parse-opts *command-line-args* cli-options)
        {:keys [help step]} options]
    (if (some? errors)
      (println errors)
      (if help
        (println summary)
        (when step
          (test-graphql-paging step))))))

(run-it!)

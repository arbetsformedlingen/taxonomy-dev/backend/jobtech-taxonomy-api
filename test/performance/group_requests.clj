#!/usr/bin/env bb
(ns group-requests)
(require '[babashka.fs :as fs]
         '[clojure.data.csv :as csv]
         '[clojure.string :as str]
         '[clojure.tools.cli :refer [parse-opts]])

(set! *warn-on-reflection* true)

(def cli-options
  [["-l" "--logfile APILOG" "path to the API log file"
    :parse-fn identity
    :validate [#(fs/exists? (fs/file %)) "API log file does not exist!"]]
   ["-c" "--csvfile CSV" "path to the csv log file"
    :parse-fn identity
    :validate [#(fs/exists? (fs/file %)) "API log file does not exist!"]]
   ["-h" "--help"]])

(defn parse-params [p]
  (reduce (fn [ncoll [k v]]
            (assoc ncoll (keyword k) v))
          {}
          (map #(str/split % #"=") (str/split p #"\&"))))

(defn parse-logline-log [l]
  (let [cols (str/split l #" ")
        endp (str/split (get cols 13) #"\?")
        endpoint (get endp 0)
        params (get endp 1)]
    {:status (Integer/parseInt (apply str (drop-last (get cols 8))))
     :time (get cols 9)
     :endpoint endpoint
     :params (when params (keys (into (sorted-map) (parse-params params))))}))

(defn parse-logline-csv [l]
  (let [cnv (second (first (csv/read-csv l)))]
    (parse-logline-log cnv)))

(defn is-request [l]
  (and (.contains l "request:")
       (not (.contains l "api-key: readiness"))
       (not (.contains l "api-key: liveness"))))

(defn remove-non-requests [ls]
  (filter is-request ls))

(defn mean [ls]
  (bigint (/ (reduce + ls) (count ls))))

(defn variance [xs]
  (let [x-bar (mean xs)
        square-deviation (fn [x]
                           (Math/pow (- x x-bar) 2))]
    (mean (map square-deviation xs))))

(defn standard-deviation [xs]
  (Math/sqrt (variance xs)))

(defn median [coll]
  (let [sorted (sort coll)
        cnt (count sorted)
        halfway (quot cnt 2)]
    (if (odd? cnt)
      (nth sorted halfway)
      (let [bottom (dec halfway)
            bottom-val (nth sorted bottom)
            top-val (nth sorted halfway)]
        (mean [bottom-val top-val])))))

(defn stat-for-group [[group requests]]
  (let [ts (map #(Integer/parseInt (:time %)) requests)]
    [(int (median ts)) (int (mean ts)) (int (standard-deviation ts)) (count ts) group]))

(defn calculate-stat [gs]
  (map stat-for-group gs))

(defn group-logs [f line-parser]
  (let [all (slurp f)
        lines (str/split-lines all)
        filterred (remove-non-requests lines)
        reqs (filter #(= 200 (:status %)) (map line-parser filterred))
        groups (group-by #(select-keys % [:endpoint :params]) reqs)
        stat (calculate-stat groups)]
    (println "median mean standard-deviation count endpoint")
    (doseq [i (sort-by first > stat)]
      (println i))))

(defn run-it! []
  (let [{:keys [errors options summary]} (parse-opts *command-line-args* cli-options)
        {:keys [help logfile csvfile]} options]
    (if (some? errors)
      (println errors)
      (if help
        (println summary)
        (do
          (when logfile
            (group-logs logfile parse-logline-log))
          (when csvfile
            (group-logs csvfile parse-logline-csv)))))))

(run-it!)

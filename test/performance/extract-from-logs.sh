#!/bin/sh

# Extracts endpoint usage from jobtech-taxonomy-api logs

if [ ! -f "$1" ] ; then
  echo "API log file parameter missing"
  exit 1
fi

cat $1 |
  grep request: |
  sed 's/.*request:\ //g' |
  sed 's/api-key.*//g' |
  sed 's/[^\ ]*,//g' |
  sort | uniq -c | sort -n

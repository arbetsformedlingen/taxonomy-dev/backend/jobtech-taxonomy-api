#!/bin/sh

# Gets logs from a Taxonomy Environment in OpenShift
# The logs are then filterred and sorted on response time.

if [ -z "$1" ] ; then
  echo "ENV missing"
  exit 1
fi

DENV=$1

oc logs deploy/$DENV > $DENV.log
grep -vE "readiness|liveness" $DENV.log | grep request: | sort -n -k9 > $DENV-filterred.log
./group-requests.clj -l $DENV.log > $DENV-mean.log

#!/usr/bin/env bb
(ns bench)
(require '[babashka.cli :as cli]
         '[babashka.curl :as curl]
         '[babashka.fs :as fs]
         '[babashka.string :as str]
         '[clojure.pprint :refer [pprint]])

(set! *warn-on-reflection* true)

(def base-url "http://localhost:3000/v1/taxonomy")

(def header {"Accept" "application/json"})

(def version-date "2021-10-29T14:00:04.942Z")

(def state (atom {:failures []
                  :endpoints ["/main/versions"]
                  :graphql-queries ["{   concepts(type: [\"keyword\", \"occupation-name\"], preferred_label_contains: \"dag\") {     id     type     preferred_label   } }"]
                  :results {}}))

(def cli-options
  [["-g" "--graphql GRAPHQL" "path to graphql queries file"
    :parse-fn identity
    :validate [#(fs/exists? (fs/file %)) "Graphql queries file does not exist!"]]
   ["-e" "--endpoints ENDPOINTS" "path to endpoints file"
    :parse-fn identity
    :validate [#(fs/exists? (fs/file %)) "Endpoints file does not exist!"]]
   ["-h" "--help"]])

(defmacro timed
  "Evaluates expr. Returns the value of expr and the time in a map."
  [expr]
  `(let [start# (. System (nanoTime))
         ret# ~expr]
     {:res ret# :t (/ (double (- (. System (nanoTime)) start#)) 1000000.0)}))

(def base-list ["/status/health"
                "/status/ready"
                "/main/concepts?offset=1&limit=10&type=skill"])

(defn test-base-urls []
  (println "Testing base urls ...")
  (doseq [ep base-list]
    (let [{:keys [t]} (timed (curl/get (str base-url ep) header))]
      (swap! state assoc-in [:results :base-urls ep] t))))

(defn test-versions []
  (println "Testing versions ...")
  (when (nil? (re-find (re-pattern version-date) (:body (curl/get (str base-url "/main/versions") header))))
    (swap! state update-in [:failures] conj :versions)))

(defn test-graphql []
  (let [graphql-list ["{   concepts(type: [\"keyword\", \"occupation-name\"], preferred_label_contains: \"dag\") {     id     type     preferred_label     related {       id       preferred_label     }   } }"
                      "{   concepts(type: [\"keyword\", \"occupation-name\"], preferred_label_contains: \"dag\") {     id     type     preferred_label   } }"
                      "{   concepts(type: \"wage-type\") {     id     preferred_label   } }"]]
    (println "Testing graphql queries ...")
    (doseq [query graphql-list]
      (let [{:keys [t]} (timed (curl/get (str base-url "/graphql") {:query-params {"query" query}
                                                                    :header header}))]
        (swap! state assoc-in [:results :graphql-basic query] t)))))

(defn benchmark-graphql []
  (println "Benchmarking graphql queries ...")
  (doseq [query (:graphql-queries @state)]
    (loop [i 10
           results []]
      (if (= 0 i)
        (let [avg (/ (reduce + 0.0 results) 10.0)
              variance (/ (reduce (fn [result t] (+ result (Math/pow (- t avg) 2.0))) 0.0 results) 10.0)]
          (swap! state assoc-in [:results :graphql-benchmark query] {:avg avg :var variance}))
        (let [{:keys [t]} (timed (curl/get (str base-url "/graphql") {:query-params {"query" query}
                                                                      :header header}))]
          (recur (dec i) (conj results t)))))))

(defn benchmark-endpoints []
  (println "Benchmarking endpoints ...")
  (doseq [ep (:endpoints @state)]
    (loop [i 10
           results []]
      (if (= 0 i)
        (let [avg (/ (reduce + 0.0 results) 10.0)
              variance (/ (reduce (fn [result t] (+ result (Math/pow (- t avg) 2.0))) 0.0 results) 10.0)]
          (swap! state assoc-in [:results :endpoints-benchmark] {:avg avg :var variance}))
        (let [{:keys [t]} (timed (curl/get (str base-url ep) {:header header}))]
          (recur (dec i) (conj results t)))))))

(defn parse-graphql-txt [f]
  (let [queries (remove str/blank? (str/split (slurp f) #"---"))]
    (swap! state assoc-in [:graphql-queries] queries)))

(defn parse-endpoints-txt [f]
  (let [queries (remove str/blank? (str/split (slurp f) #"\n"))]
    (swap! state assoc-in [:endpoints] queries)))

(defn print-results []
  (let [df (java.text.SimpleDateFormat. "yyyMMddHHmmss")
        date (java.util.Date.)
        date-string (.format df date)
        file-name (str "results_" date-string ".edn")]
    (spit file-name (with-out-str (pprint @state)))
    (println "Results:" file-name)))

(defn run-it! []
  (println "JOB TECH BENCHMARKS")
  (println "-------------------")
  (let [{:keys [errors options summary]} (cli/parse-opts *command-line-args* cli-options)
        {:keys [help graphql endpoints]} options]
    (if (some? errors)
      (println errors)
      (if help
        (println summary)
        (do
          (when graphql
            (parse-graphql-txt graphql))
          (when endpoints
            (parse-endpoints-txt endpoints))
          (test-base-urls)
          (test-versions)
          (test-graphql)
          (when graphql
            (benchmark-graphql))
          (when endpoints
            (benchmark-endpoints))
          (print-results))))))

(run-it!)

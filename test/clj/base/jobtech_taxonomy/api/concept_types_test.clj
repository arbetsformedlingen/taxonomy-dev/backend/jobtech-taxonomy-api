(ns ^:integration-concept-types-tests base.jobtech-taxonomy.api.concept-types-test
  (:require [clojure.test :as test]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [utils.integration-helpers :as util]))

(test/use-fixtures :each util/fixture)

(defn- fill-database
  "Fill the test DB with some concepts"
  []
  (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "cykla" :preferred-label "cykla"})
  (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "spela" :preferred-label "spela"})
  (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "programmera" :preferred-label "programmera"})
  (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "ro" :preferred-label "ro"})
  (concepts/assert-concept util/cfg "Stallman" {:type "country" :definition "Sweden" :preferred-label "Sweden"})
  (concepts/assert-concept util/cfg "Stallman" {:type "country" :definition "Norway" :preferred-label "Norway"})
  (concepts/assert-concept util/cfg "Stallman" {:type "country" :definition "Finland" :preferred-label "Finland"})
  (concepts/assert-concept util/cfg "Stallman" {:type "country" :definition "Denmark" :preferred-label "Denmark"})
  (util/create-new-version 0))

(test/deftest ^:integration-concept-types concept-types
  (test/testing "Test the concept/types endpoint"
    (let [_ (fill-database)
          [status body] (util/send-request-to-json-service
                         :get "/v1/taxonomy/main/concept/types"
                         :query-params [])]
      (test/is (= 200 status))

      ;; See issue 796: It is the set of known values of :concept-type/id
      ;; that defines the set of types.
      (test/is (= '[] body)))))

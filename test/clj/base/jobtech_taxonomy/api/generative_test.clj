(ns ^:integration-generative-tests base.jobtech-taxonomy.api.generative-test
  (:require [clojure.pprint :refer [pprint]]
            [clojure.set :as set]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.test :as test]
            [clojure.test.check.clojure-test :refer [assert-check]]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [utils.integration-helpers :as util]))

(test/use-fixtures :each util/fixture)

(s/def ::non-empty-string?
  (s/and string?
         #(not= 0 (count %))))
(s/def ::positive-int
  (s/and int?
         #(>= % 1)))
(s/def ::natural-number
  (s/and int?
         #(>= % 0)))

;; define a concept generator
(s/def ::type
  (s/or ::random ::non-empty-string?
        ::collection #{"skill" "language" "keyword"}))
(s/def ::definition
  (s/or ::random ::non-empty-string?
        ::collection #{"programming" "digging" "driving"}))
(s/def ::preferred-label
  (s/or ::random ::non-empty-string?
        ::collection #{"programmer" "digger" "driver"}))
(s/def ::concept
  (s/keys :req [::type ::definition ::preferred-label]))

;; define a concept query
(s/def ::id
  (s/or ::random ::non-empty-string?
        ::collection #{"rEeE_DXA_Q4h" "zRtu_fYj_XC4" "mHpA_1Zm_7TP"}))
(s/def ::include-deprecated boolean?)
(s/def ::deprecated boolean?)
(s/def ::relation
  #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"})
(s/def ::related-ids (s/coll-of ::id))
(s/def ::offset ::natural-number)
(s/def ::limit ::positive-int)
(s/def ::version #{0 1 2})
(s/def ::include-legacy-information boolean?)
(s/def ::concept-query-parameters
  (s/keys :opt
          [::id ::preferred-label
           ::type ::include-deprecated
           ::deprecated ::relation
           ::related-ids ::offset
           ::limit ::version
           ::include-legacy-information]))

;; Convert the randomly generated data to:
;;  :query-params [{:key "type", :val "occupation-name"}
;;                 {:key "deprecated" :val "false"}
;;                 {:key "version" :val "next"})))
(defn adapt-concept-query [m]
  (mapv (fn [[k v]] {:key (name k) :val (str v)}) (seq m)))

(defn generate [spec]
  (gen/generate (s/gen spec)))

(defn generate-concept []
  (generate ::concept))

(defn clear-namespace [m]
  (set/rename-keys m
                   (zipmap (map (fn [k] k) (keys m))
                           (map (fn [k] (keyword (name k))) (keys m)))))

;; Assert nbr generated concepts to the database
(defn assert-concepts [nbr]
  (let [concepts (map (fn [_] (clear-namespace (generate ::concept))) (repeat nbr 0))]
    (doseq [c concepts]
      (concepts/assert-concept util/cfg "Stallman" c))
    concepts))

(defn send-concept-request [p]
  (let [params (adapt-concept-query (clear-namespace p))]
    (util/send-request-to-json-service
     :get "/v1/taxonomy/main/concepts"
     :query-params params)))

(s/fdef send-concept-request
  :args (s/cat :spec ::concept-query-parameters)
  :ret (s/cat :code #{200}
              :body (s/coll-of (s/keys))))

(defn check! [sym-or-syms opts]
  (run! (comp assert-check :clojure.spec.test.check/ret)
        (stest/check sym-or-syms opts)))

(test/deftest ^:integration-generative-concepts generative-test-concepts
  (test/testing "test query with generated parameters"
    (let [_ (util/create-new-version 0)
          _ (assert-concepts (rand-int 10))
          _ (util/create-new-version 1)
          _ (assert-concepts (rand-int 10))
          _ (util/create-new-version 2)]
      (check! `send-concept-request {:clojure.spec.test.check/opts {:num-tests 100}}))))

;; Generate a concept query used for the pagination test
(s/def ::concept-paging-query
  (s/or
   :type (s/keys :req [::type])
   :definition (s/keys :req [::definition])
   :preferred_label (s/keys :req [::preferred-label])))

(defmacro int-list [len]
  `(s/and
    (s/coll-of
     (s/int-in 1 ~len))
    #(< (count %) ~len)
    #(> (count %) 0)))

;; Generate a list of indices into a list of length len
(defn gen-list-split [len]
  (dedupe (sort (generate (int-list len)))))

;; Transform a list of indices into a list of offset/limit pairs
(defn index-to-interval [len split-idx]
  (for [[l u] (partition 2 1 (concat [0] split-idx [len]))]
    [l (- u l)]))

;; Create paging queries
(defn paging-queries [params pages]
  (let [base_params (clear-namespace params)]
    (mapv #(adapt-concept-query (merge base_params {:offset (first %) :limit (second %)})) pages)))

;; Send a main/concepts request and return the body
(defn send-concept-request-body [params]
  (let [[status body]
        (util/send-request-to-json-service
         :get "/v1/taxonomy/main/concepts"
         :query-params params)
        _ (test/is (= 200 status))]
    body))

(defn log-paging-result [ret expected actual]
  (if ret
    (println (pr-str "Test passed!"))
    (do
      (println "expected:")
      (pprint expected)
      (println "actual:")
      (pprint actual))))

(defn to-comparable-set [data]
  (sort-by hash (into () data))
  #_(if (or (list? data) (vector? data))
      {:size (count data)
       :unique (set data)}
      data))

 ;; Return true if the test passed.
 ;;
 ;; The test first runs the question without paging, then with,
 ;; and compares the results.
(defn test-concept-paging [params]
  (let [p (adapt-concept-query (clear-namespace params))
        expected (to-comparable-set (send-concept-request-body p))
        len (count expected)
        pages (cond
                (= len 0) '[]
                (= len 1) '[[0 1]]
                (= len 2) '[[0 1] [1 1]]
                (> len 2) (index-to-interval len (gen-list-split len)))]
    (if (= len 0)
      true ;; return true for now
      (let [pagingQueries (paging-queries params pages)
            actual (to-comparable-set (mapcat send-concept-request-body pagingQueries))
            ret (= expected actual)
            _ (log-paging-result ret expected actual)]
        ret))))

;; Define the input and output of the tested function
(s/fdef test-concept-paging
  :args (s/cat :query ::concept-paging-query)
  :ret true?)

(test/deftest ^:integration-generative-concepts-paging
  generative-test-concepts-paging
  (test/testing "test main/concepts paging generatively"
    (let [_ (util/create-new-version 0)
          _concepts (assert-concepts (rand-int 40))
          _ (util/create-new-version 1)]
      (check! `test-concept-paging {:clojure.spec.test.check/opts {:num-tests 100}}))))

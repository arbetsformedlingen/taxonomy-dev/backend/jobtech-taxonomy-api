(ns ^:integration-graphql-tests base.jobtech-taxonomy.api.graphql-test
  (:require [clojure.test :as test :refer [is]]
            [utils.helpers :refer [order-invariant]]
            [utils.integration-helpers :as util]))

(test/use-fixtures :each util/fixture)

(def ^:private broaderSkill {:type "skill" :definition "Drive machinery" :preferred-label "drive"})
(def ^:private narrowerSkill1 {:type "skill" :definition "ride bikes" :preferred-label "bike"})
(def ^:private narrowerSkill2 {:type "skill" :definition "fly airplanes" :preferred-label "fly"})

(def ^:private endpoint "/v1/taxonomy/graphql")

(def schemaQuery
  (str
   "query IntrospectionQuery {"
   "__schema {"
   "queryType { name }"
   "}}"))

(test/deftest ^:integration-graphql-test-0 access-schema-without-auth
  (test/testing "test that the schema is accessible without authentication"
    (let [[status _body] (util/send-request-to-json-service
                          :get endpoint
                          :query-params [{:key "query", :val schemaQuery}])]
      (test/is (= 200 status)))))

(test/deftest ^:integration-graphql-test-1 graphql-test-1
  (test/testing "test simple graphql query"
    (let [_ (util/create-new-version 0)
          _ (util/create-new-type "skill")
          [_ body1] (util/create-new-concept broaderSkill)
          new-concept (:concept body1)
          query (str "{concepts(id: \"" (:concept/id new-concept) "\") {preferred_label}}")
          _ (util/create-new-version 1)
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "drive" preferred_label)))))

(defn- buildRelationQuery
  [relation id id1 id2]
  (str
   "{concepts(id: \"" id "\") {"
   relation "(id: [\"" id1 "\" \"" id2 "\"]) {"
   "preferred_label"
   "}}}"))

(defn- test-relation
  [relation]
  (let [_ (util/create-new-version 0)
        _ (util/create-new-type "skill")
        [_ body1] (util/create-new-concept broaderSkill)
        broaderConcept (:concept body1)
        [_ body2] (util/create-new-concept narrowerSkill1)
        narrowerConcept1 (:concept body2)
        [_ body3] (util/create-new-concept narrowerSkill2)
        narrowerConcept2 (:concept body3)
        id (:concept/id broaderConcept)
        id1 (:concept/id narrowerConcept1)
        id2 (:concept/id narrowerConcept2)
        [_ _]
        (util/create-new-relation
         {:relation-type relation
          :concept-1 id
          :concept-2 id1})
        [_ _]
        (util/create-new-relation
         {:relation-type relation
          :concept-1 id
          :concept-2 id2
          :comment "desc"})
        _ (util/create-new-version 1)
        query (buildRelationQuery relation id id1 id2)
        [status body] (util/send-request-to-json-service
                       :get endpoint
                       :query-params [{:key "query", :val query}])
        rel (case relation
              "narrower" :narrower
              "broader" :broader
              "related" :related
              (throw (Exception. (str "'" relation "' not supported in test-relation"))))
        preferred_label1 (get-in body [:data :concepts 0 rel 0 :preferred_label])
        preferred_label2 (get-in body [:data :concepts 0 rel 1 :preferred_label])]
    (test/is (= 200 status))
    (test/is (= ["bike" "fly"] (sort [preferred_label1 preferred_label2])))))

(test/deftest ^:integration-graphql-test-2 graphql-test-2
  (test/testing "test a 'broader' graphql query"
    (test-relation "broader")))

(test/deftest ^:integration-graphql-test-3 graphql-test-3
  (test/testing "test a 'related' graphql query"
    (test-relation "related")))

(test/deftest ^:integration-graphql-test-4 graphql-test-4
  (test/testing "test adding a broader relation and find it by a narrower graphql query"
    (let [_ (util/create-new-version 0)
          _ (util/create-new-type "skill")
          [_ body1] (util/create-new-concept broaderSkill)
          broaderConcept (:concept body1)
          [_ body2] (util/create-new-concept narrowerSkill1)
          narrowerConcept (:concept body2)
          id (:concept/id broaderConcept)
          id1 (:concept/id narrowerConcept)
          [_ _]
          (util/create-new-relation
           {:relation-type "broader"
            :concept-1 id1
            :concept-2 id
            :comment "desc"})
          _ (util/create-new-version 1)
          query (buildRelationQuery "narrower" id id1 "incorrect conceptID")
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query", :val query}])
          preferred_label (get-in body [:data :concepts 0 :narrower 0 :preferred_label])]
      (test/is (= 200 status))
      (test/is (= "bike" preferred_label)))))

(test/deftest ^:integration-graphql-test-5 graphql-test-5
  (test/testing "changing concept between versions is observable with versions query"
    (let [_ (util/create-new-version 0)
          _ (util/create-new-type "skill")
          [_ body1] (util/create-new-concept
                     {:type "skill"
                      :definition "concept v1"
                      :preferred-label "concept v1"})
          id (get-in body1 [:concept :concept/id])
          _ (util/create-new-version 1)
          [_ _] (util/update-concept {:id id
                                      :definition "concept v2"
                                      :preferred-label "concept v2"})
          _ (util/create-new-version 2)
          [status body] (util/send-request-to-json-service
                         :get endpoint
                         :query-params [{:key "query"
                                         :val (format "
                                                 {
                                                   versions {
                                                     id
                                                     concepts(id: \"%s\") {
                                                       id
                                                       definition
                                                       preferred_label
                                                     }
                                                   }
                                                   }" id)}])]
      (test/is (= 200 status))
      (test/is (= {:data
                   {:versions
                    [{:id 1
                      :concepts [{:id id
                                  :definition "concept v1"
                                  :preferred_label "concept v1"}]}
                     {:id 2
                      :concepts [{:id id
                                  :definition "concept v2"
                                  :preferred_label "concept v2"}]}]}}
                  body)))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-event-types
  (let [_ (util/create-new-version 0)
        _ (util/create-new-type "skill")
        [_ body1] (util/create-new-concept
                   {:type "skill"
                    :definition "riding bicycle"
                    :preferred-label "cycling"})
        id (get-in body1 [:concept :concept/id])
        _ (util/create-new-version 1)
        _ (util/update-concept
           {:id id
            :preferred-label "riding bicycle"
            :alternative-labels "cycling"})
        [_ _] (util/send-request-to-json-service
               :post "/v1/taxonomy/private/concept/automatic-daynotes/"
               :headers [(util/header-auth-admin)]
               :query-params [{:key "id", :val id}
                              {:key "comment", :val "Prefer to call it riding"}])
        _ (util/create-new-version 2)
        _ (util/update-concept
           {:id id
            :deprecated true})
        [status body] (util/send-request-to-json-service
                       :get endpoint
                       :headers
                       [{:key "api-key"
                         :val "222"}]
                       :query-params
                       [{:key "query"
                         :val "{
                                                     changelog(from:0,to:\"next\") {
                                                       event: __typename
                                                       user
                                                       comment
                                                       concept {
                                                         id
                                                         type
                                                         preferred_label
                                                         alternative_labels
                                                       }
                                                       ... on Updated {
                                                         changes {
                                                           attribute
                                                           old_value
                                                           new_value
                                                         }
                                                       }
                                                     }
                                                   }"}])
        change-log (get-in body [:data :changelog])]
    (is (= 200 status))
    (is (= #{{:event "Created"
              :user "admin-1"
              :comment ""
              :concept {:id id
                        :preferred_label "cycling"
                        :type "skill"
                        :alternative_labels []}}
             {:event "Updated"
              :user "admin-1"
              :comment ""
              :concept {:id id
                        :type "skill"
                        :preferred_label "riding bicycle"
                        :alternative_labels ["cycling"]}
              :changes [{:attribute "alternative_labels"
                         :old_value []
                         :new_value ["cycling"]}
                        {:new_value "riding bicycle"
                         :attribute "preferred_label"
                         :old_value "cycling"}]}
             {:event "Commented"
              :user "admin-1"
              :comment "Prefer to call it riding"
              :concept {:id id
                        :type "skill"
                        :preferred_label "riding bicycle"
                        :alternative_labels ["cycling"]}}
             {:event "Deprecated"
              :user "admin-1"
              :comment ""
              :concept {:id id
                        :type "skill"
                        :preferred_label "riding bicycle"
                        :alternative_labels ["cycling"]}}}
           (set change-log)))
    (is (= 4 (count change-log)))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-version-filtering
  (let [_ (util/create-new-version 0)
        _ (util/create-new-type "skill")
        [_ body1] (util/create-new-concept
                   {:type "skill"
                    :definition "riding bicycle"
                    :preferred-label "cycling"})
        id (get-in body1 [:concept :concept/id])
        _ (util/create-new-version 1)
        _ (util/update-concept
           {:id id
            :preferred-label "riding bicycle"
            :alternative-labels "cycling"})
        [_ _] (util/send-request-to-json-service
               :post "/v1/taxonomy/private/concept/automatic-daynotes/"
               :headers [(util/header-auth-admin)]
               :query-params [{:key "id", :val id}
                              {:key "comment", :val "Prefer to call it riding"}])
        _ (util/create-new-version 2)
        _ (util/update-concept
           {:id id
            :deprecated true})
        [status body] (util/send-request-to-json-service
                       :get endpoint
                       :query-params
                       [{:key "query"
                         :val "{
                     changelog(from:1,to:2) {
                       event: __typename
                       concept {
                         id
                       }
                     }
                   }"}])]
    (is (= 200 status))
    (is (= (order-invariant {:data
                             {:changelog [{:event "Updated"
                                           :concept {:id id}}
                                          {:event "Commented"
                                           :concept {:id id}}]}})
           (order-invariant body)))))

(test/deftest ^:integration-graphql-changelog-test changelog-test-relations
  (let [_ (util/create-new-version 0)
        _ (util/create-new-type "skill")
        [_ body1] (util/create-new-concept
                   {:type "skill"
                    :definition "riding horse"
                    :preferred-label "riding horse"})
        horse-id (get-in body1 [:concept :concept/id])
        [_ body2] (util/create-new-concept
                   {:type "skill"
                    :definition "driving car"
                    :preferred-label "driving car"})
        car-id (get-in body2 [:concept :concept/id])
        _ (util/create-new-version 1)
        ;; 1. make 2 concepts related 
        [_ _]
        (util/create-new-relation
         {:relation-type "related"
          :concept-1 horse-id
          :concept-2 car-id
          :comment "riding and driving are related"})
        ;; 2. replace one with another 
        [_ _] (util/send-request-to-json-service
               :post "/v1/taxonomy/private/replace-concept"
               :headers [(util/header-auth-admin)]
               :query-params [{:key "old-concept-id", :val horse-id}
                              {:key "new-concept-id", :val car-id}
                              {:key "comment", :val "Horses are so 1890"}])
        _ (util/create-new-version 2)]
    (test/is
      ;; every relation assertion creates 2 events (one for each concept) that
      ;; don't have any defined order, hence using sets:
     (= #{;; 1. make 2 concepts related
          {:event "Updated"
           :concept {:id horse-id}
           :changes [{:attribute "related"
                      :old_value []
                      :new_value [{:id car-id}]}]}
          {:event "Updated"
           :concept {:id car-id}
           :changes [{:attribute "related"
                      :old_value []
                      :new_value [{:id horse-id}]}]}
           ;; 2. replace one with another
          {:event "Deprecated"
           :concept {:id horse-id}}
          {:event "Updated"
           :concept {:id horse-id}
           :changes [{:attribute "replaced_by"
                      :old_value []
                      :new_value [{:id car-id}]}]}
          {:event "Updated"
           :concept {:id car-id}
           :changes [{:attribute "replaces"
                      :old_value []
                      :new_value [{:id horse-id}]}]}}
        (-> (util/send-request-to-json-service
             :get endpoint
             :query-params
             [{:key "query"
               :val "{
                     changelog(from:1,to:2) {
                       event: __typename
                       concept {
                         id
                       }
                       ... on Updated {
                         changes {
                           attribute
                           old_value
                           new_value
                         }
                       }
                     }
                   }"}])
            second
            :data
            :changelog
            set)))))


(ns ^:integration-versions-tests base.jobtech-taxonomy.api.versions-test
  (:require [clojure.test :as test :refer [deftest is]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.versions :as versions]
            [utils.integration-helpers :as util])
  (:import [java.time.temporal ChronoUnit]
           [java.util Date]))

(set! *warn-on-reflection* true)

(test/use-fixtures :each util/fixture)

(deftest create-version-test
  (versions/create-new-version util/cfg 0)
  (is (= 0 (versions/get-current-version-id (dc/get-db util/cfg :next)))))

(deftest incremented-versions-only
  (versions/create-new-version util/cfg 0)
  (is (= 1 (:version (versions/create-new-version util/cfg 1))))
  (is (= 2 (:version (versions/create-new-version util/cfg 2))))
  (is (= ::versions/incorrect-new-version (versions/create-new-version util/cfg 10))))

(deftest rollbacks-are-not-allowed
  (let [_ (versions/create-new-version util/cfg 0)
        {:keys [version ^Date timestamp]} (versions/create-new-version util/cfg 1)
        _ (is (= 1 version))
        time-before-release (-> timestamp
                                .toInstant
                                (.minus 1 ChronoUnit/MILLIS)
                                Date/from)]
    (is (= ::versions/new-version-before-release (versions/create-new-version util/cfg 2 time-before-release)))))

(ns ^:integration-concepts-tests base.jobtech-taxonomy.api.concepts-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concept-types :as concept-types]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.core :as core]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [utils.integration-helpers :as util]))

(test/use-fixtures :each util/fixture)

(defn assert-type [type]
  (concept-types/assert-type util/cfg {:concept-type type :label-sv type :label-en type :user-id "Gustav"}))

(deftest ^:integration-concepts-test-empty-param concepts-test-empty-params
  (testing "test query with a parameter without value"
    (let [_ (util/create-new-version 0)
          [status _body] (util/send-request-to-json-service
                          :get "/v1/taxonomy/main/concepts"
                          :headers [(util/header-auth-admin)]
                          :query-params [{:key "type", :val ""}])]
      (test/is (= 400 status)))))

(deftest ^:integration-concepts-test-no concepts-test-no-params
  (testing "test query without parameters"
    (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "cyklade" :preferred-label "cykla"})
    (let [_ (util/create-new-version 0)
          [status _body] (util/send-request-to-json-service
                          :get "/v1/taxonomy/main/concepts"
                          :headers [(util/header-auth-admin)]
                          :query-params [])
          _ (util/create-new-version 1)]
      (test/is (= 200 status)))))

(deftest ^:integration-concepts-test-0 concepts-test-0
  (testing "test assert concept"
    (assert-type "skill")
    (concepts/assert-concept util/cfg "Stallman" {:type "skill" :definition "cyklade" :preferred-label "cykla"})
    (let [[_status _body] (util/send-request-to-json-service
                           :get "/v1/taxonomy/main/concepts"
                           :query-params [{:key "type", :val "skill"}])
          found-concept (first (concepts/find-concepts util/cfg {:preferred-label "cykla" :version :next}))]
      (is (= "cykla" (get found-concept :concept/preferred-label))))))

(deftest ^:integration-concepts-test-1 concepts-test-1
  (testing "test concept relation 'related'"
    (assert-type "skill")
    (let [id-1 (get-in (concepts/assert-concept util/cfg "Stallman" {:type "skill"
                                                                     :definition "pertest0"
                                                                     :preferred-label "pertest0"})
                       [2 :concept/id])
          id-2 (get-in (concepts/assert-concept util/cfg "Stallman" {:type "skill"
                                                                     :definition "pertest1"
                                                                     :preferred-label "pertest1"})
                       [2 :concept/id])
          [_tx _rel] (concepts/assert-relation util/cfg "Stallman" {:concept-1 id-1
                                                                    :concept-2 id-2
                                                                    :type "related"
                                                                    :description "desc"
                                                                    :substitutability-percentage 0})
          rel (concepts/fetch-relation-entity-id-from-concept-ids-and-relation-type util/cfg id-1 id-2 "related")]
      (is (some? rel)))))

(deftest replacement-deprecates-old-concept
  (assert-type "skill")
  (let [writing-id (get-in (concepts/assert-concept util/cfg "Stallman" {:type "skill"
                                                                         :definition "writing"
                                                                         :preferred-label "Writing"})
                           [2 :concept/id])
        texting-id (get-in (concepts/assert-concept util/cfg "Stallman" {:type "skill"
                                                                         :definition "texting"
                                                                         :preferred-label "Texting"})
                           [2 :concept/id])
        tx-data0 (core/replace-deprecated-concept util/cfg "Stallman" writing-id texting-id nil)
        tx-data1 (core/replace-deprecated-concept util/cfg "Stallman" writing-id "<<<invalid id>>>" nil)
        tx-data2 (core/replace-deprecated-concept util/cfg "Stallman" "<<<invalid id>>>" texting-id nil)
        tx-data3 (core/replace-deprecated-concept util/cfg "<<<invalid id2>>>" "<<<invalid id>>>" texting-id nil)
        deprecated-writing (first (concepts/find-concepts util/cfg {:id writing-id :version :next :deprecated true}))
        tx-data4 (core/unreplace-deprecated-concept util/cfg "Stallman" writing-id texting-id nil)
        tx-data5 (core/unreplace-deprecated-concept util/cfg "Stallman" writing-id "<<<invalid id>>>" nil)
        tx-data6 (core/unreplace-deprecated-concept util/cfg "Stallman" "<<<invalid id>>>" texting-id nil)
        tx-data7 (core/unreplace-deprecated-concept util/cfg "<<<invalid id>>>" "<<<invalid id2>>>" texting-id nil)
        deprecated-writing2 (first (concepts/find-concepts util/cfg {:id writing-id :version :next :deprecated true}))]
    (is (some? tx-data0))
    (is (nil? tx-data1))
    (is (nil? tx-data2))
    (is (nil? tx-data3))
    (is (some? tx-data4))
    (is (nil? tx-data5))
    (is (nil? tx-data6))
    (is (nil? tx-data7))
    (is (:concept/deprecated deprecated-writing))
    (is (= texting-id (-> deprecated-writing :concept/replaced-by first :concept/id)))
    (is (empty? (:concept/replaced-by deprecated-writing2)))))

;; Define desired behaviour of the include-deprecated parameter
(deftest include-deprecated
  (assert-type "occupation-name")
  (let [;; Create two concepts
        deprecated-job-id (get-in (concepts/assert-concept util/cfg "user0" {:type "occupation-name"
                                                                             :definition "deprecated-job"
                                                                             :preferred-label "deprecated-job"})
                                  [2 :concept/id])
        undeprecated-job-id (get-in (concepts/assert-concept util/cfg "user0" {:type "occupation-name"
                                                                               :definition "undeprecated-job"
                                                                               :preferred-label "undeprecated-job"})
                                    [2 :concept/id])

        ;; Deprecate one of the two created concepts
        _ (core/retract-concept util/cfg "user0" deprecated-job-id "there are no kusks anymore")

        ;; Query the DB with deprecated=[true|false|unspecified]
        dep-true (map #(get % :concept/id)
                      (concepts/find-concepts util/cfg {:type ["occupation-name"] :version :next :deprecated true}))
        dep-false (map #(get % :concept/id)
                       (concepts/find-concepts util/cfg {:type ["occupation-name"] :version :next :deprecated false}))
        dep-unspec (map #(get % :concept/id)
                        (concepts/find-concepts util/cfg {:type ["occupation-name"] :version :next}))

        ;; Query the API with deprecated=[true|false|unspecified]
        [_ body-api-dep-true] (util/send-request-to-json-service
                               :get "/v1/taxonomy/main/concepts"
                               :headers [(util/header-auth-admin)]
                               :query-params [{:key "type", :val "occupation-name"}
                                              {:key "deprecated" :val "true"}
                                              {:key "version" :val "next"}])
        [_ body-api-dep-false] (util/send-request-to-json-service
                                :get "/v1/taxonomy/main/concepts"
                                :headers [(util/header-auth-admin)]
                                :query-params [{:key "type", :val "occupation-name"}
                                               {:key "deprecated" :val "false"}
                                               {:key "version" :val "next"}])
        [_ body-api-dep-unspec] (util/send-request-to-json-service
                                 :get "/v1/taxonomy/main/concepts"
                                 :headers [(util/header-auth-admin)]
                                 :query-params [{:key "type", :val "occupation-name"}
                                                {:key "version" :val "next"}])

        api-dep-true (map #(get % :taxonomy/id) body-api-dep-true)
        api-dep-false (map #(get % :taxonomy/id) body-api-dep-false)
        api-dep-unspec (map #(get % :taxonomy/id) body-api-dep-unspec)]

    ;; If deprecated=true, we should see both concepts
    (is (= (set dep-true) (set [undeprecated-job-id deprecated-job-id])))
    ;; If deprecated=false, we should only see the undeprecated concept
    (is (= (set dep-false) (set [undeprecated-job-id])))
    ;; If deprecated is unspecified, we should get the same results as deprecated=false
    (is (= (set dep-unspec) (set dep-false)))

    ;; If deprecated=true, we should see both concepts
    (is (= (set api-dep-true) (set [undeprecated-job-id deprecated-job-id])))
    ;; If deprecated=false, we should only see the undeprecated concept
    (is (= (set api-dep-false) (set [undeprecated-job-id])))
    ;; If deprecated is unspecified, we should get the same results as deprecated=false
    (is (= (set api-dep-unspec) (set api-dep-false)))))

(deftest include-no-esco-relation
  (assert-type "occupation-name")
  (let [;; Create two concepts
        id-no-esco-relation (get-in (concepts/assert-concept util/cfg "user1" {:type "occupation-name"
                                                                               :definition "no-esco-relation-job"
                                                                               :preferred-label "no-esco-relation-job"})

                                    [2 :concept/id])
        id-has-esco-relation (get-in (concepts/assert-concept util/cfg "user1" {:type "occupation-name"
                                                                                :definition "job-with-esco-relation"
                                                                                :preferred-label "job-with-esco-relation"})
                                     [2 :concept/id])

        ;; Deprecate one of the two created concepts
        _ (concepts/accumulate-concept util/cfg "user1" {:id id-no-esco-relation :no-esco-relation true})

        ;; Query the DB
        esco-unspec (map #(get % :concept/id)
                         (concepts/find-concepts util/cfg {:type ["occupation-name"] :version :next}))

        ;; Query the API with deprecated=[true|false|unspecified]
        [_ body-api-esco-unspec] (util/send-request-to-json-service
                                  :get "/v1/taxonomy/private/concepts"
                                  :headers [(util/header-auth-admin)]
                                  :query-params [{:key "type", :val "occupation-name"}
                                                 {:key "version" :val "next"}])
        api-esco-unspec (map #(get % :taxonomy/id) body-api-esco-unspec)]

    ;; no-esco-relation entry should be included if true, false or not set (nil)
    (is (= (set esco-unspec) (set [id-no-esco-relation id-has-esco-relation])))
    (is (= (set api-esco-unspec) (set [id-no-esco-relation id-has-esco-relation])))))

(defn- check-successful-relation-transaction [x]
  (is (map? x))
  (is (contains? x :time))
  (is (contains? x :relation)))

(deftest accumulate-relation-only-test
  (testing "Context of the test assertions"
    (let [user-id "user1"
          concept-a "1111_111_111"
          concept-b "2222_222_222"]
      (#'concepts/assert-concept-part util/cfg user-id
                                      {:id concept-a
                                       :type "occupation-name"
                                       :definition "Mjukvaruutvecklare"
                                       :preferred-label "Mjukvaruutvecklare"})
      (#'concepts/assert-concept-part util/cfg user-id
                                      {:id concept-b
                                       :type "ssyk-level-4"
                                       :definition "Jobb inom data"
                                       :preferred-label "Jobb inom data"})
    ;; First check that the relation is correctly created.
      (is (= "1111_111_111" concept-a))
      (is (= "2222_222_222" concept-b))
      (let [[tx rel] (concepts/assert-relation util/cfg user-id {:concept-1 concept-a
                                                                 :concept-2 concept-b
                                                                 :type "narrower"
                                                                 :description "desc"
                                                                 :substitutability-percentage 19})
              ;;narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")
             ;; broader (concepts/fetch-relation-data util/cfg concept-a concept-b "broader")
            ]
        (is (some? tx))
        (is (= {:relation/concept-2 [:concept/id "1111_111_111"],
                :relation/concept-1 [:concept/id "2222_222_222"],
                :relation/description "desc",
                :relation/id "2222_222_222:broader:1111_111_111",
                :relation/substitutability-percentage 19,
                :relation/type "broader"}
               rel))
        (let [r (dc/q {:find '[?r ?e ?s ?attrib-name]
                       :in '[$ ?id]
                       :where '[[?r :relation/id ?id]
                                [?r ?e ?s]
                                [?e :db/ident ?attrib-name]]}
                      (dc/get-db util/cfg :next)
                      "2222_222_222:broader:1111_111_111")]
          (is (= 6 (count r))))))))

(deftest accumulate-relation-test
  (let [user-id "user1"

        _ (assert-type "occupation-name")
        _ (assert-type "ssyk-level-4")

        ;; Create two concepts
        concept-a (get-in (concepts/assert-concept util/cfg user-id {:type "occupation-name"
                                                                     :definition "Mjukvaruutvecklare"
                                                                     :preferred-label "Mjukvaruutvecklare"})

                          [2 :concept/id])
        concept-b (get-in (concepts/assert-concept util/cfg user-id {:type "ssyk-level-4"
                                                                     :definition "Jobb inom data"
                                                                     :preferred-label "Jobb inom data"})
                          [2 :concept/id])

        ;; Start by incorrectly asserting the 'narrower' relation between these concepts
        [_tx _rel] (concepts/assert-relation util/cfg user-id {:concept-1 concept-a 
                                                               :concept-2 concept-b 
                                                               :type "narrower" 
                                                               :description "desc" 
                                                               :substitutability-percentage 19})

        narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")
        broader (concepts/fetch-relation-data util/cfg concept-a concept-b "broader")

        new-desc "Mjukvaruutvecklare är smalare än begreppet 'Jobb inom data'"]

    ;; First check that the relation is correctly created.
    (is (some? narrower))
    (is (= "desc" (:relation/description narrower)))
    (is (nil? broader))

    ;; First call to accumulate-relation to change the description, but not the type
    (let [result (concepts/accumulate-relation
                  util/cfg user-id {:concept-1 concept-a
                                    :concept-2 concept-b
                                    :relation-type "narrower"
                                    :comment "Improve the description"
                                    :description new-desc})
          narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")
          broader (concepts/fetch-relation-data util/cfg concept-a concept-b "broader")]
      (check-successful-relation-transaction result)
      (is (= (:relation/description narrower) new-desc))
      (is (nil? broader)))

    ;; Then try to change the type
    (let [result (concepts/accumulate-relation util/cfg user-id {:concept-1 concept-a
                                                                 :concept-2 concept-b
                                                                 :relation-type "narrower"
                                                                 :new-relation-type "broader"
                                                                 :comment "Fix the type of relation"})
          narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")
          broader (concepts/fetch-relation-data util/cfg concept-a concept-b "broader")]
      (check-successful-relation-transaction result)
      (is (nil? narrower))
      (is (some? broader))
      (is (= (:relation/description broader) new-desc))
      (is (= (:relation/substitutability-percentage broader) 19)))

    ;; Then change the type again, but this time also update the description and substitutability.
    (let [result (concepts/accumulate-relation
                  util/cfg user-id {:concept-1 concept-a
                                    :concept-2 concept-b
                                    :relation-type "broader"
                                    :new-relation-type "broad-match"
                                    :comment "Update many things"
                                    :substitutability-percentage 57
                                    :description "broad-match is great!"})
          broader (concepts/fetch-relation-data util/cfg concept-a concept-b "broader")
          broad-match (concepts/fetch-relation-data util/cfg concept-a concept-b "broad-match")]
      (check-successful-relation-transaction result)
      (is (nil? broader))
      (is (some? broad-match))
      (is (= (:relation/description broad-match) "broad-match is great!"))
      (is (= (:relation/substitutability-percentage broad-match) 57)))

    ;; Create another relation and try to change the type to broad-match to check that
    ;; the collision is properly handled!
    (is (nil? (concepts/fetch-relation-data util/cfg concept-a concept-b "narrow-match")))
    (let [[_tx _rel] (concepts/assert-relation util/cfg user-id {:concept-1 concept-a 
                                                                 :concept-2 concept-b 
                                                                 :type "narrow-match" 
                                                                 :description "Mjao!" 
                                                                 :substitutability-percentage 64})]
      (is (some? (concepts/fetch-relation-data util/cfg concept-a concept-b "narrow-match"))))

    (let [result (try (concepts/accumulate-relation
                       util/cfg
                       user-id {:concept-1 concept-a
                                :concept-2 concept-b
                                :relation-type "narrow-match"
                                :new-relation-type "broad-match"
                                :comment "Expecting problems."})
                      (catch AssertionError _e
                        :assertion-error)
                      (catch Exception _e
                        :exception))

          broad-match (concepts/fetch-relation-data util/cfg concept-a concept-b "broad-match")
          narrow-match (concepts/fetch-relation-data util/cfg concept-a concept-b "narrow-match")]
      ;; Not sure if it is useful to check the value of `result`, because
      ;; the preconditions result in assertions and assertions can be
      ;; turned off at the JVM level. Would it be better to turn these
      ;; preconditions into runtime checks that are *always* performed?

      ;; No matter the return value `result`, we should check that the database
      ;; is the same as before.
      (is (#{:assertion-error} result))

      (is (= (:relation/description broad-match) "broad-match is great!"))
      (is (= (:relation/substitutability-percentage broad-match) 57))
      (is (= (:relation/description narrow-match) "Mjao!"))
      (is (= (:relation/substitutability-percentage narrow-match) 64)))))

(deftest accumulate-relation-with-missing-values-test
  (assert-type "occupation-name")
  (assert-type "ssyk-level-4")
  (let [user-id "user1"

        ;; Create two concepts
        concept-a (get-in (concepts/assert-concept util/cfg
                                                   user-id {:type "occupation-name"
                                                            :definition "Mjukvaruutvecklare"
                                                            :preferred-label "Mjukvaruutvecklare"})

                          [2 :concept/id])
        concept-b (get-in (concepts/assert-concept util/cfg
                                                   user-id {:type "ssyk-level-4"
                                                            :definition "Jobb inom data"
                                                            :preferred-label "Jobb inom data"})
                          [2 :concept/id])

        ;; Start by incorrectly asserting the 'narrower' relation between these concepts
        [_tx _rel] (concepts/assert-relation util/cfg user-id {:concept-1 concept-a
                                                               :concept-2 concept-b
                                                               :type "narrower"})

        narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")
        _new-desc "Mjukvaruutvecklare är smalare än begreppet 'Jobb inom data'"]
    (is (some? narrower))
    (is (contains? narrower :id))
    (is (not (contains? narrower :relation/description)))
    (is (not (contains? narrower :relation/substitutability-percentage)))

    ;; Set the substitutability percentage
    (let [result (concepts/accumulate-relation
                  util/cfg
                  user-id {:concept-1 concept-a
                           :concept-2 concept-b
                           :relation-type "narrower"
                           :substitutability-percentage 13
                           :comment "Set percentage"})
          narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")]
      (check-successful-relation-transaction result)
      (is (contains? narrower :id))
      (is (not (contains? narrower :relation/description)))
      (is (= 13 (:relation/substitutability-percentage narrower))))

    ;; Set the description
    (let [result (concepts/accumulate-relation
                  util/cfg
                  user-id {:concept-1 concept-a
                           :concept-2 concept-b
                           :relation-type "narrower"
                           :description "Related stuff"
                           :comment "Set description"})
          narrower (concepts/fetch-relation-data util/cfg concept-a concept-b "narrower")]
      (check-successful-relation-transaction result)
      (is (contains? narrower :id))
      (is (= 13 (:relation/substitutability-percentage narrower)))
      (is (= "Related stuff" (:relation/description narrower))))))

(deftest extend-query-test
  (is (= {:find ['?a '?b]} (concepts/extend-query {} {:find ['?a '?b]})))
  (is (= {:find ['?a '?b]} (concepts/extend-query {:find ['?a]} {:find ['?b]})))
  (is (= {:find ['?a '?b]} (concepts/extend-query {:find ['?a '?b]} {:find []})))
  (is (= {:find ['?a '?b]} (concepts/extend-query {:find ['?a '?b]} {})))
  (is (= {:offset 119} (concepts/extend-query {:offset 120} {:offset 119}))))

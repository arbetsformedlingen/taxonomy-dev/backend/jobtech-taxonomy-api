(ns ^:slow integration.jobtech-taxonomy.api.core-test
  (:require [clj-http.client :as http-client]
            [clojure.data.json :as json]
            [clojure.test :refer [deftest is use-fixtures]]
            [jobtech-taxonomy.api.core :as core]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [utils.db-fixtures :as udf])
  (:import [org.eclipse.jetty.server Server ServerConnector]))

(set! *warn-on-reflection* true)

(def ^:dynamic *host* nil)

(def cfg udf/real-cfg)

(defn healthy-server? [host]
  (try
    (= 200 (:status (http-client/get (str host "/v1/taxonomy/status/health"))))
    (catch Exception _e
      false)))

(def throw-error (atom false))

(defn fixture [f]
  (let [original-find-concepts concepts/find-concepts]
    (with-redefs [concepts/find-concepts
                  (fn [& args]
                    (when (deref throw-error)
                      (throw (AssertionError.)))
                    (apply original-find-concepts args))]
      (let [^Server server (core/start-http-server cfg)
            ^ServerConnector connector (-> server .getConnectors (aget 0))]
        (binding [*host* (format "http://localhost:%d" (.getPort connector))]
          (try
            (f)
            (finally
              (core/stop-http-server cfg server)
              (dc/delete-database cfg)))
          (when (healthy-server? *host*)
            (throw (ex-info "Server is still responding to health check"
                            {:host *host*}))))))))

(use-fixtures :once fixture)

(deftest server-test
  (is (healthy-server? *host*))
  (let [address (format "%s/v1/taxonomy/specific/concepts/ssyk?ssyk-code-2012=3334" *host*)
        response (http-client/get address
                                  {:accept :json
                                   :throw-exceptions false})
        expected-body [#:taxonomy{:type "ssyk-level-4",
                                  :definition
                                  "Förmedlar köp, försäljning eller uthyrning av hus, bostadsrätter, industribyggnader m.m. Marknadsför och visar objekt.",
                                  :id "Fghp_zje_WA8",
                                  :ssyk-code-2012 "3334",
                                  :preferred-label "Fastighetsmäklare"}]]
    (is (= 200 (:status response)))
    (is (= expected-body (json/read-str (:body response) {:key-fn keyword})))

    (reset! throw-error true)
    (let [response (http-client/get address
                                    {:accept :json
                                     :throw-exceptions false})]
      (is (= 500 (:status response))))
    (reset! throw-error false)

    ;; Make sure the server didn't crash and responds correctly again.
    (let [response (http-client/get address
                                    {:accept :json
                                     :throw-exceptions false})]
      (is (= 200 (:status response)))
      (is (= expected-body (json/read-str (:body response)
                                          {:key-fn keyword}))))))


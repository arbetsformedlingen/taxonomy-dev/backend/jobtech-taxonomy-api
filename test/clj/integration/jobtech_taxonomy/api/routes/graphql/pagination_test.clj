(ns ^:graphql/pagination integration.jobtech-taxonomy.api.routes.graphql.pagination-test
  (:require [clojure.set :as set]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :as rmr]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :as uih]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (udf/database-minimal udf/cfg))

(def services-endpoint (handler/app udf/cfg))

(def concept-count-for-version-query
  "query MyQuery($version: VersionRef!) {
     total_concepts_count(version: $version) { count }}")

(def paginated-concepts-for-version-query
  "query MyQuery($version: VersionRef!, $offset: Int!, $limit: Int!) {
     concepts(version: $version, limit: $limit, offset: $offset) { id }}")

(defn- ready-set [response]
  (-> response
      uih/json-body
      (get-in [:data :concepts])
      (#(map :id %))
      set))

(deftest graphql-pagination-test
  (testing "There should be 19 concepts up to version 3."
    (is (= {:data {:total_concepts_count {:count 19}}}
           (-> (rmr/request :get "/v1/taxonomy/graphql"
                            {:query concept-count-for-version-query
                             :variables "{\"version\": \"3\"}"})
               services-endpoint
               uih/json-body))))
  (testing "There should be 19 concepts up to version 3, even if we get them 7 at a time."
    (let [first-7 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                   {:query paginated-concepts-for-version-query
                                    :variables "{\"version\": \"3\", \"offset\": 0, \"limit\": 7}"})
                      services-endpoint
                      ready-set)
          next-7 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                  {:query paginated-concepts-for-version-query
                                   :variables "{\"version\": \"3\", \"offset\": 7, \"limit\": 7}"})
                     services-endpoint
                     ready-set)
          last-5 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                  {:query paginated-concepts-for-version-query
                                   :variables "{\"version\": \"3\", \"offset\": 14, \"limit\": 7}"})
                     services-endpoint
                     ready-set)]
      (is (= 19 (count (set/union first-7 next-7 last-5))))
      (is (empty? (set/intersection first-7 next-7)))
      (is (empty? (set/intersection first-7 last-5)))
      (is (empty? (set/intersection next-7 last-5))))))

(deftest auth-graphql-pagination-test
  (testing "There should be 28 concepts up to version `next`."
    (is (= {:data {:total_concepts_count {:count 28}}}
           (-> (rmr/request :get "/v1/taxonomy/graphql"
                            {:query concept-count-for-version-query
                             :variables "{\"version\": \"next\"}"})
               services-endpoint
               uih/json-body))))
  (testing "There should be 28 concepts up to version `next`, even if we get them 10 at a time."
    (let [first-10 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                    {:query paginated-concepts-for-version-query
                                     :variables "{\"version\": \"next\", \"offset\": 0, \"limit\": 10}"})
                       (rmr/header "api-key" "222")
                       services-endpoint
                       ready-set)
          next-10 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                   {:query paginated-concepts-for-version-query
                                    :variables "{\"version\": \"next\", \"offset\": 10, \"limit\": 10}"})
                      (rmr/header "api-key" "222")
                      services-endpoint
                      ready-set)
          last-8 (-> (rmr/request :get "/v1/taxonomy/graphql"
                                  {:query paginated-concepts-for-version-query
                                   :variables "{\"version\": \"next\", \"offset\": 20, \"limit\": 10}"})
                     (rmr/header "api-key" "222")
                     services-endpoint
                     ready-set)]
      (is (= 28 (count (set/union first-10 next-10 last-8))))
      (is (empty? (set/intersection first-10 next-10)))
      (is (empty? (set/intersection first-10 last-8)))
      (is (empty? (set/intersection next-10 last-8))))))

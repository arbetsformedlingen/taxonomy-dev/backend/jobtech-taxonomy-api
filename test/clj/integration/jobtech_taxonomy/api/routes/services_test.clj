(ns integration.jobtech-taxonomy.api.routes.services-test
  (:require [clojure.string :as str]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.common.build-info :as build-info]
            [ring.mock.request :refer [request header]]
            [utils.db-fixtures :as udf]
            [utils.helpers :refer [not-working-as-expected]]
            [utils.integration-helpers :as integration-helpers :refer [*app* json-body]]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

;;Main
(deftest test-main-concept-changes-v-2
  (testing "GET /v1/taxonomy/main/concept/changes"
    (let [response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                   {:after-version 1}))
          status (:status response)
          hovslag (udf/get-from (json-body response) :taxonomy/version 2)]
      (is (= 200 status))
      (is (= {:taxonomy/event-type "CREATED"
              :taxonomy/latest-version-of-concept {:taxonomy/definition "Hovslageri"
                                                   :taxonomy/id "gjd2_JSR_AeG"
                                                   :taxonomy/preferred-label "Hovslageri"
                                                   :taxonomy/type "skill"}
              :taxonomy/new-concept {:taxonomy/definition "Hovslageri"
                                     :taxonomy/id "gjd2_JSR_AeG"
                                     :taxonomy/preferred-label "Hovslageri"
                                     :taxonomy/type "skill"}
              :taxonomy/version 2
              #_#_:taxonomy/transaction-id 536887625}
             hovslag)))))

(deftest test-main-concept-changes
  (testing "GET /v1/taxonomy/main/concept/changes"
    (let [response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                   {:after-version 2 :to-version-inclusive 3}))
          status (:status response)
          body (json-body response)]
      (is (= 200 status))
      (is (= 6 (count body)))
      #_(is (= {:taxonomy/event-type "UPDATED"
                :taxonomy/latest-version-of-concept {:taxonomy/definition "Persiska/Farsi"
                                                     :taxonomy/id "sNoC_CTL_8Ph"
                                                     :taxonomy/preferred-label "Persiska/Farsi"
                                                     :taxonomy/type "language"
                                                     :taxonomy/alternative-labels ["Farsi" "Persiska"]}
                :taxonomy/version 15
                #_#_:taxonomy/transaction-id 536881815
                :taxonomy/concept-attribute-changes '({:taxonomy/added ("Persiska/Farsi")
                                                       :taxonomy/attribute "preferred-label"
                                                       :taxonomy/new-value "Persiska/Farsi"
                                                       :taxonomy/old-value "Persiska (Farsi)"
                                                       :taxonomy/removed ("Persiska (Farsi)")})}
               (first (json-body response)))))))

(deftest test-main-concept-changes-invalid-version
  (testing "GET /v1/taxonomy/main/concept/changes"
    (let [response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                   {}))]
      (is (= 400 (:status response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                   {:after-version "----"}))]
      (is (= 400 (:status response))))))

(defn slice-offset-limit [data offset limit]
  (let [data (vec data)
        n (count data)]
    (case [(some? offset) (some? limit)]
      [false false] data
      [false true] (subvec data 0 (min n limit))
      [true false] (subvec data offset)
      [true true] (subvec data offset (min n (+ offset limit))))))

(defn count= [a b]
  (= (count a) (count b)))

(deftest test-main-concept-changes-with-limit-and-offset
  (testing "GET /v1/taxonomy/main/concept/changes"
    (let [all-response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                       {:after-version 1}))
          all-changes (vec (json-body all-response))
          n (count all-changes)]
      (is (= 200 (:status all-response)))

      ;; Generative test: Compare against slices of the full response.
      (doseq [offset [0 1 5 7]
              limit [1 4 5 10 30]]
        (let [expected-changes (subvec all-changes offset (min n (+ offset limit)))
              response (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                       {:after-version 1
                                        :offset offset
                                        :limit limit}))]
          (is (= 200 (:status response)))
          (is (= expected-changes (json-body response))))))))

(deftest test-main-concept-changes-with-invalid-offset-or-limit
  (testing "GET /v1/taxonomy/main/concept/changes"
    (is (= 400 (:status (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                        {:after-version 1
                                         :offset -10
                                         :limit 1})))))
    (is (= 400 (:status (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                        {:after-version 1
                                         :offset 0
                                         :limit 0})))))
    (is (= 400 (:status (*app* (request :get "/v1/taxonomy/main/concept/changes"
                                        {:after-version 1
                                         :offset 0
                                         :limit -10})))))))

(deftest test-main-concept-types
  (let [expected-types-v1 ["continent"
                           "country"
                           "driving-licence"
                           "employment-duration"
                           "employment-type"
                           "isco-level-4"
                           "keyword"
                           "language"
                           "language-level"
                           "municipality"
                           "occupation-collection"
                           "occupation-experience-year"
                           "occupation-field"
                           "occupation-name"
                           "region"
                           "skill"
                           "skill-headline"
                           "sni-level-1"
                           "sni-level-2"
                           "sni-level-3"
                           "sni-level-4"
                           "sni-level-5"
                           "ssyk-level-1"
                           "ssyk-level-2"
                           "ssyk-level-3"
                           "ssyk-level-4"
                           "sun-education-field-1"
                           "sun-education-field-2"
                           "sun-education-field-3"
                           "sun-education-level-1"
                           "sun-education-level-2"
                           "sun-education-level-3"
                           "unemployment-fund"
                           "unemployment-type"
                           "wage-type"
                           "worktime-extent"]
        expected-types-all (sort (conj expected-types-v1 "sun-education-field-4"))]
    (testing "GET /v1/taxonomy/main/concept/types"
      (let [response (*app* (request :get "/v1/taxonomy/main/concept/types"
                                     {:version 1}))
            status (:status response)]
        (is (= 200 status))
        (is (= expected-types-v1 (json-body response))))
      (let [response (*app* (request :get "/v1/taxonomy/main/concept/types"
                                     {}))
            status (:status response)]
        (is (= 200 status))
        (is (= (set expected-types-all) (set (json-body response)))))
      (let [response (*app* (request :get "/v1/taxonomy/main/concept/types"
                                     {:version :bad-version-id}))
            status (:status response)]
        (is (= 400 status))))))

(deftest test-main-concepts
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:id "MS1s_AYg_kZe"}))
          status (:status response)]
      (is (= 200 status))
      (is (= [{:taxonomy/definition "Caymanöarna"
               :taxonomy/id "MS1s_AYg_kZe"
               :taxonomy/preferred-label "Caymanöarna"
               :taxonomy/type "country"}] (json-body response)))))
  (testing "GET /v1/taxonomy/main/concepts should fail"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:id "MS1s_AYg_kZe"}))
          status (:status response)
          body (json-body response)
          next-gen (udf/get-from body :taxonomy/id "NEXT_GEN_ONE")]
      (is (= 200 status))
      (is (nil? next-gen)))))

(deftest test-main-concepts-slices-for-different-versions
  (testing "GET /v1/taxonomy/main/concepts"
    (doseq [vdata [{} {:version 7} {:version 3}]]
      (let [all-response (*app* (request :get "/v1/taxonomy/main/concepts"
                                         vdata))
            all-concepts-for-version (vec (json-body all-response))
            n (count all-concepts-for-version)]
        (is (< 4 n))
        (doseq [offset [0 1 3]]
          (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                         (merge vdata {:offset offset})))]
            (is (= 200 (:status response)))
            (is (count= (slice-offset-limit all-concepts-for-version offset nil)
                        (json-body response))))
          (doseq [limit [1 2 5]]
            (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                           (merge vdata
                                                  {:offset offset
                                                   :limit limit})))]
              (is (= 200 (:status response)))
              (is (count= (slice-offset-limit all-concepts-for-version offset limit)
                          (json-body response))))))
        (doseq [limit [1 2 5]]
          (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                         (merge vdata
                                                {:limit limit})))]
            (is (= 200 (:status response)))
            (is (count= (slice-offset-limit all-concepts-for-version 0 limit)
                        (json-body response)))))
        (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                       (merge vdata
                                              {:limit 0})))]
          (is (= 400 (:status response))))
        (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                       (merge vdata {:limit -10})))]
          (is (= 400 (:status response))))
        (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                       (merge vdata {:offset -10})))]
          (is (= 400 (:status response))))))))

(deftest test-main-concepts-too-high-version
  (testing "GET /v1/taxonomy/main/concepts should fail"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:version 80}))]
      (is (= 400 (:status response))))))

(deftest test-main-concepts-latest
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {}))
          all-latest-concepts (json-body response)]
      (is (= 27 (count all-latest-concepts)))
      (is (= 200 (:status response)))
      (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                     {:version 7}))]
        (is (= 200 (:status response)))
        (is (= all-latest-concepts (json-body response))))
      (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                     {:version 6}))]
        (is (= 200 (:status response)))
        (is (not= all-latest-concepts (json-body response)))))))

(deftest test-main-concepts-filter-by-type
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:version 7 :type "language"}))]
      (is (= 200 (:status response)))
      (is (= [#:taxonomy{:type "language",
                         :definition "Quechua",
                         :id "gBd9_PJY_gbT",
                         :preferred-label "Quechua"}]
             (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:version 7 :type "cookbook"}))]
      (is (= 200 (:status response)))
      (is (= [] (json-body response))))))

(deftest test-main-concepts-filter-by-preferred-label
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:version 7
                                    :preferred-label "Quechua"}))]
      (is (= 200 (:status response)))
      (is (= [#:taxonomy{:type "language",
                         :definition "Quechua",
                         :id "gBd9_PJY_gbT",
                         :preferred-label "Quechua"}]
             (json-body response))))))

(deftest test-main-concepts-include-legacy
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:version 7
                                    :id "bh3H_Y3h_5eD"
                                    :include-legacy-information true}))]

      (is (= 200 (:status response)))
      (is (= [#:taxonomy{:type "occupation-field",
                         :definition "Exempel på arbetsuppgifter ...",
                         :id "bh3H_Y3h_5eD",
                         :preferred-label "Chefer av olika slag",
                         :deprecated-legacy-id "20"}]
             (json-body response))))))

(deftest test-main-concepts-deprecated
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:deprecated true
                                    :taxonomy/id "CaRE_1nn_cSU"}))]
      (is (= 200 (:status response)))
      (is (= [#:taxonomy{:type "region",
                         :definition "Skåne län",
                         :id "CaRE_1nn_cSU",
                         :replaced-by
                         [#:taxonomy{:id "TAKE_THE_BOB",
                                     :definition "Land of Bob",
                                     :type "country",
                                     :preferred-label "Land of Bob"}],
                         :preferred-label "Skåne län",
                         :deprecated true}]
             (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:taxonomy/id "CaRE_1nn_cSU"}))]
      (is (= 200 (:status response)))
      (is (= [] (json-body response))))))

(deftest test-main-concepts-related-ids-and-relation
  (testing "GET /v1/taxonomy/main/concepts"
    (let [response (*app* (request :get "/v1/taxonomy/main/concepts"
                                   {:deprecated true
                                    :relation "broader"
                                    :related-ids ["RdVK_4zC_kFV"]}))]
      (is (= 200 (:status response)))
      (is (= #{"AJ4a_a1z_UzB" "sWzF_6pd_Y6L"}
             (into #{}
                   (map :taxonomy/id)
                   (json-body response)))))))

(deftest test-main-graph
  (testing "GET /v1/taxonomy/main/graph"
    (let [response (*app* (request :get "/v1/taxonomy/main/graph"
                                   {:edge-relation-type "broad-match"
                                    :source-concept-type "esco-skill"
                                    :target-concept-type "keyword"}))
          status (:status response)]
      (is (= 200 status))
      #_(is (= {:taxonomy/graph
                {:taxonomy/edges
                 #{{:taxonomy/relation-type "broad-match"
                    :taxonomy/source "8b8r_KZN_zPh"
                    :taxonomy/target "qgYx_RG2_Q6R"}
                   {:taxonomy/relation-type "broad-match"
                    :taxonomy/source "Htnh_MPD_hBd"
                    :taxonomy/target "hwCv_Zc3_HSC"}
                   {:taxonomy/relation-type "broad-match"
                    :taxonomy/source "X4gR_17h_Smf"
                    :taxonomy/target "qgYx_RG2_Q6R"}
                   {:taxonomy/relation-type "broad-match"
                    :taxonomy/source "dTB3_9hj_wJr"
                    :taxonomy/target "Mgpb_E51_mpT"}
                   {:taxonomy/relation-type "broad-match"
                    :taxonomy/source "tTTb_9ig_qzQ"
                    :taxonomy/target "jodz_kWf_iKx"}}
                 :taxonomy/nodes
                 #{{:taxonomy/definition "Djurskydd"
                    :taxonomy/id "qgYx_RG2_Q6R"
                    :taxonomy/preferred-label "Djurskydd"
                    :taxonomy/type "keyword"}
                   {:taxonomy/definition "Dokumentation"
                    :taxonomy/id "jodz_kWf_iKx"
                    :taxonomy/preferred-label "Dokumentation"
                    :taxonomy/type "keyword"}
                   {:taxonomy/definition "Laboratorieteknik (allmän)"
                    :taxonomy/id "hwCv_Zc3_HSC"
                    :taxonomy/preferred-label "Laboratorieteknik (allmän)"
                    :taxonomy/type "keyword"}
                   {:taxonomy/definition "VVS-teknik, vatten och avlopp, ej ingenjörsutbildning"
                    :taxonomy/id "Mgpb_E51_mpT"
                    :taxonomy/preferred-label "VVS-teknik, vatten och avlopp, ej ingenjörsutbildning"
                    :taxonomy/type "keyword"}
                   {:taxonomy/definition "genomför behandlingar av fisk"
                    :taxonomy/id "X4gR_17h_Smf"
                    :taxonomy/preferred-label "genomför behandlingar av fisk"
                    :taxonomy/type "esco-skill"}
                   {:taxonomy/definition "kommunicera med externa laboratorier"
                    :taxonomy/id "Htnh_MPD_hBd"
                    :taxonomy/preferred-label "kommunicera med externa laboratorier"
                    :taxonomy/type "esco-skill"}
                   {:taxonomy/definition "registrera information om träbehandling"
                    :taxonomy/id "tTTb_9ig_qzQ"
                    :taxonomy/preferred-label "registrera information om träbehandling"
                    :taxonomy/type "esco-skill"}
                   {:taxonomy/definition "rensa avloppsrör"
                    :taxonomy/id "dTB3_9hj_wJr"
                    :taxonomy/preferred-label "rensa avloppsrör"
                    :taxonomy/type "esco-skill"}
                   {:taxonomy/definition "utarbeta behandlingsplaner för fisk"
                    :taxonomy/id "8b8r_KZN_zPh"
                    :taxonomy/preferred-label "utarbeta behandlingsplaner för fisk"
                    :taxonomy/type "esco-skill"}}}} (json-body response))))))

(deftest test-main-relation-changes
  (testing "GET /v1/taxonomy/main/relation/changes"
    (let [response (*app*
                    (request :get "/v1/taxonomy/main/relation/changes"
                             {:after-version 0}))
          status (:status response)
          body (json-body response)]
      (is (= 200 status))
      (is (= [{:taxonomy/event-type "CREATED"
               :taxonomy/relation
               {:taxonomy/relation-type "broad-match"
                :taxonomy/source
                {:taxonomy/id "sWzF_6pd_Y6L"
                 :taxonomy/preferred-label "Övriga chefer inom samhällsservice"
                 :taxonomy/type "ssyk-level-4"}
                :taxonomy/target
                {:taxonomy/id "j1Yr_UhC_X16"
                 :taxonomy/preferred-label "Arbetsledarerfarenhet"
                 :taxonomy/type "skill"}}
               :taxonomy/substitutability-percentage 0
               :taxonomy/version 1
               #_#_:taxonomy/transaction-id 536887543}]
             body)))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {}))]
      (is (= 400 (:status response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:after-version 1}))]
      (is (= 200 (:status response)))
      (is (= [] (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:to-version-inclusive 1}))]
      (is (= 400 (:status response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:after-version 0
                                    :to-version-inclusive 1}))]
      (is (= 200 (:status response)))
      (is (= 1 (count (json-body response)))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:after-version 0
                                    :to-version-inclusive 0}))]
      (is (= 200 (:status response)))
      (is (empty? (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:after-version 3
                                    :to-version-inclusive 0}))]
      (is (= 200 (:status response)))
      (is (empty? (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/changes"
                                   {:after-version 3
                                    :to-version-inclusive -10}))]
      (is (= 200 (:status response)))
      (is (empty? (json-body response))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/main/relation/changes"
                             {:after-version 0
                              :offset 0
                              :limit 1}))]
      (is (= 200 (:status response)))
      (is (= 1 (count (json-body response)))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/main/relation/changes"
                             {:after-version 0
                              :offset 1
                              :limit 1}))]
      (is (= 200 (:status response)))
      (is (= 0 (count (json-body response)))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/main/relation/changes"
                             {:after-version 0
                              :offset 0
                              :limit 0}))]
      (is (= 400 (:status response))))))

(deftest test-main-relation-types
  (testing "GET /v1/taxonomy/main/relation/types"
    (let [response (*app* (request :get "/v1/taxonomy/main/relation/types"))
          status (:status response)]
      (is (= 200 status))
      (is (= ["broad-match"
              "broader"
              "close-match"
              "exact-match"
              "possible-combination"
              "related"
              "substitutability"
              "unlikely-combination"] (json-body response))))))

(deftest test-main-replaced-by-changes
  (testing "GET /v1/taxonomy/main/replaced-by-changes"
    (let [expected-change {:taxonomy/concept
                           {:taxonomy/definition "Skåne län"
                            :taxonomy/deprecated true
                            :taxonomy/id "CaRE_1nn_cSU"
                            :taxonomy/preferred-label "Skåne län"
                            :taxonomy/type "region"
                            :taxonomy/replaced-by
                            [{:taxonomy/definition "Land of Bob"
                              :taxonomy/id "TAKE_THE_BOB"
                              :taxonomy/preferred-label "Land of Bob"
                              :taxonomy/type "country"}]}
                           :taxonomy/version 2}]
      (doseq [[desc expected-status expected-body params]
              [["Valid after version 0"
                200 [expected-change] {:after-version 0}]
               ["Valid after version 1"
                200 [expected-change] {:after-version 1}]
               ["Valid to version 1"
                200 [] {:after-version 1 :to-version-inclusive 1}]
               ["Valid version interval (i)"
                200 [expected-change] {:after-version 1
                                       :to-version-inclusive 2}]
               ["Valid version interval (ii)"
                200 [expected-change] {:after-version 1
                                       :to-version-inclusive 7}]
               ["Invalid to version"
                400 :ignore {:to-version-inclusive 7}]
               ["Partially valid interval"
                200 [expected-change] {:after-version 1
                                       :to-version-inclusive "7"}]

               ;; Versions that don't exist at the boundaries
               ["Valid after version -1"
                200 [] {:after-version -1}]
               ["Valid version interval (iii)"
                200 [] {:after-version 1 :to-version-inclusive 8}]
               ["Valid version interval (iv)"
                200 [] {:after-version 1 :to-version-inclusive 80}]]]
        (testing desc
          (let [response (*app*
                          (request :get "/v1/taxonomy/main/replaced-by-changes"
                                   params))
                status (:status response)]
            (is (= expected-status status))
            (when (= :ignore expected-body)
              (println "\n\n\n---------------------------------")
              (println (json-body response)))
            (is (or (= expected-body :ignore)
                    (= expected-body (json-body response))))))))))

(deftest test-main-versions
  (testing "GET /v1/taxonomy/main/versions"
    (let [response (*app* (request :get "/v1/taxonomy/main/versions"))
          status (:status response)
          first-version (first (json-body response))]
      (is (= 200 status))
      (is (= {:taxonomy/timestamp "2001-01-01T00:00:00.000Z"
              :taxonomy/version 1} first-version)))))

;;Specific Types 
(deftest test-specific-concepts-country
  (testing "GET /v1/taxonomy/specific/concepts/country"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/country"))
          status (:status response)
          body (json-body response)
          cayman (filter #(= "MS1s_AYg_kZe" (:taxonomy/id %)) body)]
      (is (= 200 status))
      (is (= 2 (count body)))
      (is (= {:taxonomy/definition "Caymanöarna"
              :taxonomy/id "MS1s_AYg_kZe"
              :taxonomy/iso-3166-1-alpha-2-2013 "KY"
              :taxonomy/iso-3166-1-alpha-3-2013 "CYM"
              :taxonomy/preferred-label "Caymanöarna"
              :taxonomy/type "country"} (first cayman))))))

(deftest test-specific-concepts-driving-licence
  (testing "GET /v1/taxonomy/specific/concepts/driving-licence"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/driving-licence"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Körkortsbehörighet för moped klass I, den så kallade EU-mopeden. Den är konstruerad för en hastighet av högst 45 km/tim och för att köra den krävs att man har körkort, traktorkort (utfärdat före den 1 oktober 2009), eller AM-körkort \r\n(eller ett förarbevis för moped klass I som man har tagit före den 1 februari 2009).\r\n\r\nKörkortsålder: 15 år"
              :taxonomy/driving-licence-code-2013 "AM"
              :taxonomy/id "4HpY_e2U_TUH"
              :taxonomy/preferred-label "AM"
              :taxonomy/short-description "Moped klass 1 och 2"
              :taxonomy/sort-order 2
              :taxonomy/type "driving-licence"} (first (json-body response)))))))

(deftest test-specific-concepts-employment-duration
  (testing "GET /v1/taxonomy/specific/concepts/employment-duration"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/employment-duration"))
          status (:status response)]
      (is (= 200 status))
      (is (= [{:taxonomy/definition "Tills vidare"
               :taxonomy/eures-code-2014 "PF"
               :taxonomy/id "a7uU_j21_mkL"
               :taxonomy/preferred-label "Tills vidare"
               :taxonomy/sort-order 1
               :taxonomy/type "employment-duration"}
              {:taxonomy/definition "12 månader - upp till 2 år"
               :taxonomy/eures-code-2014 "PF"
               :taxonomy/id "9RGe_UxD_FZw"
               :taxonomy/preferred-label "12 månader - upp till 2 år"
               :taxonomy/sort-order 2
               :taxonomy/type "employment-duration"}
              {:taxonomy/definition "6 månader – upp till 12 månader"
               :taxonomy/eures-code-2014 "PF"
               :taxonomy/id "gJRb_akA_95y"
               :taxonomy/preferred-label "6 månader – upp till 12 månader"
               :taxonomy/sort-order 3
               :taxonomy/type "employment-duration"}
              {:taxonomy/definition "3 månader – upp till 6 månader"
               :taxonomy/eures-code-2014 "TF"
               :taxonomy/id "Xj7x_7yZ_jEn"
               :taxonomy/preferred-label "3 månader – upp till 6 månader"
               :taxonomy/sort-order 4
               :taxonomy/type "employment-duration"}
              {:taxonomy/definition "11 dagar - upp till 3 månader"
               :taxonomy/eures-code-2014 "TF"
               :taxonomy/id "Sy9J_aRd_ALx"
               :taxonomy/preferred-label "11 dagar - upp till 3 månader"
               :taxonomy/sort-order 5
               :taxonomy/type "employment-duration"}
              {:taxonomy/definition "Upp till 10 dagar"
               :taxonomy/eures-code-2014 "TF"
               :taxonomy/id "cAQ8_TpB_Tdv"
               :taxonomy/preferred-label "Upp till 10 dagar"
               :taxonomy/sort-order 6
               :taxonomy/type "employment-duration"}] (json-body response))))))

(deftest test-specific-concepts-isco
  (testing "GET /v1/taxonomy/specific/concepts/isco"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/isco"))
          status (:status response)
          barnmorskor (udf/get-from (json-body response) :taxonomy/id "dSc4_snH_7WR")]
      (is (= 200 status))
      (is (= {:taxonomy/definition "En lång ISCO-beskrivning"
              :taxonomy/id "dSc4_snH_7WR"
              :taxonomy/isco-code-08 "3222"
              :taxonomy/preferred-label "Barnmorskor"
              :taxonomy/type "isco-level-4"} barnmorskor)))))

(deftest test-specific-concepts-language
  (testing "GET /v1/taxonomy/specific/concepts/language"
    (let [response (*app*
                    (request :get "/v1/taxonomy/specific/concepts/language"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Quechua"
              :taxonomy/id "gBd9_PJY_gbT"
              :taxonomy/iso-639-1-2002 "QU"
              :taxonomy/iso-639-2-1998 "QUE"
              :taxonomy/iso-639-3-alpha-2-2007 "QU"
              :taxonomy/iso-639-3-alpha-3-2007 "QUE"
              :taxonomy/preferred-label "Quechua"
              :taxonomy/type "language"} (first (json-body response)))))))

(deftest test-specific-concepts-municipality
  (testing "GET /v1/taxonomy/specific/concepts/municipality"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/municipality"))
          status (:status response)
          nacka (udf/get-from (json-body response) :taxonomy/id "aYA7_PpG_BqP")]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Nacka"
              :taxonomy/id "aYA7_PpG_BqP"
              :taxonomy/lau-2-code-2015 "0182"
              :taxonomy/preferred-label "Nacka"
              :taxonomy/type "municipality"} nacka)))))

(deftest test-specific-concepts-region
  (testing "GET /v1/taxonomy/specific/concepts/region"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/region"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Campobasso"
              :taxonomy/id "iRmD_coH_YZ6"
              :taxonomy/preferred-label "Campobasso"
              :taxonomy/type "region"} (first (json-body response)))))))

(deftest test-specific-concepts-sni-level
  (testing "GET /v1/taxonomy/specific/concepts/sni-level"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/sni-level"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Kontorstjänster och andra företagstjänster"
              :taxonomy/id "7uGH_h7w_W21"
              :taxonomy/preferred-label "Kontorstjänster och andra företagstjänster"
              :taxonomy/sni-level-code-2007 "82"
              :taxonomy/type "sni-level-2"} (first (json-body response)))))))

(deftest test-specific-concepts-ssyk
  (testing "GET /v1/taxonomy/specific/concepts/ssyk"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/ssyk"))
          status (:status response)
          results (json-body response)
          fr (udf/get-from results :taxonomy/id "rxmK_wwM_1NA")]
      (is (= 200 status))
      (is (= 3 (count results)))
      (is (= 5 (count (keys fr))))
      (is (= {:taxonomy/definition "Styr och övervakar maskiner som bearbetar, blandar och formar kemikalier och andra ämnen för att framställa farmaceutiska produkter och hygienartiklar."
              :taxonomy/id "rxmK_wwM_1NA"
              :taxonomy/preferred-label "Maskinoperatörer, farmaceutiska produkter"
              :taxonomy/ssyk-code-2012 "8131"
              :taxonomy/type "ssyk-level-4"} fr)))))

(deftest test-specific-concepts-sun-education-field
  (testing "GET /v1/taxonomy/specific/concepts/sun-education-field"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/sun-education-field"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Ämneslärarutbildning, matematik, data, naturvetenskap och teknik i kombination med praktisk-estetiska ämnen"
              :taxonomy/id "tJzG_DQt_aRT"
              :taxonomy/preferred-label "Ämneslärarutbildning, matematik, data, naturvetenskap och teknik i kombination med praktisk-estetiska ämnen"
              :taxonomy/sun-education-field-code-2020 "145g"
              :taxonomy/type "sun-education-field-4"} (first (json-body response)))))))

(deftest test-specific-concepts-sun-education-level
  (testing "GET /v1/taxonomy/specific/concepts/sun-education-level"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/sun-education-level"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Här ingår utbildningar för främst sexåringar."
              :taxonomy/id "pCha_vWk_pbR"
              :taxonomy/preferred-label "Förskoleklass"
              :taxonomy/sun-education-level-code-2020 "002"
              :taxonomy/type "sun-education-level-3"} (first (json-body response)))))))

(deftest test-specific-concepts-unemployment-fund
  (testing "GET /v1/taxonomy/specific/concepts/unemployment-fund"
    (let [response (*app* (request :get "/v1/taxonomy/specific/concepts/unemployment-fund"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Sveriges arbetares a-kassa"
              :taxonomy/id "mQNn_ZiR_VMJ"
              :taxonomy/preferred-label "Sveriges arbetares a-kassa"
              :taxonomy/type "unemployment-fund"
              :taxonomy/unemployment-fund-code-2017 "41"} (first (json-body response)))))))

;;Suggesters
(deftest test-suggesters-autocomplete
  (testing "GET /v1/taxonomy/suggesters/autocomplete"
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "q"}))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/id "gBd9_PJY_gbT"
              :taxonomy/preferred-label "Quechua"
              :taxonomy/type "language"} (first (json-body response)))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"}))
          status (:status response)]
      (is (= 200 status))
      (is (= 3 (count (json-body response)))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :relation "broader"
                                    :related-ids ["sWzF_6pd_Y6L"]}))
          status (:status response)]
      (is (= 200 status))
      (is (= [#:taxonomy{:id "bh3H_Y3h_5eD",
                         :type "occupation-field",
                         :preferred-label "Chefer av olika slag"}]
             (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:type "occupation-field"
                                    :query-string "Chefer"}))
          status (:status response)]
      (is (= 200 status))
      (is (= [#:taxonomy{:id "bh3H_Y3h_5eD",
                         :type "occupation-field",
                         :preferred-label "Chefer av olika slag"}]
             (json-body response))))
    (let [response (*app*
                    (request
                     :get "/v1/taxonomy/suggesters/autocomplete"

                     ;; It is ok to provide a number:
                     ;; It will be mapped to the string "99"
                     {:query-string 99}))]
      (is (= 200 (:status response))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :offset 1
                                    :limit 1}))]
      (is (= 200 (:status response)))
      (not-working-as-expected
       "The body is empty but it should be one element I think.
      It looks like limit isn't the length of the list but where the upper
      bound is."
       (is (= 1 (count (json-body response))))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :offset 1
                                    :limit 2}))]
      (is (= 200 (:status response)))
      (not-working-as-expected
       "The body is empty but it should be one element I think.
      It looks like limit isn't the length of the list but where the upper
      bound is."
       (is (= 2 (count (json-body response))))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :offset 0
                                    :limit 2}))]
      (is (= 200 (:status response)))
      (is (= 2 (count (json-body response)))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :offset -10
                                    :limit 2}))]
      (is (= 400 (:status response))))
    (let [response (*app* (request :get "/v1/taxonomy/suggesters/autocomplete"
                                   {:query-string "Chefer"
                                    :limit 0}))]
      (is (= 400 (:status response))))))

;;Legacy
(deftest test-legacy-convert-matching-component-id-to-new-id
  (testing "GET /v1/taxonomy/legacy/convert-matching-component-id-to-new-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id "3" :type "skill"}))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Forskning, erfarenhet"
              :taxonomy/deprecated-legacy-id "3"
              :taxonomy/id "T5KD_ZPr_ysR"
              :taxonomy/preferred-label "Forskning, erfarenhet"
              :taxonomy/type "skill"} (json-body response))))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id "3000" :type "skill"}))
          status (:status response)]
      (is (= 404 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id "3"
                                                                                                       :type "occupation-name"}))
          status (:status response)]
      (is (= 404 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id 3
                                                                                                       :type "occupation-name"}))
          status (:status response)]
      (is (= 404 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id "3"
                                                                                                       :type 9}))
          status (:status response)]
      (is (= 400 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:matching-component-id "3"}))
          status (:status response)]
      (is (= 400 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {:type "occupation-name"}))
          status (:status response)]
      (is (= 400 status)))
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-matching-component-id-to-new-id" {}))
          status (:status response)]
      (is (= 400 status)))))

(deftest test-legacy-convert-municipality-code-to-new-id
  (testing "GET /v1/taxonomy/legacy/convert-municipality-code-to-new-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-municipality-code-to-new-id" {:code "1489"}))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Alingsås"
              :taxonomy/deprecated-legacy-id "280"
              :taxonomy/id "UQ75_1eU_jaC"
              :taxonomy/lau-2-code-2015 "1489"
              :taxonomy/preferred-label "Alingsås"
              :taxonomy/type "municipality"} (json-body response))))))

(deftest test-legacy-convert-new-id-to-old-id
  (testing "GET /v1/taxonomy/legacy/convert-new-id-to-old-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-new-id-to-old-id" {:id "T5KD_ZPr_ysR"}))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Forskning, erfarenhet"
              :taxonomy/deprecated-legacy-id "3"
              :taxonomy/id "T5KD_ZPr_ysR"
              :taxonomy/preferred-label "Forskning, erfarenhet"
              :taxonomy/type "skill"} (json-body response))))))

(deftest test-legacy-convert-old-id-to-new-id
  (testing "GET /v1/taxonomy/legacy/convert-old-id-to-new-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-old-id-to-new-id" {:legacy-id "1" :type "skill"}))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Arbetsledarerfarenhet"
              :taxonomy/deprecated-legacy-id "1"
              :taxonomy/id "j1Yr_UhC_X16"
              :taxonomy/preferred-label "Arbetsledarerfarenhet"
              :taxonomy/type "skill"} (json-body response))))))

(deftest test-legacy-convert-ssyk-2012-code-to-new-id
  (testing "GET /v1/taxonomy/legacy/convert-ssyk-2012-code-to-new-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-ssyk-2012-code-to-new-id" {:code "5330"}))
          status (:status response)]
      (is (= {:taxonomy/definition "Ger individuell vård och omsorg till äldre i eget eller särskilt boende.\r\n"
              :taxonomy/deprecated-legacy-id "1257"
              :taxonomy/id "tAJS_JNb_hDH"
              :taxonomy/preferred-label "Vårdbiträden"
              :taxonomy/ssyk-code-2012 "5330"
              :taxonomy/type "ssyk-level-4"} (json-body response)))
      (is (= 200 status)))

    (doseq [[expected-status params] [[404 {:code "9999"}]
                                      [404 {:code 119}]
                                      [400 {}]]
            :let [response (*app*
                            (request
                             :get "/v1/taxonomy/legacy/convert-ssyk-2012-code-to-new-id"
                             params))]]
      (is (= expected-status (:status response))))))

(deftest test-legacy-convert-swedish-region-code-to-new-id
  (testing "GET /v1/taxonomy/legacy/convert-swedish-region-code-to-new-id"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/convert-swedish-region-code-to-new-id" {:code "12"}))
          status (:status response)
          skåne (json-body response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Skåne län"
              :taxonomy/deprecated-legacy-id "195"
              :taxonomy/id "CaRE_1nn_cSU"
              :taxonomy/national-nuts-level-3-code-2019 "12"
              :taxonomy/preferred-label "Skåne län"
              :taxonomy/type "region"}
             skåne)))))

(deftest test-legacy-get-occupation-name-with-relations
  (testing "GET /v1/taxonomy/legacy/get-occupation-name-with-relations"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/get-occupation-name-with-relations"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Redaktionschef"
              :taxonomy/deprecated-legacy-id "5065"
              :taxonomy/id "RdVK_4zC_kFV"
              :taxonomy/occupation-field-deprecated-legacy-id "20"
              :taxonomy/occupation-field-id "bh3H_Y3h_5eD"
              :taxonomy/preferred-label "Redaktionschef"
              :taxonomy/ssyk-code-2012 "1590"
              :taxonomy/ssyk-deprecated-legacy-id "1492"
              :taxonomy/ssyk-id "sWzF_6pd_Y6L"
              :taxonomy/type "occupation-name"} (first (json-body response)))))))

(deftest test-legacy-get-old-legacy-education-levels
  (testing "GET /v1/taxonomy/legacy/get-old-legacy-education-levels"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/get-old-legacy-education-levels"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:definition "Eftergymnasial utbildning kortare än två år"
              :deprecated_legacy_id "5"
              :deprecated_legacy_id_2 "5"
              :id "KBjB_gpS_ZJL"
              :preferred_label "Eftergymnasial utbildning kortare än två år"
              :sun_education_level_code_2000 "4"} (first (json-body response)))))))

(deftest test-legacy-postalcodes-municipality
  (testing "GET /v1/taxonomy/legacy/postalcodes-municipality"
    (let [response (*app* (request :get "/v1/taxonomy/legacy/postalcodes-municipality"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:lau_2_code_2015 "0188"
              :locality_preferred_label "Stockholm"
              :municipality_deprecated_legacy_id "24"
              :municipality_id "btgf_fS7_sKG"
              :municipality_preferred_label "Norrtälje"
              :national_nuts_level_3_code_2019 "01"
              :postal_code "10004"
              :region_deprecated_legacy_id "197"
              :region_id "CifL_Rzy_Mku"
              :region_term "Stockholms län"} (first (json-body response)))))))

;;Eures 
(deftest test-eures-convert-occupation-name-concept-id-to-eures-esco-id
  (testing "GET /v1/taxonomy/eures/convert-occupation-name-concept-id-to-eures-esco-id"
    (let [response (*app* (request :get "/v1/taxonomy/eures/convert-occupation-name-concept-id-to-eures-esco-id" {:id "RdVK_4zC_kFV"}))
          status (:status response)]
      (is (= 200 status))
      (is (= ["http://data.europa.eu/esco/isco/C1349"] (json-body response))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/eures/convert-occupation-name-concept-id-to-eures-esco-id"
                             {:id "RdVK_4zC_asdf"}))]

      (not-working-as-expected
       "This fails: We would expect status 404 (not found) but we get returned status 500
      probably because of a failed assertion."
       (is (= 400 (:status response)))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/eures/convert-occupation-name-concept-id-to-eures-esco-id"
                             {}))]
      (is (= 400 (:status response))))
    (let [response (*app*
                    (request :get "/v1/taxonomy/eures/convert-occupation-name-concept-id-to-eures-esco-id"
                             {:id 1132}))]

      (not-working-as-expected
       "This fails: We would expect status 400 but we get status 500 returned."
       (is (= 400 (:status response)))))))

;;Status
(deftest test-status-health
  (testing "GET /v1/taxonomy/status/health"
    (let [response (*app* (request :get "/v1/taxonomy/status/health"))
          status (:status response)]
      (is (= 200 status))
      (is (= {:available true} (json-body response))))))

(deftest test-status-ready
  (testing "GET /v1/taxonomy/status/ready without debug"
    (let [response (*app* (request :get "/v1/taxonomy/status/ready"))
          status (:status response)
          headers (:headers response)]
      (is (= 200 status))
      (is (= {:available true} (json-body response)))
      (is (not (contains? headers "X-build-info-timestamp")))
      (is (not (contains? headers "X-build-info-commit")))
      (is (not (contains? headers "X-latest-taxonomy-version")))))
  (testing "GET /v1/taxonomy/status/ready with debug"
    (let [response (-> (request :get "/v1/taxonomy/status/ready")
                       (header "x-include-response-debug-headers" "1")
                       *app*)
          status (:status response)
          headers (:headers response)]
      (is (= 200 status))
      (is (= {:available true} (json-body response)))
      (is (= build-info/build-timestamp (headers "X-build-info-timestamp")))
      (is (= build-info/commit (headers "X-build-info-commit")))
      (is (= "7" (headers "X-latest-taxonomy-version"))))))

(deftest test-graphviz
  (doseq [params [{} {:version 7}]]
    (let [response (*app*
                    (request :get "/v1/taxonomy/graphviz/types.svg"
                             params))]
      ;; This status will depend on whether Graphviz is installed on
      ;; the system. If it is installed, the `dot` command will be present.
      ;; We could possibly improve this test by checking whether `dot`
      ;; is present or not on the system using the `graphviz/sh!` function.
      (is (not= 500 (:status response)))
      (when (= 200 (:status response))
        (is (str/ends-with? (str/trim (:body response))
                            "</svg>"))))))

(deftest test-status-options
  (testing "OPTIONS /v1/taxonomy/status/ready"
    (let [response (*app* (request :options "/v1/taxonomy/status/ready"))
          status (:status response)]
      (is (= 200 status))
      (is (nil? (json-body response))))))

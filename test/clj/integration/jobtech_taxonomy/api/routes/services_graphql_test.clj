(ns integration.jobtech-taxonomy.api.routes.services-graphql-test
  (:require [clojure.data.json :as json]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :refer [header request]]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :as integration-helpers]))

(set! *warn-on-reflection* true)

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(def services-endpoint
  (handler/app udf/cfg))

(def query-changelog-fmt "

query Mjao {
     changelog(%s) {
       ... on Updated {
         __typename
       }
       ... on Deprecated {
         __typename
       }
       ... on Created {
         __typename
       }
       ... on Commented {
         __typename
       }
       concept {
         id
         type
       }
     }
   }

")

(deftest graphiql-test
  (testing "GET /v1/taxonomy/graphiql"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphiql"))]
      (is (= 200 (:status response)))
      (is (= (slurp "resources/graphiql.html")
             (slurp (:body response))))))

  (testing "GET /v1/taxonomy/graphiql.json"
    (let [response (services-endpoint (request :get "/v1/taxonomy/graphiql.json"))]
      (is (= 200 (:status response)))
      (is (= {"url" "/v1/taxonomy/graphql"}
             (json/read-str (slurp (:body response))))))))

;;GraphQL
(deftest graphql-endpoint-test
  (testing "GET /v1/taxonomy/graphql"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\"){preferred_label}}"}))]
      (is (= 200
             (:status response)))
      (is (= {:data {:concepts [{:preferred_label "Förskoleklass"}]}}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - With variables json"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query MopedQuery($myVar: String!) {concepts(id: $myVar){short_description}}"
                              :variables "{\"myVar\": \"4HpY_e2U_TUH\"}"}))]
      (is (= 200
             (:status response)))
      (is (= {:data {:concepts [{:short_description "Moped klass 1 och 2"}]}}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - versions in range"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query MyQuery {
                                        versions(from: \"1\", to: \"3\") {
                                          id
                                        }
                                      }"}))]
      (is (= 200 (:status response)))
      (is (= {:data {:versions [{:id 1} {:id 2} {:id 3}]}}
             (-> response :body slurp json/read-json)))))
  (testing "GET /v1/taxonomy/graphql - versions from latest"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query MyQuery {
  versions(from: \"latest\") {
    id
  }
}"}))]
      (is (= 200 (:status response)))
      (is (= {:data {:versions [{:id 7}]}}
             (-> response :body slurp json/read-json)))))

  (testing "GET /v1/taxonomy/graphql - changelog"
    (doseq [[desc
             expected-status
             expected-response
             changelog-args]
            [["Non authorized access to next version"
              401
              #:taxonomy{:error "Not authorized"}
              "to: \"next\""]
             ["Non authorized access from next version"
              401
              #:taxonomy{:error "Not authorized"}
              "from: \"next\""]
             ["Specific type and id"
              200
              {:data {:changelog
                      [{:__typename "Updated",
                        :concept {:type "skill",
                                  :id "j1Yr_UhC_X16"}}]}}
              "type: \"skill\", id: \"j1Yr_UhC_X16\""]
             ["Specific type"
              200

              ;; The reason why j1Yr_UhC_X16 is not listed as "Created" in the tests below
              ;; is that it already exists in the very first version of mini-db. And
              ;; the changelog computes the *deltas between versions*.
              {:data
               {:changelog
                [{:__typename "Updated",
                  :concept {:type "skill", :id "j1Yr_UhC_X16"}}
                 {:__typename "Created",
                  :concept {:type "skill", :id "gjd2_JSR_AeG"}}]}}

              "type: \"skill\""]]]
      (testing desc
        (let [response (services-endpoint
                        (request :get "/v1/taxonomy/graphql"
                                 {:query (format query-changelog-fmt changelog-args)}))]
          (is (= expected-status (:status response)))
          (is (= expected-response (integration-helpers/json-body response)))))))
  (testing "GET /v1/taxonomy/graphql - With variables not json"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query MopedQuery($myVar: String!) {concepts(id: $myVar){short_description}}"
                              :variables "0"}))]
      (is (= 400
             (:status response)))
      (is (= {:taxonomy/error "-- Spec failed --------------------\n\n  {:variables \"0\", :query ...}\n              ^^^\n\nshould satisfy\n\n  json-map?\n\n-------------------------\nDetected 1 error\n"}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - With variables not json and query 0"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "0"
                              :variables "False"}))]
      (is (= 400
             (:status response)))
      (is (= {:taxonomy/error "-- Spec failed --------------------\n\n  {:variables \"False\", :query ...}\n              ^^^^^^^\n\nshould satisfy\n\n  json-map?\n\n-------------------------\nDetected 1 error\n"}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - Not authorized"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\",version: \"next\"){preferred_label}}"}))]
      (is (= 401
             (:status response)))
      (is (= {:taxonomy/error "Not authorized"}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - With API key"
    (let [response (services-endpoint
                    (-> (request :get "/v1/taxonomy/graphql"
                                 {:query "query{concepts(id:\"pCha_vWk_pbR\",version: \"next\"){preferred_label}}"})
                        (header :api-key 222)))]
      (is (= 200
             (:status response)))
      (is (= {:data {:concepts [{:preferred_label "Förskoleklass"}]}}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - Invalid version"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\",version: -3){preferred_label}}"}))]
      (is (= 400
             (:status response)))
      (is (= {:taxonomy/error {:taxonomy/arguments {:taxonomy/id ["pCha_vWk_pbR"],
                                                    :taxonomy/include_deprecated false,
                                                    :taxonomy/version -3},
                               :taxonomy/field-name "taxonomy/concepts",
                               :taxonomy/location {:taxonomy/column 7, :taxonomy/line 1},
                               :taxonomy/path ["taxonomy/concepts"]}}
             (json/read-json (slurp (:body response)))))))

  (testing "GET /v1/taxonomy/graphql - Empty query"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"))]
      (is (= 400
             (:status response)))
      (is (.contains (slurp (:body response)) "should contain key: :query"))))

  (testing "GET /v1/taxonomy/graphql - Bad query"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/graphql"
                             {:query "invalid"}))]
      (is (= 400 (:status response)))
      (is (.contains (slurp (:body response)) "Failed to parse GraphQL query."))))

  (testing "POST /v1/taxonomy/graphql"
    (let [response (services-endpoint
                    (request :post "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\"){preferred_label}}"}))]
      (is (= 200
             (:status response)))
      (is (= {:data {:concepts [{:preferred_label "Förskoleklass"}]}}
             (json/read-json (slurp (:body response)))))))

  (testing "POST /v1/taxonomy/graphql - Not authorized"
    (let [response (services-endpoint
                    (request :post "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\",version: \"next\"){preferred_label}}"}))]
      (is (= 401
             (:status response)))
      (is (= {:taxonomy/error "Not authorized"}
             (json/read-json (slurp (:body response)))))))

  (testing "POST /v1/taxonomy/graphql - With API key"
    (let [response (services-endpoint
                    (-> (request :post "/v1/taxonomy/graphql"
                                 {:query "query{concepts(id:\"pCha_vWk_pbR\",version: \"next\"){preferred_label}}"})))]
      (is (= 401
             (:status response)))
      (is (= {:taxonomy/error "Not authorized"}
             (json/read-json (slurp (:body response)))))))

  (testing "POST /v1/taxonomy/graphql - Invalid version"
    (let [response (services-endpoint
                    (request :post "/v1/taxonomy/graphql"
                             {:query "query{concepts(id:\"pCha_vWk_pbR\",version: -3){preferred_label}}"}))]
      (is (= 400
             (:status response)))
      (is (= {:taxonomy/error {:taxonomy/arguments {:taxonomy/id ["pCha_vWk_pbR"],
                                                    :taxonomy/include_deprecated false,
                                                    :taxonomy/version -3},
                               :taxonomy/field-name "taxonomy/concepts",
                               :taxonomy/location {:taxonomy/column 7, :taxonomy/line 1},
                               :taxonomy/path ["taxonomy/concepts"]}}
             (json/read-json (slurp (:body response)))))))

  (testing "POST /v1/taxonomy/graphql - Empty query"
    (let [response (services-endpoint
                    (request :post "/v1/taxonomy/graphql"))]
      (is (= 400
             (:status response)))
      (is (.contains (slurp (:body response)) "should contain key: :query"))))

  (testing "POST /v1/taxonomy/graphql - Bad query"
    (let [response (services-endpoint
                    (request :post "/v1/taxonomy/graphql"
                             {:query "invalid"}))]
      (is (= 400 (:status response)))
      (is (.contains (slurp (:body response)) "Failed to parse GraphQL query.")))))


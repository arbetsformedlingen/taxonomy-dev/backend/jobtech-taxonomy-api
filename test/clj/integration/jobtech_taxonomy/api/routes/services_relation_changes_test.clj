(ns ^:slow integration.jobtech-taxonomy.api.routes.services-relation-changes-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.routes.services :as service]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as coercion]
            [reitit.ring.middleware.parameters :as parameters]
            [ring.mock.request :refer [request]]
            [utils.db-fixtures :as udf]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))
(def services-endpoint (ring/ring-handler
                        (ring/router
                         (service/service-routes udf/cfg)
                         {:data {:middleware [parameters/parameters-middleware
                                              coercion/coerce-request-middleware]}})))
;; TODO: Update `udf/database-minimal` with data to make this test interesting. 
(deftest test-main-relation-changes
  (testing "GET /v1/taxonomy/main/relation/changes"
    (let [response (services-endpoint (request :get "/v1/taxonomy/main/relation/changes"
                                               {:after-version 0
                                                :to-version-inclusive 7}))
          status (:status response)
          body (:body response)]
      (is (= 200 status))
      (is (= 1 (count body)))
      (is (= "CREATED"
             (first (map :taxonomy/event-type body))))
      #_(is (= #{"COMMENTED" "CREATED" "DEPRECATED"}
               (set (map :taxonomy/event-type body)))))))

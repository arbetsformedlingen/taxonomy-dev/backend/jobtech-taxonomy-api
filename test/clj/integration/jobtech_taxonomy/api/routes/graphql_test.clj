(ns integration.jobtech-taxonomy.api.routes.graphql-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.routes.graphql :as graphql]
            [utils.db-fixtures :as udf]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(deftest test-graphql-response-handler
  (let [cfg udf/cfg
        query "query occupation_names {concepts (type:  \"skill\", id: \"j1Yr_UhC_X16\" , limit: 1) {preferred_label}}"]
    (testing "test graphql-response-handler"
      (is (= {:body {:data {:concepts [{:preferred_label "Arbetsledarerfarenhet"}]}},
              :status 200} (graphql/graphql-response-handler cfg {:query query}))))

    (testing "test graphql-response-handler with json string variables and empty operationName"
      (is (= {:body {:data {:concepts [{:preferred_label "Arbetsledarerfarenhet"}]}},
              :status 200} (graphql/graphql-response-handler cfg {:query query :variables "{}" :operationName ""}))))

    (testing "test graphql-response-handler with zero string query"
      (is (= {:body {:taxonomy/error '({:extensions {:errors ({:locations [{:column nil, :line 1}]
                                                               :message "mismatched input '0' expecting {'query', 'mutation', 'subscription', '{', 'fragment'}"})},
                                        :message "Failed to parse GraphQL query."})},
              :status 400} (graphql/graphql-response-handler cfg {:query "0"}))))

    (testing "test graphql-response-handler with json string variables and zero string operationName"
      (is (= {:body {:taxonomy/error '({:extensions {:op-name "0"},
                                        :message "Single operation did not provide a matching name."})},
              :status 400} (graphql/graphql-response-handler cfg {:query query :variables "{}" :operationName "0"}))))

    (testing "test graphql-response-handler with zero string query and variables and operationName"
      (is (= {:body {:taxonomy/error '({:extensions {:errors ({:locations [{:column nil, :line 1}]
                                                               :message "mismatched input '0' expecting {'query', 'mutation', 'subscription', '{', 'fragment'}"})},
                                        :message "Failed to parse GraphQL query."})},
              :status 400} (graphql/graphql-response-handler cfg {:query "0" :variables "0" :operationName "0"}))))

    (testing "test graphql-response-handler with zero string variables and operationName"
      (is (= {:body {:taxonomy/error '({:extensions {:op-name "0"},
                                        :message "Single operation did not provide a matching name."})},
              :status 400} (graphql/graphql-response-handler cfg {:query query :variables "0" :operationName "0"}))))

    (testing "test graphql-response-handler with zero string variables and empty operationName"
      (is (= {:body {:data {:concepts [{:preferred_label "Arbetsledarerfarenhet"}]}},
              :status 200} (graphql/graphql-response-handler cfg {:query query :variables "0" :operationName ""}))))

    (testing "test graphql-response-handler with zero string query and variables"
      (is (= {:body {:taxonomy/error [{:extensions {:errors [{:locations [{:column nil, :line 1}],
                                                              :message "mismatched input '0' expecting {'query', 'mutation', 'subscription', '{', 'fragment'}"}]},
                                       :message "Failed to parse GraphQL query."}]},
              :status 400} (graphql/graphql-response-handler cfg {:query "0" :variables "0"}))))))

(deftest test-graphql-offset-parameter
  (let [cfg udf/cfg
        query "query filtered_concepts {concepts (type:  \"country\", offset: 1) {preferred_label}}"
        response (graphql/graphql-response-handler cfg {:query query})]
    (is (= 200 (:status response)))
    (is (= 1 (-> response
                 :body
                 :data
                 :concepts
                 count))))
  (let [cfg udf/cfg
        query "query filtered_concepts {concepts (type:  \"country\") {preferred_label}}"
        response (graphql/graphql-response-handler cfg {:query query})]
    (is (= 200 (:status response)))
    (is (= 2 (-> response
                 :body
                 :data
                 :concepts
                 count))))
  (let [cfg udf/cfg
        query "query filtered_concepts {concepts (preferred_label_contains:  \"Cayman\") {preferred_label}}"
        response (graphql/graphql-response-handler cfg {:query query})]
    (is (= {:status 200,
            :body
            {:data
             {:concepts
              [{:preferred_label "Caymanöarna"}]}}}
           response)))
  (let [cfg udf/cfg
        query "query filtered_concepts {concepts (preferred_label_contains:  \"ostkakebomb\") {preferred_label}}"
        response (graphql/graphql-response-handler cfg {:query query})]
    (is (= {:status 200,
            :body
            {:data
             {:concepts
              []}}}
           response))))


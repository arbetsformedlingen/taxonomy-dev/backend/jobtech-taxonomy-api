(ns integration.jobtech-taxonomy.api.routes.services-graphql-empty-db-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.common.db-schema :refer [schema]]
            [ring.mock.request :refer [request]]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :as integration-helpers]))

(set! *warn-on-reflection* true)

(defn initialize-empty-db [cfg]
  (dc/create-database cfg)
  (dc/transact (dc/connect cfg)
               {:tx-data schema}))

(test/use-fixtures :each (udf/database-minimal udf/cfg initialize-empty-db))

(def services-endpoint
  (handler/app udf/cfg))

(def query-changelog-skill-id-abc "

query Mjao {
     changelog(type: \"skill\", id: \"abc\") {
       ... on Updated {
         __typename
       }
       ... on Deprecated {
         __typename
       }
       ... on Created {
         __typename
       }
       ... on Commented {
         __typename
       }
       concept {
         id
         type
       }
     }
   }

")

(def query-changelog-skill "

query Mjao {
     changelog(type: \"skill\") {
       ... on Updated {
         __typename
       }
       ... on Deprecated {
         __typename
       }
       ... on Created {
         __typename
       }
       ... on Commented {
         __typename
       }
       concept {
         id
         type
       }
     }
   }

")

(def query-changelog "

query Mjao {
     changelog {
       ... on Updated {
         __typename
       }
       ... on Deprecated {
         __typename
       }
       ... on Created {
         __typename
       }
       ... on Commented {
         __typename
       }
       concept {
         id
         type
       }
     }
   }

")

(deftest empty-changelog-test
  (testing "GET /v1/taxonomy/graphql - Empty changelog"
    (doseq [query [query-changelog-skill-id-abc
                   query-changelog-skill
                   query-changelog]]
      (let [response (services-endpoint
                      (request :get "/v1/taxonomy/graphql"
                               {:query query}))]
        (is (= 200 (:status response)))
        (is (= {:data {:changelog []}}
               (integration-helpers/json-body response)))))))



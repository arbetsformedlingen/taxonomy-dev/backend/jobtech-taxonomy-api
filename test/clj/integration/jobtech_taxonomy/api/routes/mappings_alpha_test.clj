(ns ^:mappings ^:endpoints integration.jobtech-taxonomy.api.routes.mappings-alpha-test
  (:require [clojure.data.json :as json]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.features.mapping.setup :as mapping-setup]
            [ring.mock.request :refer [request]]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema-and-data udf/cfg))

(defn read-body [response]
  (let [response-body (:body response)]
    (log/warn :resp response)
    (if (string? response-body)
      response-body
      (json/read-json (slurp response-body)))))

(def services-endpoint
  (handler/app (mapping-setup/provide-endpoints udf/cfg)))

(defn ->ep [path] (str "/v1/taxonomy/alpha" path))

(deftest mappings-test
  (testing "GET mappings with taxonomy-version"
    (let [response (services-endpoint
                    (request :get (->ep "/mappings")))]
      (is (= 200 (:status response)))
      (is (= #{{:mappings 4 :organisation "ESCO"}
               {:mappings 3 :organisation "The Mighty Pirates"}
               {:mappings 2 :organisation "UNESCO"}}
             (set (read-body response)))))
    (let [response (services-endpoint
                    (request :get (->ep "/mappings/")))]
      (is (= 200 (:status response)))
      (is (= #{{:mappings 4 :organisation "ESCO"}
               {:mappings 3 :organisation "The Mighty Pirates"}
               {:mappings 2 :organisation "UNESCO"}}
             (set (read-body response))))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO"))]
      (is (= 200 (:status response)))
      (is (= #{{:description "Mappings between SUN2000-level concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-levels"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-levels"
                :version "2011"}
               {:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-fields"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-fields"
                :version "2013"}}
             (set (read-body response))))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures"))]
      (is (= 200 (:status response)))
      (is (= #{{:description "Mappings between SUN2000-level concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-levels"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-levels"
                :version "2011"}
               {:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-fields"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-fields"
                :version "2013"}}
             (set (read-body response))))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields"))]
      (is (= 200 (:status response)))
      (is (= [{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
               :family "Eures"
               :name "sun->isced-fields"
               :organisation "UNESCO"
               :taxonomy-version "1"
               :type "sun->isced-fields"
               :version "2013"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013"))]
      (is (= 200 (:status response)))
      (is (= [{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
               :family "Eures"
               :name "sun->isced-fields"
               :organisation "UNESCO"
               :taxonomy-version "1"
               :type "sun->isced-fields"
               :version "2013"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013/CU11_PU7_Yj1,WFCz_teU_KFf"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013/CU11_PU7_Yj1,WFCz_teU_KFf"))]
      (is (= 200 (:status response)))
      (is (= [{:found [{:mapped-id "041"
                        :source {:preferred-label "Företagsekonomi, handel och administration"
                                 :sun-2000-field "34"
                                 :type "sun-education-field-2"
                                 :concept/id "CU11_PU7_Yj1"}
                        :target {:mapping.external/id "041"
                                 :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                                   ["label" "Business and administration"]]}}
                       {:mapped-id "071"
                        :source {:preferred-label "Teknik och teknisk industri"
                                 :sun-2000-field "52"
                                 :type "sun-education-field-2"
                                 :concept/id "WFCz_teU_KFf"}
                        :target {:mapping.external/id "071"
                                 :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                                   ["label"
                                                                    "Engineering and engineering trades"]]}}]
               :mapping {:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                         :family "Eures"
                         :name "sun->isced-fields"
                         :organisation "UNESCO"
                         :taxonomy-version "1"
                         :type "sun->isced-fields"
                         :version "2013"}}]
             (read-body response))))))

(deftest mappings-with-taxonomy-version-test
  (testing "GET mappings with taxonomy-version"
    (let [response (services-endpoint
                    (request :get (->ep "/mappings?taxonomy-version=latest")))]
      (is (= 200 (:status response)))
      (is (= []
             (read-body response))))
    (let [response (services-endpoint
                    (request :get (->ep "/mappings?taxonomy-version=23")))]
      (is (= 200 (:status response)))
      (is (= [{:mappings 3 :organisation "The Mighty Pirates"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= #{{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-fields"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-fields"
                :version "2013"}
               {:description "Mappings between SUN2000-level concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-levels"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-levels"
                :version "2011"}}
             (set (read-body response))))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= #{{:description "Mappings between SUN2000-level concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-levels"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-levels"
                :version "2011"}
               {:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                :family "Eures"
                :name "sun->isced-fields"
                :organisation "UNESCO"
                :taxonomy-version "1"
                :type "sun->isced-fields"
                :version "2013"}}
             (set (read-body response))))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= [{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
               :family "Eures"
               :name "sun->isced-fields"
               :organisation "UNESCO"
               :taxonomy-version "1"
               :type "sun->isced-fields"
               :version "2013"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= [{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
               :family "Eures"
               :name "sun->isced-fields"
               :organisation "UNESCO"
               :taxonomy-version "1"
               :type "sun->isced-fields"
               :version "2013"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= [{:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
               :family "Eures"
               :name "sun->isced-fields"
               :organisation "UNESCO"
               :taxonomy-version "1"
               :type "sun->isced-fields"
               :version "2013"}]
             (read-body response)))))
  (testing "GET /v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013/CU11_PU7_Yj1,WFCz_teU_KFf?taxonomy-version=1"
    (let [response (services-endpoint
                    (request :get "/v1/taxonomy/alpha/mappings/UNESCO/Eures/sun-%3Eisced-fields/sun-%3Eisced-fields/2013/CU11_PU7_Yj1,WFCz_teU_KFf?taxonomy-version=1"))]
      (is (= 200 (:status response)))
      (is (= [{:found [{:mapped-id "041"
                        :source {:preferred-label "Företagsekonomi, handel och administration"
                                 :sun-2000-field "34"
                                 :type "sun-education-field-2"
                                 :concept/id "CU11_PU7_Yj1"}
                        :target {:mapping.external/id "041"
                                 :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                                   ["label" "Business and administration"]]}}
                       {:mapped-id "071"
                        :source {:preferred-label "Teknik och teknisk industri"
                                 :sun-2000-field "52"
                                 :type "sun-education-field-2"
                                 :concept/id "WFCz_teU_KFf"}
                        :target {:mapping.external/id "071"
                                 :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                                   ["label"
                                                                    "Engineering and engineering trades"]]}}]
               :mapping {:description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                         :family "Eures"
                         :name "sun->isced-fields"
                         :organisation "UNESCO"
                         :taxonomy-version "1"
                         :type "sun->isced-fields"
                         :version "2013"}}]
             (read-body response))))))

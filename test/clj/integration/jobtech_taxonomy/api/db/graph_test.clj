(ns integration.jobtech-taxonomy.api.db.graph-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.graph :as graph]
            [utils.db-fixtures :as udf]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(deftest test-fetch-graph
  (let [cfg udf/cfg
        include-deprecated true
        offset 0
        limit 4
        version :latest]
    (testing "test broad relation type"
      (is (= {:taxonomy/graph
              {:taxonomy/edges #{{:taxonomy/relation-type "broader",
                                  :taxonomy/source "RdVK_4zC_kFV",
                                  :taxonomy/target "sWzF_6pd_Y6L"}},
               :taxonomy/nodes #{{:taxonomy/definition "Någon sorts chef",
                                  :taxonomy/id "sWzF_6pd_Y6L",
                                  :taxonomy/preferred-label "Övriga chefer inom samhällsservice",
                                  :taxonomy/type "ssyk-level-4"}
                                 {:taxonomy/definition "Redaktionschef",
                                  :taxonomy/id "RdVK_4zC_kFV",
                                  :taxonomy/preferred-label "Redaktionschef",
                                  :taxonomy/type "occupation-name"}}}}
             (graph/fetch-graph cfg "broader" "occupation-name" "ssyk-level-4" include-deprecated offset limit version))))

    (testing "test broad-match relation type is empty"
      (is (= {:taxonomy/graph {:taxonomy/edges #{}, :taxonomy/nodes #{}}}
             (graph/fetch-graph cfg "broad-match" "occupation-name" "ssyk-level-4" include-deprecated offset limit version))))

    (testing "test inputs is empty string"
      (is (= {:taxonomy/graph {:taxonomy/edges #{}, :taxonomy/nodes #{}}}
             (graph/fetch-graph cfg "" "" "" nil nil nil 5))))

    (testing "test inputs is zero string"
      (is (= {:taxonomy/graph {:taxonomy/edges #{}, :taxonomy/nodes #{}}}
             (graph/fetch-graph cfg "0" "0" "0" nil nil nil 5))))

    (testing "test relation input is wrong"
      (is (= {:taxonomy/graph {:taxonomy/edges #{}, :taxonomy/nodes #{}}}
             (graph/fetch-graph cfg "taxonomy" "0" "0" nil nil nil 5))))

    (testing "test inputs is wrong"
      (is (= {:taxonomy/graph {:taxonomy/edges #{}, :taxonomy/nodes #{}}}
             (graph/fetch-graph cfg "taxonomy" "taxonomy" "taxonomy" nil nil nil 5))))))

(deftest test-fetch-graph-with-substitutability
  (let [cfg udf/cfg
        offset 0
        limit 4
        include-deprecated true
        version :latest
        result (graph/fetch-graph cfg
                                  "broader"
                                  "ssyk-level-4"
                                  "occupation-field"
                                  include-deprecated
                                  offset limit version)]
    (is (= {:taxonomy/graph
            {:taxonomy/nodes
             #{#:taxonomy{:id "bh3H_Y3h_5eD",
                          :type "occupation-field",
                          :preferred-label
                          "Chefer av olika slag",
                          :definition
                          "Exempel på arbetsuppgifter ..."}
               #:taxonomy{:id "sWzF_6pd_Y6L",
                          :type "ssyk-level-4",
                          :preferred-label
                          "Övriga chefer inom samhällsservice",
                          :definition "Någon sorts chef"}},
             :taxonomy/edges
             #{#:taxonomy{:source "sWzF_6pd_Y6L",
                          :target "bh3H_Y3h_5eD",
                          :relation-type "broader",
                          :substitutability-percentage 0}}}}
           result))))

(deftest test-handle-relations
  (testing "empty query"
    (let [query {}
          relation "relation"
          concept-1-type "concept-1-type"
          concept-2-type "concept-2-type"
          query-with-relations (#'graph/handle-relations query relation concept-1-type concept-2-type)]
      (is (= '{:args ("concept-2-type" "concept-1-type" "relation"),
               :query {:in (?c2-type ?c1-type ?relation-type),
                       :where ([?c2 :concept/type ?c2-type]
                               [?c1 :concept/type ?c1-type]
                               (edge ?c1 ?relation-type ?c2 ?r))}} query-with-relations))))

  (testing "with default query"
    (let [query {:query '{:find [(pull ?r
                                       [:relation/type
                                        :relation/substitutability-percentage
                                        {:relation/concept-1 [:concept/id]
                                         :relation/concept-2 [:concept/id]}])
                                 (pull ?c1 concept-pull-pattern)
                                 (pull ?c2 concept-pull-pattern)]
                          :in [concept-pull-pattern $ %]
                          :where []}
                 :args ['concept-pull-pattern]}
          relation "relation"
          concept-1-type "concept-1-type"
          concept-2-type "concept-2-type"
          query-with-relations (#'graph/handle-relations query relation concept-1-type concept-2-type)]
      (is (= '{:args [concept-pull-pattern "relation" "concept-1-type" "concept-2-type"],
               :query {:find [(pull
                               ?r
                               [:relation/type
                                :relation/substitutability-percentage
                                {:relation/concept-1 [:concept/id], :relation/concept-2 [:concept/id]}])
                              (pull ?c1 concept-pull-pattern)
                              (pull ?c2 concept-pull-pattern)],
                       :in [concept-pull-pattern $ % ?relation-type ?c1-type ?c2-type],
                       :where [(edge ?c1 ?relation-type ?c2 ?r)
                               [?c1 :concept/type ?c1-type]
                               [?c2 :concept/type ?c2-type]]}} query-with-relations)))))





(ns ^:datomic integration.jobtech-taxonomy.api.db.database-connection-datomic-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]))

(def cfg {:backend {:allow-create true
                    :allow-delete true
                    :type :datomic
                    :cfg {:server-type :datomic-local
                          :db-name "oak"
                          :system "dev"
                          :storage-dir :mem}}})

(deftest datomic-backend-test
  (testing "Create database, connect and transact some data"
    (try
      (dc/create-database cfg)
      (let [conn (dc/connect cfg)]
        (is (= :datomic (dc/database-backend conn)))
        (let [schema-result
              (dc/transact
               conn
               {:tx-data [{:db/ident :name :db/valueType :db.type/string
                           :db/cardinality :db.cardinality/one}
                          {:db/ident :age :db/valueType :db.type/long
                           :db/cardinality :db.cardinality/one}]})]
          (is (= :datomic (dc/database-backend (:db-before schema-result))))
          (is (= :datomic (dc/database-backend (:db-after schema-result))))
          (let [data-result
                (dc/transact
                 (dc/connect cfg)
                 {:tx-data
                  [{:name "bob" :age 42} {:name "linda" :age 47}]})]
            (is (some? data-result)))
          (let [db (dc/get-db cfg :next)]
            (is (= :datomic
                   (dc/database-backend db)))
            (is (= [["linda"]]
                   (dc/q '[:find ?name
                           :in $ [?age]
                           :where
                           [?e :name ?name]
                           [?e :age ?age]]
                         db
                         [47]))))))
      (finally
        (dc/delete-database cfg)))))

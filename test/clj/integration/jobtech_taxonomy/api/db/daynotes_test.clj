(ns ^:daynotes integration.jobtech-taxonomy.api.db.daynotes-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.daynotes :as daynotes]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mini-db :as mini-db]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (udf/database-minimal udf/cfg))

;; TODO: Update `udf/database-minimal` with data to make this test interesting. 
(deftest test-get-concept-changes-with-pagination-public
  (testing "get-concept-changes-with-pagination-public"
    (let [after-version 1
          to-version-inclusive 7
          offset nil
          limit nil
          result (daynotes/get-concept-changes-with-pagination-public
                  udf/cfg after-version to-version-inclusive offset limit)]
      (is (= 19 (count result)))
      (is (= {:event-type "CREATED"
              :latest-version-of-concept {:concept/definition "Hovslageri"
                                          :concept/id "gjd2_JSR_AeG"
                                          :concept/preferred-label "Hovslageri"
                                          :concept/type "skill"}
              :version 2
              :new-concept {:concept/definition "Hovslageri"
                            :concept/id "gjd2_JSR_AeG"
                            :concept/preferred-label "Hovslageri"
                            :concept/type "skill"}}
             (first result)))))
  (testing "limits and offset"
    (let [after-version 1
          to-version-inclusive 5
          offset nil
          limit 5
          result (daynotes/get-concept-changes-with-pagination-public
                  udf/cfg after-version to-version-inclusive offset limit)]
      (is (= limit (count result)))
      (is (= [{:event-type "CREATED"
               :latest-version-of-concept {:concept/definition "Hovslageri"
                                           :concept/id "gjd2_JSR_AeG"
                                           :concept/preferred-label "Hovslageri"
                                           :concept/type "skill"}
               :version 2
               :new-concept {:concept/definition "Hovslageri"
                             :concept/id "gjd2_JSR_AeG"
                             :concept/preferred-label "Hovslageri"
                             :concept/type "skill"}}
              {:event-type "CREATED"
               :latest-version-of-concept {:concept/definition "Caymanöarna"
                                           :concept/id "MS1s_AYg_kZe"
                                           :concept/preferred-label "Caymanöarna"
                                           :concept/type "country"}
               :version 2
               :new-concept {:concept/definition "Caymanöarna"
                             :concept/id "MS1s_AYg_kZe"
                             :concept/preferred-label "Caymanöarna"
                             :concept/type "country"
                             :concept.external-database.ams-taxonomy-67/id "36",
                             :concept.external-standard/iso-3166-1-alpha-2-2013 "KY"
                             :concept.external-standard/iso-3166-1-alpha-3-2013 "CYM"}}
              {:event-type "CREATED"
               :latest-version-of-concept {:concept/definition "Land of Bob"
                                           :concept/id "TAKE_THE_BOB"
                                           :concept/preferred-label "Land of Bob"
                                           :concept/type "country"}
               :new-concept {:concept/definition "Land of Bob"
                             :concept/id "TAKE_THE_BOB"
                             :concept/preferred-label "Land of Bob"
                             :concept/type "country"
                             :concept.external-database.ams-taxonomy-67/id "42"
                             :concept.external-standard/iso-3166-1-alpha-2-2013 "LB"
                             :concept.external-standard/iso-3166-1-alpha-3-2013 "LOB"}
               :version 2}
              {:event-type "DEPRECATED"
               :latest-version-of-concept {:concept/definition "Skåne län"
                                           :concept/id "CaRE_1nn_cSU"
                                           :concept/preferred-label "Skåne län"
                                           :concept/type "region"}
               :version 2}
              {:event-type "CREATED"
               :latest-version-of-concept {:concept/definition "Campobasso"
                                           :concept/id "iRmD_coH_YZ6"
                                           :concept/preferred-label "Campobasso"
                                           :concept/type "region"}
               :new-concept {:concept/definition "Campobasso"
                             :concept/id "iRmD_coH_YZ6"
                             :concept/preferred-label "Campobasso"
                             :concept/type "region"}
               :version 2}]

             result)))))

(deftest test-get-tx-version-map
  (testing "get-tx-version-map"
    (is (= [0 1 2 3 4 5 6 7]
           (-> (dc/get-db udf/cfg :next)
               (#'daynotes/get-tx-version-map)
               vals)))))

(deftest build-fetch-relation-txs-query-test
  (testing "with nils"
    (let [hist-db 'hist-db
          concept-id nil
          from-version nil
          to-version nil
          result (#'daynotes/build-fetch-relation-txs-query hist-db concept-id from-version to-version)]
      (is (= {:query '{:find [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage]
                       :keys [tx timestamp user-id comment relation substitutability_percentage]
                       :in [$]
                       :where [[?c :concept/id ?input-concept-id]
                               (or [?r :relation/concept-1 ?c]
                                   [?r :relation/concept-2 ?c])
                               (or [?r _ _ ?tx]
                                   [?tx :daynote/ref ?r])
                               [(get-else $ ?r :relation/substitutability-percentage 0) ?substitutability_percentage]
                               [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                               [(get-else $ ?tx :daynote/comment false) ?comment]
                               [?tx :db/txInstant ?inst]]}
              :args [hist-db]}
             result))))
  (testing "with concept-id, from-version as inst?, and to-version as inst?"
    (let [hist-db 'hist-db
          concept-id "gjd2_JSR_AeG"
          from-version #inst "2000-01-01T00:00:00.000-00:00"
          to-version #inst "2022-01-01T00:00:00.000-00:00"
          result (#'daynotes/build-fetch-relation-txs-query hist-db concept-id from-version to-version)]
      (is (= {:query '{:find [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage]
                       :keys [tx timestamp user-id comment relation substitutability_percentage]
                       :in [$ ?input-concept-id ?from-version ?to-version]
                       :where [[?c :concept/id ?input-concept-id]
                               (or [?r :relation/concept-1 ?c]
                                   [?r :relation/concept-2 ?c])
                               (or [?r _ _ ?tx]
                                   [?tx :daynote/ref ?r])
                               [(get-else $ ?r :relation/substitutability-percentage 0) ?substitutability_percentage]
                               [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                               [(get-else $ ?tx :daynote/comment false) ?comment]
                               [?tx :db/txInstant ?inst]
                               [(<= ?from-version ?inst)]
                               [(< ?inst ?to-version)]]}
              :args [hist-db concept-id from-version to-version]}
             result))))
  (testing "with concept-id, from-version as int?, and to-version as int?"
    (let [hist-db 'hist-db
          concept-id "gjd2_JSR_AeG"
          from-version 1
          to-version 5
          result (#'daynotes/build-fetch-relation-txs-query hist-db concept-id from-version to-version)]
      (is (= {:query '{:find [?tx ?inst ?user-id ?comment ?r ?substitutability_percentage]
                       :keys [tx timestamp user-id comment relation substitutability_percentage]
                       :in [$ ?input-concept-id ?from-version ?to-version]
                       :where [[?c :concept/id ?input-concept-id]
                               (or [?r :relation/concept-1 ?c]
                                   [?r :relation/concept-2 ?c])
                               (or [?r _ _ ?tx]
                                   [?tx :daynote/ref ?r])
                               [(get-else $ ?r :relation/substitutability-percentage 0) ?substitutability_percentage]
                               [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                               [(get-else $ ?tx :daynote/comment false) ?comment]
                               [?tx :db/txInstant ?inst]
                               [_ :taxonomy-version/id ?from-version ?from-tx]
                               [?from-tx :db/txInstant ?from-inst]
                               [(<= ?from-inst ?inst)]
                               [_ :taxonomy-version/id ?to-version ?to-tx]
                               [?to-tx :db/txInstant ?to-inst]
                               [(< ?inst ?to-inst)]]}
              :args [hist-db concept-id from-version to-version]}
             result)))))

(deftest fetch-relation-txs-test
  (testing "Fetching relation transactions"
    (let [hist-db (dc/history (dc/get-db udf/cfg :next))
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2005-01-01T00:00:00.000-00:00"
          result (#'daynotes/fetch-relation-txs hist-db nil from-timestamp to-timestamp)]
      (is (= 1 (count result)))
      (is (= {:comment "Just a little change",
              :substitutability_percentage 0,
              :timestamp #inst "2000-01-01T00:00:07.000-00:00",
              :user-id "editor-1"}
             (dissoc (first result)
                     :relation :tx)))))
  (testing "Fetching relation transactions for concept-1 should match concept-2"
    (comment
      ;; See the change made in mini-db/version-1
      mini-db/version-1)
    (let [hist-db (dc/history (dc/get-db udf/cfg :next))
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2005-01-01T00:00:00.000-00:00"
          concept-1 "sWzF_6pd_Y6L"
          result-1 (#'daynotes/fetch-relation-txs
                    hist-db concept-1 from-timestamp to-timestamp)
          concept-2 "j1Yr_UhC_X16"
          result-2 (#'daynotes/fetch-relation-txs
                    hist-db concept-2 from-timestamp to-timestamp)]
      (is (= 1 (count result-1)))
      (is (= result-1 result-2))))
  (testing "Fetching relation transactions for a concept after it's last change should be empty"
    (let [hist-db (dc/history (dc/get-db udf/cfg :next))
          from-timestamp #inst "2003-01-01T00:00:00.000-00:00"
          concept-id "sWzF_6pd_Y6L"
          result (#'daynotes/fetch-relation-txs
                  hist-db concept-id from-timestamp nil)]
      (is (empty? result)))))

(deftest get-for-relation-test
  (testing "get-for-relation with empty result"
    (let [db (dc/get-db udf/cfg :next)
          concept-id "non-existent-concept"
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2022-01-01T00:00:00.000-00:00"
          offset nil
          limit nil
          show-unpublished true
          result (daynotes/get-for-relation db concept-id from-timestamp to-timestamp offset limit show-unpublished)]
      (is (empty? result))))
  (testing "get-for-relation with pagination limit"
    (let [db (dc/get-db udf/cfg :next)
          concept-id "sWzF_6pd_Y6L"
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2022-01-01T00:00:00.000-00:00"
          offset nil
          limit 5
          show-unpublished true
          result (daynotes/get-for-relation db concept-id from-timestamp to-timestamp offset limit show-unpublished)]
      (is (= 1 (count result)))))
  (testing "get-for-relation with pagination offset"
    (let [db (dc/get-db udf/cfg :next)
          concept-id "sWzF_6pd_Y6L"
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2022-01-01T00:00:00.000-00:00"
          offset 10
          limit 10
          show-unpublished true
          result (daynotes/get-for-relation db concept-id from-timestamp to-timestamp offset limit show-unpublished)]
      (is (empty? result))))
  (testing "get-for-relation with empty result"
    (let [db (dc/get-db udf/cfg :next)
          concept-id "non-existent-concept"
          from-timestamp #inst "2000-01-01T00:00:00.000-00:00"
          to-timestamp #inst "2022-01-01T00:00:00.000-00:00"
          offset nil
          limit nil
          show-unpublished true
          result (daynotes/get-for-relation db concept-id from-timestamp to-timestamp offset limit show-unpublished)]
      (is (empty? result)))))

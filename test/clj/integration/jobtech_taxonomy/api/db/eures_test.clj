(ns integration.jobtech-taxonomy.api.db.eures-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.eures :as eures]
            [ring.util.http-predicates :as predicates]
            [utils.db-fixtures :as udf]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(deftest test-handler
  (testing "test handler"
    (let [result (eures/handler udf/cfg {:parameters {:query {:id "RdVK_4zC_kFV"}}})]
      (is (predicates/ok? result))
      (is (= ["http://data.europa.eu/esco/isco/C1349"] (:body result)))))

  (testing "test handler with empty id"
    (let [result (eures/handler udf/cfg {:parameters {:query {:id ""}}})]
      (is (predicates/not-found? result))
      (is (= {:taxonomy/error "ID not found."
              :taxonomy/info {:id ""}} (:body result)))))

  (testing "test handler with wrong id"
    (let [result (eures/handler udf/cfg {:parameters {:query {:id "the wrong id"}}})]
      (is (predicates/not-found? result))
      (is (= {:taxonomy/error "ID not found."
              :taxonomy/info {:id "the wrong id"}} (:body result)))))

  (testing "test handler with zero number id"
    (let [result (eures/handler udf/cfg {:parameters {:query {:id 0}}})]
      (is (predicates/not-found? result))
      (is (= {:taxonomy/error "ID not found."
              :taxonomy/info {:id 0}} (:body result)))))

  (testing "test handler with nil"
    (let [result (eures/handler udf/cfg {:parameters {:query {}}})]
      (is (predicates/not-found? result))
      (is (= {:taxonomy/error "ID not found."
              :taxonomy/info {:id nil}} (:body result))))))


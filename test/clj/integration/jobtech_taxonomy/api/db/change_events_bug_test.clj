(ns integration.jobtech-taxonomy.api.db.change-events-bug-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.daynotes :as daynotes]
            [jobtech-taxonomy.api.db.versions :as versions]
            [jobtech-taxonomy.common.io-utils :as iou]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(set! *warn-on-reflection* true)

(defonce cfg (let [init-dir-path (-> (Files/createTempDirectory
                                      "datomic-local-minimal"
                                      (into-array FileAttribute []))
                                     .toFile
                                     .getPath
                                     str)
                   _ (iou/unzip-file (iou/existing-file "resources/taxonomy.zip") init-dir-path)
                   new-cfg (assoc udf/cfg
                                  :backend
                                  {:allow-create true
                                   :allow-delete true
                                   :type :datomic
                                   :cfg {:db-name "jobtech-taxonomy-db"
                                         :server-type :datomic-local
                                         :system "jobtech-taxonomy"
                                         :storage-dir init-dir-path}})]
               (db/connect new-cfg)
               new-cfg))

(test/use-fixtures :once
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :warn]
                   [#{"datahike.*"} :error]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :warn]]}
      (fun-run))))

(deftest test-get-concept-changes
  (testing "get-concept-changes-with-pagination-public"
    (let [versions (->> (versions/get-all-versions cfg)
                        (map :version))
          latest (apply max versions)
          previous (dec latest)
          public-count (-> (daynotes/get-concept-changes-with-pagination-public
                            cfg previous latest nil nil)
                           count)
          private-count (-> (daynotes/get-concept-changes-with-pagination-private
                             cfg previous latest nil nil)
                            count)
          public-latest->next (-> (daynotes/get-concept-changes-with-pagination-public
                                   cfg latest :next nil nil)
                                  count)]

      (is (> public-count 0))
      (is (= public-count private-count))
      (is (= 0 public-latest->next)))))

(deftest test-get-relation-changes
  (testing "get-relation-changes-with-pagination-public"
    (let [versions (->> (versions/get-all-versions cfg)
                        (map :version))
          latest (apply max versions)
          previous (dec latest)
          public-count (-> (daynotes/get-relation-changes-with-pagination-public
                            cfg previous latest nil nil)
                           count)
          private-count (-> (daynotes/get-relation-changes-with-pagination-private
                             cfg previous latest nil nil)
                            count)
          public-latest->next (-> (daynotes/get-relation-changes-with-pagination-public
                                   cfg latest :next nil nil)
                                  count)]

      (is (> public-count 0))
      (is (= public-count private-count))
      (is (= 0 public-latest->next)))))

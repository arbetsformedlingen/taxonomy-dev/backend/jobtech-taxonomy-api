(ns ^:real-server-test integration.jobtech-taxonomy.api.api-test
  (:require [babashka.fs :as fs]
            [clojure.data.json :as json]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.test :refer [deftest is testing use-fixtures]]
            [clojure.walk :refer [postwalk]]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [malli.core :as malli]
            [taoensso.timbre :as timbre]
            [utils.db-fixtures :refer [real-cfg]]
            [utils.integration-helpers :as integration-helpers]))

(def sample-repo-schema [:map [:root [:fn fs/directory?]]])
(def api-test-config-schema [:map
                    [:sample-repo sample-repo-schema]
                    [:lower-version :int]
                    [:api-cfg config/taxonomy-single-backend-config-schema]
                    [:sample-pred ifn?]])
(def indexed-file-repo-schema [:map
                               [:root-path :any]
                               [:filename-prefix :string]])


(use-fixtures :once integration-helpers/real-server-fixture)

(defn recursively-sort-collections [data]
  (postwalk (fn [x]
              (cond
                (set? x) (into (sorted-set) x)
                (map? x) (into (sorted-map) x)
                :else x))
            data))

(defn- indexed-file-repo-make-filename [{:keys [root-path filename-prefix]} index]
  (fs/file
   root-path
   (format "%s_%d.edn" filename-prefix index)))

(def ifr-pattern #"^(.*)_(\d+).edn$")

(defn- indexed-file-repo-parse-path [{:keys [_root-path filename-prefix]} path]
  (when-let [[_ prefix index] (re-matches ifr-pattern (fs/file-name path))]
    (when (= prefix filename-prefix)
      (Long/parseLong index))))

(defn indexed-file-repo-list [{:keys [root-path] :as ifr}]
  (sort (keep #(indexed-file-repo-parse-path ifr %)
              (when (fs/directory? root-path)
                (fs/list-dir root-path)))))

(defn indexed-file-repo-delete [ifr index]
  (fs/delete-if-exists
   (indexed-file-repo-make-filename ifr index)))

(defn indexed-file-repo-read [ifr index]
  (with-open [r (io/reader (indexed-file-repo-make-filename ifr index))]
    (edn/read (java.io.PushbackReader. r))))

(defn indexed-file-repo-write [ifr index data]
  (let [filename (indexed-file-repo-make-filename ifr index)]
    (io/make-parents filename)
    (spit filename
          (with-out-str
            (pprint (recursively-sort-collections data))))))


(defn indexed-file-repo [root-path filename-prefix]
  {:root-path root-path
   :filename-prefix filename-prefix})

(defn indexed-file-repo? [x]
  (malli/validate indexed-file-repo-schema x))

(defn lookup-latest-until [ifr index]
  {:pre [(indexed-file-repo? ifr)
         (int? index)]}
  (when-let [index (->> ifr
                        (indexed-file-repo-list)
                        (sort-by -)
                        (drop-while #(< index %))
                        first)]
    (indexed-file-repo-read ifr index)))

(def sample-repo? (malli/validator sample-repo-schema))
(def api-test-config? (malli/validator api-test-config-schema))

(defn sample-repo-metadata-file [sample-repo]
  {:pre [(sample-repo? sample-repo)]}
  (fs/file (:root sample-repo) "metadata.edn"))

(defn sample-repo-metadata
  ([sample-repo]
   (-> sample-repo
       sample-repo-metadata-file
       slurp
       edn/read-string))
  ([sample-repo new-data]
   (spit (sample-repo-metadata-file sample-repo)
         (pr-str new-data))))

(defn- list-sample-ids [sample-repo]
  {:pre [(sample-repo? sample-repo)]}
  (let [root (:root sample-repo)]
    (for [f (-> sample-repo :root fs/file file-seq)
          :when (= "data.edn" (fs/file-name f))]
      (str (fs/relativize root (fs/parent f))))))

(defn sample-data? [x]
  (and (map? x)
       (string? (:relative-path x))
       (string? (:log-id x))))

(defn id-from-data [data]
  {:pre [(sample-data? data)]}
  (str (subs (:relative-path data) 1)
       "/"
       (:log-id data)))

(defn load-request-data [repo sample-id]
  {:pre [(sample-repo? repo)
         (string? sample-id)]
   :post [(= sample-id (id-from-data %))]}
  (let [path (fs/file (:root repo) sample-id "data.edn")]
    (if (fs/exists? path)
      (with-open [r (io/reader path)]
        (edn/read (java.io.PushbackReader. r)))
      (throw (ex-info "Failed to load data.edn" {:path path})))))

(defn versioned-check-repo-data-path [repo sample-id]
  (fs/path (:root repo)
           sample-id
           "checks"))

(defn versioned-check-repo-for-sample
  "This returns a function that can be used to read and update the specific checks for every version."
  [repo sample-id]
  (indexed-file-repo (versioned-check-repo-data-path repo sample-id) "version"))

(defn sample-id-collection [repo sample-id-predicate]
  {:pre [(sample-repo? repo)
         (ifn? sample-id-predicate)]}
  (into []
        (filter sample-id-predicate)
        (list-sample-ids repo)))

(defn sample-collection [repo sample-id-predicate]
  (let [ids (sample-id-collection repo sample-id-predicate)
        n (count ids)
        data (seq (map #(load-request-data repo %) ids))]
    (reify clojure.lang.Seqable
      (seq [_] data)
      clojure.lang.Counted
      (count [_] n))))

(defn leaf-shape-fn [x]
  (cond
    (nil? x) nil
    (number? x) :number
    (string? x) :string
    (boolean? x) :boolean
    (symbol? x) :symbol
    (keyword? x) :keyword
    :else :other))

(defn path-descriptor [data leaf-fn]
  (loop [stack [[[] data]]
         paths (sorted-set)]
    (if (empty? stack)
      paths
      (let [[[path x] & stack] stack
            expand (fn [f] (into stack (map f) x))]
        (cond
          (map? x)
          (recur (expand (fn [[k v]] [(conj path [:key k]) v]))
                 (conj paths (conj path [:map])))

          (coll? x)
          (recur (expand (fn [x] [(conj path [:elem]) x]))
                 (conj paths (conj path [:coll])))
          :else (recur stack (conj paths (conj path [:leaf (leaf-fn x)]))))))))

(defn shape-path-from-value-path [path]
  (mapv (fn [[k data :as kv]]
          (if (= k :leaf)
            [k (leaf-shape-fn data)]
            kv))
        path))

(defn sample-representative-value-paths [value-paths]
  (into (sorted-set)
        (map val)
        (into {}
              (map (fn [p] [(shape-path-from-value-path p) p]))
              (shuffle value-paths))))

(def varying-endpoint-set #{"/v1/taxonomy/suggesters/autocomplete"})

(defn varying-response? [sample]
  {:pre [(sample-data? sample)]}
  (or (contains? varying-endpoint-set (:relative-path sample))
      (let [s (str (-> sample :test :query-params))]
        ;; This is a bit approximate at the moment
        (or (str/includes? s "offset")
            (str/includes? s "limit")
            (not (str/includes? s "version"))))))

(defn read-json-str [x]
  (try
    (json/read-str x :key-fn keyword)
    (catch Exception _e ::failed)))

(defn with-json-body [response]
  (merge response
         (let [json-data (read-json-str (:body response))]
           (if (= ::failed json-data)
             {}
             {:json-body json-data}))))

(defn http-run-sample [sample-data]
  (let [path (:relative-path sample-data)
        params (-> sample-data :test :query-params)]
    (assert (string? path))
    (assert (or (nil? params) (map? params)))
    (->> (cond-> {:accept :json
                  :throw-exceptions false
                  :headers {"x-include-response-debug-headers" "1"}}
           params (assoc :query-params params))
         (integration-helpers/http-get path)
         with-json-body)))

(defn some-body [response]
  (or (:json-body response)
      (:body response)))

(defn evaluate-response-descriptor [sample]
  (let [response (http-run-sample sample)
        varying (varying-response? sample)]
    {:status (:status response)
     :varying varying
     :descriptor (path-descriptor
                  (some-body response)
                  (if varying leaf-shape-fn identity))}))

(set! *warn-on-reflection* true)

(defn as-of-version [cfg exclusive-upper-version body-fn]
  {:pre [(map? cfg)
         (int? exclusive-upper-version)
         (fn? body-fn)]}
  (let [db (dc/db (dc/connect cfg))
        tx-id (dc/get-transaction-id-from-version db exclusive-upper-version)
        old-db dc/db]
    (with-redefs [dc/db (fn [conn]
                          (let [db (old-db conn)]
                            (if tx-id (dc/as-of db tx-id) db)))]
      (body-fn))))

(defn evaluate-response-descriptors-for-versions [cfg sample versions]
  {:pre [(map? cfg)
         (sample-data? sample)
         (coll? versions)
         (every? number? versions)]}
  (into (sorted-map)
        (map (fn [version]
               (as-of-version
                cfg (inc version)
                (fn []
                  [version (evaluate-response-descriptor sample)]))))
        versions))

(defn generate-sequential-diff-checks
  ([version-descriptor-map]
   (generate-sequential-diff-checks version-descriptor-map identity))
  ([version-descriptor-map descriptor-fn]
   {:pre [(map? version-descriptor-map)
          (every? number? (keys version-descriptor-map))
          (ifn? descriptor-fn)]}
   (->> version-descriptor-map
        (sort-by key)
        (partition 2 1)
        (reduce (fn [result [[av a] [bv b]]]
                  {:pre [(map? result)]}
                  (let [a (descriptor-fn a)
                        b (descriptor-fn b)
                        intersection (set/intersection a b)
                        only-a (set/difference a intersection)
                        only-b (set/difference b intersection)]
                    (-> result
                        (update-in [av :includes] into only-a)
                        (update-in [av :excludes] into only-b)
                        (update-in [bv :includes] into only-b)
                        (update-in [bv :excludes] into only-a))))
                (into (sorted-map)
                      (map (fn [v] [v {:includes (sorted-set)
                                       :excludes (sorted-set)}]))
                      (keys version-descriptor-map))))))

(defn generate-overall-diff-checks
  ([version-descriptor-map]
   (generate-overall-diff-checks version-descriptor-map identity))
  ([version-descriptor-map descriptor-fn]
   (let [all-descs (mapv (comp set descriptor-fn val) version-descriptor-map)
         union (reduce set/union all-descs)
         intersection (reduce set/intersection all-descs)]
     (into {}
           (map (fn [[k v]]
                  (let [desc (set (descriptor-fn v))]
                    [k {:includes (set/difference desc intersection)
                        :excludes (set/difference union desc)}])))
           version-descriptor-map))))

(defn checks-from-varying-responses [version-descriptor-map]
  (into (sorted-map)
        (map (fn [[k desc]]
               [k (assoc (select-keys desc [:status :varying])
                         :checks
                         {:path-structure (:descriptor desc)})]))
        version-descriptor-map))

(defn path-frequencies [descriptor]
  (frequencies
   (map shape-path-from-value-path
        descriptor)))

(defn parent-collection-path [p]
  (when (seq p)
    (let [[k _] (peek p)]
      (case k
        :key (conj (pop p) [:map])
        :elem (conj (pop p) [:coll])
        (recur (pop p))))))

(defn parent-collection-paths [p]
  (into #{}
        (take-while some?)
        (rest (iterate parent-collection-path p))))

(defn parent-collection-path-in-set? [set p]
  (some set (parent-collection-paths p)))

(defn simplify-includes [includes]
  {:pre [(set? includes)]}
  (transduce (mapcat parent-collection-paths)
             disj
             includes
             includes))

(defn simplify-excludes [excludes]
  {:pre [(set? excludes)]}
  (into #{}
        (remove #(parent-collection-path-in-set? excludes %))
        excludes))

(defn checks-from-descriptor [seq-checks
                              overall-checks
                              desc]
  (let [descriptor (:descriptor desc)]
    (assoc (select-keys desc [:status :varying])
           :checks
           {:path-frequencies (path-frequencies descriptor)
            :includes (simplify-includes
                       (into #{}
                             (mapcat sample-representative-value-paths)
                             [(:includes seq-checks)
                              (:includes overall-checks)
                              descriptor]))
            :excludes (simplify-excludes
                       (into #{}
                             (mapcat sample-representative-value-paths)
                             [(:excludes seq-checks)
                              (:excludes overall-checks)]))})))

(defn checks-from-constant-responses [version-descriptor-map]
  (let [seq-diff-checks (generate-sequential-diff-checks version-descriptor-map :descriptor)
        overall-diff-checks (generate-overall-diff-checks version-descriptor-map :descriptor)]
    (into (sorted-map)
          (map (fn [[k desc]]
                 [k (checks-from-descriptor
                     (seq-diff-checks k)
                     (overall-diff-checks k)
                     desc)]))
          version-descriptor-map)))

(defn checks-from-version-response-descriptors [version-descriptor-map]
  (let [varying (into #{} (map :varying (vals version-descriptor-map)))]
    ((case varying
       #{false} checks-from-constant-responses
       #{true} checks-from-varying-responses)
     version-descriptor-map)))

(def default-sample-repo {:root (fs/path "test"
                                         "resources"
                                         "sample_requests")})

(defn default-sample-pred [sample-id]
  (not (str/starts-with?
        sample-id
        "v1/taxonomy/graphviz/types.")))

(def default-config {:sample-repo default-sample-repo
                     :lower-version 1
                     :api-cfg real-cfg
                     :sample-pred default-sample-pred})

(defn get-latest-released-version [cfg]
  (-> cfg
      dc/connect
      dc/db
      dc/get-latest-released-version))

(defn approximately-same-structure? [a b]
  {:pre [(set? a)
         (set? b)]}
  (let [union-cardinality (count (set/union a b))
        intersection-cardinality (count (set/intersection a b))]
    (<= (min 4 union-cardinality)
        intersection-cardinality)))

(defn dedupe-by-key [key-fn]
  (let [inner-dedupe-step ((dedupe) (fn [_ _] true))]
    (fn [step]
      (fn
        ([dst] (step dst))
        ([dst x]
         (if (inner-dedupe-step false (key-fn x))
           (step dst x)
           dst))))))

(defn drop-unchanged-subsequent-versions [version-descriptor-map]
  {:pre [(map? version-descriptor-map)]}
  (into {}
        (dedupe-by-key val)
        (sort-by key version-descriptor-map)))

(defn rebuild-test-data
  "Rebuild the test data used by `run-api-tests` using the provided configuration"
  ([] (rebuild-test-data default-config))
  ([{:keys [api-cfg sample-repo lower-version sample-pred]
     :as config}]
   {:pre [(api-test-config? config)]}
   (integration-helpers/real-data-fixture
    (fn []
      (let [samples (sample-collection sample-repo sample-pred)
            n (count samples)
            upper-version (get-latest-released-version api-cfg)
            versions (range lower-version (inc upper-version))]
        (sample-repo-metadata
         sample-repo
         {:latest-version upper-version})
        (doseq [[i sample] (map-indexed vector samples)
                :let [id (id-from-data sample)]]
          (timbre/info (format "--- Build checks for sample %d/%d: %s"
                               (inc i) n id))
          (let [check-repo (versioned-check-repo-for-sample sample-repo id)
                previous-inds (set (indexed-file-repo-list check-repo))
                descs (drop-unchanged-subsequent-versions
                       (evaluate-response-descriptors-for-versions
                        api-cfg sample versions))
                version-check-map
                (checks-from-version-response-descriptors descs)]

            ;; Remove previous checks not needed.
            (doseq [previous-index previous-inds
                    :when (not (contains? version-check-map previous-index))]
              (indexed-file-repo-delete check-repo previous-index))

            ;; Write new checks
            (doseq [[version checks-for-version] version-check-map
                    :when (not (previous-inds version))]
              (indexed-file-repo-write check-repo version checks-for-version))))))
    api-cfg)))

(defn delete-checks
  "This delete the subdirectory of checks for a specific sample. That subdirectory in turn contains the checks for every version of the taxonomy."
  [sample-repo id]
  (fs/delete-tree (versioned-check-repo-data-path sample-repo id)))

(defn clear-test-data
  ([] (clear-test-data default-config))
  ([{:keys [sample-repo sample-pred]}]
   (doseq [id (sample-id-collection sample-repo sample-pred)]
     (delete-checks sample-repo id))))

(defn symbol-from-sample-id [sample-id]
  {:pre [(string? sample-id)]}
  (let [b (StringBuilder.)]
    (doseq [^Character c sample-id]
      (.append b (if (or (Character/isAlphabetic (int c))
                         (Character/isDigit (int c))
                         (contains? #{\- \_} c))
                   c
                   \-)))
    (symbol (str b))))

(defmacro deftest-per-sample [run-sample-name config]
  (let [{:keys [sample-repo sample-pred]} default-config]
    `(do ~@(for [id (sample-id-collection
                     sample-repo
                     sample-pred)
                 :let [sym (symbol-from-sample-id id)]]
             `(deftest ~sym
                (~run-sample-name ~config ~id))))))

(defn test-sample [config id]
  (testing (format "API tests for sample %s" id)
    (let [sample-repo (:sample-repo config)
          sample (load-request-data sample-repo id)
          latest-testable-version (-> sample-repo
                                      sample-repo-metadata
                                      :latest-version)
          check-repo (versioned-check-repo-for-sample sample-repo id)
          response (http-run-sample sample)          
          deployed-version (-> response
                               :headers
                               (get "x-latest-taxonomy-version")
                               (or (throw (ex-info
                                           "Missing header 'x-latest-taxonomy-version'"
                                           {:response response})))
                               Long/parseLong)
          _ (when (< latest-testable-version deployed-version)
              (throw (ex-info "The deployed version is newer than the version for which the test data was built"
                              {:deployed-version deployed-version
                               :latest-testable-version
                               latest-testable-version})))

          {:keys [status varying checks]}
          (lookup-latest-until check-repo deployed-version)]
      (testing "HTTP status"
        (is (= status (:status response))))
      (testing "HTTP body"
        (if varying
          (let [desc (path-descriptor (some-body response)
                                      leaf-shape-fn)]
            (testing "Path structure"
              (is (approximately-same-structure?
                   (:path-structure checks)
                   desc))))
          (let [desc (path-descriptor (some-body response)
                                      identity)]
            (testing "Path frequencies"
              (is (= (:path-frequencies checks)
                     (path-frequencies desc))))
            (testing "Includes"
              (doseq [path (:includes checks)]
                (testing (format "Includes path '%s'" (pr-str path))
                  (is (contains? desc path)))))
            (testing "Excludes"
              (doseq [path (:excludes checks)]
                (testing (format "Excludes path '%s'" (pr-str path))
                  (is (not (contains? desc path))))))))))))

(deftest-per-sample test-sample default-config)

(deftest path-descriptor-test
  (testing "API test utilities"

    (is (= nil (leaf-shape-fn nil)))
    (is (= :number (leaf-shape-fn 9)))
    (is (= :string (leaf-shape-fn "abc")))
    (is (= :boolean (leaf-shape-fn true)))
    (is (= :symbol (leaf-shape-fn 'x)))
    (is (= :keyword (leaf-shape-fn :x)))
    (is (= :other (leaf-shape-fn (reduced :x))))
    (is (= #{[[:elem] [:leaf 1]] [[:elem] [:leaf 2]] [[:coll]]}
           (path-descriptor [1 2] identity)))
    (is (= #{[[:elem] [:leaf :number]] [[:coll]]}
           (path-descriptor [1 2] leaf-shape-fn)))
    (is (= #{[[:key :b] [:elem] [:leaf :number]]
             [[:key :a] [:coll]]
             [[:key :a] [:elem] [:leaf :number]]
             [[:map]]
             [[:key :b] [:coll]]}
           (path-descriptor {:a [1 2] :b [3]} leaf-shape-fn)))
    (is (= #{[[:key :a] [:coll]]
             [[:key :b] [:elem] [:leaf 3]]
             [[:key :a] [:elem] [:leaf 1]]
             [[:key :a] [:elem] [:leaf 2]]
             [[:map]]
             [[:key :b] [:coll]]}
           (path-descriptor {:a [1 2] :b [3]} identity)))
    (is (= #{[[:key :a] [:elem] [:leaf :number]]
             [[:key :a] [:coll]]
             [[:key :b] [:coll]]
             [[:map]]
             [[:key :b] [:elem] [:leaf :number]]}
           (into #{}
                 (map shape-path-from-value-path)
                 #{[[:key :a] [:coll]]
                   [[:key :b] [:elem] [:leaf 3]]
                   [[:key :a] [:elem] [:leaf 1]]
                   [[:key :a] [:elem] [:leaf 2]]
                   [[:map]]
                   [[:key :b] [:coll]]})))
    (let [paths (path-descriptor [1 2 3] identity)
          repr (sample-representative-value-paths paths)]
      (is (= #{[[:elem] [:leaf :number]] [[:coll]]}
             (into #{}
                   (map shape-path-from-value-path)
                   repr)))
      (is (set/subset? repr paths)))))

(deftest diff-checks-test
  (testing "API test utilities"
    (is (= {}
           (generate-sequential-diff-checks {})))
    (is (= {1 {:includes #{}, :excludes #{}}}
           (generate-sequential-diff-checks {1 #{:a :b}})))
    (is (= {1 {:includes #{:a}, :excludes #{:c}},
            2 {:includes #{:c}, :excludes #{:a}}}
           (generate-sequential-diff-checks {1 #{:a :b}
                                             2 #{:b :c}})))
    (is (= {1 {:includes #{:a}, :excludes #{:c}},
            2 {:includes #{:c}, :excludes #{:a}}}
           (generate-sequential-diff-checks {1 {:desc #{:a :b}}
                                             2 {:desc #{:b :c}}}
                                            :desc)))
    (is (= {1 {:includes #{:a}, :excludes #{:c}},
            2 {:includes #{:c :b}, :excludes #{:d :a}},
            3 {:includes #{:d}, :excludes #{:b}}}
           (generate-sequential-diff-checks {1 #{:a :b}
                                             2 #{:b :c}
                                             3 #{:c :d}})))))

(deftest check-test
  (testing "API test utilities"
    (is (= {1
            {:status 200,
             :varying true,
             :checks {:path-structure #{[[:elem] [:leaf :number]]}}},
            2
            {:status 200,
             :varying true,
             :checks {:path-structure #{[[:elem] [:leaf :string]]}}}}
           (checks-from-version-response-descriptors
            {1 {:status 200
                :varying true
                :descriptor #{[[:elem] [:leaf :number]]}}
             2 {:status 200
                :varying true
                :descriptor #{[[:elem] [:leaf :string]]}}})))
    (is (= {1
            {:status 200,
             :varying false,
             :checks
             {:path-frequencies {[[:elem] [:leaf :number]] 1}
              :includes #{[[:elem] [:leaf 9]]},
              :excludes #{[[:elem] [:leaf 10]]}}},
            2
            {:status 200,
             :varying false,
             :checks
             {:path-frequencies {[[:elem] [:leaf :number]] 1}
              :includes #{[[:elem] [:leaf 10]]},
              :excludes #{[[:elem] [:leaf 9]]}}}}
           (checks-from-version-response-descriptors
            {1 {:status 200
                :varying false
                :descriptor #{[[:elem] [:leaf 9]]}}
             2 {:status 200
                :varying false
                :descriptor #{[[:elem] [:leaf 10]]}}})))))

(deftest recursively-sort-collections-test
  (testing "API test utilities"
    (let [data {:a #{9 8 7 6}
                :b {9 :a
                    8 :b
                    7 :c}}
          result (recursively-sort-collections data)]
      (is (= result data))
      (is (-> result
              :a
              vec
              (= [6 7 8 9])))
      (is (-> result
              :a
              sorted?))
      (is (-> result
              :b
              vec
              (= [[7 :c] [8 :b] [9 :a]])))
      (is (-> result
              :b
              sorted?)))))

(deftest repo-and-sample-test
  (testing "API test utilities"
    (let [repo default-sample-repo
          id (first (list-sample-ids repo))
          sample (load-request-data repo id)
          samples (sample-collection repo #{id})]
      (is (= 1 (count samples)))
      (is (= [sample] (seq samples)))
      (is (sample-repo? repo))
      (is (string? id))
      (is (map? sample)))))

(deftest indexed-file-repo-test
  (testing "API test utilities"
    (let [dir (fs/create-temp-dir)
          r (indexed-file-repo dir "version")]
      (is (empty? (indexed-file-repo-list r)))
      (indexed-file-repo-write r 1 [:a 1 {:b 2}])
      (indexed-file-repo-write r 2 "two")
      (indexed-file-repo-write r 3 "three")
      (is (= [:a 1 {:b 2}] (indexed-file-repo-read r 1)))
      (is (= [1 2 3] (indexed-file-repo-list r)))
      (is (= "three" (indexed-file-repo-read r 3)))
      (is (= [:a 1 {:b 2}] (indexed-file-repo-read r 1)))
      (indexed-file-repo-write r 1 "one")
      (is (= "one" (indexed-file-repo-read r 1)))
      (indexed-file-repo-delete r 2)
      (is (= [1 3] (indexed-file-repo-list r)))
      (is (= "three" (lookup-latest-until r 9)))
      (is (= "three" (lookup-latest-until r 3)))
      (is (= "one" (lookup-latest-until r 2)))
      (is (nil? (lookup-latest-until r 0))))))

(deftest generate-overall-diff-checks-test
  (testing "API test utilities"
    (is (= {1 {:includes #{:a}, :excludes #{:c}},
            2 {:includes #{}, :excludes #{:c :a}},
            3 {:includes #{:c}, :excludes #{:a}}}
           (generate-overall-diff-checks {1 {:d #{:a :b}}
                                          2 {:d #{:b}}
                                          3 {:d #{:b :c}}}
                                         :d)))))

(deftest parent-collection-path-test
  (is (nil? (parent-collection-path [[:map]])))
  (is (nil? (parent-collection-path [[:coll]])))
  (is (= [[:key :a] [:elem] [:map]]
         (parent-collection-path [[:key :a] [:elem] [:key :b]])))
  (is (= [[:map]] (parent-collection-path [[:key :a]])))
  (is (= [[:coll]] (parent-collection-path [[:elem]])))
  (is (nil? (parent-collection-path []))))

(deftest parent-collection-path-in-set-test
  (is (parent-collection-path-in-set?
       #{[[:key :a] [:map]]}
       [[:key :a] [:key :b] [:key :c] [:leaf 9]]))
  (is (not (parent-collection-path-in-set?
            #{[[:key :a] [:key :y] [:map]]}
            [[:key :a] [:key :b] [:key :c] [:leaf 9]])))
  (is (not (parent-collection-path-in-set?
            #{[[:key :a] [:map]]}
            [[:key :x] [:key :b] [:key :c] [:leaf 9]]))))

(deftest simplify-excludes-test
  (is (= #{[[:map]]}
         (simplify-excludes #{[[:map]]
                              [[:key :a] [:key :b] [:leaf :c]]})))
  (is (= #{[[:map]]
           [[:coll] [:elem] [:leaf "asdb"]]}
         (simplify-excludes #{[[:map]]
                              [[:coll] [:elem] [:leaf "asdb"]]
                              [[:key :a] [:key :b] [:leaf :c]]}))))

(deftest simplify-includes-test
  (is (= #{[[:key :a] [:key :b] [:leaf :c]]}
         (simplify-includes
          #{[[:map]] [[:key :a] [:key :b] [:leaf :c]]}))))

(deftest dedupe-by-key-test
  (is (= [1 2 17 20]
         (into [] (dedupe-by-key odd?) [1 3 5 2 4 8 10 17 20]))))

(deftest drop-unchanged-subsequent-versions-test
  (is (= {1 :a, 4 :b, 6 :c}
         (drop-unchanged-subsequent-versions
          {1 :a
           2 :a
           3 :a
           4 :b
           5 :b
           6 :c
           7 :c
           8 :c
           9 :c}))))

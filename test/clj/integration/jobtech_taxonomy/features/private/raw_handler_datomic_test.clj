(ns integration.jobtech-taxonomy.features.private.raw-handler-datomic-test
  (:require [clojure.edn :as edn]
            [clojure.test :refer [deftest is testing] :as test]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.api.routes.raw-handler :as raw-handler]
            [ring.mock.request :as mock :refer [header request]]
            [ring.util.http-predicates :as http-pred]
            [taoensso.nippy :as nippy]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :as uih]
            [utils.test-file-helpers :as test-util])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(set! *warn-on-reflection* false)
(defn- log-level [fun-run]
  (log/with-merged-config
    {:min-level [[#{"io.methvin.watcher.*"} :error]
                 [#{"datahike.*"} :error]
                 [#{"jobtech-taxonomy.api.db.database-connection"} :error]
                 [#{"mapping.mappings.config"} :error]
                 [#{"*"} :error]]}
    (fun-run)))

(test/use-fixtures :each log-level)

(deftest test-read-raw
  (testing "GET /v1/taxonomy/private/raw/datoms"
    (let [cfg udf/cfg-datomic]
      ((udf/database-minimal cfg)
       #(doseq [[format unpack-fn] [["edn" uih/parse-edn]
                                    ["nippy" nippy/thaw]]]
          (let [services-endpoint (handler/app cfg)
                response (-> (request :get
                                      "/v1/taxonomy/private/raw/datoms"
                                      {:format format})
                             (header :api-key 222)
                             services-endpoint)
                result (unpack-fn (:body response))
                attrib-freqs (frequencies (map second result))]
            (is (http-pred/ok? response))
            (is (= 678 (count result)))
            (is (= 29 (:concept/preferred-label attrib-freqs)))))))))

(deftest test-private-concept-types
  (testing "get /v1/taxonomy/private/raw/datomic-local-file-db running datomic-local in memory returns 400
            and explanation of current config"
    (let [minimal-cfg {:backend {:type :datomic
                                 :cfg {:db-name "my-test-db"
                                       :server-type :datomic-local
                                       :system "my-system"
                                       :storage-dir :mem}
                                 :init-db {:wanderung/type :nippy-jar
                                           :filename "taxonomy.nippy"}}
                       :jobtech-taxonomy-api {:auth-tokens {:111 :user, :222 :admin},
                                              :user-ids {:222 "admin-1"}}}
          response (-> (request :get "/v1/taxonomy/private/raw/datomic-local-file-db")
                       (header :api-key 222)
                       ((handler/app minimal-cfg)))]
      (is (http-pred/method-not-allowed? response))
      (is (= {:taxonomy/error "db backend not running file based datomic-local. Current config is: :datomic"}
             (uih/parse-json (:body response))))))

  (testing "get /v1/taxonomy/private/raw/datomic-local-file-db | unzipped files same as simulated local file db directory"
    (let [;first create directory that simulates the datomic local file db.
          mocked-datomic-local-dir (-> (Files/createTempDirectory "Test" (into-array FileAttribute []))
                                       .toFile)
          _ (test-util/create-dummy-files mocked-datomic-local-dir)
          actual-files (test-util/relativize-path-with-byte-count mocked-datomic-local-dir)
          minimal-cfg {:backend
                       {:type :datomic
                        :cfg {:db-name "hylozoist",
                              :server-type :datomic-local,
                              :system "testing",
                              :storage-dir (str mocked-datomic-local-dir)}},
                       :jobtech-taxonomy-api {:auth-tokens {:111 :user, :222 :admin}
                                              :user-ids {:222 "admin-1"}}}
                                        ; get zip file from api and check if unzipped content is the same as simulated local file db directory 
          response ((handler/app minimal-cfg) (-> (request :get "/v1/taxonomy/private/raw/datomic-local-file-db") (header :api-key 222)))
          zip-file (:body response)
          unzipped-files (test-util/get-file-content zip-file)]
      (is (= actual-files
             unzipped-files))
      (is (http-pred/ok? response))
      (test-util/force-delete-directory mocked-datomic-local-dir))))

(deftest raw-zipped-handler-test
  (testing "raw-zipped-handler"
    (let [mocked-datomic-local-dir (-> (Files/createTempDirectory "Test" (into-array FileAttribute []))
                                       .toFile)
          _ (test-util/create-dummy-files mocked-datomic-local-dir)
          actual-files (test-util/relativize-path-with-byte-count mocked-datomic-local-dir)
          minimal-cfg {:backend
                       {:type :datomic
                        :cfg {:db-name "hylozoist",
                              :server-type :datomic-local,
                              :system "testing",
                              :storage-dir (str mocked-datomic-local-dir)}},
                       :jobtech-taxonomy-api {:auth-tokens {:111 :user, :222 :admin}
                                              :user-ids {:222 "admin-1"}}}
          response (-> (request :get "/v1/taxonomy/private/raw/datomic-local-file-db")
                       (header :api-key 222)
                       ((handler/app minimal-cfg)))
          zip-file (:body response)
          unzipped-files (test-util/get-file-content zip-file)]

      (testing "Response is OK"
        (is (http-pred/ok? response)))

      (testing "Unzipped files are the same as actual files"
        (is (= actual-files unzipped-files)))

      (testing "Response headers"
        (is (= "application/zip" (get-in response [:headers "Content-Type"])))
        (is (= (format "attachment; filename=%s.zip" (config/->backend-name minimal-cfg)) (get-in response [:headers "Content-Disposition"]))))

      (testing "Data written during zipping"
        (let [mock-zip-with-write-check (fn [_] (throw (ex-info "Data was written during zipping" {:type :data-written-during-zipping})))
              response ((#'raw-handler/datomic-raw-zipped-handler minimal-cfg mock-zip-with-write-check) nil)]
          (is (http-pred/conflict? response))
          (is (= {:taxonomy/error "Data was written to the database during the zipping process"} (:body response)))))

      (testing "Unexpected error during zipping"
        (let [mock-zip-with-write-check (fn [_] (throw (ex-info "Data was written during zipping" {:type :gorel})))
              response ((#'raw-handler/datomic-raw-zipped-handler minimal-cfg mock-zip-with-write-check) nil)]
          (is (http-pred/internal-server-error? response))
          (is (= {:taxonomy/error "An unexpected error occurred during the zipping process"} (:body response)))))

      (testing "Exception handling"
        (let [response ((handler/app minimal-cfg) (-> (request :get "/v1/taxonomy/private/raw/nonexistent-dir") (header :api-key 222)))]
          (is (http-pred/not-found? response))))

      (test-util/force-delete-directory mocked-datomic-local-dir))))



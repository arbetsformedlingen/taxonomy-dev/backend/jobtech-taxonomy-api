#_(ns ^:integration-private-concepts-tests integration.jobtech-taxonomy.features.private.basic-concept-test
    (:require [clojure.test :as test :refer [deftest is testing]]
              [java-time.api :as jt]
              [jobtech-taxonomy.api.db.concept-types :as concept-types]
              [jobtech-taxonomy.api.db.concepts :as concepts]
              [jobtech-taxonomy.api.db.daynotes :as daynotes]
              [jobtech-taxonomy.api.db.versions :as versions]
              [jobtech-taxonomy.common.taxonomy :as taxonomy]
              [taoensso.timbre :as log]
              [utils.db-fixtures :as udf]))

#_(test/use-fixtures :each
    (udf/database-with-schema udf/cfg)
    #_(fn [fun-run]
        (concept-types/assert-type udf/cfg {:concept-type "skill"})
        (fun-run)))

#_(deftest ^:integration-concept-private-test-0 concepts-private-test-0
    (testing "test assert concept"
;    (log/error udf/cfg)
      (is (:success (concept-types/assert-type udf/cfg {:concept-type "skill"})))
      (let [label (gensym "potatisodling-")
            create-result (concepts/assert-concept
                           udf/cfg "editor"
                           {:type "skill"
                            :definition label
                            :preferred-label label
                            :alternative-labels "a | b | c "
                            :hidden-labels " a|"})

;          id (:concept/id (:concept body))
;          _created-concept (first (concepts/find-concepts udf/cfg {:id id :version :next}))
            #_(concepts/accumulate-concept
               udf/cfg "updating-editor"
               {:id id
                :hidden-labels "new-hidden-label-1"
                :alternative-labels "new-alternative-label-1|new-alternative-label-2"})
            #_#_updated-concept (first (concepts/find-concepts udf/cfg {:id id :version :next}))]
        (is (= [] create-result))
        #_(is (= ["a" "b" "c" "new-alternative-label-1" "new-alternative-label-2"]
                 (:concept/alternative-labels updated-concept)))
        #_(is (= ["a" "new-hidden-label-1"]
                 (:concept/hidden-labels updated-concept))))))

#_(deftest ^:integration-private-concept-changes concepts-private-test-1
    (testing "test /v1/taxonomy/private/concept/changes"
      (let [_ (versions/create-new-version udf/cfg 0 (jt/instant))
            [status1 body1]
            (concepts/assert-concept
             udf/cfg "editor"
             {:type "skill"
              :definition "skulptör"
              :preferred-label "skulptör"
              :alternative-labels "alt1 | alt2"
              :hidden-labels "a1 | a2"})
            createdConcept (-> body1
                               :concept
                               taxonomy/map->nsmap
                               (update :taxonomy/alternative-labels set)
                               (update :taxonomy/hidden-labels set))
            _nv (versions/create-new-version udf/cfg 1 (jt/instant))
            [status2 body2]
            (daynotes/get-concept-changes-with-pagination-private
             udf/cfg 0 nil nil nil)
            newConcept (-> body2
                           first
                           :taxonomy/new-concept
                           (update :taxonomy/alternative-labels set)
                           (update :taxonomy/hidden-labels set))
            latestConcept (-> body2
                              first
                              :taxonomy/latest-version-of-concept
                              (update :taxonomy/alternative-labels set)
                              (update :taxonomy/hidden-labels set))
            [status3 body3]
            (daynotes/get-concept-changes-with-pagination-private
             udf/cfg 1 nil nil nil)]
        (is (= 200 status1))
        (is (string? (get-in body1 [:concept :concept/id])))
        (is (some? (:time body1)))
        (is (= {:concept/alternative-labels ["alt1" "alt2"],
                :concept/definition "skulptör",
                :concept/hidden-labels ["a1" "a2"],
                :concept/preferred-label "skulptör"
                :concept/type "skill"}
               (dissoc (:concept body1) :time :concept/id)))
        (is (= 200 status2))
        (is (= 1 (count body2)))
        (is (= "CREATED" (:taxonomy/event-type (first body2))))
        (is (= 200 status3))
        (is (= [] body3))
        (is (= createdConcept newConcept))
        (is (= newConcept latestConcept)))))

#_(deftest ^:integration-private-concept-changes concepts-private-test-2
    (testing "test /v1/taxonomy/private/concept/changes"
      (let [_ (versions/create-new-version udf/cfg 0 (jt/instant))
            [status1 body1]
            (concepts/assert-concept
             udf/cfg "editor"
             {:type "skill"
              :definition "skulptör"
              :preferred-label "skulptör"
              :alternative-labels "alt1 | alt2"
              :hidden-labels "a1 | a2"})
            createdConcept (-> body1
                               :concept
                               taxonomy/map->nsmap
                               (update :taxonomy/alternative-labels set)
                               (update :taxonomy/hidden-labels set))
            [status2 body2]
            (daynotes/get-concept-changes-with-pagination-private
             udf/cfg 0 nil nil nil)
            newConcept (-> body2
                           first
                           :taxonomy/new-concept
                           (update :taxonomy/alternative-labels set)
                           (update :taxonomy/hidden-labels set))
            latestConcept (-> body2
                              first
                              :taxonomy/latest-version-of-concept
                              (update :taxonomy/alternative-labels set)
                              (update :taxonomy/hidden-labels set))]
        (is (= createdConcept newConcept))
        (is (= newConcept latestConcept))
        (is (= 200 status1))
        (is (= 200 status2)))))

#_(deftest ^:integration-private-concept-changes concepts-private-test-3
    (testing "test one hidden and alt label, /v1/taxonomy/private/concept/changes"
      (let [_ (util/create-new-version 0)
            [status1 body1]
            (util/create-new-concept
             {:type "skill"
              :definition "skulptör"
              :preferred-label "skulptör"
              :alternative-labels "alt"
              :hidden-labels "hidden"})
            createdConcept (taxonomy/map->nsmap (:concept body1))
            [status2 body2]
            (util/send-request-to-json-service
             :get "/v1/taxonomy/private/concept/changes"
             :headers [(util/header-auth-admin)]
             :query-params [{:key "after-version", :val "0"}])
            newConcept (:taxonomy/new-concept (first body2))
            latestConcept (:taxonomy/latest-version-of-concept (first body2))]
        (is (= createdConcept newConcept))
        (is (= newConcept latestConcept))
        (is (= 200 status1))
        (is (= 200 status2)))))

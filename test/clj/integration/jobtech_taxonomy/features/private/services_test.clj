(ns integration.jobtech-taxonomy.features.private.services-test
  (:require [clojure.edn :as edn]
            [clojure.test :as test :refer [deftest is testing]]
            [ring.mock.request :refer [header request]]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :refer
             [json-body concept-exists? push-version *app*
              parse-edn parse-json]]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

;;Private GET

(deftest test-private-graph
  (testing "GET /v1/taxonomy/private/graph"
    (let [response (*app* (-> (request :get "/v1/taxonomy/private/graph" {:edge-relation-type "broader"
                                                                          :source-concept-type "esco-skill"
                                                                          :target-concept-type "skill"}) (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      #_(is (= {:taxonomy/graph {:taxonomy/edges #{{:taxonomy/relation-type "broader"
                                                    :taxonomy/source "4USc_cwY_UwA"
                                                    :taxonomy/target "D7zq_sxj_A2e"}}
                                 :taxonomy/nodes #{{:taxonomy/definition "Djursjukvård"
                                                    :taxonomy/id "D7zq_sxj_A2e"
                                                    :taxonomy/preferred-label "Djursjukvård"
                                                    :taxonomy/type "skill"}
                                                   {:taxonomy/definition "planera djurs fysiska rehabilitering"
                                                    :taxonomy/id "4USc_cwY_UwA"
                                                    :taxonomy/preferred-label "planera djurs fysiska rehabilitering"
                                                    :taxonomy/type "esco-skill"}}}} (json-body response))))))

(deftest test-private-relation-automatic-daynotes-get
  (testing "GET /v1/taxonomy/private/relation/automatic-daynotes/"
    (let [response (*app* (-> (request :get "/v1/taxonomy/private/relation/automatic-daynotes/") (header :api-key 222)))
          status (:status response)
          result (json-body response)]
      (is (= 200 status))
      (is (= 5 (count result)))
      (is (= [{:CREATED "broader" :user "@system" :version 0}
              {:CREATED "broader" :user "@system" :version 0}
              {:CREATED "broad-match" :user "@system" :version 0}
              {:CREATED "broader" :user "@system" :version 0}
              {:CREATED "broad-match" :user "editor-1" :version 1}]
             (map (fn [e] (assoc {}
                                 (keyword (:event-type e))
                                 (get-in e [:relation :relation-type])
                                 :user (:user-id e)
                                 :version (:version e))) result))))))

(deftest test-private-relation-changes
  (testing "GET /v1/taxonomy/private/relation/changes"
    (let [response (*app* (-> (request :get "/v1/taxonomy/private/relation/changes" {:after-version 17}) (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      #_(is (= {:taxonomy/comment "Enligt egen utredning."
                :taxonomy/event-type "CREATED"
                :taxonomy/relation {:taxonomy/relation-type "broad-match"
                                    :taxonomy/source {:taxonomy/id "fYjS_eZN_XMo"
                                                      :taxonomy/preferred-label "välja utrustning för flytt av föremål"
                                                      :taxonomy/type "esco-skill"}
                                    :taxonomy/target {:taxonomy/id "BcdB_x4v_jpy"
                                                      :taxonomy/preferred-label "Kontorsflyttning"
                                                      :taxonomy/type "skill"}}
                :taxonomy/substitutability-percentage 0
                :taxonomy/timestamp #inst "2022-11-08T15:16:49.716-00:00"
                #_#_:taxonomy/transaction-id 536887543
                :taxonomy/user-id "admin-3"
                :taxonomy/version 18} (first (json-body response)))))))

;;Private POST
(deftest test-private-concept-automatic-daynotes-post
  (testing "POST /v1/taxonomy/private/automatic-daynotes/"
    (let [response (*app* (->
                           (request :post "/v1/taxonomy/private/concept/automatic-daynotes/?id=MS1s_AYg_kZe&comment=comment")
                           (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "ok"}
             (json-body response))))))

(deftest test-private-relation
  (testing "POST /v1/taxonomy/private/relation"
    (let [response (*app* (-> (request :post "/v1/taxonomy/private/relation?relation-type=broad-match&concept-1=T5KD_ZPr_ysR&concept-2=MS1s_AYg_kZe") (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "Created relation."} (json-body response))))))

(deftest test-private-relation-automatic-daynotes-post
  (testing "POST /v1/taxonomy/private/relation/automatic-daynotes/"
    (let [response (*app* (-> (request :post "/v1/taxonomy/private/relation/automatic-daynotes/?concept-1=RdVK_4zC_kFV&concept-2=AJ4a_a1z_UzB&relation-type=broader&comment=comment") (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "ok"} (json-body response))))))

(defn has-been-replaced-by? [id rid]
  (let [response (-> (request :get "/v1/taxonomy/main/concepts"
                              {:id id
                               :include-deprecated true
                               :version "next"})
                     (header :api-key 222)
                     *app*)
        data (first (json-body response))]
    (when (and (= 200 (:status response)) data)
      (boolean (some
                #(= rid (:taxonomy/id %))
                (-> data :taxonomy/replaced-by))))))

(deftest test-private-replace-concept
  (testing "POST /v1/taxonomy/private/replace-concept"
    (is (false? (has-been-replaced-by? "rxmK_wwM_1NA" "tAJS_JNb_hDH")))
    (let [response (*app* (-> (request :post "/v1/taxonomy/private/replace-concept?old-concept-id=rxmK_wwM_1NA&new-concept-id=tAJS_JNb_hDH&comment=comment") (header :api-key 222)))
          status (:status response)]
      (is (true? (has-been-replaced-by? "rxmK_wwM_1NA" "tAJS_JNb_hDH")))
      (is (= 200 status))
      (is (= {:taxonomy/message "ok"} (json-body response))))))

(deftest test-private-unreplace-concept
  (testing "POST /v1/taxonomy/private/unreplace-concept"
    (is (true? (has-been-replaced-by? "CaRE_1nn_cSU" "TAKE_THE_BOB")))
    (let [response (*app* (-> (request :post "/v1/taxonomy/private/unreplace-concept?old-concept-id=CaRE_1nn_cSU&new-concept-id=TAKE_THE_BOB&comment=No") (header :api-key 222)))
          status (:status response)]
      (is (false? (has-been-replaced-by? "CaRE_1nn_cSU" "TAKE_THE_BOB")))
      (is (= 200 status))
      (is (= {:taxonomy/message "ok"} (json-body response))))))

(deftest test-private-versions
  (testing "GET /v1/taxonomy/main/versions"
    (let [version-response (*app* (request :get "/v1/taxonomy/main/versions"))
          version-body (json-body version-response)
          first-version (first version-body)
          version-next (inc (:taxonomy/version (last version-body)))]
      (is (= 200 (:status version-response)))
      (is (= {:taxonomy/timestamp "2001-01-01T00:00:00.000Z"
              :taxonomy/version 1}
             first-version))
      (testing "POST /v1/taxonomy/private/versions"
        (let [post-response (-> (request
                                 :post
                                 (str "/v1/taxonomy/private/versions?new-version-id="
                                      version-next))
                                (header :api-key 222)
                                *app*)
              updated-response (*app* (request :get "/v1/taxonomy/main/versions"))]
          (is (= 200 (:status post-response)))
          (is (= 200 (:status updated-response)))
          (is (= version-next (-> updated-response
                                  json-body
                                  last
                                  :taxonomy/version))))))))

;;Private PATCH 
(deftest test-private-accumulate-concept
  (testing "PATCH /v1/taxonomy/private/accumulate-concept"
    (let [response (*app*
                    (-> (request :patch "/v1/taxonomy/private/accumulate-concept?hidden-labels=h&preferred-label=p&id=MS1s_AYg_kZe"
                                 #_#_"/v1/taxonomy/private/accumulate-concept"
                                   {:hidden-labels "my-hidden-label"
                                    :preferred-label "my-preferred-label"
                                    :id "MS1s_AYg_kZe"})
                        (header :api-key 222)))
          status (:status response)
          body (json-body response)]
      (is (= 200 status))
      (is (= {:concept/definition "Caymanöarna"
              :concept/hidden-labels ["h"]
              :concept/id "MS1s_AYg_kZe"
              :concept/preferred-label "p"
              :concept/type "country"} (:concept body))))))

(deftest test-private-accumulate-relation
  (testing "POST /v1/taxonomy/private/relation"
    (let [response (*app*
                    (-> (request :post "/v1/taxonomy/private/relation?relation-type=related&concept-1=MS1s_AYg_kZe&concept-2=T5KD_ZPr_ysR"
                                 #_#_"/v1/taxonomy/private/relation"
                                   {:relation-type "related"
                                    :concept-1 "MS1s_AYg_kZe"
                                    :concept-2 "T5KD_ZPr_ysR"})
                        (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "Created relation."} (json-body response)))))
  (testing "PATCH /v1/taxonomy/private/accumulate-relation"
    (let [response (*app*
                    (-> (request :patch "/v1/taxonomy/private/accumulate-relation?concept-1=MS1s_AYg_kZe&concept-2=T5KD_ZPr_ysR&relation-type=related&comment=c&new-relation-type=close-match"
                                 #_#_"/v1/taxonomy/private/accumulate-relation"
                                   {:concept-1 "MS1s_AYg_kZe"
                                    :concept-2 "T5KD_ZPr_ysR"
                                    :relation-type "related"
                                    :comment "c"
                                    :new-relation-type "close-match"}) (header :api-key 222)))
          status (:status response)
          body (json-body response)]
      (is (= 200 status))
      (is (= {:relation/concept-1 ["concept/id" "MS1s_AYg_kZe"]
              :relation/concept-2 ["concept/id" "T5KD_ZPr_ysR"]
              :relation/id "MS1s_AYg_kZe:close-match:T5KD_ZPr_ysR"
              :relation/type "close-match"}
             (:relation body))))))

;;Private DELETE
(deftest test-private-delete-concept
  (testing "DELETE /v1/taxonomy/private/delete-concept"
    (is (concept-exists? "T5KD_ZPr_ysR" :current))
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-concept" {:id "T5KD_ZPr_ysR"}) (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "ok"} (json-body response)))
      (is (concept-exists? "T5KD_ZPr_ysR" :current))
      (is (push-version 8))
      (is (concept-exists? "T5KD_ZPr_ysR" :deprecated))

      ;; Is it OK to delete it again? Probably.
      (let [response2 (*app*
                       (-> (request :delete "/v1/taxonomy/private/delete-concept" {:id "T5KD_ZPr_ysR"})
                           (header :api-key 222)))]
        (is (= 200 (:status response2)))))
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-concept" {:id "MS1s_AYg_kZe"}) (header :api-key 99)))]
      (is (= 401 (:status response))))
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-concept" {}) (header :api-key 222)))]
      (is (= 400 (:status response))))
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-concept" {:id 333}) (header :api-key 222)))]
      (is (= 404 (:status response))))))

(deftest test-private-delete-relation
  (testing "POST /v1/taxonomy/private/relation"
    (let [response (*app* (-> (request :post "/v1/taxonomy/private/relation?relation-type=related&concept-1=T5KD_ZPr_ysR&concept-2=MS1s_AYg_kZe") (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "Created relation."} (json-body response)))))
  (testing "DELETE /v1/taxonomy/private/delete-relation"
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-relation" {:relation-type "related" :concept-1 "T5KD_ZPr_ysR" :concept-2 "MS1s_AYg_kZe"}) (header :api-key 222)))
          status (:status response)]
      (is (= 200 status))
      (is (= {:taxonomy/message "Retracted relation."} (json-body response)))))
  (testing "DELETE /v1/taxonomy/private/delete-relation again"
    (let [response (*app* (-> (request :delete "/v1/taxonomy/private/delete-relation" {:relation-type "related" :concept-1 "T5KD_ZPr_ysR" :concept-2 "MS1s_AYg_kZe"}) (header :api-key 222)))
          status (:status response)]
      (is (= 400 status)))))

(deftest test-read-raw
  (testing "GET /v1/taxonomy/private/raw/datoms in edn format"
    (let [response (-> (request :get "/v1/taxonomy/private/raw/datoms"
                                {:format "edn"})
                       (header :api-key 222)
                       *app*)
          status (:status response)
          result (-> response
                     :body
                     parse-edn)
          attrib-freqs (frequencies (map second result))]
      (is (= 200 status))
      (is (= 29 (:concept/preferred-label attrib-freqs)))
      (is (= 678 (count result)))))
  (testing "GET /v1/taxonomy/private/raw/datoms with content negotiated as edn"
    (let [response (-> (request :get "/v1/taxonomy/private/raw/datoms")
                       (header :api-key 222)
                       (header "Accept" "application/edn")
                       *app*)
          status (:status response)
          result (-> response
                     :body
                     parse-edn)
          attrib-freqs (frequencies (map second result))]
      (is (= 200 status))
      (is (= 29 (:concept/preferred-label attrib-freqs)))
      (is (= 678 (count result)))))
  (testing "GET /v1/taxonomy/private/raw/datoms with content negotiated as json"
    (let [response (-> (request :get "/v1/taxonomy/private/raw/datoms")
                       (header :api-key 222)
                       (header "Accept" "application/json")
                       *app*)
          status (:status response)
          result (-> response
                     :body
                     parse-json)
          attrib-freqs (frequencies (map second result))]
      (is (= 200 status))
      (is (= 29 (get attrib-freqs "concept/preferred-label")))
      (is (= 678 (count result))))))



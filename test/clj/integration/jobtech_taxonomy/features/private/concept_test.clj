(ns integration.jobtech-taxonomy.features.private.concept-test
  (:require [clojure.data.json :as json]
            [clojure.instant :as inst]
            [clojure.string :as str]
            [clojure.test :refer [deftest is testing] :as test]
            [jobtech-taxonomy.api.db.nano-id :refer [generate-new-id-with-underscore]]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :as mock :refer [header request]]
            [utils.db-fixtures :as udf]))

(def cfg udf/cfg)
(test/use-fixtures :each (udf/database-minimal cfg))

(def services-endpoint
  (handler/app cfg))

(defn body [response]
  (json/read-json
   (slurp (:body response))
   true))

(defn suggest-non-existing-concept-id []
  (loop []
    (let [id (generate-new-id-with-underscore)
          response (services-endpoint
                    (request :get "/v1/taxonomy/main/concepts"
                             {:id id}))]
      (if (empty? (body response))
        id
        (recur)))))

(deftest test-private-concept-types
  (testing "POST /v1/taxonomy/private/concept-types"
    (let [post-response (services-endpoint
                         (-> (request :post "/v1/taxonomy/private/concept-types?concept-type=q")
                             (header :api-key 222)))
          new-types (-> (request :get "/v1/taxonomy/main/concept/types?version=next")
                        (header :api-key 222)
                        services-endpoint
                        body
                        set)]
      (is (= 200 (:status post-response)))
      (is (= {:success true} (body post-response)))
      (is (new-types "q")))
    (doseq [[expected qstring] [[{:id "cfg"
                                  :label_en "Configuration"
                                  :label_sv "Konfiguration"}
                                 {"concept-type" "cfg"
                                  "label-en" "Configuration"
                                  "label-sv" "Konfiguration"}]
                                [{:id "dresscode---"
                                  :label_en "dresscode---"
                                  :label_sv "Klädkod"}
                                 {"concept-type" "dresscode---"
                                  "label-sv" "Kl%C3%A4dkod"}]
                                [{:id "hmc"
                                  :label_en "Hammock"
                                  :label_sv "hmc"}
                                 {"concept-type" "hmc"
                                  "label-en" "Hammock"}]]]
      (let [url (str "/v1/taxonomy/private/concept-types?"
                     (str/join "&" (map (fn [[k v]] (str k "=" v)) qstring)))
            post-response (-> (request :post url)
                              (header :api-key 222)
                              services-endpoint)

            ;; The only way to obtain label-en and label-sv from the API is
            ;; by using the GraphQL endpoint.
            new-types (-> (request :get "/v1/taxonomy/graphql?query=query%20Q%7Bconcept_types(version%3A%20%22next%22)%7Bid%20label_en%20label_sv%7D%7D")
                          (header :api-key 222)
                          services-endpoint
                          body
                          :data
                          :concept_types
                          set)]
        (is (= 200 (:status post-response)))
        (is (= {:success true} (body post-response)))
        (is (new-types expected))))))

(deftest test-private-concept-automatic-daynotes-get
  (testing "GET /v1/taxonomy/private/concept/automatic-daynotes/"
    (let [response (services-endpoint (-> (request :get "/v1/taxonomy/private/concept/automatic-daynotes/" {:id "MS1s_AYg_kZe"}) (header :api-key 222)))
          status (:status response)
          body (body response)]
      (is (= 200 status))
      (is (= 1 (count body)))
      (is (= {:event-type "CREATED"
              :latest-version-of-concept {:concept/definition "Caymanöarna"
                                          :concept/id "MS1s_AYg_kZe"
                                          :concept/preferred-label "Caymanöarna"
                                          :concept/type "country"}
              :new-concept {:concept/definition "Caymanöarna"
                            :concept/id "MS1s_AYg_kZe"
                            :concept/preferred-label "Caymanöarna"
                            :concept/type "country"
                            :concept.external-database.ams-taxonomy-67/id "36"
                            :concept.external-standard/iso-3166-1-alpha-2-2013 "KY"
                            :concept.external-standard/iso-3166-1-alpha-3-2013 "CYM"}
              :timestamp "2001-01-02T00:00:07.000Z"
              :user-id "@system"
              :version 2}
             (dissoc (first body) :transaction-id))))))

(deftest test-private-concept-changes
  (testing "GET /v1/taxonomy/private/concept/changes"
    (let [response (services-endpoint (-> (request :get "/v1/taxonomy/private/concept/changes" {:after-version 1}) (header :api-key 222)))
          status (:status response)
          hovslag (udf/get-from (body response) :taxonomy/version 2)]
      (is (= 200 status))
      (is (= {:taxonomy/comment "Slå ett slag för skodda hästar"
              :taxonomy/event-type "CREATED"
              :taxonomy/latest-version-of-concept {:taxonomy/definition "Hovslageri"
                                                   :taxonomy/id "gjd2_JSR_AeG"
                                                   :taxonomy/preferred-label "Hovslageri"
                                                   :taxonomy/type "skill"}
              :taxonomy/new-concept {:taxonomy/definition "Hovslageri"
                                     :taxonomy/id "gjd2_JSR_AeG"
                                     :taxonomy/preferred-label "Hovslageri"
                                     :taxonomy/type "skill"}
              :taxonomy/timestamp "2001-01-01T00:00:07.000Z"
              :taxonomy/user-id "admin-3"
              :taxonomy/version 2} hovslag)))))

(deftest test-private-concepts
  (testing "GET /v1/taxonomy/private/concepts"
    (let [response (services-endpoint (-> (request :get "/v1/taxonomy/private/concepts") (header :api-key 222)))
          status (:status response)
          body (body response)
          next-gen (udf/get-from body :taxonomy/id "NEXT_GEN_ONE")]
      (is (= 200 status))
      (is (= {:taxonomy/definition "Next gen. concept"
              :taxonomy/id "NEXT_GEN_ONE"
              :taxonomy/preferred-label "Next gen. concept"
              :taxonomy/type "skill"} next-gen)))))

(deftest test-private-concept
  (testing "POST /v1/taxonomy/private/concept"
    (let [type (str "type-" (random-uuid))
          concept-definition (str "definition-" (random-uuid))
          preferred-label (str "preferred-label-" (random-uuid))
          request-uri (str "/v1/taxonomy/private/concept?type=" type "&definition=" concept-definition "&preferred-label=" preferred-label)
          type-response (services-endpoint
                         (-> (request :post
                                      (str "/v1/taxonomy/private/concept-types?concept-type=" type))
                             (header :api-key 222)))
          concept-request (-> (request :post request-uri) (header :api-key 222))
          response (services-endpoint concept-request)
          status (:status response)
          body (body response)
          concept (:concept body)
          time (:time body)

          second-response (-> (request :post request-uri)
                              (header :api-key 222)
                              services-endpoint)]
      (is (= 200 (:status type-response)))
      (is (= 200 status))
      (is (= {:concept/definition concept-definition
              :concept/preferred-label preferred-label
              :concept/type type}
             (dissoc concept :concept/id)))
      (is (>= (count (:concept/id concept)) 12))
      (is (inst/validated time))
      (is (= 409 (:status second-response))))))

(deftest test-private-remove-alternative-label
  (testing "POST /v1/taxonomy/private/concept"
    (let [type (str "type-" (random-uuid))
          concept-definition (str "definition-" (random-uuid))
          preferred-label (str "preferred-label-" (random-uuid))
          alternative-label (str "alternative-labels-" (random-uuid))
          hidden-label (str "hidden-labels-" (random-uuid))
          type-response (services-endpoint
                         (-> (request :post
                                      (str "/v1/taxonomy/private/concept-types?concept-type=" type))
                             (header :api-key 222)))
          version-response (services-endpoint
                            (-> (request :post
                                         (str "/v1/taxonomy/private/concept?type=" type
                                              "&definition=" concept-definition
                                              "&preferred-label=" preferred-label
                                              "&alternative-labels=" alternative-label
                                              "&hidden-labels=" hidden-label))
                                (header :api-key 222)))
          version-body (body version-response)
          concept-id (:concept/id (:concept version-body))]
      (is (= 200 (:status type-response)))
      (is (= 200 (:status version-response)))
      (is (= {:concept/id concept-id
              :concept/type type
              :concept/definition concept-definition
              :concept/preferred-label preferred-label
              :concept/alternative-labels [alternative-label]
              :concept/hidden-labels [hidden-label]}
             (:concept version-body)))
      (testing "GET /v1/taxonomy/main/concepts"
        (let [response (services-endpoint
                        (-> (request :get "/v1/taxonomy/main/concepts"
                                     {:id concept-id
                                      :version "next"})
                            (header :api-key 222)))
              status (:status response)]
          (is (= 200 status))
          (is (= {:concept/id concept-id
                  :concept/type type
                  :concept/definition concept-definition
                  :concept/preferred-label preferred-label
                  :concept/alternative-labels [alternative-label]
                  :concept/hidden-labels [hidden-label]}
                 (:concept version-body)))))
      (testing "PATCH /v1/taxonomy/private/remove-alternative-label"
        (let [response (services-endpoint
                        (-> (request :patch
                                     (str "/v1/taxonomy/private/remove-alternative-label?id=" concept-id
                                          "&alternative-label=" alternative-label))
                            (header :api-key 222)))
              status (:status response)]
          (is (= 200 status))
          (is (= {:concept
                  {:concept/id concept-id
                   :concept/type type
                   :concept/definition concept-definition
                   :concept/preferred-label preferred-label
                   :concept/hidden-labels [hidden-label]}}
                 (body response)))))
      (testing "And again PATCH /v1/taxonomy/private/remove-alternative-label"
        (let [response (services-endpoint
                        (-> (request :patch
                                     (str "/v1/taxonomy/private/remove-alternative-label?id=" concept-id
                                          "&alternative-label=" alternative-label))
                            (header :api-key 222)))
              status (:status response)]
          (is (= 200 status))
          (is (= {:concept
                  {:concept/id concept-id
                   :concept/type type
                   :concept/definition concept-definition
                   :concept/preferred-label preferred-label
                   :concept/hidden-labels [hidden-label]}}
                 (body response)))))
      (testing "PATCH /v1/taxonomy/private/remove-alternative-label for concept that does not exist"
        (let [response (services-endpoint
                        (-> (request :patch
                                     (str "/v1/taxonomy/private/remove-alternative-label?id="
                                          (suggest-non-existing-concept-id)
                                          "&alternative-label=" alternative-label))
                            (header :api-key 222)))
              status (:status response)]
          (is (= 409 status)))))))

(deftest test-private-remove-hidden-label
  (testing "POST /v1/taxonomy/private/concept"
    (let [type (str "type-" (random-uuid))

          concept-definition (str "definition-" (random-uuid))
          preferred-label (str "preferred-label-" (random-uuid))
          alternative-label (str "alternative-labels-" (random-uuid))
          hidden-label (str "hidden-labels-" (random-uuid))
          type-response (services-endpoint
                         (-> (request :post
                                      (str "/v1/taxonomy/private/concept-types?concept-type=" type))
                             (header :api-key 222)))
          version-response (services-endpoint
                            (-> (request :post
                                         (str "/v1/taxonomy/private/concept?type=" type
                                              "&definition=" concept-definition
                                              "&preferred-label=" preferred-label
                                              "&alternative-labels=" alternative-label
                                              "&hidden-labels=" hidden-label))
                                (header :api-key 222)))
          version-body (body version-response)
          concept-id (:concept/id (:concept version-body))]
      (is (= 200 (:status type-response)))
      (is (= 200 (:status version-response)))
      (is (= {:concept/id concept-id
              :concept/type type
              :concept/definition concept-definition
              :concept/preferred-label preferred-label
              :concept/alternative-labels [alternative-label]
              :concept/hidden-labels [hidden-label]}
             (:concept version-body)))
      (testing "GET /v1/taxonomy/main/concepts"
        (let [response (services-endpoint
                        (-> (request :get "/v1/taxonomy/main/concepts"
                                     {:id concept-id
                                      :version "next"})
                            (header :api-key 222)))
              status (:status response)]
          (is (= 200 status))
          (is (= {:concept/id concept-id
                  :concept/type type
                  :concept/definition concept-definition
                  :concept/preferred-label preferred-label
                  :concept/alternative-labels [alternative-label]
                  :concept/hidden-labels [hidden-label]}
                 (:concept version-body)))))
      (testing "PATCH /v1/taxonomy/private/remove-hidden-label"
        (let [response (services-endpoint
                        (-> (request :patch
                                     (str "/v1/taxonomy/private/remove-hidden-label?id=" concept-id
                                          "&hidden-label=" hidden-label))
                            (header :api-key 222)))
              status (:status response)]
          (is (= 200 status))
          (is (= {:concept
                  {:concept/id concept-id
                   :concept/type type
                   :concept/definition concept-definition
                   :concept/preferred-label preferred-label
                   :concept/alternative-labels [alternative-label]}}
                 (body response)))))
      (testing "PATCH /v1/taxonomy/private/remove-hidden-label on concept that does not exist"
        (let [response (services-endpoint
                        (-> (request :patch
                                     (str "/v1/taxonomy/private/remove-hidden-label?id="
                                          (suggest-non-existing-concept-id)
                                          "&hidden-label=" hidden-label))
                            (header :api-key 222)))
              status (:status response)]
          (is (= 409 status)))))))

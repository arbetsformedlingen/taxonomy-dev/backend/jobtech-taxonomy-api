(ns integration.jobtech-taxonomy.features.private.raw-handler-datahike-test
  (:require [clojure.edn :as edn]
            [clojure.test :refer [deftest is testing] :as test]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :refer [header request]]
            [ring.util.http-predicates :as http-pred]
            [taoensso.nippy :as nippy]
            [utils.db-fixtures :as udf]
            [utils.integration-helpers :as uih]))

(def cfg udf/cfg-datahike)

(test/use-fixtures :each (udf/database-minimal cfg))

(def services-endpoint (handler/app cfg))

(deftest test-read-raw
  (testing "GET /v1/taxonomy/private/raw/datoms"
    (doseq [[format unpack-fn] [["edn" uih/parse-edn]
                                ["nippy" nippy/thaw]]]
      (let [response (-> (request
                          :get
                          "/v1/taxonomy/private/raw/datoms"
                          {:format format})
                         (header :api-key 222)
                         services-endpoint)
            result (unpack-fn (:body response))]
        (is (http-pred/ok? response))
        (is (= 678 (count result)))
        (is (= [[536870913 :db/txInstant #inst "1998-01-06T23:00:07.000-00:00" 536870913 true]
                [39 :db/valueType :db.type/string 536870913 true]
                [39 :db/doc "A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations." 536870913 true]]
               (take 3 result)))))
    (let [response (-> (request
                        :get
                        "/v1/taxonomy/private/raw/datoms"
                        {:format "invalid-format" :version 0})
                       (header :api-key 222)
                       services-endpoint)]
      (is (http-pred/method-not-allowed? response)))))

(deftest request-datomic-test
  (let [response (-> (request :get "/v1/taxonomy/private/raw/datomic-local-file-db")
                     (header :api-key 222)
                     services-endpoint)]
    (is (http-pred/method-not-allowed? response))
    (is (= {:taxonomy/error "Incompatible database type: datahike"}
           (uih/parse-json (:body response))))))

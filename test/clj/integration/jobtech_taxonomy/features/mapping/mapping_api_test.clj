(ns ^:mappings integration.jobtech-taxonomy.features.mapping.mapping-api-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.setup :as mapping]
            [ring.mock.request :as mock]
            [ring.util.http-predicates :as http-predicates]
            [taoensso.timbre :as log]
            [utils.integration-helpers :as uih]))

(def ^:dynamic cfg* nil)

(defn update-config
  "Make the storage-dir absolute relative to the project root"
  [cfg]
  {:pre [(config/valid-config? cfg)]
   :post [(config/valid-config? %)]}
  (let [storage-dir (config/->backend-local-storage-dir cfg)]
    (or (and (iou/is-relative-path? storage-dir)
             (config/update-storage-dir-path cfg storage-dir))
        cfg)))

(defn db-fix [fun-run]
  (binding [cfg* (-> (config/get-config [])
                     update-config
                     dc/init-db
                     mapping/init)]
    (fun-run)))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  db-fix)

(deftest mapping-presence-test
  (let [services-endpoint (handler/app cfg*)]
    (testing "Verify that /v1/taxonomy/mappings has 5 mappings present"
      (let [request (mock/request :get "/v1/taxonomy/mappings")
            response (services-endpoint request)]
        (is (http-predicates/ok? response))
        (is (= #{{:mappings 2 :organisation "ESCO"}
                 {:mappings 1 :organisation "Eurostat"}
                 {:mappings 2 :organisation "UNESCO"}}
               (into #{} (uih/json-body response))))))
    (testing "Verify that ESCO/Eures/occupation-name/esco-occupation/1.0.9/EbSk_Hv8_qog only has one match"
      (let [request (mock/request :get "/v1/taxonomy/mappings/ESCO/Eures/occupation-name/esco-occupation/1.0.9/EbSk_Hv8_qog")
            response (services-endpoint request)]
        (is (http-predicates/ok? response))
        (is (= [{:found [{:mapped-id "http://data.europa.eu/esco/occupation/377865f0-c327-4599-8c7e-d428c6588edf"
                          :source {:id "EbSk_Hv8_qog" :preferred-label "Inköpschef" :type "occupation-name"}
                          :target {:esco-uri "http://data.europa.eu/esco/occupation/377865f0-c327-4599-8c7e-d428c6588edf"
                                   :preferred-label "inköpschef"
                                   :type "esco-occupation"
                                   :concept/id "YoG7_kES_zzQ"}}]
                 :mapping {:description "Mappings between occupation-name concepts and esco-occupation concepts."
                           :family "Eures"
                           :name "occupation-name"
                           :organisation "ESCO"
                           :taxonomy-version "16"
                           :type "esco-occupation"
                           :version "1.0.9"}}]
               (uih/json-body response)))))
    (testing "Verify that ESCO/Eures/skill/esco-skill/1.0.9/rdtv_EuD_NfB has two matches"
      (let [request (mock/request :get "/v1/taxonomy/mappings/ESCO/Eures/skill/esco-skill/1.0.9/rdtv_EuD_NfB")
            response (services-endpoint request)]
        (is (http-predicates/ok? response))
        (is (= [{:found [{:mapped-id "http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc"
                          :source {:id "rdtv_EuD_NfB"
                                   :preferred-label "Allmänna svetskunskaper"
                                   :type "skill"}
                          :target {:esco-uri "http://data.europa.eu/esco/skill/2ee57de4-bffd-4194-b009-0060edec77cc"
                                   :preferred-label "använda svetsutrustning"
                                   :type "esco-skill"
                                   :concept/id "yMjH_oGb_2Ri"}}
                         {:mapped-id "http://data.europa.eu/esco/skill/530cb489-d900-46ab-9350-4d50ccd49eea"
                          :source {:id "rdtv_EuD_NfB"
                                   :preferred-label "Allmänna svetskunskaper"
                                   :type "skill"}
                          :target {:esco-uri "http://data.europa.eu/esco/skill/530cb489-d900-46ab-9350-4d50ccd49eea"
                                   :preferred-label "svetstekniker"
                                   :type "esco-skill"
                                   :concept/id "17hV_8J1_8ke"}}]
                 :mapping {:description "Mappings between skill concepts and esco-skill concepts."
                           :family "Eures"
                           :name "skill"
                           :organisation "ESCO"
                           :taxonomy-version "21"
                           :type "esco-skill"
                           :version "1.0.9"}}]
               (uih/json-body response)))))
    (testing "Verify that UNESCO/Eures/sun/isced/2013/be55_L6H_rYK,T2aZ_5dP_myb resolves to \"084\" and \"0311\""
      (let [request (mock/request :get "/v1/taxonomy/mappings/UNESCO/Eures/sun/isced/2013/be55_L6H_rYK,T2aZ_5dP_myb")
            response (services-endpoint request)]
        (is (http-predicates/ok? response))
        (is (= [{:found
                 [{:source
                   {:id "T2aZ_5dP_myb"
                    :preferred-label "Nationalekonomi och ekonomisk historia"
                    :type "sun-education-field-3"
                    :sun-2000-field "314"}
                   :target
                   {:mapping.external/id "0311"
                    :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                      ["label" "Economics"]]}
                   :mapped-id "0311"}
                  {:source
                   {:id "be55_L6H_rYK"
                    :preferred-label "Djursjukvård"
                    :type "sun-education-field-2"
                    :sun-2000-field "64"}
                   :target
                   {:mapping.external/id "084"
                    :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                      ["label" "Veterinary"]]}
                   :mapped-id "084"}]
                 :mapping {:taxonomy-version "1"
                           :version "2013"
                           :family "Eures"
                           :description "Mapping between SUN2000 fields and levels to their ISCED counterparts."
                           :organisation "UNESCO"
                           :type "isced"
                           :name "sun"}}]
               (uih/json-body response)))
        (testing "Verify that UNESCO/Eures/sun/isced/2013/be55_L6H_rYK,T2aZ_5dP_myb resolves to \"084\" and \"0311\""
          (let [request (mock/request :get "/v1/taxonomy/mappings/UNESCO/Eures/sun/isced/2013/be55_L6H_rYK,T2aZ_5dP_myb")
                response (services-endpoint request)]
            (is (http-predicates/ok? response))
            (is (= [{:found
                     [{:source
                       {:id "T2aZ_5dP_myb"
                        :preferred-label "Nationalekonomi och ekonomisk historia"
                        :type "sun-education-field-3"
                        :sun-2000-field "314"}
                       :target
                       {:mapping.external/id "0311"
                        :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                          ["label" "Economics"]]}
                       :mapped-id "0311"}
                      {:source
                       {:id "be55_L6H_rYK"
                        :preferred-label "Djursjukvård"
                        :type "sun-education-field-2"
                        :sun-2000-field "64"}
                       :target
                       {:mapping.external/id "084"
                        :mapping.external/key-value-data [["isced-type" "isced-field"]
                                                          ["label" "Veterinary"]]}
                       :mapped-id "084"}]
                     :mapping {:taxonomy-version "1"
                               :version "2013"
                               :family "Eures"
                               :description "Mapping between SUN2000 fields and levels to their ISCED counterparts."
                               :organisation "UNESCO"
                               :type "isced"
                               :name "sun"}}]
                   (uih/json-body response)))))))
    (testing "Verify that Eurostat/Eures/sni/nace/2007/49100 resolves to \"H49.1.0\""
      (let [request (mock/request :get "/v1/taxonomy/mappings/Eurostat/Eures/sni/nace/2007/49100")
            response (services-endpoint request)]
        (is (http-predicates/ok? response))
        (is (= [{:found
                 [{:source
                   {:id "ESy3_gyJ_fpu"
                    :preferred-label "Järnvägstransport, passagerartrafik"
                    :type "sni-level-5"
                    :sni-level-code-2007 "49100"}
                   :target
                   {:mapping.external/id "H49.1.0"
                    :mapping.external/key-value-data [["label" "Passenger rail transport, interurban"]]}
                   :mapped-id "H49.1.0"}]
                 :mapping {:taxonomy-version "22"
                           :version "2007"
                           :family "Eures"
                           :description "Mapping between SNI and NACE, where NACE codes are expressed according to EURES standard."
                           :organisation "Eurostat"
                           :type "nace"
                           :name "sni"}}]
               (uih/json-body response)))))))

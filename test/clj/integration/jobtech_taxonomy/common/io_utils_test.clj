(ns integration.jobtech-taxonomy.common.io-utils-test
  (:require [base.jobtech-taxonomy.api.generative-test :as gen]
            [clojure.java.io :as io]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.crypto-utils :as cu]
            [jobtech-taxonomy.common.io-utils :as iou]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.test-file-helpers :as test-util])
  (:import [java.nio.file Files Paths]
           [java.nio.file.attribute FileAttribute]))

(set! *warn-on-reflection* true)

(defn- log-level [fun-run]
  (log/with-merged-config
    {:min-level [[#{"io.methvin.watcher.*"} :error]
                 [#{"datahike.*"} :error]
                 [#{"jobtech-taxonomy.api.db.database-connection"} :error]
                 [#{"mapping.mappings.config"} :error]
                 [#{"*"} :error]]}
    (fun-run)))

(def ^:dynamic cfg* nil)

(test/use-fixtures :once
  log-level
  (fn [f] (let [db-path "resources/taxonomy.zip"
                zip-bytes (Files/readAllBytes (Paths/get db-path (into-array String [])))
                init-dir (-> (Files/createTempDirectory
                              "datomic-local-minimal"
                              (into-array FileAttribute []))
                             .toFile)
                init-dir-path (str (.getPath init-dir))
                _ (iou/unzip-byte-array zip-bytes init-dir-path)
                new-cfg (assoc udf/cfg :backend {:allow-create true
                                                 :allow-delete true
                                                 :type :datomic
                                                 :cfg {:db-name "jobtech-taxonomy-db"
                                                       :server-type :datomic-local
                                                       :system "jobtech-taxonomy"
                                                       :storage-dir init-dir-path}})
                ;; After unzipping the database we need to initialise the database
                ;; this is done by connecting to it and waiting for the connection
                ;; to be established. This is normally handeled in `dc/init-db`.
                _ (#'dc/can-connect? new-cfg)]
            (binding [cfg* new-cfg]
              (f))
            (test-util/force-delete-directory init-dir))))

(defn- write-data-controlled [stop? cfg]
  (while (not @stop?)
    (let [concepts (map (fn [_] (gen/clear-namespace (gen/generate-concept))) (repeat 2 0))]
      (doseq [c concepts]
        (concepts/assert-concept cfg (str (random-uuid)) c)))))

(deftest zip-test-multithread
  (testing "zip function multi threading safety."
    (let [stop? (atom false)
          write-fn (fn [] (write-data-controlled stop? cfg*))]
      (future (write-fn))
      (is (thrown-with-msg? clojure.lang.ExceptionInfo #"Data was written to the database during the zipping process"
                            (iou/zip-with-write-check (config/->backend-local-storage-dir cfg*))))
      (reset! stop? true))))

(deftest zip-test-dummy-files
  (testing "zip function"
    (let [dir-to-zip (-> (Files/createTempDirectory "Test" (into-array FileAttribute []))
                         (.toFile))
          _ (test-util/create-dummy-files dir-to-zip)
          zip-bytes (iou/zip-with-write-check dir-to-zip)
          unzip-dir (-> (Files/createTempDirectory "unzip" (into-array FileAttribute []))
                        .toFile)]
      (iou/unzip-byte-array zip-bytes unzip-dir)
      (let [actual-files (test-util/relativize-path-with-byte-count dir-to-zip)
            unzipped-files (test-util/get-file-content zip-bytes)]
        (is (= actual-files unzipped-files)))
      (test-util/force-delete-directory dir-to-zip))))

(deftest zip-and-unzip-test
  (testing "unzipping zipped files should return the same files"
    (let [dir-to-zip (-> (Files/createTempDirectory "Test" (into-array FileAttribute []))
                         (.toFile))
          _ (test-util/create-dummy-files dir-to-zip)
          zip-bytes (iou/zip-with-write-check dir-to-zip)
          unzip-dir (-> (Files/createTempDirectory "unzip" (into-array FileAttribute []))
                        .toFile)]
      (iou/unzip-byte-array zip-bytes unzip-dir)
      (let [actual-files (test-util/relativize-path-with-byte-count dir-to-zip)
            unzipped-files (test-util/get-file-content zip-bytes)]
        (is (= actual-files unzipped-files)))
      (test-util/force-delete-directory dir-to-zip))))

(deftest calculate-checksum-test
  (testing "Calculating checksum for a directory"
    (let [test-dir (-> (Files/createTempDirectory "Test" (into-array FileAttribute []))
                       (.toFile))
          test-file-1 (io/file test-dir "file1.txt")
          test-file-2 (io/file test-dir "file2.txt")
          test-file-3 (io/file test-dir "file3.txt")
          test-file-4 (io/file test-dir "file4.txt")
          test-content-1 "Lorem ipsum dolor sit amet"
          test-content-2 "consectetur adipiscing elit"
          test-content-3 "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
          test-content-4 "Ut enim ad minim veniam"
          _ (spit test-file-1 test-content-1)
          _ (spit test-file-2 test-content-2)
          _ (spit test-file-3 test-content-3)
          checksum-1 (cu/dir->sha-3 test-dir)
          _ (spit test-file-4 test-content-4)
          checksum-2 (cu/dir->sha-3 test-dir)
          _ (.delete test-file-4)
          checksum-3 (cu/dir->sha-3 test-dir)]
      (is (not= checksum-1 checksum-2)) ;
      (is (= checksum-1 checksum-3))
      (test-util/force-delete-directory test-dir))))

(ns utils.mini-db
  (:require
   [java-time.api :as jt]
   [jobtech-taxonomy.api.db.api-util :as api-util]
   [jobtech-taxonomy.api.db.concept-types :as concept-types]
   [jobtech-taxonomy.api.db.concepts :as c]
   [jobtech-taxonomy.api.db.core :refer [replace-deprecated-concept]]
   [jobtech-taxonomy.api.db.database-connection :as dc]
   [jobtech-taxonomy.api.db.nano-id :as nano-id]
   [jobtech-taxonomy.api.db.versions :as v]
   [jobtech-taxonomy.common.db-schema :refer [schema]]
   [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)

(defn validate-entry [entry]
  (when-not (:id entry)
    (throw (ex-info "Entry missing :id" {:entry entry}))))

(defn generate-transaction-instant
  "Generates a transaction timestamp for a given version."
  [version]
  (-> (jt/local-date-time 2001 1 1 0 0)
      (jt/plus (jt/years (dec version)))
      (jt/zoned-date-time (jt/zone-id "Europe/Stockholm"))
      (jt/plus (jt/days (rand 10)) (jt/seconds 7))
      jt/instant
      jt/java-date))

(defn process-concept-entry
  "Processes a concept entry by validating and asserting it to the database."
  [cfg tx-user tx-inst tx-comment entry]
  (validate-entry entry)
  (with-redefs [nano-id/generate-new-id-with-underscore (fn [] (:id entry))]
    (c/assert-concept cfg tx-user (assoc entry :comment tx-comment :tx-inst tx-inst))))

(defn handle-version-change
  "We use redef to manipulate transaction timestamp without altering 
   create-new-version function just for testing purposes."
  [cfg version tx-inst]
  (println "now we are in handle-version-change")

  (with-redefs [v/tx-data (fn [id tx] {:tx-data [{:db/id "datomic.tx"
                                                  :db/txInstant tx-inst}
                                                 {:taxonomy-version/id id
                                                  :taxonomy-version/tx tx}]})]
    (v/create-new-version cfg version tx-inst)))

(defn process-generic-entry
  "Processes a generic entry by directly transacting it to the database."
  [cfg tx-inst tx-comment tx-user entry]
  (log/debug "Processing generic entry" entry)
  (when (nil? tx-inst)
    (throw (ex-info "Transaction instant is nil" {:entry entry})))
  (dc/transact (dc/connect cfg)
               {:tx-data [(-> {:db/id "datomic.tx"}
                              (cond-> tx-comment (assoc :daynote/comment tx-comment)
                                      tx-user (assoc :taxonomy-user/id tx-user))
                              (api-util/add-tx-instant-if-present tx-inst))
                          entry]}))

(defn process-replace-concept [cfg tx-user tx-inst tx-comment entry]
  (assert (= 1 (count (:replaced-by entry))))
  (let [[_attr old-concept-id] (:db/id entry)
        [[_attr new-concept-id]] (:replaced-by entry)]
    (log/debug "Processing replace concept" entry
               "with old id" old-concept-id
               "and new id" new-concept-id
               "and tx-inst" tx-inst)
    (when (nil? tx-inst)
      (throw (ex-info "Transaction instant is nil" {:entry entry})))
    (replace-deprecated-concept cfg tx-user old-concept-id new-concept-id tx-comment tx-inst)))

(defn process-version-items
  "Processes a collection of version items for a given version."
  [cfg version-items offset-time]
  (doseq [[tx-id [tx-comment tx-user entry]] (map-indexed vector version-items)]
    (let [tx-inst (-> offset-time
                      (jt/plus (jt/years -1) (jt/days tx-id) (jt/seconds 7))
                      jt/sql-timestamp
                      jt/java-date)
          user-id (if tx-user tx-user "@system")]
      (cond
        (:concept-1 entry) (c/assert-relation cfg user-id (assoc entry :tx-inst tx-inst :comment tx-comment))
        (:replaced-by entry) (process-replace-concept cfg user-id tx-inst tx-comment entry)
        (:concept-type/id entry) (concept-types/assert-type cfg {:concept-type (:concept-type/id entry)
                                                                 :label-sv (:label-sv entry)
                                                                 :label-en (:label-en entry)
                                                                 :user-id user-id
                                                                 :tx-inst tx-inst})
        (:id entry) (process-concept-entry cfg user-id tx-inst tx-comment entry)
        :else (process-generic-entry cfg tx-inst tx-comment tx-user entry)))))

(defn process-transactions [cfg db-versions]
  (doseq [[version version-items] (map-indexed vector db-versions)]
    (let [version-inst (jt/plus
                        (jt/local-date-time #inst "2001" (jt/zone-id))
                        (jt/years (dec version)))
          tx-version-inst (jt/java-date (jt/sql-timestamp version-inst))]
      (process-version-items cfg version-items version-inst)
       ; dont add new version if last, to create :next version
      (when (< (inc version) (count db-versions))
        (handle-version-change cfg version tx-version-inst)))))

;;; database entries
(def version-0
  (into [] (concat
            (mapv (fn [t] [nil nil {:relation/type t}])
                  ["broad-match"
                   "broader"
                   "close-match"
                   "exact-match"
                   "possible-combination"
                   "related"
                   "substitutability"
                   "unlikely-combination"])
            (mapv (fn [t] [nil nil {:concept-type/id t}])
                  ["continent"
                   "country"
                   "driving-licence"
                   "employment-duration"
                   "employment-type"
                   "isco-level-4"
                   "keyword"
                   "language"
                   "language-level"
                   "municipality"
                   "occupation-collection"
                   "occupation-experience-year"
                   "occupation-field"
                   "occupation-name"
                   "region"
                   "skill"
                   "skill-headline"
                   "sni-level-1"
                   "sni-level-2"
                   "sni-level-3"
                   "sni-level-4"
                   "sni-level-5"
                   "ssyk-level-1"
                   "ssyk-level-2"
                   "ssyk-level-3"
                   "ssyk-level-4"
                   "sun-education-field-1"
                   "sun-education-field-2"
                   "sun-education-field-3"
                   "sun-education-level-1"
                   "sun-education-level-2"
                   "sun-education-level-3"
                   "unemployment-fund"
                   "unemployment-type"
                   "wage-type"
                   "worktime-extent"])
            (mapv (fn [entry] [nil nil entry])
                  [{:definition "Ger individuell vård och omsorg till äldre i eget eller särskilt boende.\r\n"
                    :concept.external-database.ams-taxonomy-67/id "1257"
                    :id "tAJS_JNb_hDH"
                    :preferred-label "Vårdbiträden"
                    :concept.external-standard/ssyk-code-2012 "5330"
                    :type "ssyk-level-4"}
                   {:definition "Arbetsledarerfarenhet"
                    :concept.external-database.ams-taxonomy-67/id "1"
                    :id "j1Yr_UhC_X16",
                    :preferred-label "Arbetsledarerfarenhet"
                    :type "skill"}
                   {:id "AJ4a_a1z_UzB"
                    :preferred-label "Övriga chefer inom utbildning"
                    :concept.external-standard/esco-uri "http://data.europa.eu/esco/isco/C1349"
                    :type "isco-level-4"
                    :definition "Denna undergrupp omfattar chefer inom utbildning"
                    :concept.external-standard/isco-code-08 "1349"}
                   {:id "sWzF_6pd_Y6L"
                    :type "ssyk-level-4"
                    :definition "Någon sorts chef"
                    :preferred-label "Övriga chefer inom samhällsservice"
                    :concept.external-standard/ssyk-code-2012 "1590"
                    :concept.external-database.ams-taxonomy-67/id "1492"}
                   {:id "bh3H_Y3h_5eD"
                    :type "occupation-field"
                    :preferred-label "Chefer av olika slag"
                    :definition "Exempel på arbetsuppgifter ..."
                    :concept.external-database.ams-taxonomy-67/id "20"}
                   {:definition "Redaktionschef"
                    :concept.external-database.ams-taxonomy-67/id "5065"
                    :id "RdVK_4zC_kFV"
                    :preferred-label "Redaktionschef"
                    :type "occupation-name"}
                   {:concept-1 "RdVK_4zC_kFV"
                    :concept-2 "sWzF_6pd_Y6L"
                    :description "broader"
                    :id "RdVK_4zC_kFV:broader:sWzF_6pd_Y6L"
                    :type "broader"}
                   {:concept-1 "RdVK_4zC_kFV"
                    :concept-2 "AJ4a_a1z_UzB"
                    :description "broader"
                    :id "RdVK_4zC_kFV:broader:AJ4a_a1z_UzB"
                    :type "broader"}
                   {:concept-1 "RdVK_4zC_kFV"
                    :concept-2 "AJ4a_a1z_UzB"
                    :description "broad-match"
                    :id "RdVK_4zC_kFV:broad-match:AJ4a_a1z_UzB"
                    :type "broad-match"}
                   {:concept-1 "sWzF_6pd_Y6L"
                    :concept-2 "bh3H_Y3h_5eD"
                    :description "broader"
                    :id "sWzF_6pd_Y6L:broader:bh3H_Y3h_5eD"
                    :substitutability-percentage 0
                    :type "broader"}])
            [[nil nil {:id "CaRE_1nn_cSU"
                       :type "region"
                       :definition "Skåne län"
                       :preferred-label "Skåne län"
                       :concept.external-database.ams-taxonomy-67/id "195"
                       :concept.external-standard/national-nuts-level-3-code-2019 "12"}]
             [nil nil {:id "UQ75_1eU_jaC"
                       :definition "Alingsås"
                       :preferred-label "Alingsås"
                       :concept.external-database.ams-taxonomy-67/id "280"
                       :concept.external-standard/lau-2-code-2015 "1489"
                       :type "municipality"}]
             [nil nil {:id "aYA7_PpG_BqP"
                       :definition "Nacka"
                       :preferred-label "Nacka"
                       :concept.external-standard/lau-2-code-2015 "0182"
                       :type "municipality"}]
             [nil nil {:definition "Forskning, erfarenhet"
                       :concept.external-database.ams-taxonomy-67/id "3"
                       :id "T5KD_ZPr_ysR"
                       :preferred-label "Forskning, erfarenhet"
                       :type "skill"}]])))

(def version-1
  [["Just a little change" "editor-1" {:concept-1 "sWzF_6pd_Y6L"
                                       :concept-2 "j1Yr_UhC_X16"
                                       :description "broad-match"
                                       :id "sWzF_6pd_Y6L:broad-match:j1Yr_UhC_X16"
                                       :type "broad-match"}]])

(def version-2
  [["Slå ett slag för skodda hästar" "admin-3"
    {:id "gjd2_JSR_AeG"
     :definition "Hovslageri"
     :preferred-label "Hovslageri"
     :type "skill"}]
   [nil nil {:id "MS1s_AYg_kZe"
             :type "country"
             :definition "Caymanöarna"
             :preferred-label "Caymanöarna"
             :concept.external-database.ams-taxonomy-67/id "36"
             :concept.external-standard/iso-3166-1-alpha-2-2013 "KY"
             :concept.external-standard/iso-3166-1-alpha-3-2013 "CYM"}]
   [nil nil {:id "TAKE_THE_BOB"
             :type "country"
             :definition "Land of Bob"
             :preferred-label "Land of Bob"
             :concept.external-database.ams-taxonomy-67/id "42"
             :concept.external-standard/iso-3166-1-alpha-2-2013 "LB"
             :concept.external-standard/iso-3166-1-alpha-3-2013 "LOB"}]
   [nil nil {:db/id [:concept/id "CaRE_1nn_cSU"]
             :replaced-by [[:concept/id "TAKE_THE_BOB"]]
             :deprecated true}]
   [nil nil {:id "iRmD_coH_YZ6"
             :type "region"
             :definition "Campobasso"
             :preferred-label "Campobasso"}]])

(def version-3
  (conj
   (mapv (fn [[d e i s]]
           [nil nil {:definition d
                     :preferred-label d
                     :concept.external-standard/eures-code-2014 e
                     :id i
                     :sort-order s
                     :type "employment-duration"}])
         [["Tills vidare" "PF" "a7uU_j21_mkL" 1]
          ["12 månader - upp till 2 år" "PF" "9RGe_UxD_FZw" 2]
          ["6 månader – upp till 12 månader" "PF" "gJRb_akA_95y" 3]
          ["3 månader – upp till 6 månader" "TF" "Xj7x_7yZ_jEn" 4]
          ["11 dagar - upp till 3 månader" "TF" "Sy9J_aRd_ALx" 5]
          ["Upp till 10 dagar" "TF" "cAQ8_TpB_Tdv" 6]])))

(def version-4
  (mapv (fn [e] [nil nil e])
        [{:definition "Styr och övervakar maskiner som bearbetar, blandar och formar kemikalier och andra ämnen för att framställa farmaceutiska produkter och hygienartiklar."
          :concept.external-database.ams-taxonomy-67/id "1257"
          :id "rxmK_wwM_1NA"
          :preferred-label "Maskinoperatörer, farmaceutiska produkter"
          :concept.external-standard/ssyk-code-2012 "8131"
          :type "ssyk-level-4"}
         {:definition "Sveriges arbetares a-kassa"
          :id "mQNn_ZiR_VMJ"
          :type "unemployment-fund"
          :preferred-label "Sveriges arbetares a-kassa"
          :concept.external-database.ams-taxonomy-67/id "21"
          :concept.external-standard/unemployment-fund-code-2017 "41"}]))

(def version-5
  [[nil nil {:definition "Quechua",
             :id "gBd9_PJY_gbT",
             :concept.external-standard/iso-639-1-2002 "QU",
             :concept.external-standard/iso-639-2-1998 "QUE",
             :concept.external-standard/iso-639-3-alpha-2-2007 "QU",
             :concept.external-standard/iso-639-3-alpha-3-2007 "QUE",
             :preferred-label "Quechua",
             :type "language"}]
   [nil nil {:id "7uGH_h7w_W21"
             :definition "Kontorstjänster och andra företagstjänster"
             :preferred-label "Kontorstjänster och andra företagstjänster"
             :concept.external-standard/sni-level-code-2007 "82"
             :type "sni-level-2"}]
   [nil nil {:concept.external-standard/isco-code-08 "3222"
             :id "dSc4_snH_7WR"
             :type "isco-level-4"
             :preferred-label "Barnmorskor"
             :definition "En lång ISCO-beskrivning"}]])

(def version-6
  (mapv (fn [e] [nil nil e])
        [{:definition "Här ingår utbildningar för främst sexåringar."
          :concept.external-standard/sun-education-level-code-2020 "002"
          :id "pCha_vWk_pbR"
          :preferred-label "Förskoleklass"
          :type "sun-education-level-3"}
         {:definition "Körkortsbehörighet för moped klass I, den så kallade EU-mopeden. Den är konstruerad för en hastighet av högst 45 km/tim och för att köra den krävs att man har körkort, traktorkort (utfärdat före den 1 oktober 2009), eller AM-körkort \r\n(eller ett förarbevis för moped klass I som man har tagit före den 1 februari 2009).\r\n\r\nKörkortsålder: 15 år"
          :concept.external-standard/driving-licence-code-2013 "AM"
          :id "4HpY_e2U_TUH"
          :preferred-label "AM"
          :short-description "Moped klass 1 och 2"
          :sort-order 2
          :type "driving-licence"}]))

(def version-7
  [[nil nil {:concept-type/id "sun-education-field-4"}]
   [nil nil {:definition
             "Ämneslärarutbildning, matematik, data, naturvetenskap och teknik i kombination med praktisk-estetiska ämnen"
             :id "tJzG_DQt_aRT"
             :preferred-label
             "Ämneslärarutbildning, matematik, data, naturvetenskap och teknik i kombination med praktisk-estetiska ämnen"
             :type "sun-education-field-4"
             :concept.external-standard/sun-education-field-code-2020
             "145g"}]])

(def version-next
  [["RSN" "1337" {:id "NEXT_GEN_ONE"
                  :definition "Next gen. concept"
                  :preferred-label "Next gen. concept"
                  :type "skill"}]])

(def db-versions [version-0
                  version-1
                  version-2
                  version-3
                  version-4
                  version-5
                  version-6
                  version-7
                  version-next])

(def ^:export cache-db-cfg
  {:database-backend :datahike-v.kaocha-cache
   :backends [{:id :datahike-v.kaocha-cache
               :type :datahike
               :threads 1
               :cfg {:store {:backend :mem
                             :id "testing-cache"}
                     :attribute-refs? true
                     :wanderung/type :datahike
                     :name "unfortunate-conflict-of-evidence"}}]})

(defn populate [cfg]
  (dc/transact (dc/connect cfg)
               {:tx-data (concat [{:db/id "datomic.tx"
                                        ; the first transaction sets database basis, in this case 
                                        ; two years before the first version
                                   :db/txInstant #inst "1998-01-06T23:00:07.000-00:00"}]
                                 schema)})
  (process-transactions cfg db-versions))

(defn init! [cfg]
  (log/trace cfg)
  (dc/create-database cfg)
  (populate cfg))

(comment)

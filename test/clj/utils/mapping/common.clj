(ns utils.mapping.common
  (:require [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.io-utils :as iou]
            [jobtech-taxonomy.features.mapping.common :as common]
            [jobtech-taxonomy.features.mapping.core :as core]
            [jobtech-taxonomy.features.mapping.create :as create]
            [jobtech-taxonomy.features.mapping.mapping :as mapping.mapping]
            [utils.mini-db :as mini-db]))

(defn display-lookup [result]
  (->> (:lookup result)
       (map (fn [hit]
              {:mapping (mapping.mapping/mapping->sort-order-vector (:mapping hit))
               :targets (sort-by :mapped-id (vec (:found hit)))}))
       (sort-by :mapping)))

(defn read-test-data [schema file-name]
  (let [resource-name (str "mapping/" file-name)]
    (iou/read-edn-resource schema resource-name)))

(defn- tx-data-schema [map-schema]
  [:map {:closed true}
   [:tx-data [:vector map-schema]]])

(defn treasure-island-1950 [] (read-test-data core/mapping-file-schema "maps/treasure-island-1950.edn"))
(defn treasure-island-2012 [] (read-test-data core/mapping-file-schema "maps/treasure-island-2012.edn"))
(defn esco-data [] (read-test-data
                    (tx-data-schema
                     [:map {:closed true}
                      [:concept/id :string]
                      [:concept/type :string]
                      [:concept/preferred-label :string]
                      [:concept.external-standard/isco-code-08 {:optional true} :string]
                      [:concept.external-standard/esco-uri {:optional true} :string]])
                    "data/esco-data.edn"))
(defn esco-mappings [] (read-test-data core/mapping-file-schema "maps/esco-mappings.edn"))
(defn mapping-for-new-targets [] (read-test-data core/mapping-file-schema "maps/mapping-for-new-targets.edn"))
(defn sun-level-data [] (read-test-data
                         (tx-data-schema
                          [:map {:closed true}
                           [:concept/id :string]
                           [:concept/type :string]
                           [:concept/preferred-label :string]
                           [:concept.external-standard/sun-education-level-code-2000 :string]])
                         "data/sun-level-data.edn"))
(defn sun-to-isced-levels [] (read-test-data core/mapping-file-schema "maps/sun-to-isced-levels.edn"))
(defn sun-field-data [] (read-test-data
                         (tx-data-schema
                          [:map {:closed true}
                           [:concept/id :string]
                           [:concept/type :string]
                           [:concept/preferred-label :string]
                           [:concept.external-standard/sun-education-field-code-2000 :string]])
                         "data/sun-field-data.edn"))
(defn sun-to-isced-fields [] (read-test-data core/mapping-file-schema "maps/sun-to-isced-fields.edn"))
(defn sni-to-nace [] (read-test-data core/mapping-file-schema "maps/sni.edn"))
(defn sni-to-nace-data []
  (read-test-data
   (tx-data-schema
    [:map {:closed true}
     [:concept/id :string]
     [:concept/type :string]
     [:concept/preferred-label :string]
     [:concept.external-standard/sni-level-code-2007 :string]])
   "data/sni-data.edn"))

(defn toy-schema []
  {:tx-data (read-test-data common/db-schema-schema "data/treasure-island-schema.edn")})

(defn toy-data []
  {:tx-data
   (read-test-data
    [:vector
     [:map {:closed true}
      [:local-id {:optional true} :string]
      [:remote-id {:optional true} :string]
      [:annotation {:optional true} [:or :string [:vector :string]]]
      [:occupation {:optional true} :string]]]
    "data/treasure-island-data.edn")})

(defn pull-id [conn id-ref]
  (:db/id (dc/pull (dc/db conn) '[:db/id] id-ref)))

(defn ^:export database-with-schema [cfg]
  (fn [fun-run]
    (try
      (mini-db/init! cfg)
      (dc/transact (dc/connect cfg) (toy-schema))
      (dc/transact (dc/connect cfg) (toy-data))
      ;; load mapping schema
      (core/install-schema cfg)
      (fun-run)
      (finally
        (dc/delete-database cfg)))))

(defn ^:export database-with-schema-and-data [cfg]
  (fn [fun-run]
    (try
      (mini-db/init! cfg)
      ;; load some toy data and schemas
      (dc/transact (dc/connect cfg) (toy-schema))
      (dc/transact (dc/connect cfg) (toy-data))
      (dc/transact (dc/connect cfg) (esco-data))
      (dc/transact (dc/connect cfg) (sun-level-data))
      (dc/transact (dc/connect cfg) (sun-field-data))
      ;; load mapping schema
      (core/install-schema cfg)
      ;; load mappings
      (create/create
       (dc/connect cfg) (treasure-island-1950))
      (create/create
       (dc/connect cfg) (treasure-island-2012))
      (doseq [esco-mapping (esco-mappings)]
        (create/create
         (dc/connect cfg) esco-mapping))
      (create/create
       (dc/connect cfg) (mapping-for-new-targets))
      (create/create
       (dc/connect cfg) (sun-to-isced-fields))
      (create/create
       (dc/connect cfg) (sun-to-isced-levels))
      (fun-run)
      (finally
        (dc/delete-database cfg)))))

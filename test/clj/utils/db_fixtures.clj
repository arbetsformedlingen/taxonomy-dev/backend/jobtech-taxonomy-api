(ns utils.db-fixtures
  (:require [babashka.fs :as fs]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.db.concepts :as c]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.versions :refer [create-new-version]]
            [jobtech-taxonomy.common.db-schema :refer [schema]]
            [jobtech-taxonomy.common.io-utils :as iou]
            [taoensso.timbre :as log]
            [utils.mini-db :as mini-db]))

(def active-backend-type (-> "DATABASE_BACKEND"
                             iou/get-env
                             iou/read-edn-string
                             iou/keyword-from-x
                             (or :datomic)))

(log/info "Active backend type:" active-backend-type)

(defn get-test-config [backend-type config-type]
  {:post [(= backend-type (config/->backend-type %))]}
  (-> "test/resources/config"
      (fs/path (name backend-type)
               (case config-type
                 :test "test.edn"
                 :full-data "full_data.edn"))
      str
      config/load-config-from-file
      config/valid-config?))

(def cfg-datomic (get-test-config :datomic :test))
(def cfg-datahike (get-test-config :datahike :test))
(def cfg (get-test-config active-backend-type :test))
(def real-cfg (get-test-config active-backend-type :full-data))

(defn ^:export database-empty [cfg]
  (fn [fun-run]
    (try
      (dc/create-database cfg)
      (fun-run)
      (finally
        (dc/delete-database cfg)))))

;; 53 tests, 104 assertions, 3 errors, 77 failures.
(defn database-minimal
  ([cfg] (database-minimal cfg mini-db/init!))
  ([cfg populate]
   (fn [fun-run]
     (try
       (populate cfg)
       (fun-run)
       (finally
         (dc/delete-database cfg))))))

(defn get-from [collection kwd value]
  (first (filter #(= value (kwd %)) collection)))

(defn database-with-schema [cfg]
  (fn [fun-run]
    (try
      (dc/create-database cfg)
      (dc/transact (dc/connect cfg) {:tx-data schema})
      (fun-run)
      (finally
        (dc/delete-database cfg)))))

(defn add-version [cfg version]
  (fn [fun-run] (create-new-version cfg version) (fun-run)))

(defn add-concepts [cfg]
  (fn [fun-run]
    (#'c/assert-concept-part
     cfg
     "my-user-id"
     {:id "1111_111_111"
      :type "my-type"
      :definition "my-definition"
      :preferred-label "my-label"
      :comment "my-comment"
      :quality-level 7
      :description "my-description"
      :substitutability-percentage 11
      :alternative-labels "my-alternative-label|another-alternative-label"
      :hidden-labels "my-hidden-label|another-hidden-label"})
    (#'c/assert-concept-part
     cfg
     "my-user-id"
     {:id "2222_222_222"
      :type "my-type"
      :definition "my-definition1"
      :preferred-label "my-label1"
      :comment "my-comment"
      :quality-level 7
      :description "my-description"
      :substitutability-percentage 22
      :alternative-labels "my-alternative-label1|another-alternative-label1"
      :hidden-labels "my-hidden-label1|another-hidden-label1"})
    (#'c/assert-concept-part
     cfg
     "my-user-id"
     {:id "3333_333_333"
      :type "my-type"
      :definition "my-definition2"
      :preferred-label "my-label2"
      :comment "my-comment"
      :quality-level 7
      :description "my-description"
      :substitutability-percentage 33
      :alternative-labels "my-alternative-label2|another-alternative-label2"
      :hidden-labels "my-hidden-label2|another-hidden-label2"})
    (#'c/assert-concept-part
     cfg
     "my-user-id"
     {:id "4444_444_444"
      :type "my-type"
      :definition "my-definition4"
      :preferred-label "my-label4"
      :comment "my-comment"
      :quality-level 7
      :deprecated false
      :description "my-description"
      :substitutability-percentage 44
      :alternative-labels "my-alternative-label4|another-alternative-label4"
      :hidden-labels "my-hidden-label4|another-hidden-label4"
      :no-esco-relation true})
    (fun-run)))

(def minimum-expected-version 22)

(defn has-real-data?
  "This function tests that the database configured by `cfg` contains (some of) the data of the full taxonomy."
  [cfg]
  (<= minimum-expected-version
      (-> cfg
          (dc/get-db :next)
          dc/get-latest-released-version)))

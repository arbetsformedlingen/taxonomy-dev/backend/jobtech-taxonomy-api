(ns utils.mini-db-test
  (:require [clojure.set :as s]
            [clojure.test :as test :refer [deftest is testing]]
            [java-time.api :as jt]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.versions :as v]
            [jobtech-taxonomy.common.db-schema :refer [schema]]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mini-db :refer [populate generate-transaction-instant
                                   handle-version-change
                                   process-version-items validate-entry]]))

(set! *warn-on-reflection* false)

(def cfg udf/cfg)

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :debug]]}
      (fun-run)))
  (udf/database-minimal udf/cfg (fn init! [cfg]
                                  (log/trace cfg)
                                  (dc/create-database cfg))))

(def version-disney
  (into []
        (concat
         (mapv (fn [t] [nil nil {:concept-type/id t}])
               ["disney-character"
                "continent"
                "country"])
         (mapv (fn [t] [nil nil {:relation/type t}])
               ["related"])
         (mapv (fn [[d e i]]
                 [nil nil {:definition d
                           :preferred-label d
                           :concept.external-standard/eures-code-2014 e
                           :id i
                           :type "disney-character"}])
               [["Mickey Mouse" "MM" "abcd_efg_hij"]
                ["Donald Duck" "DD" "1a2B_3cD_4eF"]
                ["Goofy" "GF" "stuv_wxy_zab"]
                ["Minnie Mouse" "MN" "mnop_qrs_tuv"]
                ["Pluto" "PL" "wxyz_abc_def"]
                ["Daisy Duck" "DS" "ghgj_kum_ngp"]])
         (mapv (fn [[c1 c2 desc type]]
                 [nil nil {:relation/concept-1 [:concept/id c1]
                           :relation/concept-2 [:concept/id c2]
                           :relation/description desc
                           :relation/id (str c1 ":" type ":" c2)
                           :relation/type type}])
               [["abcd_efg_hij" "mnop_qrs_tuv" "Mickey and Minnie are related" "related"]
                ["1a2B_3cD_4eF" "ghgj_kum_ngp" "Donald and Daisy are related" "related"]
                ["stuv_wxy_zab" "wxyz_abc_def" "Goofy and Pluto are related" "related"]
                ["abcd_efg_hij" "1a2B_3cD_4eF" "Mickey and Donald are related" "related"]
                ["mnop_qrs_tuv" "ghgj_kum_ngp" "Minnie and Daisy are related" "related"]]))))

(defn- get-db []
  (dc/get-db cfg :next))

(deftest test-validate-entry
  (testing "validate-entry should throw an exception if :concept/id is missing"
    (is (thrown? clojure.lang.ExceptionInfo
                 (validate-entry {:concept/definition "Test"})))))

(deftest test-generate-transaction-instant
  (testing "generate-transaction-instant should generate a valid timestamp"
    (let [version 1
          tx-inst (generate-transaction-instant version)]
      (is (instance? java.util.Date tx-inst)))))

(deftest test-handle-version-change
  (testing "handle-version-change should correctly handle valid and invalid versions"
    (dc/transact (dc/connect cfg) {:tx-data schema})
    (let [valid-version 0
          invalid-version 1
          timestamp (java.util.Date.)

          ;; Execute the function with valid and invalid versions
          result-valid   (:version (handle-version-change cfg valid-version timestamp))
          result-invalid (handle-version-change cfg invalid-version timestamp)]
      
      (is (= 0
             result-valid)
          "Version 0 is allowed and should return 0")

      (is (= :jobtech-taxonomy.api.db.versions/new-version-before-release
             result-invalid)
          "Version 1 is invalid and should return :new-version-before-release"))))

(deftest test-process-version-items-disney
  (testing "process-version-items should process a collection of version items"
    (dc/transact (dc/connect cfg)
                 {:tx-data (concat [{:db/id "datomic.tx"
                                     :db/txInstant (generate-transaction-instant -1)}]
                                   schema)})
    (let [version-items version-disney
          tx-inst (jt/local-date-time (jt/zone-id)) ; transaction instant
          _ (process-version-items cfg version-items tx-inst)
          relations-after (dc/q '[:find ?desc
                                  :where
                                  [?r :relation/description ?desc]]
                                (get-db))]
      (is (= #{["Mickey and Minnie are related"]
               ["Donald and Daisy are related"]
               ["Goofy and Pluto are related"]
               ["Mickey and Donald are related"]
               ["Minnie and Daisy are related"]}
             (set relations-after)) "Relations should be in the database"))))

(defn- year-only [inst]
  (-> inst
      .getTime
      (java.time.Instant/ofEpochMilli)
      (.atZone (java.time.ZoneId/systemDefault))
      .getYear))

(deftest creates-all-preferred-labels-and-relations
  (populate cfg)
  (let [concepts (concepts/find-concepts cfg {})
        versions (v/get-all-versions cfg)
        v-next (s/difference
                (set (concepts/find-concepts cfg {:version :next}))
                (set (concepts/find-concepts cfg {:version 7})))]
    (is (= [{:version 1, :year 2001}
            {:version 2, :year 2002}
            {:version 3, :year 2003}
            {:version 4, :year 2004}
            {:version 5, :year 2005}
            {:version 6, :year 2006}
            {:version 7, :year 2007}]
           (map (fn [{:keys [timestamp version]}]
                  {:year (year-only timestamp)
                   :version version})
                versions)))

    (is (= #{{:concept/definition "Next gen. concept",
              :concept/id "NEXT_GEN_ONE",
              :concept/preferred-label "Next gen. concept",
              :concept/type "skill"}}
           v-next))

    (is (= #{["tJzG_DQt_aRT" "Ämneslärarutbildning, matematik, data, naturvetenskap och teknik i kombination med praktisk-estetiska ämnen"]
             ["iRmD_coH_YZ6" "Campobasso"]
             ["gBd9_PJY_gbT" "Quechua"]
             ["a7uU_j21_mkL" "Tills vidare"]
             ["9RGe_UxD_FZw" "12 månader - upp till 2 år"]
             ["TAKE_THE_BOB" "Land of Bob"]
             ["bh3H_Y3h_5eD" "Chefer av olika slag"]
             ["Xj7x_7yZ_jEn" "3 månader – upp till 6 månader"]
             ["gjd2_JSR_AeG" "Hovslageri"]
             ["MS1s_AYg_kZe" "Caymanöarna"]
             ["UQ75_1eU_jaC" "Alingsås"]
             ["Sy9J_aRd_ALx" "11 dagar - upp till 3 månader"]
             ["T5KD_ZPr_ysR" "Forskning, erfarenhet"]
             ["j1Yr_UhC_X16" "Arbetsledarerfarenhet"]
             ["aYA7_PpG_BqP" "Nacka"]
             ["mQNn_ZiR_VMJ" "Sveriges arbetares a-kassa"]
             ["7uGH_h7w_W21" "Kontorstjänster och andra företagstjänster"]
             ["rxmK_wwM_1NA" "Maskinoperatörer, farmaceutiska produkter"]
             ["tAJS_JNb_hDH" "Vårdbiträden"]
             ["dSc4_snH_7WR" "Barnmorskor"]
             ["4HpY_e2U_TUH" "AM"]
             ["sWzF_6pd_Y6L" "Övriga chefer inom samhällsservice"]
             ["pCha_vWk_pbR" "Förskoleklass"]
             ["gJRb_akA_95y" "6 månader – upp till 12 månader"]
             ["AJ4a_a1z_UzB" "Övriga chefer inom utbildning"]
             ["cAQ8_TpB_Tdv" "Upp till 10 dagar"]
             ["RdVK_4zC_kFV" "Redaktionschef"]}
           (into #{}
                 (map (juxt :concept/id :concept/preferred-label))
                 concepts)))))

(ns utils.test-file-helpers
  (:require [clojure.java.io :as io]
            [jobtech-taxonomy.common.io-utils :as iou])
  (:import [java.nio.file
            FileVisitResult
            Files
            LinkOption
            SimpleFileVisitor]
           [java.nio.file Files Paths]
           [java.nio.file.attribute FileAttribute]))

(set! *warn-on-reflection* false)

(defn create-dummy-files
  "This function creates nested dir structure where each
   dir contain all .clj-files."
  [^String parent-dir]
  (let [sub-dirs ["concepts" "concepts/subdir1" "concepts/subdir2"]
        files [["relations_test.clj" 5296]
               ["concepts_test.clj" 13422]
               ["core_test.clj" 981]
               ["database_connection_datahike_test.clj" 6425]
               ["database_connection_dispatch_test.clj" 1016]
               ["database_connection_test.clj" 2315]
               ["daynotes_test.clj" 1611]
               ["eures_test.clj" 11905]
               ["graphql_test.clj" 1279]
               ["legacy_test.clj" 1052]
               ["search_test.clj" 1741]
               ["versions_test.clj" 1229]]]
    (doseq [dir sub-dirs]
      (let [sub-dir (io/file parent-dir dir)]
        (.mkdirs sub-dir)
        (doseq [[file-name byte-count] files]
          (let [file (io/file sub-dir file-name)
                content (apply str (repeat byte-count "x"))]
            (with-open [writer (clojure.java.io/writer file)]
              (.write writer content))))))))

(defn relativize-path-with-byte-count
  "Returns set with tuples of relative paths
   and corresponding file-size in bytes."
  [^java.io.File dir-to-be-zipped]
  (into #{}
        (map (fn [f]
               [(str (.relativize (Paths/get (.toUri (.toPath dir-to-be-zipped)))
                                  (Paths/get (.toUri (.toPath f)))))
                (.length f)]))
        (file-seq dir-to-be-zipped)))

(defn force-delete-directory [^java.io.File path-file]
  (let [path (Paths/get (.toUri (.toPath path-file)))]
    (when (Files/exists path (into-array LinkOption []))
      (Files/walkFileTree path
                          (proxy [SimpleFileVisitor] []
                            (visitFile [file attrs]
                              (Files/delete file)
                              FileVisitResult/CONTINUE)
                            (postVisitDirectory [dir exc]
                              (Files/delete dir)
                              FileVisitResult/CONTINUE))))))

(defn get-file-content [^"[B" zipped-data]
  {:pre [(fn [zipped-data]
           (and (instance? byte-array zipped-data)
                (not (nil? zipped-data))))]}
  (let [tmp-destination (.toFile (Files/createTempDirectory "Test" (into-array FileAttribute [])))]
    (try
      (iou/unzip-byte-array zipped-data tmp-destination)
      (relativize-path-with-byte-count tmp-destination)
      (finally
        (force-delete-directory tmp-destination)))))
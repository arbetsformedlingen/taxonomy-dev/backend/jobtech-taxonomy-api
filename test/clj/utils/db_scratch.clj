(ns utils.db-scratch
  {:clj-kondo/config '{:linters
                       {:unresolved-var
                        {:exclude [datahike.api/connect
                                   datahike.api/db
                                   datahike.api/q]}}}}
  (:require
   [clojure.pprint :refer [pprint]]
   [datahike.api :as dh]))

(comment
  ;; Feel free to dig in to the database!
  ;; The config is from the root config. Update with nippy-stuff
  ;; as needed.
  (def cfg {:store {:backend :mem
                    :id "in-mem-nippy-data"}
            :wanderung/type :datahike
            :schema-flexibility :write
            :attribute-refs? true
            :index :datahike.index/persistent-set
            :keep-history true
            :name "Nippy testing data"})
  (def u-conn (dh/connect cfg))
  (def u-db (dh/db u-conn))
  (-> (dh/q '[:find (pull ?c pull-pattern)
              :in $ pull-pattern ?code
              :where
              [?c :concept/type "municipality"]
              [?c :concept.external-standard/lau-2-code-2015 ?code]]
            u-db
            [:concept/id
             :concept/type
             :concept/preferred-label
             :concept/definition
             :concept.external-database.ams-taxonomy-67/id
             :concept.external-standard/ssyk-code-2012
             :concept.external-standard/lau-2-code-2015
             :concept.external-standard/national-nuts-level-3-code-2019]
            "1489")
      pprint)
  (-> (dh/q '[:find ?e ?pn ?v ?tx
              :in $
              :where
              [?e ?p ?v ?tx]
              [?p :db/ident ?pn]
              [?e :concept.external-database.ams-taxonomy-67/id "3"]
              #_[?e _ "T5KD_ZPr_ysR"]]
            u-db)
      sort
;      first
      pprint)
  (-> (dh/q '[:find ?e ?v
              :in $
              :where
              [?e ?p ?v]
              [?p :db/ident :daynote/ref]]
            u-db)
      sort
      first
      pprint)

  (-> (dh/q '[:find ?pn ?v
              :in $
              :where
              [536887625 ?p ?v]
              [?p :db/ident ?pn]]
            u-db)
      sort
  ;      first
      pprint)
  (-> (dh/q '[:find ?pn ?v
              :in $
              :where
              [6912 ?p ?v]
              [?p :db/ident ?pn]]
            u-db)
      sort
  ;      first
      pprint)
  'bye)
(ns utils.integration-helpers
  (:require [clj-http.client :as http-client]
            [clojure.pprint :as pp]
            [clojure.string :as str]
            [clojure.test :as test]
            [clojure.walk :refer [postwalk]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.handler :as handler]
            [jobtech-taxonomy.api.middleware.formats :as formats]
            [jobtech-taxonomy.common.db-schema :refer [schema]]
            [jobtech-taxonomy.features.mapping.setup :as mapping-setup]
            [kaocha.repl :as kaocha]
            [muuntaja.core :as m]
            [ring.mock.request :as mock]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf])
  (:import [java.util UUID]))

(set! *warn-on-reflection* true)

(def cfg udf/cfg)

(def ^:dynamic *app* (handler/app cfg))
(def ^:dynamic *host* "Bind this to a string, 
such as 'https://taxonomy.api.jobtechdev.se', 
to point http-get at a real server." nil)

(def host-address-pattern #"^http.*\p{Alnum}$")

(comment
  ; clj -M:test -e "(require 'utils.integration-helpers) (utils.integration-helpers/run-real-server-tests-with-host \"http://localhost:8080\")"
  (run-real-server-tests-with-host "https://taxonomy-i1.api.jobtechdev.se")
  (run-real-server-tests-with-host "http://localhost:8080"))

(defn run-real-server-tests-with-host
  "Run tests from all namespaces tagged with `:real-server-test` against a real server with host address `host`.

Example usage: `(run-real-server-tests-with-host \"https://taxonomy.api.jobtechdev.se\")`

This function is useful to check that a deployed taxonomy is working properly."
  [host]
  {:pre [(re-matches host-address-pattern host)]}
  (binding [*host* host]
    (let [res (time (kaocha/run (kaocha/config {:kaocha.filter/focus-meta [:real-server-test]})))
          errors (:kaocha.result/error res)
          failures (:kaocha.result/fail res)]
      (System/exit (+ errors failures)))))

(defn body->str
  "Turn whatever string-like data is in the body into a string."
  [response-body]
  {:post [(string? %)]}
  (if (string? response-body)
    response-body
    (slurp response-body)))

(defn add-mock-headers [dst header-map]
  (reduce (fn [dst [k v]] (mock/header dst k v))
          dst
          header-map))

(defn headers->lower-case [headers]
  (into {}
        (map (fn [[k v]] [(str/lower-case k) v]))
        headers))

(defn http-get
  "This function will either make a request to a real server or a mock app depending on whether or not *host* is bound to a string with the server address."

  ;; There is not http-post at the moment because there are no
  ;; tests that post against a real server.

  [path args]
  (assert (or (nil? *host*) (string? *host*)))
  (if *host*
    (http-client/get (str *host* path) args)
    (-> (mock/request :get path (:query-params args))
        (add-mock-headers (:headers args))
        *app*
        (update :body body->str)
        (update :headers headers->lower-case))))

(defn header-auth-admin []
  {:key "api-key"
   :val "222"})

(defn fixture
  "Setup a temporary database, run (f), and then teardown the database."
  [f]
  (log/info "database-backend" (dc/database-backend cfg))
  (dc/create-database cfg)
  (dc/transact (dc/connect cfg) {:tx-data schema})
  (f)
  (dc/delete-database cfg))

(defn unique-cfg
  "Derive a unique configuration from `cfg` by giving all databases unique names, either by providing a suffix or generating a suffix."
  ([] (unique-cfg nil))
  ([unique-suffix]
   (let [unique-suffix (or unique-suffix (str (UUID/randomUUID)))
         decorate-id-at-key (fn [m k]
                              (if-let [s (and (map? m)
                                              (string? (m k))
                                              (m k))]
                                (assoc m k (str s "-" unique-suffix))
                                m))]
     (postwalk (fn [x]
                 (-> x
                     (decorate-id-at-key :db-name)
                     (decorate-id-at-key :id)))
               cfg))))

(defn run-with-app [cfg f]
  (let [cfg (-> cfg
                dc/init-db
                mapping-setup/init)]
    (assert (map? cfg))
    (binding [*app* (handler/app cfg)]
      (f cfg))))

(defn real-data-fixture
  "Setup a temporary in-memory database initialized with real taxonomy data, run (f), and then tear down the database."
  ([f] (real-data-fixture f udf/real-cfg))
  ([f cfg]
   (run-with-app
    cfg
    (fn [cfg]
      (let [result (f)]
        (dc/delete-database cfg)
        result)))))

(defn real-server-fixture
  "If *host* is set it means that the tests talk directly to a real server and there is no need to initialize a database. Otherwise run the tests wrapped in the `real-data-fixture`."
  ([f cfg]
   (if *host*
     (f)
     (real-data-fixture f cfg)))
  ([f] (real-server-fixture f udf/real-cfg)))

(defn parse-json [body]
  (m/decode formats/instance "application/json" body))

(defn parse-edn [body]
  (m/decode formats/instance "application/edn" body))

(defn json-body [response]
  (-> response
      :body
      parse-json))

(defn make-req [req [first & rest]]
  (if (nil? first)
    req
    (make-req (mock/header req (get first :key) (get first :val)) rest)))

(defn make-request [method endpoint & {:keys [headers query-params]}]
  (let [req (mock/request method endpoint)
        req-w-headers (if headers
                        (make-req req headers)
                        req)]
    (if query-params
      (mock/query-string req-w-headers
                         (str/join "&" (map #(str/join "=" (list (get % :key) (get % :val))) query-params)))
      req-w-headers)))

(defn send-request [method endpoint & {:keys [headers query-params]}]
  (let [req (make-request method endpoint :headers headers, :query-params query-params)]
    (*app* req)))

(defn send-request-to-json-service [method endpoint & {:keys [headers query-params]}]
  (let [response (send-request method endpoint :headers headers, :query-params query-params)
        status (:status response)
        body (json-body response)]
    (list status body)))

(defn- map2query-params [m]
  (mapv (fn [[k v]] {:key (name k) :val v}) m))

(defn create-new-version [version]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/versions"
   :headers [(header-auth-admin)]
   :query-params [{:key "new-version-id", :val version}]))

(defn create-new-concept [params]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/concept"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn create-new-type [type]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/concept-types"
   :headers [(header-auth-admin)]
   :query-params [{:key "concept-type" :val type}]))

(defn update-concept [params]
  (send-request-to-json-service
   :patch "/v1/taxonomy/private/accumulate-concept"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn create-new-relation [params]
  (send-request-to-json-service
   :post "/v1/taxonomy/private/relation"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn update-relation [params]
  (send-request-to-json-service
   :patch "/v1/taxonomy/private/accumulate-relation"
   :headers [(header-auth-admin)]
   :query-params (map2query-params params)))

(defn ^:export test-var
  "Test a var and print results

  Useful at the REPL

  Note that for integration tests the system should be stopped before testing
  the var"
  [var]
  (let [report test/report]
    (binding [test/*test-out* *out*
              test/*report-counters* (ref test/*initial-report-counters*)
              test/report (fn [m]
                            (case (:type m)
                              :fail
                              (test/with-test-out
                                (test/inc-report-counter :fail)
                                (println "\nFAIL in" (test/testing-vars-str m))
                                (when (seq test/*testing-contexts*) (println (test/testing-contexts-str)))
                                (when-let [message (:message m)] (println message))
                                (println "expected:")
                                (pp/pprint (:expected m))
                                (println "  actual:")
                                (pp/pprint (:actual m)))
                              (report m)))]
      (test/test-vars [var])
      (test/report (assoc @test/*report-counters* :type :summary)))))

(defn concept-by-id
  "This is a helper function used by some tests to look up a specific concept."
  [id]
  (let [response (*app*
                  (mock/request :get "/v1/taxonomy/main/concepts"
                                {:id id
                                 :include-deprecated true}))]

    (and (= 200 (:status response))
         (first (json-body response)))))

(defn concept-exists?
  "This is a helper function used by some tests to test for the existence of a concept and its deprecation status."
  [concept-id deprecated-status]
  (when-let [c (concept-by-id concept-id)]
    (case deprecated-status
      nil true
      :deprecated (boolean (:taxonomy/deprecated c))
      :current (not (:taxonomy/deprecated c)))))

(defn push-version
  "This is a helper function used by some tests to release a new version. This is needed for some changes to take effect."
  [version-id]
  (= 200 (-> (mock/request :post
                           (format "/v1/taxonomy/private/versions?new-version-id=%d"
                                   version-id))
             (mock/header :api-key 222)
             *app*
             :status)))


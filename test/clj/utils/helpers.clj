(ns utils.helpers
  (:require [clojure.string :as str]
            [clojure.walk :refer [postwalk]]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)

(defmacro catch-ex-info [expr]
  `(try ~expr
        (catch clojure.lang.ExceptionInfo e#
          {:message (ex-message e#)
           :data (ex-data e#)})))

(defn- trim-user-dir [s]
  (let [user-dir (System/getProperty "user.dir")]
    (if (str/starts-with? s user-dir)
      (subs s (inc (count user-dir)))
      s)))

(defmacro not-working-as-expected
  "Use this macro to highlight test assertions that are not working but should work. This macro will do two things: (1) It will print a message to fix the assertion and (2) it will change the assertion to assert that it is in fact currently not working. Using this macro makes it easy to locate broken parts of the code."
  [explanation body]
  {:pre [(string? explanation)
         (sequential? body)
         (= 2 (count body))
         (= 'is (first body))]}

  ;; This message is displayed when the macro is expanded.
  (log/warn (format "Assertion not working as expected in file %s on line %d"
                    (trim-user-dir *file*) ;; <-- this is the file from where the macro is called.
                    (:line (meta &form))))

  (let [[_is-sym expr] body]
    `(~'is (not ~expr))))

(defn order-invariant [x]
  (postwalk
   #(if (and (sequential? %)
             (not (map-entry? %)))
      (set %)
      %)
   x))

(ns unit.jobtech-taxonomy.features.private.relation-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy.api.db.concepts :as concepts] 
   [taoensso.timbre :as log]
   [utils.integration-helpers :as util]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"jobtech-taxonomy.common.relation"} :info]
                   [#{"jobtech-taxonomy.common.concept"} :info]
                   [#{"*"} :warn]]}
      (fun-run)))
  util/fixture)

(deftest ^:private-relation private-relation-0
  (testing "test asserting a relation"
    (util/create-new-type "skill")
    (util/create-new-type "esco-skill")
    (let [relation "substitutability"
          [_ bd1]
          (util/create-new-concept
           {:type "skill"
            :definition "Haskell, programmeringsspråk"
            :preferred-label "Haskell, programmeringsspråk"})
          id1 (:concept/id (:concept bd1))
          [_ bd2]
          (util/create-new-concept
           {:type "esco-skill"
            :definition "Haskell"
            :preferred-label "Haskell"})
          id2 (:concept/id (:concept bd2))
          [status1 _body1]
          (util/create-new-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "100"})
          [status2 _body2] ;; fail to create the same relation again
          (util/create-new-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "100"})
          [status3 _body3] ;; fail to update the relation without a comment
          (util/update-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "50"})
          [status4 _body4]
          (util/update-relation
           {:relation-type relation
            :concept-1 id1
            :concept-2 id2
            :substitutability-percentage "50"
            :comment "Update percentage"})
          substituted-by4 (concepts/fetch-relation-data util/cfg id1 id2 "substituted-by")
          substitutability4 (concepts/fetch-relation-data util/cfg id1 id2 relation)
          [status5 body5]
          (util/update-relation
           {:relation-type relation
            :new-relation-type "substituted-by"
            :concept-1 id1
            :concept-2 id2
            :comment "Change the type"})
          relation5 (:relation body5)
          substituted-by5 (concepts/fetch-relation-data util/cfg id1 id2 "substituted-by")
          substitutability5 (concepts/fetch-relation-data util/cfg id1 id2 relation)]
      (is (= 200 status1))
      (is (= 409 status2))
      (is (= 400 status3))
      (is (= 200 status4))
      (is (= 50 (:relation/substitutability-percentage substitutability4)))
      (is (nil? substituted-by4))
      (is (= 200 status5))

      ;; The data returned is the datalog transaction for
      ;; asserting the relation. Technically, opposite
      ;; relations like 'substitutability' <-> 'substituted-by'
      ;; are represented by only one of them. See how this is
      ;; implemented in the function
      ;; `jobtech-taxonomy.common.relation/edge-tx`. This is why the type
      ;; is still `substitutability` (and not `substituted-by`) and the
      ;; concept ids are flipped.
      (is (= id1 (second (:relation/concept-2 relation5))))
      (is (= "substitutability" (:relation/type relation5)))
      (is (= id2 (second (:relation/concept-1 relation5))))

      (is (= 50 (:relation/substitutability-percentage substituted-by5)))
      (is (nil? substitutability5)))))
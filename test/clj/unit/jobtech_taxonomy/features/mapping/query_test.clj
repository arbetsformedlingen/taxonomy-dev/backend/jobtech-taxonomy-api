(ns ^:mappings unit.jobtech-taxonomy.features.mapping.query-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.query :as query]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema-and-data udf/cfg)
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"mapping.edge"} :debug]
                   [#{"mapping.query"} :debug]]}
      (fun-run))))

(deftest query-mapping-test
  (let [conn (dc/connect udf/cfg)
        mappings (query/mappings (dc/db conn)
                                 {:name "Skattkammarön" :version "1950"})]
    (testing "Create mapping from EDN"
      (is (= #{{:mapped-id "Basil Sydney"
                :source {:local-id "Captain Smollett"}
                :target {:remote-id "Basil Sydney"}}
               {:mapped-id "Fred Tooze",
                :source {:local-id "Captain Smollett"},
                :target {:remote-id "Fred Tooze"}}}
             (-> (query/edges (dc/db conn) (:mappings mappings) ["Captain Smollett"])
                 :lookup first :found)))
      (is (= #{{:mapped-id "Walt Disney"
                :source {:local-id "Presenter" :role "Disembodied Voice of the Narrative"}
                :target {:remote-id "Walt Disney"}}
               {:mapped-id "Hispaniola"
                :source {:local-id "Hispaniola"}
                :target {:remote-id "Hispaniola"}}}
             (-> (query/edges (dc/db conn) (:mappings mappings) ["Presenter" "Hispaniola"])
                 :lookup first :found)))
      (is (= [{:mapping (first (:mappings mappings))
               :missing #{}
               :found
               #{{:mapped-id "Robert Newton"
                  :source {:local-id "Long John Silver"
                           :role "Mighty Pirate!"}
                  :target {:remote-id "Robert Newton"
                           :occupation "Not A Scientist Actually"}}
                 {:mapped-id "L. Charles"
                  :source {:local-id "Long John Silver"
                           :role "Mighty Pirate!"}
                  :target {:remote-id "L. Charles"
                           :occupation "Actor!"}}}}]
             (:lookup (query/edges
                       (dc/db conn)
                       (:mappings mappings)
                       ["Long John Silver"])))))))
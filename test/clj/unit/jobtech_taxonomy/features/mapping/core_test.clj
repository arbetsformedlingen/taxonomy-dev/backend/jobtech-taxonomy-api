(ns ^:mappings unit.jobtech-taxonomy.features.mapping.core-test
  (:require [babashka.fs :as fs]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.core :as core]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema-and-data udf/cfg))

(defn- display-mappings [mappings-result]
  (update mappings-result
          :mappings
          #(sort (map mapping/mapping->sort-order-vector %))))

(defn- display-lookup [result]
  (->> (:lookup result)
       (map (fn [hit]
              {:mapping (mapping/mapping->sort-order-vector (:mapping hit))
               :targets (sort-by :mapped-id (vec (:found hit)))}))
       (sort-by :mapping)))

(deftest get-installed-schema-test
  (testing "Read installed schema"
    (let [defined-schema (sort-by :db/ident core/schema)
          installed-schema (->>
                            (core/get-installed-schema udf/cfg)
                            (sort-by :db/ident))]
      (is (= defined-schema
             installed-schema)))))

(deftest check-for-schema-test
  (testing "Test that the schema is installed"
    (is (core/schema-present? udf/cfg))))

(deftest list-mappings-test
  (let [db (dc/db (dc/connect udf/cfg))]
    (testing "Search among the available maps"
      (is (= {:result :error/invalid-query
              :error {:not-a-key ["disallowed key"]}}
             (core/mappings db {:not-a-key "NOT A VALUE"})))
      (is (= {:result :ok/filter
              :mappings #{}}
             (core/mappings db {:family "NOT A VALUE"})))
      (is (= {:result :ok/all :mappings [["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]
                                         ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :close-match "16"]
                                         ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :exact-match "16"]
                                         ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :narrow-match "16"]
                                         ["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
                                         ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                                         ["The Mighty Pirates" "Epics" "Skattkammarön" "STORY" :treasure->characters "23"]
                                         ["UNESCO" "Eures" "sun->isced-fields" "2013" :sun->isced-fields "1"]
                                         ["UNESCO" "Eures" "sun->isced-levels" "2011" :sun->isced-levels "1"]]}
             (display-mappings (core/mappings db {}))))
      (is (= {:result :ok/filter :mappings [["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]
                                            ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :close-match "16"]
                                            ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :exact-match "16"]
                                            ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :narrow-match "16"]]}
             (display-mappings (core/mappings db {:organisation "ESCO"}))))
      (is (= {:result :ok/filter :mappings [["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]]}
             (display-mappings (core/mappings db {:organisation "ESCO"
                                                  :family "ESCO"
                                                  :name "ESCO-occupations"
                                                  :version "1.0.8"
                                                  :type :broad-match}))))
      (is (= {:result :ok/filter :mappings [["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
                                            ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                                            ["The Mighty Pirates" "Epics" "Skattkammarön" "STORY" :treasure->characters "23"]]}
             (display-mappings (core/mappings db {:name "Skattkammarön"}))))
      (is (= {:result :ok/filter
              :mappings [["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]]}
             (display-mappings (core/mappings db {:name "Skattkammarön" :version "2012"})))))))

(deftest simple-translations-test
  (let [db (dc/db (dc/connect udf/cfg))]
    (testing "Try some simple lookups in the 2012 version"
      (let [result (core/translations
                    db
                    {:name "Skattkammarön" :version "2012"}
                    ["Long John Silver"])]
        (is (= [{:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                 :targets [{:mapped-id "Eddie Izzard"
                            :source {:local-id "Long John Silver"}
                            :target {:remote-id "Eddie Izzard"}}]}]
               (display-lookup result))))
      (let [result (core/translations
                    db
                    {:name "Skattkammarön" :version "2012"}
                    ["Hispaniola"])]
        (is (= [{:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                 :targets [{:mapped-id "Hispaniola"
                            :source {:local-id "Hispaniola"}
                            :target {:remote-id "Hispaniola"}}]}]
               (display-lookup result))))
      (let [result (core/translations
                    db
                    {:name "Skattkammarön" :version "2012"}
                    ["Long John Silver" "Hispaniola" "Jim Hawkins"])]
        (is (= [{:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                 :targets [{:mapped-id "Eddie Izzard"
                            :source {:local-id "Long John Silver"}
                            :target {:remote-id "Eddie Izzard"}}
                           {:mapped-id "Hispaniola"
                            :source {:local-id "Hispaniola"}
                            :target {:remote-id "Hispaniola"}}
                           {:mapped-id "Toby Regbo"
                            :source {:local-id "Jim Hawkins"}
                            :target {:remote-id "Toby Regbo"}}]}]
               (display-lookup result)))))
    (testing "Try some simple lookups in the 1950 version"
      (let [result (core/translations
                    db
                    {:name "Skattkammarön" :version "1950"}
                    ["Long John Silver"])]
        (is (= [{:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
                 :targets [{:mapped-id "L. Charles"
                            :source {:local-id "Long John Silver"
                                     :role "Mighty Pirate!"}
                            :target {:remote-id "L. Charles"
                                     :occupation "Actor!"}}
                           {:mapped-id "Robert Newton"
                            :source {:local-id "Long John Silver"
                                     :role "Mighty Pirate!"}
                            :target {:remote-id "Robert Newton"
                                     :occupation "Not A Scientist Actually"}}]}]
               (display-lookup result))))
      (let [result (core/translations
                    db
                    {:name "Skattkammarön" :version "1950"}
                    ["Presenter"])]
        (is (= [{:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
                 :targets [{:mapped-id "Walt Disney"
                            :source {:local-id "Presenter" :role "Disembodied Voice of the Narrative"}
                            :target {:remote-id "Walt Disney"}}]}]
               (display-lookup result)))))
    (testing "Failure is always an option"
      (let [result (core/translations
                    db {:name "Skattkammarön" :version "2012"}
                    ["Bloody Mary" "Hispaniola" "Prester John"])]
        (is (= :ok/query (:result result)))
        (is (= {:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
                :missing #{"Bloody Mary" "Prester John"}
                :found #{{:mapped-id "Hispaniola"
                          :source {:local-id "Hispaniola"}
                          :target {:remote-id "Hispaniola"}}}}
               (update (first (:lookup result)) :mapping mapping/mapping->sort-order-vector)))))))

(deftest read-mapping-file-test
  (let [filename (->> (range)
                      (map #(format "nonexistingfile%d.dat" %))
                      (drop-while fs/exists?)
                      first)]
    (is (thrown? clojure.lang.ExceptionInfo
                 (core/read-mapping-file filename)))))

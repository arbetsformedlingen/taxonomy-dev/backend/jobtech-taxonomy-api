(ns ^:mappings unit.jobtech-taxonomy.features.mapping.config-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.config :as config]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"mapping.mappings.config"} :debug]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema udf/cfg))

(deftest config-simple-test
  (testing "Context of the test assertions"
    (let [config-tx-data (config/->tx-data {} nil)]
      (is (= {:mapping.external/source-attribute :db/id
              :mapping.external/target-attribute :db/id
              :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]}
             config-tx-data)))))

(deftest pull-patterns-test
  (testing "Test getting pull patterns"
    (let [config {:source {:id-attr :local-id :pull #{:local-id}}
                  :target {:pull #{:remote-id :bob}}
                  :mapping {:mapped-id :bob
                            :pull [:mapping.mapping/type]}}]
      (is (= #{:local-id}
             (config/->pull config :source)))
      (is (= #{:local-id :extra :items} (config/->pull config :source :extra :items)))
      (is (= #{:remote-id :bob}
             (config/->pull config :target))))))

(deftest node-lookup-and-validation-test
  (let [config {:attr-types {:local-id :string}
                :source {:id-attr :local-id :pull [:local-id]}
                :target {:id-attr :db/id :pull [:remote-id]}
                :mapping {:mapped-id :this-is-it
                          :pull [:mapping.mapping/type]}}
        [source-fn target-fn] (config/->node-fns config)]
    (testing "A valid lookup pattern should result in a :id-attr/:value map or an ID"
      (is (= {:id-attr :local-id :value "My ID"} (source-fn "My ID")))
      (is (= 3 (target-fn 3))))
    (testing "A specified :db/id is always valid"
      (is (= {:db/id 3} (source-fn {:db/id 3})))
      (is (= {:db/id 3} (target-fn {:db/id 3}))))
    (testing "A specified :db/id is always valid, also for tempids"
      (is (= {:db/id "tempid"} (source-fn {:db/id "tempid"})))
      (is (= {:db/id "tempid"} (target-fn {:db/id "tempid"}))))
    (testing "Invalid types generate :error maps."
      (is (= {:error [:local-id 3]} (source-fn 3)))
      (is (= {:error [:db/id 3.14]} (target-fn 3.14))))))

(deftest config-created-target-id-test
  (testing "Context of the test assertions"
    (let [config-tx-data
          (config/->tx-data
           {:source {:id :lookup-ref-key
                     :include {:occupation :job}}
            :mapping {:mapped-id :this-is-it}
            :target {}}
           [{:db/id "one" :bob "Bobbing"}
            {:db/id "two"
             :mapping.external/key-value-data [:what "Now?"]
             :linda "Lindaing"}])]
      (is (= {:mapping.external/source-attribute :lookup-ref-key
              :mapping.external/target-attribute :db/id
              :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]
                                                   [:source :occupation :job]
                                                   [:target :bob :bob]
                                                   [:target :mapping.external/key-value-data :mapping.external/key-value-data]
                                                   [:target :linda :linda]]}
             config-tx-data)))))

(deftest config-complex-test
  (testing "Context of the test assertions"
    (let [config-tx-data (config/->tx-data
                          {:source
                           {:id :lookup-ref-key
                            :include
                            {:occupation :occupation}}
                           :target
                           {:id :other-lookup-ref-key
                            :mapping :remote-id
                            :include {:occupation :occupation
                                      :bobs/special :bobs}}}
                          nil)]
      (is (= {:mapping.external/source-attribute :lookup-ref-key
              :mapping.external/target-attribute :other-lookup-ref-key
              :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                   [:source :occupation :occupation]
                                                   [:target :occupation :occupation]
                                                   [:target :bobs/special :bobs]]}
             config-tx-data)))))

(deftest mapping->config-test
  (testing "Create a config from mapping"
    (let [conn (dc/connect udf/cfg)
          mapping
          {:db/id 2701
           :mapping.mapping/type :treasure->characters
           :mapping.mapping/organisation "The Mighty Pirates"
           :mapping.mapping/family "Epics"
           :mapping.mapping/name "Skattkammarön"
           :mapping.mapping/version "STORY"
           :mapping.mapping/taxonomy-version "23"
           :mapping.mapping/description "Mapping to character information."
           :mapping.mapping/uri "https://www.example.com"
           :mapping.external/source-attribute :local-id
           :mapping.external/target-attribute :mapping.external/id
           :mapping.external/attribute-mapping
           [[:mapping :mapped-id :mapped-target-id]
            [:source :occupation :role]
            [:target :db/id :db/id]
            [:target :mapping.external/annotations :mapping.external/annotations]
            [:target :mapping.external/id :mapping.external/id]
            [:target :mapping.external/key-value-data :mapping.external/key-value-data]
            [:target :mapping.external/uri :mapping.external/uri]]}
          config (config/mapping->config (dc/db conn) mapping)]
      (is (= {:target {:pull #{:db/id
                               :mapping.external/annotations
                               :mapping.external/id
                               :mapping.external/key-value-data
                               :mapping.external/uri}
                       :rename {}
                       :id-attr :mapping.external/id}
              :source {:pull #{:local-id :occupation}
                       :rename {:occupation :role}
                       :id-attr :local-id}
              :mapping {:mapped-id :mapped-target-id
                        :db/id 2701}
              :attr-types {:db/id [:and :int [:> 0]]
                           :local-id :string
                           :mapping.external/id :string}}
             config))
      (is (= #{:local-id :occupation}
             (config/->pull config :source)))
      (is (= #{:db/id
               :mapped-target-id
               :mapping.external/annotations
               :mapping.external/id
               :mapping.external/key-value-data
               :mapping.external/uri}
             (config/->pull config :target)))
      (is (= :local-id (config/->id-attr config :source)))
      (is (= :string (config/->id-attr-type config :source)))
      (is (= :mapping.external/id (config/->id-attr config :target)))
      (is (= :string (config/->id-attr-type config :target))))))
(ns ^:mappings unit.jobtech-taxonomy.features.mapping.common-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.features.mapping.common :as common]))

(deftest db-schema->malli-test
  (testing "Keyword schema"
    (is (= [:my/ident :keyword]
           (common/db-schema->malli
            {:db/ident :my/ident
             :db/valueType :db.type/keyword
             :db/cardinality :db.cardinality/one}))))
  (testing "String schema"
    (is (= [:my/ident :string]
           (common/db-schema->malli
            {:db/ident :my/ident
             :db/valueType :db.type/string
             :db/cardinality :db.cardinality/one}))))
  (testing "Tuple schema"
    (is (= [:my/ident [:tuple :keyword :string]]
           (common/db-schema->malli
            {:db/ident :my/ident
             :db/valueType :db.type/tuple
             :db/tupleTypes [:db.type/keyword :db.type/string]
             :db/cardinality :db.cardinality/one}))))
  (testing "Many tuples schema"
    (is (= [:my/ident [:or [:tuple :keyword :string] [:vector [:tuple :keyword :string]]]]
           (common/db-schema->malli
            {:db/ident :my/ident
             :db/valueType :db.type/tuple
             :db/tupleTypes [:db.type/keyword :db.type/string]
             :db/cardinality :db.cardinality/many}))))
  (testing "With options"
    (is (= [:my/ident {:optional true} :keyword]
           (common/db-schema->malli
            {:db/ident :my/ident
             :db/valueType :db.type/keyword
             :db/cardinality :db.cardinality/one}
            [:optional true]))))
  (testing "Invalid input"
    (is (nil? (common/db-schema->malli-type 9)))
    (is (nil? (common/db-schema->malli-type {:db/valueType 9})))
    (is (nil? (common/db-schema->malli-type {:db/cardinality 9})))))

(deftest drop-keyword-ns-ns
  (testing "drop-keyword-ns"
    (is (= :a (common/drop-keyword-ns :a)))
    (is (= :a (common/drop-keyword-ns :x/a)))
    (is (= 9 (common/drop-keyword-ns 9)))))

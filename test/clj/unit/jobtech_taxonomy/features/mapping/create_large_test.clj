(ns ^:mappings unit.jobtech-taxonomy.features.mapping.create-large-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.config :as config]
            [jobtech-taxonomy.features.mapping.create :as create]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [jobtech-taxonomy.features.mapping.query :as query]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema udf/cfg))

(deftest create-multi-mapping-test
  (let [conn (dc/connect udf/cfg)
        esco-data (umc/esco-data)
        txr (dc/transact conn esco-data)
        mappings-edn (umc/esco-mappings)]
    (testing "Create multiple at once using more Taxonmy-like data"
      (is (= [:broad-match :close-match :narrow-match :exact-match]
             (map #(get-in % [:mapping :type]) mappings-edn)))
      (is (= 203 (count (:tx-data txr))))
      (is (= [{:result :ok/created-new
               :mapping ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]
               :edges {:created 12 :existing 0 :missing #{} :updated 0}
               :targets {:created 0}}
              {:result :ok/created-new
               :mapping ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :close-match "16"]
               :edges {:created 14 :existing 0 :missing #{} :updated 0}
               :targets {:created 0}}
              {:result :ok/created-new
               :mapping ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :narrow-match "16"]
               :edges {:created 0 :existing 0 :missing #{} :updated 0}
               :targets {:created 0}}
              {:result :ok/created-new
               :mapping ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :exact-match "16"]
               :edges {:created 8 :existing 0 :missing #{} :updated 0}
               :targets {:created 0}}]
             (->> (map (partial create/create conn) mappings-edn)
                  (map #(update % :mapping mapping/mapping->sort-order-vector))))))))

(deftest create-multi-and-single-mapping-test
  (let [conn (dc/connect udf/cfg)
        esco-data (umc/esco-data)
        _txr (dc/transact conn esco-data)
        esco-mappings-edn (umc/esco-mappings)
        treasure-1950-edn (umc/treasure-island-1950)
        treasure-2012-edn (umc/treasure-island-2012)]
    (doseq [mapping-edn (conj esco-mappings-edn treasure-1950-edn treasure-2012-edn)]
      (create/create conn mapping-edn))
    (testing "Create multiple at once using more Taxonmy-like data"
      (is (= [["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :close-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :exact-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :narrow-match "16"]
              ["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
              ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]]
             (->> (mapping/mappings (dc/db conn) {})
                  :mappings (map mapping/mapping->sort-order-vector) sort))))
    (let [db (dc/db conn)
          mapping (-> (query/mappings db {:type :broad-match})
                      :mappings first)
          config (config/mapping->config db mapping)
          mapping-id (config/->mapping-id config)]
      (testing "Get a mapping"
        (is (= (:db/id mapping) mapping-id))
        (is (= {:db/id mapping-id
                :mapping.mapping/type :broad-match
                :mapping.mapping/name "ESCO-occupations"
                :mapping.mapping/organisation "ESCO"
                :mapping.mapping/family "ESCO"
                :mapping.mapping/version "1.0.8"
                :mapping.mapping/description "Occupation mappings between Taxonomy and ESCO."
                :mapping.mapping/uri "https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_yrken.md?ref_type=heads"
                :mapping.mapping/taxonomy-version "16"
                :mapping.external/attribute-mapping [[:mapping :mapped-id :concept.external-standard/esco-uri]
                                                     [:source :concept/preferred-label :preferred-label]
                                                     [:source :concept/type :type]
                                                     [:target :concept/preferred-label :preferred-label]
                                                     [:target :concept/type :type]
                                                     [:target :concept.external-standard/isco-code-08 :isco]]
                :mapping.external/source-attribute :concept/id
                :mapping.external/target-attribute :concept/id} mapping)))
      (testing "Get a config"
        (is (= {:attr-types {:concept/id :string, :db/id [:and :int [:> 0]]}
                :mapping {:mapped-id :concept.external-standard/esco-uri :db/id mapping-id}
                :source {:id-attr :concept/id
                         :pull #{:concept/id :concept/preferred-label :concept/type}
                         :rename {:concept/preferred-label :preferred-label :concept/type :type}}
                :target {:id-attr :concept/id
                         :pull #{:concept/id
                                 :concept/preferred-label
                                 :concept/type
                                 :concept.external-standard/isco-code-08}
                         :rename {:concept/preferred-label :preferred-label
                                  :concept/type :type
                                  :concept.external-standard/isco-code-08 :isco}}}
               config))))
    (testing "Query one of them"
      (is (= [{:mapped-id "http://data.europa.eu/esco/isco/C2164"
               :source {:preferred-label "Planeringsarkitekt/Fysisk planerare"
                        :type "occupation-name"
                        :concept/id "ghs4_JXU_BYt"}
               :target {:isco "2164"
                        :preferred-label "Planeringsarkitekter m.fl."
                        :type "isco-level-4"
                        :concept.external-standard/esco-uri "http://data.europa.eu/esco/isco/C2164"
                        :concept/id "XAcf_SQ9_fJw"}}]
             (query/display-targets (dc/db conn) #{{:type :broad-match}} #{"ghs4_JXU_BYt"}))))))

(deftest create-isced-mapping-test
  (let [conn (dc/connect udf/cfg)
        _field-txr (dc/transact conn (umc/sun-field-data))
        _level-txr (dc/transact conn (umc/sun-level-data))]
    (testing "Create SUN to ISCED field mapping"
      (let [isced-fields (create/create conn (umc/sun-to-isced-fields))
            isced-fields-mapping (:mapping isced-fields)]
        (is (= {:result :ok/created-new
                :edges {:created 153 :existing 0 :missing #{} :updated 0}
                :targets {:created 141}
                :mapping {:db/id true
                          :mapping.external/attribute-mapping
                          [[:mapping :mapped-id :mapping.external/id]
                           [:source :concept/preferred-label :preferred-label]
                           [:source :concept/type :type]
                           [:source :concept.external-standard/sun-education-field-code-2000 :sun-2000-field]
                           [:target :mapping.external/id :mapping.external/id]
                           [:target :mapping.external/key-value-data :mapping.external/key-value-data]]
                          :mapping.external/source-attribute :concept/id
                          :mapping.external/target-attribute :db/id
                          :mapping.mapping/description "Mappings between SUN2000-field concepts and their ISCED counterparts."
                          :mapping.mapping/family "Eures"
                          :mapping.mapping/name "sun->isced-fields"
                          :mapping.mapping/organisation "UNESCO"
                          :mapping.mapping/taxonomy-version "1"
                          :mapping.mapping/type :sun->isced-fields
                          :mapping.mapping/version "2013"}}
               (update isced-fields :mapping #(update % :db/id pos-int?))))
        (is (= [{:mapped-id "0114"
                 :source {:preferred-label "Ämneslärarutbildning"
                          :sun-2000-field "145"
                          :type "sun-education-field-3"
                          :concept/id "4PW9_gBS_F1S"}
                 :target {:db/id "db-id"
                          :mapping.external/id "0114"
                          :mapping.external/key-value-data [[:isced-type "isced-field"]
                                                            [:label "Teacher training with subject specialisation"]]}}]
               (->> (query/edges (dc/db conn) #{isced-fields-mapping} ["4PW9_gBS_F1S"])
                    :lookup
                    first
                    :found
                    (map #(assoc-in % [:target :db/id] "db-id")))))))
    (testing "Create SUN to ISCED levels mapping"
      (let [isced-levels (create/create conn (umc/sun-to-isced-levels))
            isced-levels-mapping (:mapping isced-levels)]
        (is (= {:result :ok/created-new
                :edges {:created 71 :existing 0 :missing #{} :updated 0}
                :targets {:created 9}
                :mapping {:db/id true
                          :mapping.external/attribute-mapping
                          [[:mapping :mapped-id :mapping.external/id]
                           [:source :concept/preferred-label :preferred-label]
                           [:source :concept/type :type]
                           [:source :concept.external-standard/sun-education-level-code-2000 :sun-2000-level]
                           [:target :mapping.external/id :mapping.external/id]
                           [:target :mapping.external/key-value-data :mapping.external/key-value-data]]
                          :mapping.external/source-attribute :concept/id
                          :mapping.external/target-attribute :db/id
                          :mapping.mapping/description "Mappings between SUN2000-level concepts and their ISCED counterparts."
                          :mapping.mapping/family "Eures"
                          :mapping.mapping/name "sun->isced-levels"
                          :mapping.mapping/organisation "UNESCO"
                          :mapping.mapping/taxonomy-version "1"
                          :mapping.mapping/type :sun->isced-levels
                          :mapping.mapping/version "2011"}}
               (update isced-levels :mapping #(update % :db/id pos-int?))))
        (is (= [{:mapped-id "8"
                 :source {:preferred-label "Licentiatutbildning"
                          :sun-2000-level "620"
                          :type "sun-education-level-3"
                          :concept/id "xbWJ_Qn1_bcc"}
                 :target {:db/id "db-id"
                          :mapping.external/id "8"
                          :mapping.external/key-value-data [[:isced-type "isced-level"] [:label "Doctoral or equivalent level"]]}}]
               (->> (query/edges (dc/db conn) #{isced-levels-mapping} ["xbWJ_Qn1_bcc"])
                    :lookup
                    first
                    :found
                    (map #(assoc-in % [:target :db/id] "db-id")))))))))

(deftest create-sni-mapping-test
  (let [conn (dc/connect udf/cfg)
        _sni-txr (dc/transact conn (umc/sni-to-nace-data))]
    (testing "Create SNI to NACE mapping"
      (let [sni-to-nace (create/create conn (umc/sni-to-nace))
            sni-to-nace-mapping (:mapping sni-to-nace)]
        (is (= {:result :ok/created-new
                :edges {:created 1817 :existing 0 :missing #{} :updated 0}
                :targets {:created 996}
                :mapping {:db/id true
                          :mapping.external/attribute-mapping
                          [[:mapping :mapped-id :mapping.external/id]
                           [:source :concept/id :id]
                           [:source :concept/preferred-label :preferred-label]
                           [:source :concept/type :type]
                           [:source :concept.external-standard/sni-level-code-2007 :sni-level-code-2007]
                           [:target :mapping.external/id :mapping.external/id]
                           [:target :mapping.external/key-value-data :mapping.external/key-value-data]]
                          :mapping.external/source-attribute :concept.external-standard/sni-level-code-2007
                          :mapping.external/target-attribute :db/id
                          :mapping.mapping/description "Mapping between SNI and NACE, where NACE codes are expressed according to EURES standard."
                          :mapping.mapping/family "Eures"
                          :mapping.mapping/name "sni"
                          :mapping.mapping/organisation "Eurostat"
                          :mapping.mapping/taxonomy-version "22"
                          :mapping.mapping/type :nace
                          :mapping.mapping/version "2007"}}
               (update sni-to-nace :mapping #(update % :db/id pos-int?))))
        (is (= [{:mapped-id "A2.2.0"
                 :source {:preferred-label "Drivning"
                          :sni-level-code-2007 "02200"
                          :type "sni-level-5"
                          :id "YPtg_LAu_Jam"}
                 :target {:db/id "db-id"
                          :mapping.external/id "A2.2.0"
                          :mapping.external/key-value-data [[:label "Logging"]]}}]
             ; Note 1: for SNI->NACE, we query on sni code and not concept-id
             ; Note 2: sni-level-5 concepts are mapped towards items
             ;         at nace level 4, since the fifth level is unique to Sweden 
               (->> (query/edges (dc/db conn) #{sni-to-nace-mapping} ["02200"])
                    :lookup
                    first
                    :found
                    (map #(assoc-in % [:target :db/id] "db-id")))))
        (is (= [{:mapped-id "A2.2.0"
                 :source {:preferred-label "Drivning"
                          :sni-level-code-2007 "0220"
                          :type "sni-level-4"
                          :id "A6eV_SXy_PRz"}
                 :target {:db/id "db-id"
                          :mapping.external/id "A2.2.0"
                          :mapping.external/key-value-data [[:label "Logging"]]}}]
               (->> (query/edges (dc/db conn) #{sni-to-nace-mapping} ["0220"])
                    :lookup
                    first
                    :found
                    (map #(assoc-in % [:target :db/id] "db-id")))))))))
(ns ^:mappings unit.jobtech-taxonomy.features.mapping.nodes-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.nodes :as nodes]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (umc/database-with-schema udf/cfg))

(deftest node-ids-from-test
  (testing "Get all nodes from source+target-list"
    (let [config {:attr-types {:num :int :str :string}
                  :source {:id-attr :num}
                  :target {:id-attr :str}}
          edge-set (nodes/apply-tempids config {} [[1 2] [1 {:db/id 42}]])]
      (is (= #{{:id-attr :num :value 1}
               {:mapping.nodes/error [:str 2]}
               {:db/id 42}}
             (nodes/node-ids-from config edge-set)))))
  (testing "Get all nodes from config and source+target-list"
    (let [config {:attr-types {:source-id string? :target-id string?}
                  :source {:id-attr :source-id}
                  :target {:id-attr :target-id}}
          source+target-list [["A" "B"]
                              [{:db/id -73} {:db/id "My Temporary ID"}]
                              ["A" {:db/id 42}]]
          edge-set (nodes/apply-tempids config {-73 43} source+target-list)]
      (is (= #{["A" "B"]
               ["A" {:db/id 42}]
               [{:db/id 43 :mapping.nodes/tempid -73}
                {:mapping.nodes/tempid "My Temporary ID"}]}
             edge-set))
      (is (= #{{:db/id 42}
               {:id-attr :source-id :value "A"}
               {:mapping.nodes/error {:mapping.nodes/tempid "My Temporary ID"}}
               {:id-attr :target-id :value "B"}
               {:db/id 43 :mapping.nodes/tempid -73}}
             (nodes/node-ids-from config edge-set))))))

(deftest validate-lookup-by-attr-test
  (let [conn (dc/connect udf/cfg)
        txr (dc/transact
             conn {:tx-data
                   [{:db/id "bob"
                     :local-id "Bob"
                     :occupation "Burger Slinger"}
                    {:db/id "linda"
                     :local-id "Linda"
                     :occupation "Restauranteur"}
                    {:db/id "jimmy"
                     :local-id "Jimmy Pesto Jr."
                     :occupation "Restauranteur"}
                    {:db/id "benjamin"
                     :remote-id "H. Jon Benjamin"}
                    {:db/id "roberts"
                     :remote-id "John Roberts"}]})
        tempids (:tempids txr)
        bob-id (tempids "bob")
        linda-id (tempids "linda")
        benjamin-id (tempids "benjamin")
        db (:db-after txr)]
    (testing "Validate on :db/id"
      (is (= {:found #{{:db/id bob-id}} :missing #{{:db/id -79}}}
             (#'nodes/validate-direct
              db #{{:db/id bob-id} {:db/id -79}}))))
    (testing "Get all nodes from source+target-list"
      (is (= {:found #{{:db/id bob-id :id-attr :local-id :value "Bob"}
                       {:db/id linda-id :id-attr :local-id :value "Linda"}}
              :missing #{{:id-attr :local-id :value "Weird Al"}
                         {:id-attr :local-id :value 42}}}
             (#'nodes/validate-value-lookup-by-attr
              db :local-id #{"Bob" "Linda" 42 "Weird Al"}))))
    (testing "Get all nodes from source+target-list"
      (is (= {:found #{{:db/id linda-id :id-attr :local-id :value "Linda"}}
              :missing #{{:id-attr :local-id :value "Weird Al"}}}
             (#'nodes/validate-lookup
              db #{{:id-attr :local-id :value "Linda"} {:id-attr :local-id :value "Weird Al"}}))))
    (testing "Get all nodes from config and source+target-list"
      (let [config {:attr-types {:local-id string? :remote-id string?}
                    :source {:id-attr :local-id}
                    :target {:id-attr :remote-id}}
            edge-set (nodes/apply-tempids
                      config (merge {-73 43} tempids)
                      [["Bob" "H. Jon Benjamin"]
                       [{:db/id -73} {:db/id "My Temporary ID"}]
                       ["Linda" {:db/id 42}]])]
        (is (= #{["Bob" "H. Jon Benjamin"]
                 [{:db/id 43 :mapping.nodes/tempid -73}
                  {:mapping.nodes/tempid "My Temporary ID"}]
                 ["Linda" {:db/id 42}]} edge-set))
        (is (= {:found #{{:db/id 43 :mapping.nodes/tempid -73}
                         {:db/id 42}
                         {:db/id bob-id :id-attr :local-id :value "Bob"}
                         {:db/id benjamin-id :id-attr :remote-id :value "H. Jon Benjamin"}
                         {:db/id linda-id :id-attr :local-id :value "Linda"}}
                :missing #{{:mapping.nodes/error
                            {:mapping.nodes/tempid "My Temporary ID"}}}
                :edges #{[bob-id benjamin-id]
                         [linda-id 42]}}
               (nodes/validate db config edge-set)))))))

(deftest populate-and-validate-test
  (let [conn (dc/connect udf/cfg)
        config {:attr-types {:local-id :string :remote-id :string}
                :source {:id-attr :local-id}
                :target {:id-attr :remote-id}}
        nodes [{:db/id "bob"
                :local-id "Bob"
                :occupation "Burger Slinger"}
               {:db/id "linda"
                :local-id "Linda"
                :occupation "Restauranteur"}
               {:db/id "jimmy"
                :local-id "Jimmy Pesto Jr."
                :occupation "Restauranteur"}
               {:db/id "benjamin"
                :remote-id "H. Jon Benjamin"}
               {:db/id "roberts"
                :remote-id "John Roberts"}]
        edges [["Bob" "H. Jon Benjamin"]
               ["Linda" {:db/id "roberts"}]
               [{:db/id -3} "not a :remote-id value"]]]
    (testing "Get all nodes from source+target-list"
      (let [{:keys [tempids db]} (nodes/populate conn nodes)
            edge-set (nodes/apply-tempids config tempids edges)]
        (is (= {:found #{{:id-attr :local-id :value "Bob" :db/id (tempids "bob")}
                         {:id-attr :local-id :value "Linda" :db/id (tempids "linda")}
                         {:id-attr :remote-id :value "H. Jon Benjamin" :db/id (tempids "benjamin")}
                         {:mapping.nodes/tempid "roberts" :db/id (tempids "roberts")}}
                :missing #{{:mapping.nodes/error {:mapping.nodes/tempid -3}}
                           {:id-attr :remote-id :value "not a :remote-id value"}}
                :edges #{[(tempids "bob") (tempids "benjamin")]
                         [(tempids "linda") (tempids "roberts")]}}
               (nodes/validate db config edge-set)))))))

(ns ^:mappings unit.jobtech-taxonomy.features.mapping.mapping-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (umc/database-with-schema udf/cfg))

(deftest validate-mapping-test
  (testing "Simplest mapping"
    (is (mapping/validate-mapping
         {:mapping.mapping/type :very-simple})))
  (testing "With name and ID"
    (is (mapping/validate-mapping
         {:mapping.mapping/name "A simple mapping"
          :mapping.mapping/type :basic
          :db/id 99})))
  (testing "Missing type"
    (let [no-type-mapping {:mapping.mapping/name "A simple mapping"
                           :db/id 99}]
      (is (not (mapping/validate-mapping no-type-mapping)))
      (is (= {:mapping.mapping/type ["missing required key"]}
             (mapping/explain-mapping-error no-type-mapping)))))
  (testing "Extra key"
    (let [extra-key-mapping {:mapping.mapping/type :basic
                             :not-supposed-to-be-here "value"
                             :db/id 99}]
      (is (not (mapping/validate-mapping extra-key-mapping)))
      (is (= {:not-supposed-to-be-here ["disallowed key"]}
             (mapping/explain-mapping-error extra-key-mapping))))))

(deftest validate-mapping-query-test
  (testing "Simplest mapping query"
    (is (mapping/validate-mapping-query {})))
  (testing "With name and ID"
    (is (mapping/validate-mapping-query
         {:mapping.mapping/name "A simple mapping"
          :mapping.mapping/type :basic
          :db/id 99})))
  (testing "Missing type"
    (let [no-type-mapping {:mapping.mapping/name "A simple mapping"
                           :db/id 99}]
      (is (mapping/validate-mapping-query no-type-mapping))
      (is (= {:mapping.mapping/type ["missing required key"]}
             (mapping/explain-mapping-error no-type-mapping)))))
  (testing "Extra key"
    (let [extra-key-mapping {:mapping.mapping/type :basic
                             :not-supposed-to-be-here "value"
                             :db/id 99}]
      (is (not (mapping/validate-mapping-query extra-key-mapping)))
      (is (= {:not-supposed-to-be-here ["disallowed key"]}
             (mapping/explain-mapping-error extra-key-mapping))))))

(deftest mapping->short-name-test
  (testing "Convert mapping to short names"
    (is (= {:name "A simple mapping" :type :basic :db/id 99}
           (mapping/mapping->short-names
            {:mapping.mapping/name "A simple mapping"
             :mapping.mapping/type :basic
             :db/id 99}))))
  (testing "Convert already short mapping to short names"
    (is (= {:name "A simple mapping" :type :basic :db/id 99}
           (mapping/mapping->short-names
            {:name "A simple mapping"
             :type :basic
             :db/id 99})))))

(deftest mapping->short-name-only-test
  (testing "Convert mapping to short names"
    (is (= {:name "A simple mapping" :type :basic}
           (mapping/mapping->short-names-only
            {:mapping.mapping/name "A simple mapping"
             :mapping.mapping/type :basic
             :long/name :is/long
             :db/id 99}))))
  (testing "Convert already short mapping to short names"
    (is (= {:name "A simple mapping" :type :basic}
           (mapping/mapping->short-names-only
            {:name "A simple mapping"
             :type :basic
             :long/name :is/long
             :db/id 99})))))

(deftest mapping->sort-order-test
  (testing "Get sort order vector from mapping"
    (is (= ["A simple mapping" :basic]
           (mapping/mapping->sort-order-vector
            {:mapping.mapping/name "A simple mapping"
             :mapping.mapping/type :basic
             :db/id 99}))))
  (testing "Get sort order vector from short name mapping"
    (is (= ["A simple organisation" "A simple mapping" :basic]
           (mapping/mapping->sort-order-vector
            {:name "A simple mapping"
             :type :basic
             :organisation "A simple organisation"})))))

(deftest mappings-query-test
  (let [conn (dc/connect udf/cfg)
        trx (dc/transact conn {:tx-data [{:db/id "my-mapping"
                                          :mapping.mapping/name "My Mapping"
                                          :mapping.mapping/type :my-mapping}]})
        mapping-id ((:tempids trx) "my-mapping")
        db (dc/db conn)]
    (testing "All"
      (is (= {:result :ok/all
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {}))))
    (testing "Filter"
      (is (= {:result :ok/filter
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:type :my-mapping})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:mapping.mapping/type :my-mapping})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:name "My Mapping"})))
      (is (= {:result :ok/filter
              :mappings #{}}
             (mapping/mappings db {:type :not-my-mapping})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:type :my-mapping
                                   :name "My Mapping"})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:mapping.mapping/type :my-mapping
                                   :mapping.mapping/name "My Mapping"}))))
    (testing "Specific"
      (is (= {:result :ok/specific
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:db/id mapping-id :name "My Mapping"})))
      (is (= {:result :ok/specific
              :mappings #{{:db/id mapping-id
                           :mapping.mapping/type :my-mapping
                           :mapping.mapping/name "My Mapping"}}}
             (mapping/mappings db {:db/id mapping-id
                                   :mapping.mapping/type :my-mapping
                                   :mapping.mapping/name "My Mapping"})))
      (is (= {:result :error/specific
              :query {:db/id mapping-id
                      :mapping.mapping/type :not-my-mapping
                      :mapping.mapping/name "My Mapping"}}
             (mapping/mappings db {:db/id mapping-id
                                   :type :not-my-mapping
                                   :mapping.mapping/name "My Mapping"}))))))

(deftest mappings-query-slug-test
  (let [conn (dc/connect udf/cfg)
        trx (dc/transact conn {:tx-data [{:db/id "first-id"
                                          :mapping.mapping/name "First Mapping"
                                          :mapping.mapping/slug "first"
                                          :mapping.mapping/type :first-mapping}
                                         {:db/id "second-id"
                                          :mapping.mapping/name "Second Mapping"
                                          :mapping.mapping/slug ["second" "other"]
                                          :mapping.mapping/type :second-mapping}
                                         {:db/id "third-id"
                                          :mapping.mapping/name "Third Mapping"
                                          :mapping.mapping/slug ["third" "other"]
                                          :mapping.mapping/type :third-mapping}]})
        first-id ((:tempids trx) "first-id")
        second-id ((:tempids trx) "second-id")
        third-id ((:tempids trx) "third-id")
        db (dc/db conn)]
    (testing "All"
      (is (= {:result :ok/all
              :mappings #{{:db/id first-id
                           :mapping.mapping/name "First Mapping"
                           :mapping.mapping/slug ["first"]
                           :mapping.mapping/type :first-mapping}
                          {:db/id second-id
                           :mapping.mapping/name "Second Mapping"
                           :mapping.mapping/slug ["other" "second"]
                           :mapping.mapping/type :second-mapping}
                          {:db/id third-id
                           :mapping.mapping/name "Third Mapping"
                           :mapping.mapping/slug ["other" "third"]
                           :mapping.mapping/type :third-mapping}}}
             (mapping/mappings db {}))))
    (testing "Filter by `slug`"
      (is (= {:result :ok/filter
              :mappings #{{:db/id first-id
                           :mapping.mapping/name "First Mapping"
                           :mapping.mapping/slug ["first"]
                           :mapping.mapping/type :first-mapping}}}
             (mapping/mappings db {:slug "first"})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id second-id
                           :mapping.mapping/name "Second Mapping"
                           :mapping.mapping/slug ["other" "second"]
                           :mapping.mapping/type :second-mapping}
                          {:db/id third-id
                           :mapping.mapping/name "Third Mapping"
                           :mapping.mapping/slug ["other" "third"]
                           :mapping.mapping/type :third-mapping}}}
             (mapping/mappings db {:slug "other"})))
      (is (= {:result :ok/filter
              :mappings #{{:db/id second-id
                           :mapping.mapping/name "Second Mapping"
                           :mapping.mapping/slug ["other" "second"]
                           :mapping.mapping/type :second-mapping}}}
             (mapping/mappings db {:slug ["other" "second"]}))))))
(ns ^:mappings unit.jobtech-taxonomy.features.mapping.create-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.create :as create]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [jobtech-taxonomy.features.mapping.query :as query]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"mapping.mappings.create"} :debug]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema udf/cfg))

(deftest create-empty-mappings-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create empty mapping"
      (let [first (create/create conn {:mapping {:name "Alpha"}})
            first-mapping (:mapping first)
            first-id (create/created->id first)
            second (create/create conn {:mapping {:name "Alpha"}})
            re-first (create/create conn {:mapping {:db/id first-id}})]
        (is (= {:result :ok/created-new
                :mapping first-mapping
                :edges {:created 0 :existing 0 :missing #{} :updated 0}
                :targets {:created 0}}
               first))
        (is (= {:result :error/mappings-found
                :mappings #{first-mapping}}
               (dissoc second :config)))
        (is (= {:result :ok/specific
                :mapping first-mapping
                :edges {:created 0 :existing 0 :missing #{} :updated 0}
                :targets {:created 0}}
               (dissoc re-first :config)))))))

(deftest create-mapping-with-config-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping with config"
      (let [first (create/mapping
                   conn
                   {:name "Alpha"}
                   {:source {:id :local-id}
                    :target {:id :remote-id}}
                   nil)
            first-id (create/created->id first)]
        (is (= {:result :ok/created-new
                :mapping {:db/id first-id
                          :mapping.mapping/name "Alpha"
                          :mapping.mapping/type :default
                          :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]]
                          :mapping.external/source-attribute :local-id
                          :mapping.external/target-attribute :remote-id}
                :config {:mapping {:db/id first-id :mapped-id :remote-id}
                         :attr-types {:db/id [:and :int [:> 0]]
                                      :remote-id :string
                                      :local-id :string}
                         :source {:id-attr :local-id
                                  :pull #{:local-id}
                                  :rename {}}
                         :target {:id-attr :remote-id
                                  :pull #{:remote-id}
                                  :rename {}}}}
               first))))))

(deftest create-mappings-with-different-version-test
  (let [conn (dc/connect udf/cfg)
        mapping-1950 (create/create
                      conn {:mapping
                            {:name "Skattkammarön"
                             :family "Epics"
                             :version "1950"}})
        mapping-1950-id (-> mapping-1950 :mapping :db/id)
        mapping-2012 (create/create
                      conn {:mapping
                            {:name "Skattkammarön"
                             :family "Epics"
                             :version "2012"}})
        mapping-2012-id (-> mapping-2012 :mapping :db/id)]
    (testing "Context of the test assertions"
      (is (= {:result :ok/created-new
              :mapping {:db/id mapping-1950-id
                        :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                        :mapping.external/source-attribute :db/id
                        :mapping.external/target-attribute :db/id
                        :mapping.mapping/family "Epics"
                        :mapping.mapping/name "Skattkammarön"
                        :mapping.mapping/type :default
                        :mapping.mapping/version "1950"}
              :edges {:created 0 :existing 0 :missing #{} :updated 0}
              :targets {:created 0}}
             mapping-1950))
      (is (= {:result :ok/created-new
              :mapping {:db/id mapping-2012-id
                        :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                        :mapping.external/source-attribute :db/id
                        :mapping.external/target-attribute :db/id
                        :mapping.mapping/family "Epics"
                        :mapping.mapping/name "Skattkammarön"
                        :mapping.mapping/type :default
                        :mapping.mapping/version "2012"}
              :edges {:created 0 :existing 0 :missing #{} :updated 0}
              :targets {:created 0}}
             mapping-2012)))))

(deftest create-and-find-mappings-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Verify that mappings can be created and listed"
      (let [mapping-one-info {:mapping {:name "A simple mapping"}}
            result-one (create/create conn mapping-one-info)
            mapping-one-id (create/created->id result-one)
            mapping-one (:mapping result-one)
            mapping-two-info {:mapping {:name "Another mapping" :type :fundamental}}
            result-two (create/create conn mapping-two-info)
            mapping-two-id (create/created->id result-two)
            mapping-two (:mapping result-two)]
        (is (= {:result :ok/created-new
                :mapping mapping-one
                :edges {:created 0 :existing 0 :missing #{} :updated 0}
                :targets {:created 0}}
               result-one))
        (is (= {:result :ok/specific
                :mappings #{{:mapping.mapping/name "A simple mapping"
                             :mapping.mapping/type :default
                             :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                             :mapping.external/source-attribute :db/id
                             :mapping.external/target-attribute :db/id
                             :db/id mapping-one-id}}}
               (mapping/mappings (dc/db conn) {:db/id mapping-one-id})))
        (is (= {:result :error/specific
                :query {:db/id mapping-one-id
                        :mapping.mapping/name "WRONG NAME"}}
               (mapping/mappings (dc/db conn)
                                 {:db/id mapping-one-id
                                  :name "WRONG NAME"})))
        (is (= {:result :ok/created-new
                :mapping mapping-two
                :edges {:created 0 :existing 0 :missing #{} :updated 0}
                :targets {:created 0}}
               result-two))
        (is (= {:result :ok/specific
                :mappings #{{:mapping.mapping/name "Another mapping"
                             :mapping.mapping/type :fundamental
                             :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                             :mapping.external/source-attribute :db/id
                             :mapping.external/target-attribute :db/id
                             :db/id mapping-two-id}}}
               (mapping/mappings (dc/db conn) {:db/id mapping-two-id})))
        (is (= {:result :ok/all
                :mappings #{{:mapping.mapping/name "A simple mapping"
                             :mapping.mapping/type :default
                             :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                             :mapping.external/source-attribute :db/id
                             :mapping.external/target-attribute :db/id
                             :db/id mapping-one-id}
                            {:mapping.mapping/name "Another mapping"
                             :mapping.mapping/type :fundamental
                             :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                             :mapping.external/source-attribute :db/id
                             :mapping.external/target-attribute :db/id
                             :db/id mapping-two-id}}}
               (update (mapping/mappings (dc/db conn) {}) :mappings set)))
        (testing "Find a mapping given some properties"
          (is (= {:result :ok/filter
                  :mappings #{mapping-one}}
                 (mapping/mappings
                  (dc/db conn)
                  {:mapping.mapping/name "A simple mapping"})))
          (is (= {:result :ok/filter
                  :mappings #{mapping-one}}
                 (mapping/mappings
                  (dc/db conn)
                  {:mapping.mapping/type :default})))
          (is (= {:result :ok/filter
                  :mappings #{mapping-two}}
                 (mapping/mappings
                  (dc/db conn)
                  {:mapping.mapping/type :fundamental}))))
        (testing "Creating the same mapping again yields an error and the old mapping"
          (let [result-retry (create/create conn mapping-one-info)]
            (is (= {:result :error/mappings-found
                    :mappings #{mapping-one}}
                   (dissoc result-retry :config)))
            (is (= {:result :ok/all
                    :mappings #{{:mapping.mapping/name "A simple mapping"
                                 :mapping.mapping/type :default
                                 :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                                 :mapping.external/source-attribute :db/id
                                 :mapping.external/target-attribute :db/id
                                 :db/id mapping-one-id}
                                {:mapping.mapping/name "Another mapping"
                                 :mapping.mapping/type :fundamental
                                 :mapping.external/attribute-mapping [[:mapping :mapped-id :mapping.external/id]]
                                 :mapping.external/source-attribute :db/id
                                 :mapping.external/target-attribute :db/id
                                 :db/id mapping-two-id}}}
                   (update (mapping/mappings (dc/db conn) {}) :mappings set)))))))))

(deftest create-mapping-with-edges-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-desc {:type :default
                          :organisation "The Mighty Pirates"
                          :family "Epics"
                          :name "Skattkammarön"
                          :version "1950"
                          :taxonomy-version "23"
                          :description "En film om en skattkammare på en ö."
                          :uri "https://www.imdb.com/title/tt0043067/"}
            config-desc {:source {:id :local-id
                                  :include {:occupation :role}}
                         :target {:id :remote-id
                                  :mapping :remote-id
                                  :include {:occupation :occupation}}}
            edges [["Presenter" "Walt Disney"]
                   ["Hispaniola" "Hispaniola"]
                   ["Long John Silver" "Robert Newton"]
                   ["Long John Silver" "L. Charles"]
                   ["Captain Smollett" "Basil Sydney"]]
            result (create/mapping conn mapping-desc config-desc nil)
            mapping (:mapping result)
            config (:config result)
            mapping-id (create/created->id result)
            edge-result (create/add-edges conn config edges)
            db (dc/db conn)]
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "1950"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:source :occupation :role]
                                                     [:target :occupation :occupation]]
                :db/id mapping-id}
               mapping))
        (is (= {:attr-types {:local-id :string
                             :remote-id :string
                             :db/id [:and :int [:> 0]]}
                :mapping {:mapped-id :remote-id
                          :db/id mapping-id}
                :source {:id-attr :local-id
                         :pull #{:local-id :occupation}
                         :rename {:occupation :role}}
                :target {:id-attr :remote-id
                         :pull #{:occupation :remote-id}
                         :rename {}}}
               config))
        (is (= 5 (-> edge-result :created count)))

        (is (= [{:mapped-id "L. Charles"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "L. Charles"
                          :occupation "Actor!"}}
                {:mapped-id "Robert Newton"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "Robert Newton"
                          :occupation "Not A Scientist Actually"}}]
               (query/display-targets db mapping "Long John Silver")))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}]
               (query/display-targets db mapping "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping "Hispaniola")))
        (is (= [] (query/display-targets db mapping "Jim Hawkins")))))))

(deftest create-mapping-from-edn-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-edn {:mapping
                         {:type :default
                          :organisation "The Mighty Pirates"
                          :family "Epics"
                          :name "Skattkammarön"
                          :version "1950"
                          :taxonomy-version "23"
                          :description "En film om en skattkammare på en ö."
                          :uri "https://www.imdb.com/title/tt0043067/"}
                         :config
                         {:source
                          {:id :local-id
                           :include
                           {:occupation :occupation}}
                          :target
                          {:id :remote-id
                           :mapping :remote-id
                           :include {:occupation :occupation}}}
                         :data
                         {:edges
                          [["Presenter" "Walt Disney"]
                           ["Hispaniola" "Hispaniola"]
                           ["Long John Silver" "Robert Newton"]
                           ["Long John Silver" "L. Charles"]
                           ["Captain Smollett" "Basil Sydney"]]}}
            result (create/create conn mapping-edn)
            mapping (:mapping result)
            mapping-id (create/created->id result)]
        (is (= {:result :ok/created-new
                :mapping mapping
                :edges {:created 5 :existing 0 :missing #{} :updated 0}
                :targets {:created 0}}
               result))
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "1950"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:source :occupation :occupation]
                                                     [:target :occupation :occupation]]
                :db/id mapping-id}
               mapping))
        (is (= [{:mapped-id "L. Charles"
                 :source {:local-id "Long John Silver"
                          :occupation "Mighty Pirate!"}
                 :target {:remote-id "L. Charles"
                          :occupation "Actor!"}}
                {:mapped-id "Robert Newton"
                 :source {:local-id "Long John Silver"
                          :occupation "Mighty Pirate!"}
                 :target {:remote-id "Robert Newton"
                          :occupation "Not A Scientist Actually"}}]
               (query/display-targets (dc/db conn) mapping "Long John Silver")))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}]
               (query/display-targets (dc/db conn) mapping "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets (dc/db conn) mapping "Hispaniola")))
        (is (= [] (query/display-targets (dc/db conn) mapping "Jim Hawkins")))))))
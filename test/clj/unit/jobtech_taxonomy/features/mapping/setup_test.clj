(ns ^:mappings unit.jobtech-taxonomy.features.mapping.setup-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.features.mapping.mapping :as mm]
            [jobtech-taxonomy.features.mapping.setup :as ms]))

(deftest mappings-config-test
  (testing "With config"
    (is (= {:source-directory "my mappings directory"}
           (#'ms/get-mapping-config
            {:mappings
             {:source-directory
              "my mappings directory"}}))))
  (testing "Without config"
    (is (not (#'ms/get-mapping-config {})))))

(deftest load-mapping-data-test
  (let [cfg {:source-directory "test/resources/mapping/maps"}]
    (testing "Find mapping data by config"
      (is (= [["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :broad-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :close-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :exact-match "16"]
              ["ESCO" "ESCO" "ESCO-occupations" "1.0.8" :narrow-match "16"]
              ["ESCO" "Eures" "occupation-name" "1.0.9" :esco-occupation "16"]
              ["ESCO" "Eures" "skill" "1.0.9" :esco-skill "21"]
              ["Eurostat" "Eures" "sni" "2007" :nace "22"]
              ["The Mighty Pirates" "Epics" "Skattkammarön" "1950" :default "23"]
              ["The Mighty Pirates" "Epics" "Skattkammarön" "2012" :default "23"]
              ["The Mighty Pirates" "Epics" "Skattkammarön" "STORY" :treasure->characters "23"]
              ["UNESCO" "Eures" "sun" "2013" :isced "1"]
              ["UNESCO" "Eures" "sun->isced-fields" "2013" :sun->isced-fields "1"]
              ["UNESCO" "Eures" "sun->isced-levels" "2011" :sun->isced-levels "1"]]
             (->> (#'ms/get-mapping-data cfg)
                  (map :mapping)
                  (map mm/mapping->sort-order-vector)
                  sort))))))

(ns ^:mappings unit.jobtech-taxonomy.features.mapping.schema-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy.api.db.database-connection :as dc]
   [jobtech-taxonomy.features.mapping.edge :as mapping.edge]
   [jobtech-taxonomy.features.mapping.mapping :as mapping.mapping]
   [jobtech-taxonomy.features.mapping.nodes :as mapping.nodes]
   [utils.db-fixtures :as udf]))

(defn test-transact [conn schema]
  (let [tx-result (dc/transact conn {:tx-data schema})
        new-db (:db-after tx-result)
        tx (->> tx-result :tx-data first :tx)]
    (is (= (count schema)
           (count
            (dc/q
             '[:find ?id
               :in $ ?tx
               :where
               [_ :db/ident ?id ?tx]]
             new-db tx))))))

(deftest create-mapping-schema-test
  (testing "Verify that the schema can be transacted"
    (try
      (dc/create-database udf/cfg)
      (let [conn (dc/connect udf/cfg)]
        (is (#{:datomic :datahike} (dc/database-backend conn)))
        (test-transact conn mapping.mapping/schema)
        (test-transact conn mapping.edge/schema)
        (test-transact conn mapping.nodes/external-schema))
      (finally
        (dc/delete-database udf/cfg)))))
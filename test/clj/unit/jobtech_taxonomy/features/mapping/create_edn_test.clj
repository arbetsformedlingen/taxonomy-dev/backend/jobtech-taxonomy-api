(ns ^:mappings unit.jobtech-taxonomy.features.mapping.create-edn-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.config :as config]
            [jobtech-taxonomy.features.mapping.create :as create]
            [jobtech-taxonomy.features.mapping.edge :as edge]
            [jobtech-taxonomy.features.mapping.mapping :as mapping]
            [jobtech-taxonomy.features.mapping.query :as query]
            [taoensso.timbre :as log]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"mapping.mappings.create"} :debug]
                   [#{"mapping.edge"} :debug]
                   [#{"*"} :info]]}
      (fun-run)))
  (umc/database-with-schema udf/cfg))

(defn- assert-or-explain [data]
  (is (= true
         (or
          (create/validate-mapping-edn data)
          (create/explain-mapping-edn-error data)))))

(deftest validate-test-data
  (testing "Validate Treasure Island 1950"
    (assert-or-explain (umc/treasure-island-1950)))
  (testing "Validate Treasure Island 2012"
    (assert-or-explain (umc/treasure-island-2012)))
  (testing "Validate ESCO"
    (assert-or-explain (umc/esco-mappings)))
  (testing "Validate Mapping for new targets"
    (assert-or-explain (umc/mapping-for-new-targets)))
  (testing "Validate SUN to ISCED levels"
    (assert-or-explain (umc/sun-to-isced-levels)))
  (testing "Validate SUN to ISCED fields"
    (assert-or-explain (umc/sun-to-isced-fields))))

(deftest create-mapping-from-edn-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-edn {:mapping
                         {:type :default
                          :organisation "The Mighty Pirates"
                          :family "Epics"
                          :name "Skattkammarön"
                          :version "1950"
                          :taxonomy-version "23"
                          :description "En film om en skattkammare på en ö."
                          :uri "https://www.imdb.com/title/tt0043067/"}
                         :config
                         {:source
                          {:id :local-id
                           :include
                           {:occupation :occupation}}
                          :target
                          {:id :remote-id
                           :mapping :remote-id
                           :include {:occupation :occupation}}}
                         :data
                         {:edges
                          [["Presenter" "Walt Disney"]
                           ["Hispaniola" "Hispaniola"]
                           ["Long John Silver" "Robert Newton"]
                           ["Long John Silver" "L. Charles"]
                           ["Captain Smollett" "Basil Sydney"]]}}
            result (create/create conn mapping-edn)
            mapping (:mapping result)
            mapping-id (create/created->id result)
            db (dc/db conn)]
        (is (= {:result :ok/created-new
                :mapping mapping
                :targets {:created 0}
                :edges {:created 5 :existing 0 :missing #{} :updated 0}}
               result))
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "1950"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:source :occupation :occupation]
                                                     [:target :occupation :occupation]]
                :db/id mapping-id}
               mapping))
        (is (= [{:mapped-id "L. Charles"
                 :source {:local-id "Long John Silver"
                          :occupation "Mighty Pirate!"}
                 :target {:remote-id "L. Charles"
                          :occupation "Actor!"}}
                {:mapped-id "Robert Newton"
                 :source {:local-id "Long John Silver"
                          :occupation "Mighty Pirate!"}
                 :target {:remote-id "Robert Newton"
                          :occupation "Not A Scientist Actually"}}]
               (query/display-targets db mapping "Long John Silver")))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}]
               (query/display-targets db mapping "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping "Hispaniola")))
        (is (= [] (query/display-targets db mapping "Jim Hawkins")))))))

(deftest create-mapping-and-targets-edn-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping with new targets from EDN"
      (let [mapping-edn (umc/mapping-for-new-targets)
            result (create/create conn mapping-edn)
            mapping (:mapping result)
            mapping-id (create/created->id result)]
        (is (= {:result :ok/created-new
                :mapping {:db/id mapping-id
                          :mapping.mapping/type :treasure->characters
                          :mapping.mapping/organisation "The Mighty Pirates"
                          :mapping.mapping/family "Epics"
                          :mapping.mapping/name "Skattkammarön"
                          :mapping.mapping/version "STORY"
                          :mapping.mapping/taxonomy-version "23"
                          :mapping.mapping/description "Mapping to character information."
                          :mapping.mapping/uri "https://www.example.com"
                          :mapping.external/source-attribute :local-id
                          :mapping.external/target-attribute :db/id
                          :mapping.external/attribute-mapping
                          [[:mapping :mapped-id :mapping.external/id]
                           [:source :occupation :role]
                           [:target :mapping.external/annotations :mapping.external/annotations]
                           [:target :mapping.external/id :mapping.external/id]
                           [:target :mapping.external/key-value-data :mapping.external/key-value-data]
                           [:target :mapping.external/uri :mapping.external/uri]]}
                :targets {:created 5}
                :edges {:created 6 :updated 0 :existing 0 :missing #{}}}
               result))
        (is (= {:result :ok/specific
                :mappings #{{:db/id mapping-id
                             :mapping.mapping/type :treasure->characters
                             :mapping.mapping/organisation "The Mighty Pirates"
                             :mapping.mapping/family "Epics"
                             :mapping.mapping/name "Skattkammarön"
                             :mapping.mapping/version "STORY"
                             :mapping.mapping/taxonomy-version "23"
                             :mapping.mapping/description "Mapping to character information."
                             :mapping.mapping/uri "https://www.example.com"
                             :mapping.external/source-attribute :local-id
                             :mapping.external/target-attribute :db/id
                             :mapping.external/attribute-mapping
                             [[:mapping :mapped-id :mapping.external/id]
                              [:source :occupation :role]
                              [:target :mapping.external/annotations :mapping.external/annotations]
                              [:target :mapping.external/id :mapping.external/id]
                              [:target :mapping.external/key-value-data :mapping.external/key-value-data]
                              [:target :mapping.external/uri :mapping.external/uri]]}}}
               (mapping/mappings (dc/db conn) {:db/id mapping-id})))
        (is (= {:mapping ["The Mighty Pirates" "Epics" "Skattkammarön" "STORY" :treasure->characters "23"]
                :targets [{:mapped-id "Treasure Island / Long John Silver"
                           :source {:local-id "Long John Silver"
                                    :role "Mighty Pirate!"}
                           :target {:mapping.external/id "Treasure Island / Long John Silver"
                                    :mapping.external/uri "https://pirate-bay.ti/barbecue"
                                    :mapping.external/key-value-data [[:drink "Grog"]
                                                                      [:label "The Other Scourge of the Seas"]
                                                                      [:legs "1"]
                                                                      [:motto "Yo Ho Ho!"]
                                                                      [:parrot "yes"]]}}]}
               (first
                (umc/display-lookup
                 (query/edges
                  (dc/db conn)
                  #{mapping}
                  ["Long John Silver"])))))))))

(deftest create-mapping-1950-from-edn-file-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-edn (umc/treasure-island-1950)
            result (create/create conn mapping-edn)
            mapping (:mapping result)
            mapping-id (create/created->id result)
            db (dc/db conn)]
        (is (= {:result :ok/created-new
                :mapping mapping
                :targets {:created 0}
                :edges {:created 8 :existing 0 :missing #{} :updated 0}}
               result))
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "1950"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:source :occupation :role]
                                                     [:target :occupation :occupation]]
                :db/id mapping-id}
               mapping))
        (is (= [{:mapped-id "L. Charles"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "L. Charles"
                          :occupation "Actor!"}}
                {:mapped-id "Robert Newton"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "Robert Newton"
                          :occupation "Not A Scientist Actually"}}]
               (query/display-targets db mapping "Long John Silver")))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}
                {:mapped-id "Fred Tooze"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Fred Tooze"}}]
               (query/display-targets db mapping "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping "Hispaniola")))
        (is (= [{:mapped-id "Bobby Driscoll"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Bobby Driscoll"}}
                {:mapped-id "Michael Croudson"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Michael Croudson"}}]
               (query/display-targets db mapping "Jim Hawkins")))))))

(deftest create-mapping-2012-from-edn-file-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-edn (umc/treasure-island-2012)
            result (create/create conn mapping-edn)
            mapping (:mapping result)
            mapping-id (create/created->id result)
            db (dc/db conn)]
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "2012"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En helt ny film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt1820723/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:target
                                                      :mapping.external/annotations
                                                      :mapping.external/annotations]
                                                     [:target :mapping.external/id :mapping.external/id]
                                                     [:target
                                                      :mapping.external/key-value-data
                                                      :mapping.external/key-value-data]]
                :db/id mapping-id}
               mapping))
        (is (= [{:mapped-id "Eddie Izzard"
                 :source {:local-id "Long John Silver"}
                 :target {:remote-id "Eddie Izzard"}}]
               (query/display-targets db mapping "Long John Silver")))
        (is (= [{:mapped-id "Philip Glenister"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Philip Glenister"}}]
               (query/display-targets db mapping "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping "Hispaniola")))
        (is (= [{:mapped-id "Toby Regbo"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Toby Regbo"}}]
               (query/display-targets db mapping "Jim Hawkins")))
        (is (= [{:mapped-id "N/A"
                 :source {:local-id "Presenter"}
                 :target {:mapping.external/id "N/A"
                          :mapping.external/annotations ["This is source has no mapping."]
                          :mapping.external/key-value-data [[:label "Not Mapped"]]}}]
               (query/display-targets db mapping "Presenter")))
        (is (= {:mapping mapping
                :found #{}
                :missing #{"Bloody Mary"}}
               (-> (query/edges (dc/db conn) #{mapping} ["Bloody Mary"])
                   :lookup first)))))))

(deftest create-mapping-both-from-edn-file-test
  (let [conn (dc/connect udf/cfg)]
    (testing "Create mapping from EDN"
      (let [mapping-1950-edn (umc/treasure-island-1950)
            result-1950 (create/create conn mapping-1950-edn)
            mapping-1950-id (create/created->id result-1950)
            mapping-1950 (:mapping result-1950)
            hispaniola-role-id (umc/pull-id conn [:local-id "Hispaniola"])
            hispaniola-actor-id (umc/pull-id conn [:remote-id "Hispaniola"])
            hispaniola-edge {:db/id true
                             :mapping.edge/mapping #{mapping-1950-id}
                             :mapping.edge/source (umc/pull-id conn [:local-id "Hispaniola"])
                             :mapping.edge/target (umc/pull-id conn [:remote-id "Hispaniola"])}
            mapping-2012-edn (umc/treasure-island-2012)
            result-2012 (create/create conn mapping-2012-edn :full)
            mapping-2012-id (create/created->id result-2012)
            mapping-2012 (:mapping result-2012)

            presenter-edge {:db/id true
                            :mapping.edge/mapping #{mapping-2012-id}
                            :mapping.edge/source (umc/pull-id conn [:local-id "Presenter"])
                            :mapping.edge/target (-> result-2012 :targets :created first :db/id)}

            ljs-edge {:db/id true
                      :mapping.edge/mapping #{mapping-2012-id}
                      :mapping.edge/source (umc/pull-id conn [:local-id "Long John Silver"])
                      :mapping.edge/target (umc/pull-id conn [:remote-id "Eddie Izzard"])}

            cs-edge {:db/id true
                     :mapping.edge/mapping #{mapping-2012-id}
                     :mapping.edge/source (umc/pull-id conn [:local-id "Captain Smollett"])
                     :mapping.edge/target (umc/pull-id conn [:remote-id "Philip Glenister"])}

            jh-edge {:db/id true
                     :mapping.edge/mapping #{mapping-2012-id}
                     :mapping.edge/source (umc/pull-id conn [:local-id "Jim Hawkins"])
                     :mapping.edge/target (umc/pull-id conn [:remote-id "Toby Regbo"])}
            db (dc/db conn)]
        (is (= {:result :ok/created-new
                :mapping mapping-1950
                :targets {:created 0}
                :edges {:created 8 :existing 0 :missing #{} :updated 0}}
               result-1950))
        (is (= {:result :ok/created-new
                :mapping mapping-2012
                :targets {:created #{{:db/id true
                                      :mapping.external/annotations ["This is source has no mapping."]
                                      :mapping.external/id "N/A"
                                      :mapping.external/key-value-data [[:label "Not Mapped"]]
                                      :mapping.nodes/tempid "not-mapped"}}}
                :edges {:created #{presenter-edge ljs-edge cs-edge jh-edge}
                        :existing #{}
                        :missing #{}
                        :updated #{(-> hispaniola-edge
                                       (update :mapping.edge/mapping #(conj % mapping-2012-id)))}}}
               (-> result-2012
                   (update-in [:targets :created] #(into #{} (map (fn [e] (update e :db/id pos-int?))) %))
                   (update-in [:edges :created] #(into #{} (map (fn [e] (update e :db/id pos-int?))) %))
                   (update-in [:edges :updated] #(into #{} (map (fn [e] (update e :db/id pos-int?))) %)))))
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "1950"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:source :occupation :role]
                                                     [:target :occupation :occupation]]
                :db/id mapping-1950-id}
               mapping-1950))
        (is (= {:mapping.mapping/name "Skattkammarön"
                :mapping.mapping/family "Epics"
                :mapping.mapping/version "2012"
                :mapping.mapping/taxonomy-version "23"
                :mapping.mapping/type :default
                :mapping.mapping/description "En helt ny film om en skattkammare på en ö."
                :mapping.mapping/uri "https://www.imdb.com/title/tt1820723/"
                :mapping.mapping/organisation "The Mighty Pirates"
                :mapping.external/target-attribute :remote-id
                :mapping.external/source-attribute :local-id
                :db/id mapping-2012-id
                :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                     [:target
                                                      :mapping.external/annotations
                                                      :mapping.external/annotations]
                                                     [:target :mapping.external/id :mapping.external/id]
                                                     [:target
                                                      :mapping.external/key-value-data
                                                      :mapping.external/key-value-data]]}
               mapping-2012))
        (is (= {:missing #{}
                :members #{}
                :edges #{(update hispaniola-edge :mapping.edge/mapping #(conj % mapping-2012-id))}}
               (update (edge/edges-between db nil #{[hispaniola-role-id hispaniola-actor-id]})
                       :edges #(into #{} (map (fn [e] (update e :db/id pos-int?))) %))))
        (is (= [{:mapped-id "L. Charles"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "L. Charles"
                          :occupation "Actor!"}}
                {:mapped-id "Robert Newton"
                 :source {:local-id "Long John Silver"
                          :role "Mighty Pirate!"}
                 :target {:remote-id "Robert Newton"
                          :occupation "Not A Scientist Actually"}}]
               (query/display-targets db mapping-1950 "Long John Silver")))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}
                {:mapped-id "Fred Tooze"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Fred Tooze"}}]
               (query/display-targets db mapping-1950 "Captain Smollett")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping-1950 "Hispaniola")))
        (is (= [{:mapped-id "Hispaniola"
                 :source {:local-id "Hispaniola"}
                 :target {:remote-id "Hispaniola"}}]
               (query/display-targets db mapping-2012 "Hispaniola")))
        (is (= [{:mapped-id "Bobby Driscoll"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Bobby Driscoll"}}
                {:mapped-id "Michael Croudson"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Michael Croudson"}}]
               (query/display-targets db mapping-1950 "Jim Hawkins")))

        (is (= (set [{:mapping "1950"
                      :targets ["Bobby Driscoll" "Michael Croudson"]}
                     {:mapping "2012"
                      :targets ["Toby Regbo"]}])
               (set (map (fn [f] {:mapping (get-in f [:mapping :mapping.mapping/version])
                                  :targets (map :mapped-id (:found f))})
                         (:lookup (query/edges
                                   (dc/db conn)
                                   #{mapping-1950 mapping-2012}
                                   ["Jim Hawkins"]))))))))))

(deftest create-from-edn-fails-if-mapping-exists-test
  (let [conn (dc/connect udf/cfg)
        mapping-edn {:mapping
                     {:type :default
                      :organisation "The Mighty Pirates"
                      :family "Epics"
                      :name "Skattkammarön"
                      :version "1950"
                      :taxonomy-version "23"
                      :description "En film om en skattkammare på en ö."
                      :uri "https://www.imdb.com/title/tt0043067/"}
                     :config
                     {:source
                      {:id :local-id
                       :include
                       {:occupation :occupation}}
                      :target
                      {:id :remote-id
                       :mapping :remote-id
                       :include {:occupation :occupation}}}
                     :data
                     {:edges
                      [["Presenter" "Walt Disney"]
                       ["Hispaniola" "Hispaniola"]
                       ["Long John Silver" "Robert Newton"]
                       ["Long John Silver" "L. Charles"]
                       ["Captain Smollett" "Basil Sydney"]]}}
        mapping-id (create/created->id (create/create conn mapping-edn))]
    (testing "Fail with existing mapping if `:db/id` not provided"
      (is (= {:result :error/mappings-found
              :mappings #{{:mapping.mapping/name "Skattkammarön"
                           :mapping.mapping/family "Epics"
                           :mapping.mapping/version "1950"
                           :mapping.mapping/taxonomy-version "23"
                           :mapping.mapping/type :default
                           :mapping.mapping/description "En film om en skattkammare på en ö."
                           :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                           :mapping.mapping/organisation "The Mighty Pirates"
                           :mapping.external/target-attribute :remote-id
                           :mapping.external/source-attribute :local-id
                           :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                                [:source :occupation :occupation]
                                                                [:target :occupation :occupation]]
                           :db/id mapping-id}}}
             (dissoc (create/create conn mapping-edn) :config))))))

(deftest update-mapping-from-edn-test
  (let [conn (dc/connect udf/cfg)
        mapping-edn {:mapping
                     {:type :default
                      :organisation "The Mighty Pirates"
                      :family "Epics"
                      :name "Skattkammarön"
                      :version "1950"
                      :taxonomy-version "23"
                      :description "En film om en skattkammare på en ö."
                      :uri "https://www.imdb.com/title/tt0043067/"}
                     :config
                     {:source
                      {:id :local-id
                       :include
                       {:occupation :occupation}}
                      :target
                      {:id :remote-id
                       :mapping :remote-id
                       :include {:occupation :occupation}}}
                     :data
                     {:edges
                      [["Presenter" "Walt Disney"]
                       ["Hispaniola" "Hispaniola"]
                       ["Long John Silver" "Robert Newton"]
                       ["Long John Silver" "L. Charles"]
                       ["Captain Smollett" "Basil Sydney"]]}}]
    (testing "Update mapping from EDN with :db/id"
      (let [mapping-result (create/create conn mapping-edn)
            mapping-id (create/created->id mapping-result)
            mapping-config (config/mapping->config (dc/db conn) (:mapping mapping-result))
            character-cs-id (umc/pull-id conn [:local-id "Captain Smollett"])
            actor-1950-cs-id (umc/pull-id conn [:remote-id "Basil Sydney"])
            existing-edge (edge/edges-between
                           (dc/db conn) mapping-id #{[character-cs-id actor-1950-cs-id]})
            existing-edge-id (:db/id (first (:members existing-edge)))
            update-mapping-edn (-> (assoc-in mapping-edn [:mapping :db/id] mapping-id)
                                   (assoc :data {:edges
                                                 [["Captain Smollett" "Basil Sydney"]
                                                  ["Captain Smollett" "Fred Tooze"]
                                                  ["Jim Hawkins" "Bobby Driscoll"]
                                                  ["Jim Hawkins" "Michael Croudson"]]}))
            update-mapping-result (create/create conn update-mapping-edn)]
        (is (= {:attr-types {:local-id :string
                             :remote-id :string
                             :db/id [:and :int [:> 0]]}
                :mapping {:mapped-id :remote-id
                          :db/id mapping-id}
                :source {:id-attr :local-id
                         :pull #{:local-id :occupation}
                         :rename {}},
                :target {:id-attr :remote-id
                         :pull #{:occupation :remote-id}
                         :rename {}}}
               mapping-config))
        (is (= {:result :ok/created-new
                :mapping {:db/id mapping-id
                          :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                               [:source :occupation :occupation]
                                                               [:target :occupation :occupation]]
                          :mapping.external/source-attribute :local-id
                          :mapping.external/target-attribute :remote-id
                          :mapping.mapping/description "En film om en skattkammare på en ö."
                          :mapping.mapping/family "Epics"
                          :mapping.mapping/name "Skattkammarön"
                          :mapping.mapping/organisation "The Mighty Pirates"
                          :mapping.mapping/taxonomy-version "23"
                          :mapping.mapping/type :default
                          :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                          :mapping.mapping/version "1950"}
                :edges {:created 5
                        :existing 0
                        :missing #{}
                        :updated 0}
                :targets {:created 0}}
               mapping-result))
        (is (= {:edges #{}
                :members #{{:db/id existing-edge-id
                            :mapping.edge/mapping #{mapping-id}
                            :mapping.edge/source character-cs-id
                            :mapping.edge/target actor-1950-cs-id}}
                :missing #{}}
               existing-edge))
        (is (= {:result :ok/specific
                :mapping {:db/id mapping-id
                          :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                               [:source :occupation :occupation]
                                                               [:target :occupation :occupation]]
                          :mapping.external/source-attribute :local-id
                          :mapping.external/target-attribute :remote-id
                          :mapping.mapping/description "En film om en skattkammare på en ö."
                          :mapping.mapping/family "Epics"
                          :mapping.mapping/name "Skattkammarön"
                          :mapping.mapping/organisation "The Mighty Pirates"
                          :mapping.mapping/taxonomy-version "23"
                          :mapping.mapping/type :default
                          :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                          :mapping.mapping/version "1950"}
                :edges {:created 3
                        :existing 1
                        :missing #{}
                        :updated 0}
                :targets {:created 0}}
               update-mapping-result))
        (is (= {:result :ok/all
                :mappings
                #{{:mapping.mapping/name "Skattkammarön"
                   :mapping.mapping/family "Epics"
                   :mapping.mapping/version "1950"
                   :mapping.mapping/taxonomy-version "23"
                   :mapping.mapping/type :default
                   :mapping.mapping/description "En film om en skattkammare på en ö."
                   :mapping.mapping/uri "https://www.imdb.com/title/tt0043067/"
                   :mapping.mapping/organisation "The Mighty Pirates"
                   :mapping.external/target-attribute :remote-id
                   :mapping.external/source-attribute :local-id
                   :mapping.external/attribute-mapping [[:mapping :mapped-id :remote-id]
                                                        [:source :occupation :occupation]
                                                        [:target :occupation :occupation]]
                   :db/id mapping-id}}}
               (mapping/mappings (dc/db conn) {})))
        (is (= [{:mapped-id "Basil Sydney"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Basil Sydney"}}
                {:mapped-id "Fred Tooze"
                 :source {:local-id "Captain Smollett"}
                 :target {:remote-id "Fred Tooze"}}]
               (query/display-targets (dc/db conn) {} "Captain Smollett")))
        (is (= [{:mapped-id "Bobby Driscoll"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Bobby Driscoll"}}
                {:mapped-id "Michael Croudson"
                 :source {:local-id "Jim Hawkins"}
                 :target {:remote-id "Michael Croudson"}}]
               (query/display-targets (dc/db conn) {} "Jim Hawkins")))))))
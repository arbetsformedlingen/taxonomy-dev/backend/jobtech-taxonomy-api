(ns ^:mappings unit.jobtech-taxonomy.features.mapping.edge-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.features.mapping.edge :as edge]
            [utils.db-fixtures :as udf]
            [utils.mapping.common :as umc]))

(test/use-fixtures :each
  (umc/database-with-schema udf/cfg))

(deftest edges-between-test
  (let [conn (dc/connect udf/cfg)
        txr (dc/transact
             conn {:tx-data
                   [{:db/id "bob"
                     :local-id "Bob"
                     :occupation "Burger Slinger"}
                    {:db/id "linda"
                     :local-id "Linda"
                     :occupation "Restauranteur"}
                    {:db/id "hugo"
                     :local-id "Hugo"
                     :occupation "Sanitation Fiend"}
                    {:db/id "married-mapping"
                     :mapping.mapping/type :married}
                    {:db/id "edge-bl"
                     :mapping.edge/source {:db/id "bob"}
                     :mapping.edge/mapping [{:db/id "married-mapping"}]
                     :mapping.edge/target {:db/id "linda"}}
                    {:db/id "knows-mapping"
                     :mapping.mapping/type :knows}
                    {:db/id "edge-knows-lh"
                     :mapping.edge/source {:db/id "linda"}
                     :mapping.edge/mapping [{:db/id "knows-mapping"}]
                     :mapping.edge/target {:db/id "hugo"}}]})
        db (:db-after txr)
        tempids (:tempids txr)
        knows-mapping-id (tempids "knows-mapping")
        married-mapping-id (tempids "married-mapping")
        edge-bl-id (tempids "edge-bl")
        edge-knows-lh (tempids "edge-knows-lh")
        linda-id (tempids "linda")
        bob-id (tempids "bob")
        hugo-id (tempids "hugo")]
    (testing "Get all edges for mapping"
      (is (= {:members #{} :edges #{} :missing #{}}
             (edge/edges-between db nil #{})))
      (is (= {:members #{}
              :edges #{{:db/id edge-bl-id
                        :mapping.edge/source bob-id
                        :mapping.edge/mapping #{married-mapping-id}
                        :mapping.edge/target linda-id}}
              :missing #{}}
             (edge/edges-between db nil #{[bob-id linda-id]})))
      (is (= {:members #{{:db/id edge-knows-lh
                          :mapping.edge/mapping #{knows-mapping-id}
                          :mapping.edge/source linda-id
                          :mapping.edge/target hugo-id}}
              :edges #{{:db/id edge-bl-id
                        :mapping.edge/source bob-id
                        :mapping.edge/mapping #{married-mapping-id}
                        :mapping.edge/target linda-id}}
              :missing #{[linda-id bob-id]}}
             (edge/edges-between
              db knows-mapping-id
              #{[linda-id bob-id]
                [bob-id linda-id]
                [linda-id hugo-id]}))))))

(deftest add-edges-test
  (let [conn (dc/connect udf/cfg)
        txr (dc/transact
             conn {:tx-data
                   [{:db/id "bob"
                     :local-id "Bob"
                     :occupation "Burger Slinger"}
                    {:db/id "linda"
                     :local-id "Linda"
                     :occupation "Restauranteur"}
                    {:db/id "hugo"
                     :local-id "Hugo"
                     :occupation "Sanitation Fiend"}
                    {:db/id "married-mapping"
                     :mapping.mapping/type :married}
                    {:db/id "edge-bl"
                     :mapping.edge/source {:db/id "bob"}
                     :mapping.edge/mapping [{:db/id "married-mapping"}]
                     :mapping.edge/target {:db/id "linda"}}
                    {:db/id "knows-mapping"
                     :mapping.mapping/type :knows}
                    {:db/id "edge-knows-lh"
                     :mapping.edge/source {:db/id "linda"}
                     :mapping.edge/mapping [{:db/id "knows-mapping"}]
                     :mapping.edge/target {:db/id "hugo"}}]})
        tempids (:tempids txr)
        knows-mapping-id (tempids "knows-mapping")
        married-mapping-id (tempids "married-mapping")
        edge-bl-id (tempids "edge-bl")
        edge-knows-lh (tempids "edge-knows-lh")
        linda-id (tempids "linda")
        bob-id (tempids "bob")
        hugo-id (tempids "hugo")]
    (testing "Add three edges for mapping"
      (is (= {:members #{{:db/id edge-knows-lh
                          :mapping.edge/mapping #{knows-mapping-id}
                          :mapping.edge/source linda-id
                          :mapping.edge/target hugo-id}}
              :edges #{{:db/id edge-bl-id
                        :mapping.edge/source bob-id
                        :mapping.edge/mapping #{married-mapping-id}
                        :mapping.edge/target linda-id}}
              :missing #{[linda-id bob-id]}}
             (edge/edges-between (dc/db conn) knows-mapping-id #{[linda-id bob-id]
                                                                 [bob-id linda-id]
                                                                 [linda-id hugo-id]})))
      (let [add-result (edge/add-edges
                        conn
                        knows-mapping-id
                        #{[linda-id bob-id]
                          [bob-id linda-id]
                          [linda-id hugo-id]})
            created-id (-> add-result :created first :db/id)]
        (is (= {:existing #{{:db/id edge-knows-lh
                             :mapping.edge/mapping #{knows-mapping-id}
                             :mapping.edge/source linda-id
                             :mapping.edge/target hugo-id}}
                :updated #{{:db/id edge-bl-id
                            :mapping.edge/source bob-id
                            :mapping.edge/mapping #{knows-mapping-id married-mapping-id}
                            :mapping.edge/target linda-id}}
                :created #{{:db/id created-id
                            :mapping.edge/mapping #{knows-mapping-id}
                            :mapping.edge/source linda-id
                            :mapping.edge/target bob-id}}}
               add-result))
        (is (= {:members #{{:db/id edge-knows-lh
                            :mapping.edge/mapping #{knows-mapping-id}
                            :mapping.edge/source linda-id
                            :mapping.edge/target hugo-id}
                           {:db/id edge-bl-id
                            :mapping.edge/source bob-id
                            :mapping.edge/mapping #{married-mapping-id knows-mapping-id}
                            :mapping.edge/target linda-id}
                           {:db/id created-id
                            :mapping.edge/mapping #{knows-mapping-id}
                            :mapping.edge/source linda-id
                            :mapping.edge/target bob-id}}
                :edges #{}
                :missing #{}}
               (edge/edges-between (dc/db conn) knows-mapping-id #{[linda-id bob-id]
                                                                   [bob-id linda-id]
                                                                   [linda-id hugo-id]})))
        (testing "Adding again should return existing edges and not create new ones."
          (let [add-result (edge/add-edges
                            conn
                            knows-mapping-id
                            #{[linda-id bob-id]
                              [bob-id linda-id]
                              [linda-id hugo-id]})]
            (is (= {:existing #{{:db/id edge-knows-lh
                                 :mapping.edge/mapping #{knows-mapping-id}
                                 :mapping.edge/source linda-id
                                 :mapping.edge/target hugo-id}
                                {:db/id edge-bl-id
                                 :mapping.edge/source bob-id
                                 :mapping.edge/mapping #{knows-mapping-id married-mapping-id}
                                 :mapping.edge/target linda-id}
                                {:db/id created-id
                                 :mapping.edge/mapping #{knows-mapping-id}
                                 :mapping.edge/source linda-id
                                 :mapping.edge/target bob-id}}
                    :updated #{}
                    :created #{}}
                   add-result))
            (is (= {:members #{{:db/id edge-knows-lh
                                :mapping.edge/mapping #{knows-mapping-id}
                                :mapping.edge/source linda-id
                                :mapping.edge/target hugo-id}
                               {:db/id edge-bl-id
                                :mapping.edge/source bob-id
                                :mapping.edge/mapping #{married-mapping-id knows-mapping-id}
                                :mapping.edge/target linda-id}
                               {:db/id created-id
                                :mapping.edge/mapping #{knows-mapping-id}
                                :mapping.edge/source linda-id
                                :mapping.edge/target bob-id}}
                    :edges #{}
                    :missing #{}}
                   (edge/edges-between
                    (dc/db conn)
                    knows-mapping-id
                    #{[linda-id bob-id]
                      [bob-id linda-id]
                      [linda-id hugo-id]})))))))))

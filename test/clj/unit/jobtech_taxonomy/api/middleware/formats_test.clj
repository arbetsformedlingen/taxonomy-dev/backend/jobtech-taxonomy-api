(ns unit.jobtech-taxonomy.api.middleware.formats-test
  (:require [clojure.test :refer [deftest is]]
            [cognitect.transit :as transit]
            [jobtech-taxonomy.api.middleware.formats :as formats])
  (:import [java.io ByteArrayOutputStream ByteArrayInputStream]
           [java.time LocalTime LocalDate
            LocalDateTime ZonedDateTime
            ZoneId YearMonth]))

(set! *warn-on-reflection* true)

(defn encode [data]
  (let [out (ByteArrayOutputStream. 4096)
        writer (transit/writer
                out :json
                {:handlers
                 (-> #'formats/time-serialization-handlers
                     deref
                     :handlers
                     transit/write-handler-map)})]
    (transit/write writer data)
    (.toByteArray out)))

(defn decode [bytes]
  (let [in (ByteArrayInputStream. bytes)
        reader (transit/reader
                in :json
                {:handlers
                 (-> #'formats/time-deserialization-handlers
                     deref
                     :handlers
                     transit/read-handler-map)})]
    (transit/read reader)))

(deftest transit-round-trip-test
  (doseq [[x keyfn] [[(LocalTime/of 1 2 3 4)
                      LocalTime/.toSecondOfDay]
                     [(LocalDate/of 2004 6 3)
                      LocalDate/.getDayOfYear]
                     [(LocalDateTime/of 2009 3 5 18 20 4)
                      (fn [^LocalDateTime x]
                        [(.getYear x)
                         (.getDayOfYear x)
                         (.getHour x)
                         (.getMinute x)
                         (.getSecond x)])]
                     [(ZonedDateTime/of
                       2013 7 4 1 3 4 5
                       (ZoneId/of "Europe/Vilnius"))
                      (fn [^ZonedDateTime x]
                        [(.getYear x)
                         (.getDayOfYear x)
                         (.getHour x)
                         (.getMinute x)
                         (.getSecond x)
                         (.getOffset x)])]
                     [(YearMonth/of 2024 7)
                      YearMonth/.toString]]]
    (is (-> x
            encode
            decode
            keyfn
            (= (keyfn x))))))

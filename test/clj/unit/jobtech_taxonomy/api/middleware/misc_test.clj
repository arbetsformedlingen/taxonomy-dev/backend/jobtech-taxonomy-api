(ns unit.jobtech-taxonomy.api.middleware.misc-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.middleware.misc :as misc]))

(deftest multi-trim-test
  (is (= "abc" (misc/multi-trim "   abc  \t   ")))
  (is (= ["abc" "xyz"] (misc/multi-trim ["   abc  \t   " "\nxyz  "])))
  (is (= 9 (misc/multi-trim 9)))
  (is (= "" (misc/multi-trim "")))
  (is (= [""] (misc/multi-trim [""])))
  (is (= [""] (misc/multi-trim ["   "])))
  (is (= [] (misc/multi-trim [])))
  (is (= nil (misc/multi-trim nil))))

(deftest log-request-test
  (doseq [api-key [{"api-key" "mjao"} {}]
          referer [{"referer" "xyz"} {}]
          from [{"from" "9324"} {}]
          query-string [{:query-string "x=9"} {}]]
    (let [request (merge {:headers (merge api-key referer from)
                          :uri "http://ostka.ca"}
                         query-string)
          handler-impl (fn [_request] {:status 200})
          handler (misc/log-request handler-impl)]
      (is (= {:status 200} (handler request))))))


(ns unit.jobtech-taxonomy.api.routes.services-swagger-test
  (:require [clojure.data.json :as json]
            [clojure.string :as str]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :as rmr]
            [utils.db-fixtures :as udf]))

(def services-endpoint (atom nil))

(defn services-endpoint-update [fun-run]
  (reset! services-endpoint (handler/app udf/cfg))
  (fun-run))

(test/use-fixtures :each (udf/database-minimal udf/cfg) services-endpoint-update)

(deftest swagger-test
  (testing "openapi.json test"
    (let [response (@services-endpoint
                    (rmr/request :get "/v1/taxonomy/openapi.json"))
          json-data (json/read-json (str/replace (slurp (:body response)) #";" "="))]
      (is (= 200 (:status response)))
      (is (= "Jobtech Taxonomy" (:title (:info json-data))))
      (is (= {:securitySchemes {:api-key {:in "header", :name "api-key", :type "apiKey"}}} (:components json-data)))
      (is (= [{:api-key []}] (:security json-data))))))
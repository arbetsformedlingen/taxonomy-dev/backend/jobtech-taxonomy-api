(ns unit.jobtech-taxonomy.api.routes.services-graphql-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.handler :as handler]
            [ring.mock.request :as rmr]
            [utils.db-fixtures :as udf]))

(set! *warn-on-reflection* true)

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(def services-endpoint
  (handler/app udf/cfg))

(deftest graphql-post-test
  (testing "POST json-body /v1/taxonomy/graphql"
    (let [request-json-body (-> (rmr/request
                                 :post "/v1/taxonomy/graphql")
                                (rmr/json-body {:query "query{concepts(id:\"pCha_vWk_pbR\"){preferred_label}}"}))
          response (services-endpoint request-json-body)]
      (is (= 400 (:status response)))
      (is (.contains (slurp (:body response)) "")))))

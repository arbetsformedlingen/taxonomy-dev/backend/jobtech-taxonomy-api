(ns unit.jobtech-taxonomy.api.routes.parameter-util-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.routes.parameter-util :as pu]))

(deftest read-boolean-header-test
  (is (false? (pu/read-boolean-header {"x-debug?" "true"} "x")))

  (doseq [[expected stored-value] [[true "true"]
                                   [true "True"]
                                   [true "yeS"]
                                   [true "   true   "]
                                   [true "   1   "]
                                   [false "False"]
                                   [false "asdf"]
                                   [false "false"]
                                   [false "  no   "]
                                   [false "   0 "]]]
    (is (= expected (pu/read-boolean-header {"x-debug?" stored-value}
                                            "x-debug?")))))

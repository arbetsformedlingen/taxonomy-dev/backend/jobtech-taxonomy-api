(ns unit.jobtech-taxonomy.api.cache-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.cache :as cache]))

(deftest test-cache-with-and-without-maximum-size
  (doseq [maximum-size [nil 5]]
    (let [eval-count (atom 0)
          my-add (fn [a b]
                   (swap! eval-count inc)
                   (+ a b))
          c (cache/make my-add :maximum-size maximum-size)]
      (is (zero? @eval-count))
      (is (= 5 (c 2 3)))
      (is (= 1 @eval-count))
      (is (= 5 (c 2 3)))
      (is (= 1 @eval-count))
      (dotimes [i 100]
        (is (= (c i 1)
               (+ i 1))))
      (let [n @eval-count]
        (is (= 5 (c 2 3)))
        (is (= (if maximum-size (inc n) n)
               @eval-count))))))

(deftest test-cache-with-custom-key-fn
  (let [eval-count (atom 0)
        my-add (fn [_verbose? a b]
                 (swap! eval-count inc)
                 (+ a b))
        c (cache/make my-add :key-fn (fn [& args] (rest args)))]
    (is (zero? @eval-count))
    (is (= 5 (c false 2 3)))
    (is (= 1 @eval-count))
    (is (= 5 (c false 2 3)))
    (is (= 1 @eval-count))
    (is (= 5 (c true 2 3)))
    (is (= 1 @eval-count))))


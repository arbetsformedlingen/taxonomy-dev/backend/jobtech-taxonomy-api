(ns unit.jobtech-taxonomy.api.authentication-service-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.authentication-service :as auth]
            [utils.db-fixtures :as udf]))

(deftest authenticate-admin-test
  (is (auth/authenticate-admin udf/cfg :222))
  (is (not (auth/authenticate-admin udf/cfg :111)))
  (is (not (auth/authenticate-admin udf/cfg :something-else-unknown))))

(deftest get-user-id-from-api-key-test
  (is (= "admin-1" (auth/get-user-id-from-api-key udf/cfg :222)))
  (is (nil? (auth/get-user-id-from-api-key udf/cfg :something-else-unknown))))






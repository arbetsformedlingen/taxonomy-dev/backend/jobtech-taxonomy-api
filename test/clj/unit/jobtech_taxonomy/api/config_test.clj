(ns unit.jobtech-taxonomy.api.config-test
  (:require
   [babashka.fs :as fs]
   [clojure.edn :as edn]
   [clojure.string :as str]
   [clojure.test :refer [deftest testing is] :as test]
   [jobtech-taxonomy.api.config :as jtac]
   [jobtech-taxonomy.common.io-utils :as iou]
   [malli.core :as m]
   [malli.error :as me]
   [utils.helpers :as uh]))

(deftest cli-flag->key-test
  (testing "Map cli flags to keys"
    (is (= :port (#'jtac/cli-flag->key "--port")))
    (is (= :port (#'jtac/cli-flag->key "-p")))
    (is (= :config (#'jtac/cli-flag->key "--config")))
    (is (= :config (#'jtac/cli-flag->key "-c")))
    (is (= "42" (#'jtac/cli-flag->key "42")))))

(deftest key+cli-value->key+value-test
  (testing "Map keyed cli-values"
    (is (= [:port 42] (#'jtac/key+cli-value->key+value [:port "42"])))
    (let [result (#'jtac/key+cli-value->key+value [:config "test/resources/config/datahike.edn"])]
      (is (= [:config "test/resources/config/datahike.edn"] result))))
  (testing "Error messages"
    (is (= {:data {:errors {:key :not-a-key :value "not a value"}} :message "Unknown key"}
           (uh/catch-ex-info
            (#'jtac/key+cli-value->key+value [:not-a-key "not a value"]))))
    (is (= [:config "not a file"]
           (#'jtac/key+cli-value->key+value [:config "not a file"])))
    (is (= {:data {:key :port :value "not a number"} :message "Invalid port"}
           (uh/catch-ex-info
            (#'jtac/key+cli-value->key+value [:port "not a number"]))))))

(deftest validate-cli-test
  (testing "Collect params that occur more than once"
    (is (nil? (#'jtac/validate-cli [:a 1 :b 2 :c 3])))
    (is (= {:errors {:key-count [:a]}} (#'jtac/validate-cli [:a 1 :b 2 :a 3])))))

(deftest parse-cli-test
  (testing "--config or -c specifies a file name"
    (is (= {:options {:config "test/resources/config/datahike.edn"}}
           (#'jtac/parse-cli ["--config" "test/resources/config/datahike.edn"])))
    (is (= {:options {:config "test/resources/config/datahike.edn"}}
           (#'jtac/parse-cli ["-c" "test/resources/config/datahike.edn"]))))
  (testing "--config or -c specifies a possibly invalid file"
    (is (= {:options {:config "not a file"}}
           (#'jtac/parse-cli ["-c" "not a file"])))
    (is (= {:options {:config "not a file"}}
           (#'jtac/parse-cli ["--config" "not a file"]))))
  (testing "--port or -p reads a port number"
    (is (= {:options {:port 4}}
           (#'jtac/parse-cli ["--port" "4"])))
    (is (= {:options {:port 4}}
           (#'jtac/parse-cli ["-p" "4"]))))
  (testing "--port or -p must be a number"
    (is (= {:data {:key :port :value "Quinta do Noval"} :message "Invalid port"}
           (uh/catch-ex-info
            (#'jtac/parse-cli ["--port" "Quinta do Noval"]))))
    (is (= {:data {:key :port :value "Quinta do Noval"} :message "Invalid port"}
           (uh/catch-ex-info
            (#'jtac/parse-cli ["-p" "Quinta do Noval"])))))
  (testing "Missing value for -c"
    (is (= {:data {:key :config} :message "Missing value for key"}
           (uh/catch-ex-info
            (#'jtac/parse-cli ["-c"])))))
  (testing "Unknown key with missing value"
    (is (= {:data {:key "xyz"} :message "Missing value for key"}
           (uh/catch-ex-info
            (#'jtac/parse-cli ["xyz"]))))))

(defn explain-away [schema data]
  (->> (m/explain schema data)
       (me/humanize)))

(deftest get-config-test
  (testing "Get a config using cli parameter"
    (let [cfg (jtac/get-config ["-c" "test/resources/config/datomic/test.edn" "-p" "8765"])]
      (is (= 8765 (jtac/->server-port cfg)))
      (is (= :datomic (jtac/->backend-type cfg)))))
  (testing "Get a config using env variable"
    (with-redefs [iou/get-env-var (fn [_ var-name]
                                    (condp = var-name
                                      "TAXONOMY_CONFIG_PATH" "test/resources/config/datomic/test.edn"
                                      "TAXONOMY_PORT" 5555
                                      "JOBTECH_TAXONOMY_API__AUTH_TOKENS" {:999 :user :777 :admin}
                                      "JOBTECH_TAXONOMY_API__USER_IDS" {:777 "admin-1"}
                                      :else nil))]
      (let [cfg (jtac/get-config [])]
        (is (= 5555 (jtac/->server-port cfg)))
        (is (= {:999 :user, :777 :admin}
               (get-in cfg [:jobtech-taxonomy-api :auth-tokens])))
        (is (= {:777 "admin-1"} (get-in cfg [:jobtech-taxonomy-api :user-ids])))
        (is (= :datomic (jtac/->backend-type cfg)))))))

(deftest read-datomic-config
  (let [cfg (jtac/get-config ["-c" "test/resources/config/datomic/test.edn"])]
    (testing "Get a datomic config"
      (let [backend-cfg (jtac/get-backend cfg)]
        (is (= :datomic (:type backend-cfg)))
        (is (= {:auth-tokens {:111 :user, :222 :admin}
                :user-ids {:222 "admin-1"}}
               (:jobtech-taxonomy-api cfg)))))
    (testing "Get values"
      (is (= :datomic (jtac/->backend-type cfg)))
      (is (= "datomic-CI-sanctioned-parts-list"
             (jtac/->backend-name cfg)))
      (is (nil? (jtac/->backend-local-storage-dir cfg)))
      (is (true? (jtac/->can-delete? cfg)))
      (is (true? (jtac/->can-create? cfg)))
      (is (= 32 (jtac/->server-max-threads cfg)))
      (is (= 8080 (jtac/->server-port cfg))))))

(deftest read-datahike-config
  (let [cfg (jtac/get-config ["-c" "test/resources/config/datahike/test.edn"])]
    (testing "Get a datahike config"
      (let [backend-cfg (jtac/get-backend cfg)]
        (is (= :datahike (:type backend-cfg)))
        (is (= {:auth-tokens {:111 :user, :222 :admin}
                :user-ids {:222 "admin-1"}}
               (:jobtech-taxonomy-api cfg)))))
    (testing "Get values"
      (is (= :datahike (jtac/->backend-type cfg)))
      (is (= "datahike-unknown-datahike-in-mem"
             (jtac/->backend-name cfg)))
      (is (nil? (jtac/->backend-local-storage-dir cfg)))
      (is (true? (jtac/->can-delete? cfg)))
      (is (true? (jtac/->can-create? cfg)))
      (is (= 32 (jtac/->server-max-threads cfg)))
      (is (= 8080 (jtac/->server-port cfg))))))

(deftest config-test
  (testing "Command line arguments syntax error"
    (is (thrown? clojure.lang.ExceptionInfo (jtac/get-config ["-c"]))))
  (let [cfg (jtac/get-config ["-c" "test/resources/config/datomic/test.edn"])]
    (testing "Datomic is in memory datomic-local"
      (let [backend (jtac/get-backend cfg)]
        (is (= :datomic (:type backend)))
        (is (= :datomic-local (get-in backend [:cfg :server-type])))
        (is (= :mem (get-in backend [:cfg :storage-dir])))))))

(deftest config-cli-test
  (testing "Parsing CLI options"
    (let [cfg (jtac/get-config ["-p" "4223" "-c" "test/resources/config/datomic/test.edn"])]
      (is (= 4223 (get-in cfg [:options :port]))))))

(deftest taxonomy-single-backend-config-schema-test
  (testing "Test the Nippy config"
    (let [cfg (jtac/get-config ["-c" "test/resources/config/datomic/test.edn"])]
      (is (= nil (m/explain jtac/taxonomy-single-backend-config-schema cfg)))
      (testing "Broken Nippy config"
        (let [schema-errors (explain-away jtac/backend-schema {:id :my-datomic
                                                               :type :datomic
                                                               :cfg nil})]
          (is (= {:cfg ["invalid type" "invalid type"]
                  :id ["disallowed key"]}
                 schema-errors)))
        (let [schema-errors (explain-away jtac/datomic-cfg-schema {})]
          (is (= {:db-name ["missing required key"]
                  :server-type ["missing required key"]
                  :system ["missing required key"]}
                 schema-errors))))))

  (testing "Test the Datahike config"
    (let [cfg (jtac/get-config ["-c" "test/resources/config/datahike/test.edn"])]
      (is (= nil
             (m/explain jtac/taxonomy-single-backend-config-schema cfg)))
      (testing "Broken Datahike config"
        (let [schema-errors (explain-away jtac/backend-schema {:id :my-datahike
                                                               :type :datahike
                                                               :cfg nil})]
          (is (= {:cfg ["invalid type" "invalid type"]
                  :id ["disallowed key"]}
                 schema-errors)))
        (let [schema-errors (explain-away jtac/datahike-cfg-schema {})]
          (is (= {:store ["missing required key"],
                  :attribute-refs? ["missing required key"],
                  :name ["missing required key"]}
                 schema-errors)))))))

(deftest set-backend-storage-dir
  (testing "The default datomic and datahike configs use :mem as storage dir"
    (is (nil? (-> (jtac/get-config ["-c" "test/resources/config/datomic/test.edn"])
                  jtac/->backend-local-storage-dir)))
    (is (nil? (-> (jtac/get-config ["-c" "test/resources/config/datahike/test.edn"])
                  jtac/->backend-local-storage-dir))))
  (testing "Update the storage dir of the active backend"
    (is (= "my-storage-dir"
           (-> (jtac/get-config ["-c" "test/resources/config/datomic/test.edn"])
               (jtac/set-backend-storage-dir "my-storage-dir")
               jtac/->backend-local-storage-dir)))
    (is (= "my-storage-dir"
           (-> (jtac/get-config ["-c" "test/resources/config/datahike/test.edn"])
               (jtac/set-backend-storage-dir "my-storage-dir")
               jtac/->backend-local-storage-dir)))
    (is (thrown? clojure.lang.ExceptionInfo
                 (-> (jtac/get-config
                      ["-c" "test/resources/config/datahike/test.edn"])
                     (update-in [:backend :type]
                                #(->> "-corrupted"
                                      (str (name %))
                                      keyword))
                     (jtac/set-backend-storage-dir "my-storage-dir")
                     jtac/->backend-local-storage-dir)))))

(def sample-config {:options {:port 8080, :jetty-threads 32, :log-level :debug},
                    :backends
                    [{:id :datomic-v.dev,
                      :type :datomic,
                      :cfg
                      {:db-name "jobtech-taxonomy-dev-2022-09-09-13-50-37",
                       :server-type :ion,
                       :region "eu-central-1",
                       :system "tax-test-v4",
                       :endpoint
                       "https://hcvc9p1nvi.execute-api.eu-central-1.amazonaws.com/"}}
                     {:id :datahike-v.dev,
                      :type :datahike,
                      :init-db
                      {:wanderung/type :nippy-jar, :filename "taxonomy-v21.nippy"},
                      :cfg
                      {:store {:backend :mem, :id "in-mem-nippy-data"},
                       :wanderung/type :datahike,
                       :schema-flexibility :write,
                       :attribute-refs? true,
                       :index :datahike.index/persistent-set,
                       :keep-history true,
                       :name "Nippy testing data"}}],
                    :path "config.edn",
                    :database-backend :datomic-v.dev,
                    :compare-backends [:datomic-v.dev :datahike-v.dev]})

(defn looks-like-config? [x]
  (and (map? x)
       (or (contains? x :backends)
           (contains? x :backend))))

(defn scan-project-config-files []
  (for [file (file-seq (fs/file "."))
        :when (fs/regular-file? file)
        :when (str/includes? (str file) "config")
        :let [ext (fs/extension file)]
        :when ext
        :when (= "edn" (str/lower-case ext))
        :let [data (-> file slurp edn/read-string)]
        :when (looks-like-config? data)]
    [file data]))

(deftest project-wide-config-scan-test
  (testing "Testing all config files in the project against the config schema"
    (let [pairs (scan-project-config-files)]
      (testing "At least one config file was found"
        (is (seq pairs)))
      (doseq [[file data] pairs]
        (testing (format "Testing schema against %s" (str file))
          (is (nil? (m/explain [:or
                                jtac/taxonomy-multi-backend-config-schema
                                jtac/taxonomy-single-backend-config-schema]
                               data))))))))

(deftest module-test
  (let [cfg (jtac/valid-config?
             {:backend {:type :datahike
                        :init-db {:wanderung/type :nippy
                                  :filename "./test/data/taxonomy.nippy"}
                        :cfg {:store {:backend :mem}
                              :attribute-refs? true
                              :name "from-nippy"}}})]
    (testing "set-module-data and get-module-data"
      (is (= :fermat (-> cfg
                         (jtac/set-module-data :maths :fermat)
                         (jtac/get-module-data :maths)))))
    (testing "set-module-data, update-module-data and get-module-data"
      (is (= "Blaise Pascal"
             (-> cfg
                 (jtac/set-module-data :mathematician "Blaise")
                 (jtac/update-module-data :mathematician str " Pascal")
                 (jtac/get-module-data :mathematician)))))))

(deftest conform-test
  (let [cfg-single {:backend {:type :datahike
                              :init-db {:wanderung/type :nippy
                                        :filename "./test/data/taxonomy.nippy"}
                              :cfg {:store {:backend :mem}
                                    :attribute-refs? true
                                    :name "from-nippy"}}}
        cfg-multi1 {:backends [{:id :datahike-0
                                :type :datahike
                                :init-db {:wanderung/type :nippy
                                          :filename "./test/data/taxonomy.nippy"}
                                :cfg {:store {:backend :mem}
                                      :attribute-refs? true
                                      :name "from-nippy"}}
                               {:id :datahike-1
                                :type :datahike
                                :init-db {:wanderung/type :nippy
                                          :filename "./test/data/taxonomy.nippy"}
                                :cfg {:store {:backend :mem}
                                      :attribute-refs? true
                                      :name "from-nippy2"}}]
                    :database-backend :datahike-0}
        cfg-multi2 (dissoc cfg-multi1 :database-backend)]
    (doseq [cfg [cfg-single cfg-multi1 cfg-multi2]
            :let [tmpfile (fs/file (fs/create-temp-file))]]
      (is (= cfg-single (jtac/conform cfg)))
      (spit tmpfile (pr-str cfg))
      (is (= (assoc cfg-single :path (str tmpfile))
             (#'jtac/load-config-from-file tmpfile))))))

(defn print-config-file-list
  "Utility function to print all config files in the project and their formats."
  []
  (doseq [[file data] (scan-project-config-files)]
    (println
     (format "Config file %s of format %s"
             (str file)
             (str (condp m/validate data
                    jtac/taxonomy-multi-backend-config-schema :deprecated-multi-backend
                    jtac/taxonomy-single-backend-config-schema :single-backend
                    :invalid))))))


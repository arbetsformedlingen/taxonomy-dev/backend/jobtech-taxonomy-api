(ns unit.jobtech-taxonomy.api.db.graphql-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy.api.db.concepts :as concepts]
   [jobtech-taxonomy.api.db.graphql :as gq]
   [jobtech-taxonomy.api.db.versions :as v]
   [utils.db-fixtures :refer [cfg database-with-schema]]))

(defn add-version [fun-run]
  (v/create-new-version cfg 0)
  (fun-run))

(defn add-concepts [fun-run]
  (#'concepts/assert-concept-part
   cfg
   "my-user-id"
   {:id "1111_111_111"
    :type "my-type"
    :definition "My definition."
    :preferred-label "my label"
    :comment "A comment"
    :quality-level 7
    :alternative-labels "my other label"
    :hidden-labels "sort by me"})
  (fun-run))

(test/use-fixtures :each (database-with-schema cfg) add-concepts add-version)

(deftest schema-test
  (testing "Create schema"
    (let [schema (gq/schema cfg)] 
      (is (some? schema)))))


(deftest graphql-simple-test
  (testing "Try to retract a concept!"
    (let [query "query {concepts {id}}"
          variables nil
          operation-name nil
          api-key nil
          response (gq/execute cfg query variables operation-name api-key)]
      (is (= {:data {:concepts [{:id "1111_111_111"}]}}
             response)))))
(ns unit.jobtech-taxonomy.api.db.concepts-minidb-test
  (:require [clojure.test :refer [deftest testing is use-fixtures]]
            [jobtech-taxonomy.api.db.concepts :as c]
            [utils.db-fixtures :as udf :refer [cfg]]))

(use-fixtures :each (udf/database-minimal cfg))

(deftest get-total-concepts-count-test
  (testing "Get total concepts count in the database"
    (is (= 28 (c/get-total-concepts-count
               cfg
               :next)))
    (is (= 27 (c/get-total-concepts-count
               cfg
               nil)))
    (is (= 27 (c/get-total-concepts-count
               cfg
               :latest)))
    (is (= 10 (c/get-total-concepts-count
               cfg
               1)))))

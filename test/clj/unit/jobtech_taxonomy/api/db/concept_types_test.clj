(ns unit.jobtech-taxonomy.api.db.concept-types-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concept-types :as concept-types]
            [utils.db-fixtures :as udf :refer [cfg]]))

(test/use-fixtures :each (udf/database-with-schema cfg) (udf/add-concepts cfg))

(deftest assert-and-test-existence
  (testing "Try to assert types and test for existence"
    (is (not (concept-types/type-exists? cfg "office-equipment" :next)))
    (concept-types/assert-type cfg {:concept-type "office-equipment"
                                    :label-sv "Kontorsmaterial"
                                    :label-en "Office equipment"
                                    :user-id "Gustav"})
    (is (concept-types/type-exists? cfg "office-equipment" :next))))

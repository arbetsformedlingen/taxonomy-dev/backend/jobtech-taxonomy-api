(ns unit.jobtech-taxonomy.api.db.graphql.impl-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.db.graphql.impl :as impl]))

(deftest parse-version-ref-test
  (is (= :latest (impl/parse-version-ref "latest")))
  (is (= :next (impl/parse-version-ref "next")))
  (is (= 3 (impl/parse-version-ref "3")))
  (is (= 3 (impl/parse-version-ref 3)))
  (is (thrown? Exception (impl/parse-version-ref :next))))

(deftest merge-into-test
  (is (= :a (impl/merge-into :a)))
  (is (= [:a :b] (impl/merge-into [:a] [:b])))
  (is (= {:a 3, :b 4} (impl/merge-into nil {:a 3} nil {:b 4} nil))))

(deftest serialize-version-ref-test
  (is (= "latest" (impl/serialize-version-ref :latest)))
  (is (= "next" (impl/serialize-version-ref :next)))
  (is (= 3 (impl/serialize-version-ref 3))))


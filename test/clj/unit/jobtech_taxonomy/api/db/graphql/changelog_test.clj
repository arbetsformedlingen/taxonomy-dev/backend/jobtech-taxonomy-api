(ns unit.jobtech-taxonomy.api.db.graphql.changelog-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.api.db.graphql.changelog :as changelog]))

(deftest test-wrap-any
  (is (nil? (#'changelog/wrap-any nil)))
  (is (= {:jobtech-taxonomy.api.db.graphql/any-wrapper false}
         (#'changelog/wrap-any false)))
  (is (= {:jobtech-taxonomy.api.db.graphql/any-wrapper '()}
         (#'changelog/wrap-any '())))
  (is (= {:jobtech-taxonomy.api.db.graphql/any-wrapper []}
         (#'changelog/wrap-any [])))
  (is (= {:jobtech-taxonomy.api.db.graphql/any-wrapper 119}
         (#'changelog/wrap-any 119)))
  (is (= {:jobtech-taxonomy.api.db.graphql/any-wrapper :a}
         (#'changelog/wrap-any :a))))

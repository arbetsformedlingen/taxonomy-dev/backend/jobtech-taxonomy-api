(ns unit.jobtech-taxonomy.api.db.versions-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.versions :as v]
            [utils.db-fixtures :refer [cfg database-with-schema]])
  (:import [java.util Date]))

(set! *warn-on-reflection* true)

(defn assert-suffixed-concept [cfg suffix]
  (#'concepts/assert-concept-part
   cfg
   "my-user-id"
   {:id (str "my-concept-id" suffix)
    :type (str "my-type" suffix)
    :definition (str "My definition." suffix)
    :preferred-label (str "my label" suffix)
    :comment (str "A comment" suffix)
    :quality-level 7
    :alternative-labels (str "my other label" suffix)
    :hidden-labels (str suffix "sort by me")}))

(defn add-concepts [fun-run]
  (assert-suffixed-concept cfg "")
  (fun-run))

(test/use-fixtures :each (database-with-schema cfg) add-concepts)

(deftest get-all-versions-id-test
  (testing "Get all versions"
    (is (= [] (v/get-all-versions cfg)))))

(def ^Long delta-ms 3)

(deftest create-new-version-test
  (testing "Try to create a new version"
    (Thread/sleep delta-ms)
    (is (= :jobtech-taxonomy.api.db.versions/incorrect-new-version
           (v/create-new-version cfg 'clearly-wrong)))
    (Thread/sleep delta-ms)
    (is (= :jobtech-taxonomy.api.db.versions/incorrect-new-version
           (v/create-new-version cfg 1)))
    (Thread/sleep delta-ms)
    (is (= 0 (:version (v/create-new-version cfg 0))))
    (Thread/sleep delta-ms)
    (is (= 1 (:version (v/create-new-version cfg 1))))
    (Thread/sleep delta-ms)
    (is (= :jobtech-taxonomy.api.db.versions/incorrect-new-version
           (v/create-new-version cfg 3)))
    (Thread/sleep delta-ms)
    (is (= 2 (:version (v/create-new-version cfg 2))))
    (Thread/sleep delta-ms)
    (assert-suffixed-concept cfg "xyz")
    (Thread/sleep delta-ms)
    (assert-suffixed-concept cfg "abc")
    (Thread/sleep delta-ms)
    (let [db (dc/get-db cfg :next)
          tx-instant-datoms (dc/datoms db {:index :avet :components [:db/txInstant]})
          n 5
          last-n-tx-datoms (->> tx-instant-datoms
                                reverse
                                (take n)
                                reverse)
          [_ _ _ d3 _] last-n-tx-datoms]
      (is (= n (count last-n-tx-datoms)))
      (doseq [[datom-a datom-b] (partition 2 1 last-n-tx-datoms)
              :let [^Date a (:v datom-a)
                    ^Date b (:v datom-b)
                    ^Date a+1 (Date. (inc (.getTime a)))]]
        (is (.before a b))
        (is (.before a+1 b))
        (is (= (:e datom-a) (#'v/find-tx-at-time db a)))
        (is (= (:e datom-b) (#'v/find-tx-at-time db a+1)))
        (is (= (:e datom-b) (#'v/find-tx-at-time db b))))
      (is (= 3 (:version (v/create-new-version cfg 3 (:v d3)))))
      (let [db (dc/get-db cfg :next)
            v3-tx (ffirst (dc/q '[:find ?tx
                                  :in $ ?version
                                  :where
                                  [?e :taxonomy-version/id ?version]
                                  [?e :taxonomy-version/tx ?tx]]
                                db 3))]
        (is (= v3-tx (:e d3)))))))

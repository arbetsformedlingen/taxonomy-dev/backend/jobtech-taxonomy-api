(ns unit.jobtech-taxonomy.api.db.core-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concepts :as concepts]
            [jobtech-taxonomy.api.db.core :as c]
            [utils.db-fixtures :refer [cfg database-with-schema]]))

(defn add-concepts [fun-run]
  (#'concepts/assert-concept-part
   cfg
   "my-user-id"
   {:id "my-concept-id"
    :type "my-type"
    :definition "My definition."
    :preferred-label "my label"
    :comment "A comment"
    :quality-level 7
    :alternative-labels "my other label"
    :hidden-labels "sort by me"})
  (fun-run))

(test/use-fixtures :each (database-with-schema cfg) add-concepts)

(deftest retract-concept-test
  (testing "Try to retract a concept!"
    (let [result (c/retract-concept cfg
                                    "some-user-id"
                                    "my-concept-id"
                                    "retraction-comment")]
      (is (some? result)))))
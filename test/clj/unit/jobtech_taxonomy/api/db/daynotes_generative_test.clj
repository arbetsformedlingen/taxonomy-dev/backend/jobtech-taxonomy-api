(ns unit.jobtech-taxonomy.api.db.daynotes-generative-test
  (:require [clojure.test :refer [deftest is use-fixtures testing]]
            [jobtech-taxonomy.api.db.api-util :as api-util]
            [jobtech-taxonomy.api.db.database-connection :as db]
            [jobtech-taxonomy.api.db.daynotes :as d]
            [jobtech-taxonomy.api.db.versions :as versions]
            [taoensso.timbre :as log]
            [utils.db-fixtures :refer [cfg database-minimal]]
            [utils.integration-helpers :as integration-helpers])
  (:import [java.util Date]))

(set! *warn-on-reflection* true)

(use-fixtures :once (database-minimal cfg))

;;;------- Clean reference implementation -------

(defn- changes->deprecated-event-old [changes]
  (when (some #(and (:added %)
                    (#{:concept/deprecated :concept/replaced-by}
                     (:a %))
                    (:v %))
              changes)
    {:event-type "DEPRECATED"}))

(defn- changes->event-old [changes db]
  (or (#'d/changes->commented-event changes)
      (changes->deprecated-event-old changes)
      (#'d/changes->created-event changes)
      (#'d/changes->updated-event changes db)))

(defn- fetch-concept-txs [hist-db concept-id from-version to-version]
  (-> {:query '{:find [?tx ?inst ?user ?comment ?c ?id]
                :keys [tx inst user comment concept concept-id]
                :in [$]
                :where [[?c :concept/id ?id]
                        (or [?c _ _ ?tx]
                            [?tx :daynote/ref ?c])
                        [?tx :db/txInstant ?inst]
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user]
                        [(get-else $ ?tx :daynote/comment false) ?comment]]}
       :args [hist-db]}
      (cond->
       concept-id
        (#'d/conj-in [:query :in] '?id
                     [:args] concept-id)

        (inst? from-version)
        (#'d/conj-in [:query :in] '?from-version
                     [:query :where] '[(<= ?from-version ?inst)]
                     [:args] from-version)

        (int? from-version)
        (#'d/conj-in [:query :in] '?from-version
                     [:query :where] '[_ :taxonomy-version/id ?from-version ?from-tx]
                     [:query :where] '[?from-tx :db/txInstant ?from-inst]
                     [:query :where] '[(<= ?from-inst ?inst)]
                     [:args] from-version)

        (inst? to-version)
        (#'d/conj-in [:query :in] '?to-version
                     [:query :where] '[(< ?inst ?to-version)]
                     [:args] to-version)

        (int? to-version)
        (#'d/conj-in [:query :in] '?to-version
                     [:query :where] '[_ :taxonomy-version/id ?to-version ?to-tx]
                     [:query :where] '[?to-tx :db/txInstant ?to-inst]
                     [:query :where] '[(< ?inst ?to-inst)]
                     [:args] to-version))
      db/q))

(def ^:private fetch-concepts-query
  '[:find (pull ?e [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label
                    :concept/alternative-labels
                    :concept/hidden-labels])
    :in $ [?id ...]
    :where [?e :concept/id ?id]])

(defn- daynotes-txs->present-concepts-map [db daynote-txs]
  (let [ids (map :concept-id daynote-txs)
        concepts (db/q fetch-concepts-query db ids)]
    (->> concepts
         (map first)
         (map (juxt :concept/id identity))
         (into {}))))

(defn- fetch-concept-changes [hist-db tx+concepts]
  (group-by (juxt :tx :concept)
            (db/q '[:find ?tx ?c ?attr ?v ?added
                    :keys tx concept a v added
                    :in $ [[?tx ?c] ...]
                    :where
                    [?c ?a ?v ?tx ?added]
                    [?a :db/ident ?attr]]
                  hist-db tx+concepts)))

(defn get-for-concept-REF-IMPL
  "This is the original version of get-for-concept before we started optimizing"
  [db concept-id from-timestamp to-timestamp offset limit show-unpublished]
  (let [hist-db (db/history db)
        tx-id->version (#'d/get-tx-version-map db)
        daynote-txs (fetch-concept-txs hist-db concept-id from-timestamp to-timestamp)
        present-concepts-map (daynotes-txs->present-concepts-map db daynote-txs)
        tx+concept->changes (fetch-concept-changes hist-db (mapv (juxt :tx :concept) daynote-txs))]
    (->> daynote-txs
         (sort-by :tx)
         (filter #(or show-unpublished
                      (not= (#'d/convert-transaction-id-to-version-id
                             tx-id->version
                             (:tx %)) -1)))
         (api-util/pagination offset limit)
         (map (fn [{:keys [tx concept inst user comment concept-id]}]
                (-> [tx concept]
                    tx+concept->changes
                    (changes->event-old db)
                    (assoc
                     :timestamp inst
                     :user-id user
                     :latest-version-of-concept (get present-concepts-map concept-id)
                     :version (#'d/convert-transaction-id-to-version-id
                               tx-id->version tx))
                    (cond->
                     comment (assoc :comment comment)
                     show-unpublished (assoc :transaction-id tx))))))))

;;;------- Helpers -------

(defn normalize-changelog-event [x]
  (dissoc (case (:event-type x)
            "UPDATED" (update x
                              :concept-attribute-changes
                              #(into #{} (map (fn [x] (-> x
                                                          (dissoc :new-value)
                                                          (update :added set))))
                                     %))
            "CREATED" (update x
                              :new-concept
                              #(-> %
                                   (update :concept/alternative-labels set)
                                   (dissoc :concept.external-standard/implicit-driving-licences)))
            x)
          :src-changes))

(defn normalize-changelog-events [x]
  (into #{} (map normalize-changelog-event) x))

;;;------- Tests -------

(defn- list-time-points [versions]
  (into []
        (for [{:keys [version ^Date timestamp]} versions
              :let [step (* 1000 60 60 24 3)
                    epoch (.getTime timestamp)]
              endpoint [version
                        (Date. (long (- epoch step)))
                        timestamp
                        (Date. (long (+ epoch step)))]]
          endpoint)))

(defn- event-log-argument-combinations
  "Generates different argument combinations given `time-points`. The `samples-per-group` argument controls the size of the result."
  [time-points samples-per-group]
  (->> (for [from-timestamp time-points ;; <-- List all possible argument combinations
             to-timestamp time-points
             show-unpublished [false true]]
         {:from-timestamp from-timestamp
          :to-timestamp to-timestamp
          :show-unpublished show-unpublished})

       ;; Group the argument combinations by different features.
       (group-by (fn [{:keys [from-timestamp to-timestamp show-unpublished]}]
                   [(nil? from-timestamp)
                    (nil? to-timestamp)
                    (and (number? from-timestamp)
                         (number? to-timestamp)
                         (= (inc from-timestamp) to-timestamp))
                    (and (number? from-timestamp)
                         (number? to-timestamp)
                         (< from-timestamp to-timestamp))
                    (= from-timestamp to-timestamp)
                    (instance? Date from-timestamp)
                    (instance? Date to-timestamp)
                    (and (instance? Date from-timestamp)
                         (instance? Date to-timestamp)
                         (.before ^Date from-timestamp
                                  ^Date to-timestamp))
                    show-unpublished]))
       vals

       ;; Pick some argument combinations from every feature group,
       ;; concatenate, and return the result.
       (into [] (mapcat #(take samples-per-group %)))))

(defn generate-event-log-argument-data [cfg samples-per-group]
  (let [conn (db/connect cfg)
        database (db/db conn)
        versions (versions/get-all-versions cfg)
        time-points (list-time-points versions)
        all-concept-ids (into []
                              (comp (map db/datom->v)
                                    (distinct))
                              (db/datoms database {:index :aevt
                                                   :limit -1
                                                   :components [:concept/id]}))
        argument-combinations (event-log-argument-combinations time-points samples-per-group)]
    (assert (seq time-points))
    (assert (seq all-concept-ids))
    {:argument-combinations argument-combinations
     :all-concept-ids all-concept-ids}))

(defn specific-concept-ids-to-test [all-concept-ids events samples-per-group]
  (let [mentioned-ids (into []
                            (comp (map (comp :concept/id :latest-version-of-concept))
                                  (distinct))
                            events)
        mentioned-id-set (set mentioned-ids)
        not-mentioned-ids (remove mentioned-id-set all-concept-ids)]
    (into [nil]
          (comp cat (remove nil?))
          [(take samples-per-group mentioned-ids)
           (take samples-per-group not-mentioned-ids)])))

(defn generate-slices [n]
  (for [offset [nil 0 1 n]
        limit [nil 0 1 n]
        :when (not (and (nil? offset)
                        (nil? limit)))
        :when (or (nil? offset)
                  (<= offset n))
        :when (or (nil? limit)
                  (<= (+ limit (or 0 offset)) n))]
    {:offset offset
     :limit limit}))

(deftest get-for-concept-generative-test
  (testing "Test that the actual implementation of get-for-concept corresponds to the reference implementation"
    (let [database (db/db (db/connect cfg))
          {:keys [argument-combinations all-concept-ids]} (generate-event-log-argument-data cfg 1)]
      (log/info "Run for" (count argument-combinations) "different combinations")
      (doseq [{:keys [from-timestamp to-timestamp show-unpublished]} argument-combinations]
        (let [ref-events (get-for-concept-REF-IMPL database nil from-timestamp to-timestamp nil nil show-unpublished)
              events (d/get-for-concept database nil from-timestamp to-timestamp nil nil show-unpublished)
              concept-ids (specific-concept-ids-to-test all-concept-ids events 1)]
          (is (= (normalize-changelog-events ref-events)
                 (normalize-changelog-events events)))
          (testing "Events for specific concepts"
            (doseq [concept-id concept-ids]
              (let [ref-events (get-for-concept-REF-IMPL database concept-id from-timestamp to-timestamp nil nil show-unpublished)
                    events (d/get-for-concept database concept-id from-timestamp to-timestamp nil nil show-unpublished)]
                (is (= (normalize-changelog-events ref-events)
                       (normalize-changelog-events events))))))
          (testing "Offset and limit, both with and without specific concept"
            (doseq [concept-id (conj concept-ids nil)
                    {:keys [offset limit]} (generate-slices (count events))]
              (let [all-events (d/get-for-concept database concept-id from-timestamp to-timestamp nil nil show-unpublished)
                    sliced-events (d/get-for-concept database concept-id from-timestamp to-timestamp offset limit show-unpublished)
                    expected-events (cond->> all-events
                                      offset (drop offset)
                                      limit (take limit))]
                (is (= expected-events sliced-events))))))))))

;;;------- Benchmarks -------

(defmacro measure-time [expr]
  `(let [start# (System/nanoTime)
         result# ~expr
         end# (System/nanoTime)]
     [(- end# start#) result#]))

(comment

  (defn benchmark-get-for-concept
    "Benchmark get-for-concept against the reference implementation and return the results"
    []
    (let [cfg (integration-helpers/unique-cfg "real-data")]
      (integration-helpers/real-data-fixture
       (fn []
         (let [database (db/db (db/connect cfg))
               {:keys [argument-combinations]} (generate-event-log-argument-data cfg 5)
               n (count argument-combinations)
               run-all (fn [context]
                         (into
                          []
                          (map-indexed
                           (fn [index {:keys [from-timestamp to-timestamp show-unpublished] :as args}]
                             (log/info context ": Evaluate for" (inc index) "of" n)
                             (let [[ref-time ref-events]
                                   (measure-time
                                    (get-for-concept-REF-IMPL database
                                                              nil
                                                              from-timestamp
                                                              to-timestamp
                                                              nil nil show-unpublished))
                                   [actual-time actual-events]
                                   (measure-time
                                    (d/get-for-concept database
                                                       nil
                                                       from-timestamp
                                                       to-timestamp
                                                       nil nil show-unpublished))]
                               (assert (= (normalize-changelog-events ref-events)
                                          (normalize-changelog-events actual-events)))
                               {:args args
                                :result-size (count actual-events)
                                :durations-ns {:reference ref-time
                                               :actual actual-time}})))
                          argument-combinations))
               _warmup (run-all "WARMUP")
               results (run-all "MEASURE")]
           results))))))

(comment
  (defn benchmark-report
    "Print results of benchmarking get-for-concept against reference implementation"
    [results]
    (let [rel-speed-ups (vec (sort (for [r results]
                                     (double
                                      (/ (-> r :durations-ns :reference)
                                         (-> r :durations-ns :actual))))))
          n (count rel-speed-ups)
          middle (quot n 2)]
      (println (format "Min relative speed up:     %.1f" (apply min rel-speed-ups)))
      (println (format "Max relative speed up:     %.1f" (apply max rel-speed-ups)))
      (println (format "Median relative speed up:  %.1f" (nth rel-speed-ups middle)))
      (println (format "Average relative speed up: %.1f" (Math/exp (/ (transduce (map #(Math/log %)) + rel-speed-ups) n))))))

 ; (def results (benchmark))

  #_(def size-rel-speed-up-pairs (sort-by first
                                          (for [r results]
                                            [(:result-size r)
                                             (double (/ (-> r :durations-ns :reference)
                                                        (-> r :durations-ns :actual)))]))))

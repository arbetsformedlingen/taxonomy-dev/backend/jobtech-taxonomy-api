(ns ^:daynotes unit.jobtech-taxonomy.api.db.daynotes-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concepts :as c]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.daynotes :as daynotes]
            [taoensso.timbre :as log]
            [utils.db-fixtures :refer [cfg database-with-schema]]))

(defn add-concepts [fun-run]
  (#'c/assert-concept-part
   cfg
   "my-user-id"
   {:id "1111_111_11"
    :type "my-type"
    :definition "My definition."
    :preferred-label "my label"
    :comment "A comment"
    :quality-level 7
    :alternative-labels "my other label"
    :hidden-labels "sort by me"})
  (#'c/assert-concept-part
   cfg
   "my-user-id"
   {:id "2222_222_22"
    :type "my-type"
    :definition "My second definition."
    :preferred-label "my second label"
    :comment "A second comment"
    :quality-level 5})
  (fun-run))

(test/use-fixtures :each
  (fn [fun-run]
    (log/with-merged-config
      {:min-level [[#{"io.methvin.watcher.*"} :info]
                   [#{"datahike.*"} :info]
                   [#{"jobtech-taxonomy.api.db.database-connection"} :warn]
                   [#{"*"} :info]]}
      (fun-run)))
  (database-with-schema cfg) add-concepts)

(defn get-eid-for-concept-id [db concept-id]
  (-> (dc/q '[:find ?e
              :keys db/id
              :in $ ?cid
              :where
              [?e :concept/id ?cid]]
            db concept-id)
      first :db/id))

(deftest test-conj-in
  (testing "Multiple updates to map entry"
    (is (= {:a [['a] ['b] ['c]]}
           (#'daynotes/conj-in {:a [['a]]} [:a] ['b] [:a] ['c])))))

(deftest daynote-test
  (testing "Try to add a daynote to a concept"
    (let [_result (daynotes/create-for-concept
                   cfg
                   "1111_111_11"
                   "my-other-user-id"
                   "A long comment.")
          concept-id "1111_111_11"
          from-timestamp nil
          to-timestamp nil
          offset nil
          limit nil
          show-unpublished true
          daynotes (daynotes/get-for-concept
                    (dc/get-db cfg :next)
                    concept-id
                    from-timestamp
                    to-timestamp
                    offset
                    limit
                    show-unpublished)]
      (is (= ["my-user-id"
              "my-other-user-id"]
             (map :user-id daynotes)))
      (is (= ["A comment"
              "A long comment."]
             (map :comment daynotes))))))

(deftest changes->commented-event-test
  (testing "Convert changes to commented event"
    (is (= {:event-type "COMMENTED"}
           (#'daynotes/changes->commented-event nil)))
    (is (= {:event-type "COMMENTED"}
           (#'daynotes/changes->commented-event [])))
    (is (= {:event-type "COMMENTED"}
           (#'daynotes/changes->commented-event ())))
    (is (= {:event-type "COMMENTED"}
           (#'daynotes/changes->commented-event {})))
    (is (= {:event-type "COMMENTED"}
           (#'daynotes/changes->commented-event #{})))
    (is (nil? (#'daynotes/changes->commented-event "a")))
    (is (nil? (#'daynotes/changes->commented-event [nil]))))
  (testing "Throws on non-collection or not nil"
    (is (thrown? Exception (#'daynotes/changes->commented-event 1)))
    (is (thrown? Exception (#'daynotes/changes->commented-event true)))
    (is (thrown? Exception (#'daynotes/changes->commented-event false)))
    (is (thrown? Exception (#'daynotes/changes->commented-event :a)))))

(deftest changes->deprecated-event-test
  (testing "should return true when change is added, has :concept/deprecated or :concept/replaced-by, and has :v"
    (let [change [{:added true :a :concept/deprecated :v 1}]]
      (is (#'daynotes/changes->deprecated-event change)))
    (let [change [{:added true :a :concept/replaced-by :v 1}]]
      (is (#'daynotes/changes->deprecated-event change))))
  (testing "Should be true if any change is deprecated or replaced-by"
    (is (= {:event-type "DEPRECATED"}
           (#'daynotes/changes->deprecated-event [{:added true :a :concept/deprecated :v 1}])))
    (is (= {:event-type "DEPRECATED"}
           (#'daynotes/changes->deprecated-event [{:added true :a :concept/replaced-by :v 42}]))))
  (testing "Should be nil otherwise"
    (is (nil? (#'daynotes/changes->deprecated-event [{:added true :a :concept/created :v 1}])))
    (is (nil? (#'daynotes/changes->deprecated-event [{:added true :a :concept/deprecated :v nil}])))
    (is (nil? (#'daynotes/changes->deprecated-event [{:added false :a :concept/deprecated :v 1}])))
    (is (nil? (#'daynotes/changes->deprecated-event [])))
    (is (nil? (#'daynotes/changes->deprecated-event nil))))
  (testing "should return false when change is not added"
    (let [change [{:added false :a :concept/deprecated :v 1}]]
      (is (nil? (#'daynotes/changes->deprecated-event change)))))
  (testing "should return false when change does not have :concept/deprecated or :concept/replaced-by"
    (let [change [{:added true :a :concept/created :v 1}]]
      (is (nil? (#'daynotes/changes->deprecated-event change)))))
  (testing "should return false when change does not have :v"
    (let [change [{:added true :a :concept/deprecated :v nil}]]
      (is (nil? (#'daynotes/changes->deprecated-event change))))))

(deftest changes-to-new-concept-test
  (testing "Change collection reduces to map"
    (is (= {} (#'daynotes/changes-to-new-concept [])))
    (is (= {nil nil} (#'daynotes/changes-to-new-concept [{}])))
    (is (= {nil nil} (#'daynotes/changes-to-new-concept [{} {}])))
    (is (= {:concept/id nil} (#'daynotes/changes-to-new-concept [{:added false :a :concept/id}])))
    (is (= {:concept/id 3} (#'daynotes/changes-to-new-concept [{:a :concept/id :v 3}])))
    (is (= {:concept/id :not-three}
           (#'daynotes/changes-to-new-concept [{:a :concept/id :v 3} {:a :concept/id :v :not-three}])))
    (is (= {:concept/id 3 :this-is :not-three}
           (#'daynotes/changes-to-new-concept [{:a :concept/id :v 3} {:a :this-is :v :not-three}])))
    (is (= {:concept/alternative-labels '(:not-three 3)}
           (#'daynotes/changes-to-new-concept [{:a :concept/alternative-labels :v 3} {:a :concept/alternative-labels :v :not-three}])))
    (is (= {:concept/hidden-labels '(:not-three 3)}
           (#'daynotes/changes-to-new-concept [{:a :concept/hidden-labels :v 3} {:a :concept/hidden-labels :v :not-three}])))
    (is (= {:concept/hidden-labels '(3) :concept/alternative-labels '(:not-three)}
           (#'daynotes/changes-to-new-concept [{:a :concept/hidden-labels :v 3} {:a :concept/alternative-labels :v :not-three}])))))

(deftest changes->created-event-test
  (testing "Should return {:event-type \"CREATED\"} if any change is created"
    (is (= {:event-type "CREATED" :new-concept {:concept/id "JUST_ANO_THER"}}
           (#'daynotes/changes->created-event [{:added true :a :concept/id :v "JUST_ANO_THER"}])))
    (is (= {:event-type "CREATED"
            :new-concept {:concept/id "JUST_ANO_THER"
                          :concept/alternative-labels ["The One Ring" "My Precious!"]
                          :concept/hidden-labels ["Is it secret?" "Is it safe?" nil]
                          :not-a-field :not-a-value}}
           (#'daynotes/changes->created-event [{:added true :a :concept/id :v "JUST_ANO_THER"}
                                               {:added true :a :concept/hidden-labels :v nil}
                                               {:added true :a :concept/hidden-labels :v "Is it safe?"}
                                               {:added true :a :concept/alternative-labels :v "My Precious!"}
                                               {:added false :a :not-a-field :v :not-a-value}
                                               {:added true :a :concept/hidden-labels :v "Is it secret?"}
                                               {:added true :a :concept/alternative-labels :v "The One Ring"}]))))
  (testing "Should return nil otherwise"
    (is (nil? (#'daynotes/changes->created-event [{:added false :a :concept/id}])))
    (is (nil? (#'daynotes/changes->created-event [])))
    (is (nil? (#'daynotes/changes->created-event nil)))))

(deftest get-concept-id-test
  (let [db (dc/get-db cfg :next)
        concept-id "1111_111_11"
        eid (get-eid-for-concept-id db concept-id)
        max-eid (apply max (map :e (dc/datoms db {:index :eavt})))
        non-existing-eid (inc max-eid)]
    (testing "Unknown ID should return nil"
      (is (nil? (#'daynotes/get-concept-id db non-existing-eid))))
    (testing "The ID should exist in the database"
      (is (number? eid))
      (is (= concept-id (#'daynotes/get-concept-id db eid))))))

(deftest changes->updated-event-test
  (let [db (dc/get-db cfg :next)
        eid-1 (get-eid-for-concept-id db "1111_111_11")]
    (testing "Create UPDATED event from changes"
      (is (= {:event-type "UPDATED"
              :concept-attribute-changes
              [{:added [:new-value :other-new-value]
                :attribute "changed-property"
                :new-value :new-value
                :old-value :old-value
                :removed [:old-value]}]}
             (#'daynotes/changes->updated-event
              [{:added false :a :changed-property :v :old-value}
               {:added true :a :changed-property :v :new-value}
               {:added true :a :changed-property :v :other-new-value}] db))))
    (testing "Create UPDATED event with :concept/replaced-by"
      (is (= {:event-type "UPDATED"
              :concept-attribute-changes
              [{:added ["1111_111_11"]
                :attribute "replaced-by"
                :new-value "1111_111_11"
                :old-value nil
                :removed []}]}
             (#'daynotes/changes->updated-event
              [{:added true :a :concept/replaced-by :v eid-1}] db))))))

(deftest changes->event-test
  (let [db (dc/get-db cfg :next)
        eid-1 (get-eid-for-concept-id db "1111_111_11")]
    (testing "Create COMMENTED event from changes"
      (is (= {:event-type "COMMENTED"} (#'daynotes/changes->event [] db)))
      (is (= {:event-type "COMMENTED"} (#'daynotes/changes->event {} db)))
      (is (= {:event-type "COMMENTED"} (#'daynotes/changes->event #{} db)))
      (is (= {:event-type "COMMENTED"} (#'daynotes/changes->event "" db))) ; hilarity ensues
      (is (= {:event-type "COMMENTED"} (#'daynotes/changes->event nil db))))
    (testing "Create DEPRECATED event from changes"
      (is (= {:event-type "DEPRECATED"}
             (#'daynotes/changes->event [{:added true :a :concept/deprecated :v true}] db)))
      (is (= {:event-type "DEPRECATED"}
             (#'daynotes/changes->event [{:added true :a :concept/deprecated :v true}
                                         {:added true :a :concept/replaced-by :v eid-1}] db)))
      (is (= {:event-type "DEPRECATED"}
             (#'daynotes/changes->event [{:added true :a :concept/replaced-by :v eid-1}] db))))
    (testing "Create CREATED event from changes"
      (is (= {:event-type "CREATED"
              :new-concept {:concept/id "JUST_ANO_THER"}}
             (#'daynotes/changes->event [{:added true :a :concept/id :v "JUST_ANO_THER"}] db)))
      (is (= {:event-type "CREATED"
              :new-concept {:concept/id "JUST_ANO_THER"
                            :concept/alternative-labels ["The One Ring" "My Precious!"]
                            :concept/hidden-labels ["Is it secret?" "Is it safe?" nil]
                            :not-a-field :not-a-value}}
             (#'daynotes/changes->event [{:added true :a :concept/id :v "JUST_ANO_THER"}
                                         {:added true :a :concept/hidden-labels :v nil}
                                         {:added true :a :concept/hidden-labels :v "Is it safe?"}
                                         {:added true :a :concept/alternative-labels :v "My Precious!"}
                                         {:added false :a :not-a-field :v :not-a-value}
                                         {:added true :a :concept/hidden-labels :v "Is it secret?"}
                                         {:added true :a :concept/alternative-labels :v "The One Ring"}] db))))
    (testing "Create UPDATED event from changes"
      (is (= {:event-type "UPDATED"
              :concept-attribute-changes
              [{:added [:new-value :other-new-value]
                :attribute "changed-property"
                :new-value :new-value
                :old-value :old-value
                :removed [:old-value]}]}
             (#'daynotes/changes->event
              [{:added false :a :changed-property :v :old-value}
               {:added true :a :changed-property :v :new-value}
               {:added true :a :changed-property :v :other-new-value}] db)))
      (is (= {:event-type "UPDATED"
              :concept-attribute-changes
              [{:added []
                :attribute "id"
                :new-value nil
                :old-value eid-1
                :removed [eid-1]}
               {:added []
                :attribute "replaced-by"
                :new-value nil
                :old-value "1111_111_11"
                :removed ["1111_111_11"]}]}
             (#'daynotes/changes->event
              [{:added false :a :concept/replaced-by :v eid-1}
               {:added false :a :concept/id :v eid-1}] db))))))

(deftest hash-set-distinct-test
  (is (= [3 4 5 9 7]
         (into []
               (#'daynotes/hash-set-distinct)
               [3 4 5 4 4 3 9 7 9]))))

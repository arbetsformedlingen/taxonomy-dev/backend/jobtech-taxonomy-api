(ns unit.jobtech-taxonomy.api.db.concepts.relations-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy.api.db.concepts :as c]
   [jobtech-taxonomy.api.db.database-connection :as dc]
   [utils.db-fixtures :refer [cfg database-with-schema add-concepts]]))

(test/use-fixtures :each
  (database-with-schema cfg)
  (add-concepts cfg)
  (fn [fun-run]
    (#'c/assert-relation-part
     cfg
     "my-user-id"
     {:comment "my-comment"
      :concept-1 "2222_222_222"
      :concept-2 "3333_333_333"
      :type "broader"
      :description "my-description"
      :substitutability-percentage 33})
    (fun-run)))

(deftest fetch-relation-data-by-id-test
  (testing "Fetch relation data by id in the database"
    (let [relation-data
          (#'c/fetch-relation-data-by-id
           cfg "2222_222_222:broader:3333_333_333")]
      (let [internally-vectored-and-quoted-map
            {:find '[?e ?p ?v ?r]
             :in '[$ ?id]
             :where
             '[[?e :relation/id ?id]
               [?e ?p ?v]
               [?p :db/ident ?r]]}]
        (is (not= #{}
                  (dc/q {:find '[?r ?e ?s ?attrib-name]
                         :in '[$ ?id]
                         :where '[[?r :relation/id ?id]
                                  [?r ?e ?s]
                                  [?e :db/ident ?attrib-name]]}
                        (dc/get-db cfg :next) "2222_222_222:broader:3333_333_333")))
        (is (= 6 (count (dc/q internally-vectored-and-quoted-map
                              (dc/get-db cfg :next)
                              "2222_222_222:broader:3333_333_333")))))
      (is (= 1 (count relation-data)))
      (let [rd (first relation-data)
            rid (:id rd)
            cid-1 (:relation/concept-1 rd)
            cid-2 (:relation/concept-2 rd)]
        (is (int? rid))
        (is (int? cid-1))
        (is (int? cid-2))
        (is (= {:relation/description "my-description"
                :relation/id "2222_222_222:broader:3333_333_333"
                :relation/substitutability-percentage 33
                :relation/type "broader"}
               (dissoc rd :id :relation/concept-1 :relation/concept-2)))))))

(deftest fetch-relation-data-test
  (testing "Fetch relation data in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "unlikely-combination"
            :description "my-description"
            :substitutability-percentage 33})
          relation-data
          (c/fetch-relation-data
           cfg
           "2222_222_222"
           "3333_333_333"
           "unlikely-combination")]
      (is (some? result))
      (is (some? new-rel))
      (is (some? relation-data))
      (let [rid (:id relation-data)
            cid-1 (:relation/concept-1 relation-data)
            cid-2 (:relation/concept-2 relation-data)]
        (is (int? rid))
        (is (int? cid-1))
        (is (int? cid-2))
        (is (= {:relation/description "my-description"
                :relation/id "2222_222_222:unlikely-combination:3333_333_333"
                :relation/substitutability-percentage 33
                :relation/type "unlikely-combination"}
               (dissoc relation-data :id :relation/concept-1 :relation/concept-2))))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:unlikely-combination:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "unlikely-combination"}
             new-rel)))))

(deftest fetch-relation-data-by-id-2-test
  (testing "Fetch relation data by id in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})
          relation-data
          (#'c/fetch-relation-data-by-id
           cfg
           "2222_222_222:substitutability:3333_333_333")
          relation-data-2
          (c/fetch-relation-data
           cfg
           "2222_222_222"
           "3333_333_333"
           "substitutability")]
      (is (some? result))
      (let [rd (first relation-data)
            rid (:id rd)
            cid-1 (:relation/concept-1 rd)
            cid-2 (:relation/concept-2 rd)]
        (is (int? rid))
        (is (int? cid-1))
        (is (int? cid-2))
        (is (= {:relation/description "my-description"
                :relation/id "2222_222_222:substitutability:3333_333_333"
                :relation/substitutability-percentage 33
                :relation/type "substitutability"}
               (dissoc rd :id :relation/concept-1 :relation/concept-2))))
      (is (= (first relation-data)
             relation-data-2))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel)))))
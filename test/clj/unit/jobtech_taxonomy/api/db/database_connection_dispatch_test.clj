(ns unit.jobtech-taxonomy.api.db.database-connection-dispatch-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [utils.db-fixtures :as udf]))

(deftest dispatch-datomic-test
  (let [cfg udf/cfg-datomic
        db-type :datomic]
    (testing "Dispatch on config"
      (is (= db-type (dc/database-backend cfg))))
    (try
      (testing "Create database"
        (is (dc/create-database cfg)))
      (testing "Dispatch on connection"
        (let [conn (dc/connect cfg)]
          (is (= db-type (dc/database-backend conn)))))
      (testing "Dispatch on db"
        (is (= db-type (dc/database-backend (dc/get-db cfg :next)))))
      (finally
        (dc/delete-database cfg)))))

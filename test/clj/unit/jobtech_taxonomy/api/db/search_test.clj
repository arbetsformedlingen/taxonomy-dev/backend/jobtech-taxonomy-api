(ns unit.jobtech-taxonomy.api.db.search-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.search :as search]
            [utils.db-fixtures :refer [add-concepts add-version cfg
                                       database-with-schema]]))

(test/use-fixtures :each (database-with-schema cfg) (add-concepts cfg) (add-version cfg 0))

(deftest test-get-concepts-by-search
  (testing "get-concepts-by-search"
    (let [response (search/get-concepts-by-search cfg {:query-string "m" :version :latest})]
      (is (= 4 (count response)))
      (is (= #{{:concept/alternative-labels ["another-alternative-label" "my-alternative-label"],
                :concept/id "1111_111_111",
                :concept/preferred-label "my-label",
                :concept/type "my-type"}
               {:concept/alternative-labels ["another-alternative-label4" "my-alternative-label4"],
                :concept/id "4444_444_444",
                :concept/preferred-label "my-label4",
                :concept/type "my-type"}
               {:concept/alternative-labels ["another-alternative-label2" "my-alternative-label2"],
                :concept/id "3333_333_333",
                :concept/preferred-label "my-label2",
                :concept/type "my-type"}
               {:concept/alternative-labels ["another-alternative-label1" "my-alternative-label1"],
                :concept/id "2222_222_222",
                :concept/preferred-label "my-label1",
                :concept/type "my-type"}}
             (set response)))))
  (testing "get-concepts-by-search"
    (let [response (search/get-concepts-by-search cfg {:query-string "q" :version :latest})]
      (is (empty? response)))))




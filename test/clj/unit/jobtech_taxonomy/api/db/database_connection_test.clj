(ns unit.jobtech-taxonomy.api.db.database-connection-test
  (:require [babashka.fs :as fs]
            [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.api.db.database-connection.impl :as impl]
            [jobtech-taxonomy.common.io-utils :as io-utils]
            [utils.db-fixtures :as udf]
            [utils.helpers :as uh]
            [wanderung.core :as wanderung])
  (:import [java.lang IllegalArgumentException]))

(set! *warn-on-reflection* true)

(deftest create-database-test
  (testing "Fail to create a database without `:allow-create true`"
    (let [invalid-cfg (assoc-in udf/cfg [:backend :allow-create] false)]
      (is (= "`create-database` was called with `:allow-create` false"
             (-> invalid-cfg
                 dc/create-database
                 uh/catch-ex-info
                 :message)))))
  (testing "Fail to create a database from invalid configuration"
    (let [invalid-backend {:allow-create true
                           :type :invalid/backend-type
                           :id :invalid/backend-id
                           :cfg "invalid-cfg"}
          invalid-cfg (assoc udf/cfg :backend invalid-backend)]
      (is (= "Invalid data"
             (-> invalid-cfg
                 dc/create-database
                 uh/catch-ex-info
                 :message))))))

(deftest delete-database-test
  (testing "Fail to delete a database without `:allow-delete true`"
    (let [invalid-backend {:type :invalid/backend-type
                           :id :invalid/backend-id
                           :cfg "invalid-cfg"}
          invalid-cfg (assoc udf/cfg :backend invalid-backend)]
      (is (= {:data {:backend invalid-backend} :message "`delete-database` was called with `:allow-delete` false"}
             (uh/catch-ex-info
              (dc/delete-database invalid-cfg))))))
  (testing "Fail to delete a database with an unknown backend"
    (let [invalid-backend {:allow-delete true
                           :type :invalid/backend-type
                           :id :invalid/backend-id
                           :cfg "invalid-cfg"}
          invalid-cfg (assoc udf/cfg :backend invalid-backend)]
      (is (= {:data {:backend invalid-backend} :message "Unknown database backend"}
             (uh/catch-ex-info
              (dc/delete-database invalid-cfg)))))))

(deftest multi-dispatch-function-test
  (testing "Dispatch on config"
    (is (= udf/active-backend-type (dc/database-backend udf/cfg)))
    (is (= udf/active-backend-type (dc/dispatch-obj udf/cfg))))
  (testing "Dispatch on connection and db"
    (try
      (let [tx-time (java.util.Date.)
            _ (dc/create-database udf/cfg)
            _ (dc/transact (dc/connect udf/cfg) {:tx-data [{:db/id "datomic.tx" :db/txInstant tx-time}
                                                           {:db/ident :name :db/valueType :db.type/string :db/cardinality :db.cardinality/many}]})
            conn (dc/connect udf/cfg)
            db (dc/get-db udf/cfg :next)
            db-as-of (dc/as-of db tx-time)
            db-history (dc/history db)]
        (is (= udf/active-backend-type (dc/database-backend conn)))
        (is (= udf/active-backend-type (dc/database-backend (atom conn))))
        (is (= udf/active-backend-type (dc/database-backend db)))
        (is (= udf/active-backend-type (#'dc/dispatch-map [] db)))
        (is (= udf/active-backend-type (#'dc/dispatch-map [] db-as-of)))
        (is (= udf/active-backend-type (dc/database-backend db-as-of)))
        (is (= udf/active-backend-type (#'dc/dispatch-map [] db-history)))
        (is (= udf/active-backend-type (dc/database-backend db-history))))
      (finally
        (dc/delete-database udf/cfg)))))

(deftest test-get-db-valid-args
  ((udf/database-minimal udf/cfg)
   (fn []
     (testing "get-db with valid args"
       (is (dc/get-db udf/cfg :latest))
       (is (dc/get-db udf/cfg :next))
       (doseq [i (range -20 20)]
         (when (<= 0 i 7)
           (is (dc/get-db udf/cfg i))))))))

(defn corrupt-cfg [cfg]
  (assoc-in cfg [:backend :type] :oracle-sql-alpha))

(deftest test-get-db-invalid-args
  ((udf/database-minimal udf/cfg)
   (fn []
     (testing "get-db with invalid args"
       (is (thrown? IllegalArgumentException (dc/get-db (corrupt-cfg udf/cfg) :next)))
       (doseq [version [:mjao "mjao" "latest" "next"]]
         (is (= {:version version}
                (-> (dc/get-db udf/cfg version)
                    uh/catch-ex-info
                    :data))))
       (doseq [i (range -20 20)]
         (when-not (<= 0 i 7)
           (is (= :no-such-db-version
                  (-> (dc/get-db udf/cfg i)
                      uh/catch-ex-info
                      :data
                      :type)))))))))

(deftest retry-always-fail-test
  (testing "retry - always failing"
    (let [result (try
                   {:value
                    (impl/retry {:delay-ms 0 :tries 5}
                                #(throw (ex-info "Always fails"
                                                 {:type :always-fails})))}
                   (catch Exception e
                     {:exception e}))]
      (is (= [:exception] (keys result)))
      (is (= {:type :always-fails}
             (-> result
                 :exception
                 ex-data))))))

(deftest retry-succeed-after-n
  (testing "retry - succeeding after n"
    (dotimes [n 10]
      (let [counter (atom -1)
            result (try
                     {:value
                      (impl/retry {:delay-ms 0 :tries 5}
                                  #(let [i (swap! counter inc)]
                                     (if (<= n i)
                                       i
                                       (throw
                                        (ex-info "Always fails"
                                                 {:type :failed})))))}
                     (catch Exception e
                       {:exception e}))]
        (if (< n 5)
          (is (= {:value n} result))
          (do (is (= [:exception] (keys result)))
              (is (= {:type :failed}
                     (-> result
                         :exception
                         ex-data)))))))))

(deftest register-backend-test
  (testing "Registering a new database backend"
    (let [original-backend-map (deref dc/backend-map)]
      (try
        (dc/register-backend! java.util.ArrayList :my-db-backend)
        (let [new-backend-map (deref dc/backend-map)]
          (is (= (dissoc new-backend-map java.util.ArrayList)
                 original-backend-map))
          (is (= :my-db-backend
                 (get new-backend-map java.util.ArrayList))))
        (finally
          (reset! dc/backend-map original-backend-map))))))

(deftest datum-access-test
  (let [d (datomic.core.db.Datum. 10 20 30 0)]
    (is (= 10 (.e d)))
    (is (= 20 (.a d)))
    (is (= 30 (.v d)))))

(deftest invalid-datom-access-test
  (doseq [x [nil :x]]
    (is (thrown? AssertionError (dc/datom->e x)))
    (is (thrown? AssertionError (dc/datom->a x)))
    (is (thrown? AssertionError (dc/datom->v x)))
    (is (thrown? AssertionError (dc/datom->tx x)))
    (is (thrown? AssertionError (dc/datom->added x)))))

(defn zip-one [dst-zip-file src-file]
  (fs/zip dst-zip-file
          (fs/relativize (fs/cwd) src-file)
          {:path-fn #(->> %
                          fs/absolutize
                          (fs/relativize (fs/parent src-file))
                          str)}))

(defn- storage-dir-setup-fns [storage-dir]
  {:no-storage-dir (fn []
                     (when (fs/exists? storage-dir)
                       (fs/delete-tree storage-dir)))
   :existing-storage-dir (fn []
                           (fs/create-dirs storage-dir))})

(deftest backend-cfg-init-db-combination-test
  (testing "Different combinations of backend-cfg with init-db"
    (let [mini-db-storage-dir (fs/create-temp-dir)
          nippy-filename (fs/create-temp-file {:suffix ".nippy"})
          mini-db-cfg {:db-name "datomic-mini-db"
                       :server-type :datomic-local
                       :system "CI"
                       :storage-dir (str mini-db-storage-dir)}
          mini-backend-cfg {:type :datomic
                            :allow-create true
                            :allow-delete true
                            :cfg mini-db-cfg}
          wrap-cfg (fn [bcfg]
                     {:backend bcfg})
          mini-cfg (wrap-cfg mini-backend-cfg)
          wanderung-mini-db (impl/wanderung-config-from-db-cfg
                             mini-db-cfg
                             :datomic)
          datomic-zip-filename (fs/file (fs/create-temp-file {:suffix ".zip"}))
          nippy-zip-filename (fs/file (fs/create-temp-file {:suffix ".zip"}))
          
          base-backend-cfg-to-test {:allow-create true
                                    :allow-delete true}

          backend-storage-root (fs/create-temp-dir)

          datomic0-storage-dir (str (fs/path backend-storage-root "datomic-file0"))
          datomic1-storage-dir (str (fs/path backend-storage-root "datomic-file1"))
          
          backend-cfgs-to-test {:datomic-file0
                                {:backend-cfg {:type :datomic
                                               :cfg {:db-name "jobtech-taxonomy-db"
                                                     :server-type :datomic-local
                                                     :storage-dir datomic0-storage-dir
                                                     :system "jobtech-taxonomy"}}
                                 :setup-fns (storage-dir-setup-fns datomic0-storage-dir)}
                                :datomic-file1
                                {:backend-cfg {:type :datomic
                                               :cfg {:db-name "datomic-mini-db"
                                                     :server-type :datomic-local
                                                     :storage-dir datomic1-storage-dir
                                                     :system "CI"}}
                                 :setup-fns (storage-dir-setup-fns datomic1-storage-dir)}
                                :datomic-mem
                                {:backend-cfg {:type :datomic
                                               :cfg {:db-name "jobtech-taxonomy-db-in-mem"
                                                     :server-type :datomic-local
                                                     :storage-dir :mem
                                                     :system "jobtech-taxonomy"}}}
                                :datahike-mem
                                {:backend-cfg {:type :datahike
                                               :cfg {:store {:backend :mem
                                                             :id (str (gensym))}
                                                     :schema-flexibility :write,
                                                     :keep-history true,
                                                     :attribute-refs? true
                                                     :wanderung/type :datahike
                                                     :name (str (gensym))}}}}

          init-dbs-to-test  {:nippy {:type :nippy
                                     :unzip false
                                     :path (str nippy-filename)}
                             :nippy2 {:wanderung/type :nippy
                                      :filename (str nippy-filename)}
                             :nippy-zip {:type :nippy
                                         :unzip true
                                         :path (str nippy-zip-filename)}
                             :nippy-zip2 {:unzip true
                                          :filename (str nippy-zip-filename)}
                             :datomic {:type :datomic
                                       :unzip false
                                       :path (str mini-db-storage-dir)}
                             :datomic-zip {:type :datomic
                                           :unzip true
                                           :path (str datomic-zip-filename)}
                             :datomic-zip2 {:unzip true
                                            :filename
                                            (str datomic-zip-filename)}}

          sample-values-from-db (fn [cfg]
                                  (let [db (dc/get-db cfg :next)]
                                    {:latest-version
                                     (dc/get-latest-released-version db)}))]

      ;; Populate the mini-db
      ((udf/database-minimal mini-cfg)
       (fn []

         ;; Get data from the mini-db into the various sources referred to
         ;; from init-dbs-to-test
         (wanderung/migrate wanderung-mini-db
                            {:wanderung/type :nippy
                             :filename (str nippy-filename)})
         (zip-one nippy-zip-filename nippy-filename)
         (io-utils/write-bytes datomic-zip-filename
                               (io-utils/zip-with-write-check
                                (fs/file mini-db-storage-dir)))


         ;; Get the expected value that we want to compare against
         (let [expected-sample-values (sample-values-from-db mini-cfg)]
           (is (<= 7 (:latest-version expected-sample-values)))

           ;; Iterate over all combinations of backend configurations and init-db.
           (doseq [[backend-id {:keys [backend-cfg setup-fns]}] backend-cfgs-to-test
                   [setup-key setup-fn] (or setup-fns {:no-setup (fn [])})
                   [init-db-key init-db] init-dbs-to-test
                   :let [cfg-to-test (wrap-cfg
                                      (merge
                                       base-backend-cfg-to-test
                                       backend-cfg
                                       {:init-db init-db}))]]

             ;; Test that we could create and populate a particular combination.
             (testing (format "Testing %s instantiated from %s with setup %s"
                              (name backend-id)
                              (name init-db-key)
                              (name setup-key))
               (setup-fn)
               (dc/init-db cfg-to-test)
               (is (= expected-sample-values
                      (sample-values-from-db cfg-to-test)))
               (dc/delete-database cfg-to-test)))))))))

(deftest wanderung-config-from-db-config-test
  (is (= {:store {:backend :mem, :id "in-mem-store"},
          :wanderung/type :datahike,
          :schema-flexibility :write,
          :attribute-refs? true,
          :index :datahike.index/persistent-set,
          :keep-history true,
          :name "datahike-in-mem"}
         (impl/wanderung-config-from-db-cfg
          {:store {:backend :mem, :id "in-mem-store"},
           :wanderung/type :datahike,
           :schema-flexibility :write,
           :attribute-refs? true,
           :index :datahike.index/persistent-set,
           :keep-history true,
           :name "datahike-in-mem"}
          :datahike))))

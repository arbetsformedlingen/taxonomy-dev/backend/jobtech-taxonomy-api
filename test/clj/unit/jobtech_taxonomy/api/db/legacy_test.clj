(ns unit.jobtech-taxonomy.api.db.legacy-test
  (:require [clojure.test :as test :refer [deftest is testing
                                           use-fixtures]]
            [jobtech-taxonomy.api.db.legacy :as l]
            [utils.db-fixtures :as udf]))

(use-fixtures :each (udf/database-minimal udf/cfg))

(deftest test-occupation-name-with-relations
  (testing "Occupation name with relations"
    (is
     (= 3262 (count (l/occupation-name-with-relations))))))

(deftest test-get-concept-id-from-legacy-id
  (testing "test get-concept-id-from-legacy-id"
    (is
     (= #:concept{:id "fg7B_yov_smw",
                  :type "occupation-name",
                  :preferred-label "Systemutvecklare/Programmerare",
                  :definition "Systemutvecklare/Programmerare",
                  :deprecated-legacy-id "2419"}
        (l/get-concept-id-from-legacy-id {} "2419" "occupation-name")))

    (is
     (=
      #:concept{:id "otpy_PEs_Cjn",
                :type "occupation-name",
                :preferred-label "Chefskock",
                :definition "Chefskock",
                :deprecated-legacy-id "217"}
      (l/get-concept-id-from-legacy-id {} "217" "occupation-name")))))

(deftest test-get-concept-id-from-matching-component-id
  (doseq [component-type ["ssyk-level-4" "occupation-group"]]
    (is (= {:concept/id "tAJS_JNb_hDH",
            :concept/type "ssyk-level-4",
            :concept/preferred-label "Vårdbiträden",
            :concept/definition
            "Ger individuell vård och omsorg till äldre i eget eller särskilt boende.\r
",
            :concept.external-standard/ssyk-code-2012 "5330",
            :concept/deprecated-legacy-id "1257"}
           (l/get-concept-id-from-matching-component-id
            udf/cfg "5330" component-type))))
  (is (= {:concept/id "aYA7_PpG_BqP",
          :concept/type "municipality",
          :concept/preferred-label "Nacka",
          :concept/definition "Nacka",
          :concept.external-standard/lau-2-code-2015 "0182"}
         (l/get-concept-id-from-matching-component-id
          udf/cfg "0182" "municipality")))
  (is (= {:concept/id "CaRE_1nn_cSU",
          :concept/type "region",
          :concept/preferred-label "Skåne län",
          :concept/definition "Skåne län",
          :concept.external-standard/national-nuts-level-3-code-2019 "12",
          :concept/deprecated-legacy-id "195"}
         (l/get-concept-id-from-matching-component-id
          udf/cfg "12" "region"))))

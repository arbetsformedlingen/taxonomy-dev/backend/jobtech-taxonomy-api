(ns unit.jobtech-taxonomy.api.db.database-connection-datahike-test
  (:require [clojure.set :as set]
            [clojure.test :as test :refer [deftest is testing]]
            [datahike.api :as dh]
            [datahike.datom :as datahike-datom]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [utils.db-fixtures :as udf]))

(def cfg udf/cfg-datahike)

(defn datahike-fixture [fun-run]
  (try (dc/create-database cfg)
       (fun-run)
       (finally
         (dc/delete-database cfg))))

(test/use-fixtures :each datahike-fixture)

(deftest transact-some-test
  (testing "Try to write some data and find it again"
    (let [conn (dc/connect cfg)]
      (dc/transact conn
                   {:tx-data [{:db/ident :name :db/valueType :db.type/string
                               :db/cardinality :db.cardinality/one}
                              {:db/ident :age :db/valueType :db.type/long
                               :db/cardinality :db.cardinality/one}]})
      (dc/transact
       (dc/connect cfg)
       {:tx-data
        [{:name "bob" :age 42} {:name "linda" :age 47}]})

      (is (= #{["linda"]}
             (dc/q '[:find ?name
                     :in $ [?age]
                     :where
                     [?e :name ?name]
                     [?e :age ?age]]
                   (dc/get-db cfg :next)
                   [47]))))))

(def micro-db
  [[{:db/id "datomic.tx"
     :db/txInstant #inst "2023-01-01"}
    {:db/ident :name :db/valueType :db.type/string
     :db/cardinality :db.cardinality/one}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-01-02"}
    {:db/ident :age :db/valueType :db.type/long
     :db/cardinality :db.cardinality/one}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-01-03"}
    {:db/ident :taxonomy-version/id :db/valueType :db.type/long
     :db/cardinality :db.cardinality/one}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-01-04"}
    {:db/ident :taxonomy-version/tx :db/valueType :db.type/ref
     :db/cardinality :db.cardinality/one}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-01-05"}
    {:taxonomy-version/id 0
     :taxonomy-version/tx "datomic.tx"}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-02-01"}
    {:name "linda" :age 47}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-02-02"}
    {:taxonomy-version/id 1
     :taxonomy-version/tx "datomic.tx"}]
   [{:db/id "datomic.tx"
     :db/txInstant #inst "2023-03-01"}
    {:name "bob" :age 42}]])

(defn micro-db-populate [id]
  (let [cfg {:backend {:type :datahike
                       :allow-create true
                       :allow-delete true
                       :cfg {:store {:backend :mem
                                     :id (str "tiny-" (name id))}
                             :attribute-refs? true
                             :wanderung/type :datahike
                             :name (str "prime-mover-" (name id))}}}]
    (when (#'dc/can-connect? cfg)
      (dc/delete-database cfg))
    (dc/create-database cfg)
    (doseq [tx-data micro-db]
      (dc/transact (dc/connect cfg) {:tx-data tx-data}))
    cfg))

(deftest get-version+tx-inst-test
  (let [cfg (micro-db-populate "versions")]
    (testing "Try get the versions"
      (is (= [{:tx-inst #inst "2023-02-02T00:00:00.000-00:00", :version 1}
              {:tx-inst #inst "2023-01-05T00:00:00.000-00:00", :version 0}]
             (dc/get-version+tx-inst
              (dc/get-db cfg :next))))
      (is (= #inst "2023-01-05T00:00:00.000-00:00"
             (dc/get-version-tx-inst (dc/get-db cfg :next) 0)))
      (is (= #inst "2023-02-02T00:00:00.000-00:00"
             (dc/get-version-tx-inst (dc/get-db cfg :next) 1)))
      (is (= #inst "2023-02-02T00:00:00.000-00:00"
             (dc/get-version-tx-inst (dc/get-db cfg :next) :latest)))
      (is (= #inst "2023-02-02T00:00:00.000-00:00"
             (dc/get-version-tx-inst (dc/get-db cfg :next) 42)))
      (is (= #inst "2023-02-02T00:00:00.000-00:00"
             (dc/get-version-tx-inst (dc/get-db cfg :next) nil)))
      (is (= nil
             (dc/get-version-tx-inst (dc/get-db cfg :next) :next))))))

(deftest cache-key-test
  (let [cfg (micro-db-populate "cache-key")]
    (testing "Test cache key for Datahike"
      (let [db (dc/get-db cfg :next)
            v1 #inst "2023-02-02T00:00:00.000-00:00"]
        (is (= :a (#'dc/datahike-arg->cache-key :a)))
        (doseq [cache-key [#'dc/datahike-arg->cache-key
                           dc/db->cache-key]]
          (is (-> db
                  cache-key
                  keys
                  set
                  (= #{:max-tx :id})))
          (is (contains? (cache-key (dc/as-of db v1)) :as-of))
          (is (contains? (cache-key (dh/since db v1)) :since))
          (is (contains? (cache-key (dh/history db)) :history))
          (is (contains? (cache-key (dc/history db)) :history)))
        (is (contains? (#'dc/datahike-arg->cache-key
                        (dh/filter db (constantly true)))
                       :pred))))))

(deftest get-datom-bytes-test
  (testing "Try to write some data and find it again"
    (let [cfg {:backend {:type :datahike
                         :allow-create true
                         :allow-delete true
                         :cfg {:store {:backend :mem :id "tiny"}
                               :attribute-refs? true
                               :wanderung/type :datahike
                               :name "prime-mover"}}}]
      (when (#'dc/can-connect? cfg) (dc/delete-database cfg))
      (dc/create-database cfg)
      (doseq [tx-data micro-db]
        (dc/transact (dc/connect cfg) {:tx-data tx-data}))
      (is (= #{["linda"]}
             (dc/q '[:find ?name
                     :in $ [?age]
                     :where
                     [?e :name ?name]
                     [?e :age ?age]]
                   (dc/get-db cfg 1)
                   [47])))
      (is (= #{}
             (dc/q '[:find ?name
                     :in $ [?age]
                     :where
                     [?e :name ?name]
                     [?e :age ?age]]
                   (dc/get-db cfg 1)
                   [42])))
      (is (= #{["bob"]}
             (dc/q '[:find ?name
                     :in $ [?age]
                     :where
                     [?e :name ?name]
                     [?e :age ?age]]
                   (dc/get-db cfg :next)
                   [42])))
      (is (= 979 (count (dc/read-raw cfg {:format :nippy-bytes}))))
      (is (= (dc/read-raw cfg {:format :edn})
             (dc/read-raw cfg))))))

(deftest datom-access-test
  (let [datom0 (datahike-datom/datom 1 2 :mjao 4 true)
        datom1 (datahike-datom/datom 10 100 :katt 1000 false)]
    (is (= 1 (dc/datom->e datom0)))
    (is (= 2 (dc/datom->a datom0)))
    (is (= :mjao (dc/datom->v datom0)))
    (is (= 4 (dc/datom->tx datom0)))
    (is (true? (dc/datom->added datom0)))

    (is (= 10 (dc/datom->e datom1)))
    (is (= 100 (dc/datom->a datom1)))
    (is (= :katt (dc/datom->v datom1)))
    (is (= 1000 (dc/datom->tx datom1)))
    (is (false? (dc/datom->added datom1)))))

(ns unit.jobtech-taxonomy.api.db.eures-test
  (:require
   [clojure.test :as test :refer [deftest is testing]]
   [jobtech-taxonomy.api.db.eures :as eu]
   [utils.db-fixtures :as udf]))

(test/use-fixtures :each (udf/database-minimal udf/cfg))

(deftest test-lookup-occupation-name-id-to-esco-ids
  (let [cfg udf/cfg]
    (testing "test lookup-occupation-name-id-to-esco-ids"
      (is (= '("http://data.europa.eu/esco/isco/C1349") (eu/lookup-occupation-name-id-to-esco-ids cfg "RdVK_4zC_kFV"))))

    (testing "test lookup-occupation-name-id-to-esco-ids with empty concept-id"
      (is (= nil (eu/lookup-occupation-name-id-to-esco-ids cfg ""))))

    (testing "test lookup-occupation-name-id-to-esco-ids with zero string concept-id"
      (is (= nil (eu/lookup-occupation-name-id-to-esco-ids cfg "0"))))))

(def test-data-exact [[#:concept{:id "GPNi_fJR_B2B",
                                 :type "occupation-name",
                                 :preferred-label "Inredningsdesigner"}
                       {:concept/id "iCXb_uUw_Fye",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "inredningsplanerare",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/773e5ea3-235b-4e72-afc5-290dd841eed8"}]
                      [#:concept{:id "GPNi_fJR_B2B",
                                 :type "occupation-name",
                                 :preferred-label "Inredningsdesigner"}
                       {:concept/id "TYat_S2W_WHT",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "inredare",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/73e776fb-4d99-4031-bad4-7716f121155d"}]
                      [#:concept{:id "rQds_YGd_quU",
                                 :type "occupation-name",
                                 :preferred-label "Mjukvaruutvecklare"}
                       {:concept/id "ApaR_Uzn_nd2",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "programvaruutvecklare",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/f2b15a0e-e65a-438a-affb-29b9d50b77d1"}]
                      [#:concept{:id "YqiW_QLd_qHo",
                                 :type "occupation-name",
                                 :preferred-label "Applikationsingenjör"}
                       {:concept/id "GyLz_ubY_Akf",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "applikationsingenjör",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/fde4372d-8293-42de-afbb-8b5a9b274fe7"}]])

(def test-data-isco [[#:concept{:id "ghs4_JXU_BYt",
                                :type "occupation-name",
                                :preferred-label "Planeringsarkitekt/Fysisk planerare"}
                      {:concept/id "XAcf_SQ9_fJw",
                       :concept/type "isco-level-4",
                       :concept/preferred-label "Planeringsarkitekter m.fl.",
                       :concept.external-standard/esco-uri
                       "http://data.europa.eu/esco/isco/C2164"}]
                     [#:concept{:id "GPNi_fJR_B2B",
                                :type "occupation-name",
                                :preferred-label "Inredningsdesigner"}
                      {:concept/id "iw8P_7PW_2By",
                       :concept/type "isco-level-4",
                       :concept/preferred-label "Inredare och dekoratörer",
                       :concept.external-standard/esco-uri
                       "http://data.europa.eu/esco/isco/C3432"}]
                     [#:concept{:id "Jx4V_6tm_fUH",
                                :type "occupation-name",
                                :preferred-label "Projekteringsingenjör, elkraft"}
                      {:concept/id "DmTx_t6n_jbW",
                       :concept/type "isco-level-4",
                       :concept/preferred-label "Civilingenjörer m.fl., miljöteknik",
                       :concept.external-standard/esco-uri
                       "http://data.europa.eu/esco/isco/C2143"}]
                     [#:concept{:id "YcvM_Gqk_6U7",
                                :type "occupation-name",
                                :preferred-label "Utvecklingsingenjör, elkraft"}
                      {:concept/id "TMGH_QHL_aE3",
                       :concept/type "isco-level-4",
                       :concept/preferred-label "Civilingenjörer m.fl., elkraft",
                       :concept.external-standard/esco-uri
                       "http://data.europa.eu/esco/isco/C2151"}]])

(def test-data-close [[#:concept{:id "JQYZ_oUR_LEq",
                                 :type "occupation-name",
                                 :preferred-label "Processingenjör, el-tele"}
                       {:concept/id "aE6g_Moa_R3y",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "telekomanalytiker",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/c5d36eef-df20-47a8-a332-5b4358fa83e3"}]
                      [#:concept{:id "JQYZ_oUR_LEq",
                                 :type "occupation-name",
                                 :preferred-label "Processingenjör, el-tele"}
                       {:concept/id "N2D8_PTA_tYn",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "telekommunikationsingenjör",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/02eb0ae6-ecdd-4602-9c8e-60ffe6dbe1e2"}]
                      [#:concept{:id "TRVV_bXp_BG9",
                                 :type "occupation-name",
                                 :preferred-label "Processingenjör, maskin"}
                       {:concept/id "5imw_7zt_qEf",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "civilingenjör, processteknik",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/b5eaf231-77ad-4a86-8e54-15cc4398aad2"}]
                      [#:concept{:id "T6yh_MNf_CKv",
                                 :type "occupation-name",
                                 :preferred-label "Textilingenjör"}
                       {:concept/id "116C_d8K_W8e",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "textiltekniker",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/85acc7e9-1fac-4e91-962b-8b8031f39487"}]])

(def test-data-narrow [[#:concept{:id "ghs4_JXU_BYt",
                                  :type "occupation-name",
                                  :preferred-label "Planeringsarkitekt/Fysisk planerare"}
                        {:concept/id "R4Sb_jub_zy7",
                         :concept/type "esco-occupation",
                         :concept/preferred-label "chef, mobilitetstjänster",
                         :concept.external-standard/esco-uri
                         "http://data.europa.eu/esco/occupation/3b129d8d-268e-465a-b0c5-bf32cae59a41"}]
                       [#:concept{:id "rQds_YGd_quU",
                                  :type "occupation-name",
                                  :preferred-label "Mjukvaruutvecklare"}
                        {:concept/id "L7zN_R22_H1V",
                         :concept/type "esco-occupation",
                         :concept/preferred-label "molnspecialist",
                         :concept.external-standard/esco-uri
                         "http://data.europa.eu/esco/occupation/349ee6f6-c295-4c38-9b98-48765b55280e"}]
                       [#:concept{:id "rQds_YGd_quU",
                                  :type "occupation-name",
                                  :preferred-label "Mjukvaruutvecklare"}
                        {:concept/id "YAGa_ytC_kt9",
                         :concept/type "esco-occupation",
                         :concept/preferred-label "blockkedjeutvecklare",
                         :concept.external-standard/esco-uri
                         "http://data.europa.eu/esco/occupation/24135b84-cbdd-4d42-9ed2-02fd982d15b2"}]
                       [#:concept{:id "VUEH_TU9_KqG",
                                  :type "occupation-name",
                                  :preferred-label "Provningsingenjör, maskin"}
                        {:concept/id "eoMU_aPi_3UH",
                         :concept/type "esco-occupation",
                         :concept/preferred-label "provningsingenjör, flyg",
                         :concept.external-standard/esco-uri
                         "http://data.europa.eu/esco/occupation/608edf30-fe37-4ae5-bdd1-2d3a8fd347bd"}]])

(def test-data-broad [[#:concept{:id "ghs4_JXU_BYt",
                                 :type "occupation-name",
                                 :preferred-label "Planeringsarkitekt/Fysisk planerare"}
                       {:concept/id "XAcf_SQ9_fJw",
                        :concept/type "isco-level-4",
                        :concept/preferred-label "Planeringsarkitekter m.fl.",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/isco/C2164"}]
                      [#:concept{:id "YcvM_Gqk_6U7",
                                 :type "occupation-name",
                                 :preferred-label "Utvecklingsingenjör, elkraft"}
                       {:concept/id "3eya_Cn2_Bt1",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "civilingenjör, elkraft",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/58db3ac6-5217-4d46-8a4c-126598be1d13"}]
                      [#:concept{:id "6ZJd_HTR_Bki",
                                 :type "occupation-name",
                                 :preferred-label "Beräkningsingenjör, el-tele"}
                       {:concept/id "tvYo_sjZ_JZW",
                        :concept/type "esco-occupation",
                        :concept/preferred-label "beräkningsingenjör",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/occupation/fbceeac6-798b-4307-a825-626707a753ad"}]
                      [#:concept{:id "Qzzb_67o_n2P",
                                 :type "occupation-name",
                                 :preferred-label "Utvecklingsingenjör, el-tele"}
                       {:concept/id "fAjK_TWk_kpj",
                        :concept/type "isco-level-4",
                        :concept/preferred-label "Civilingenjörer m.fl., teleteknik",
                        :concept.external-standard/esco-uri
                        "http://data.europa.eu/esco/isco/C2153"}]])

(defn build-concept-relation-map-test-data []
  (-> {}

      (eu/build-concept-relation-map :broad-match test-data-broad)
      (eu/build-concept-relation-map :close-match test-data-close)
      (eu/build-concept-relation-map :narrow-match test-data-narrow)
      (eu/build-concept-relation-map :exact-match test-data-exact)
      (eu/build-concept-relation-map :isco test-data-isco)))

(defn occupation-esco-id-lookup []
  (into {} (map eu/convert-concept-to-esco-ids-2 (build-concept-relation-map-test-data))))

(deftest test-lookup
  (testing "Testing convert occupation-name to esco"
    (let [lookup (occupation-esco-id-lookup)]

      (is
       (= (get lookup "YqiW_QLd_qHo") ["http://data.europa.eu/esco/occupation/fde4372d-8293-42de-afbb-8b5a9b274fe7"]))

      (is
       (= (get lookup "GPNi_fJR_B2B")
          ["http://data.europa.eu/esco/occupation/73e776fb-4d99-4031-bad4-7716f121155d" "http://data.europa.eu/esco/occupation/773e5ea3-235b-4e72-afc5-290dd841eed8"]))

      (is
       (= (get lookup "YcvM_Gqk_6U7")
          ["http://data.europa.eu/esco/occupation/58db3ac6-5217-4d46-8a4c-126598be1d13"]))

      (is
       (= (get lookup "ghs4_JXU_BYt") ["http://data.europa.eu/esco/isco/C2164"])))))

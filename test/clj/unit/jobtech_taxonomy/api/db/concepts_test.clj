(ns unit.jobtech-taxonomy.api.db.concepts-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.concept-types :as concept-types]
            [jobtech-taxonomy.api.db.concepts :as c]
            [jobtech-taxonomy.api.db.database-connection :as dc]
            [jobtech-taxonomy.common.relation :as r]
            [utils.db-fixtures :as udf :refer [cfg]] 
            [jobtech-taxonomy.api.db.nano-id :as nano-id]))

(test/use-fixtures :each (udf/database-with-schema cfg) (udf/add-concepts cfg))

(defn assert-type [type]
  (concept-types/assert-type
   cfg
   {:concept-type type
    :label-sv type
    :label-en type
    :user-id "Gustav"}))

(deftest transact-schema-test
  (testing "Try to write some data and find it again"
    (let [conn (dc/connect cfg)]
      (dc/transact conn {:tx-data [{:concept-type/id "my-id"
                                    :concept-type/label-en "my-english-label"
                                    :concept-type/label-sv "min-svenska-etikett"}]})

      (let [result (dc/q '[:find ?eng-label
                           :in $
                           :where
                           [?e :concept-type/id "my-id"]
                           [?e :concept-type/label-en ?eng-label]]
                         (dc/db conn))]
        (is (= 1 (count result)))
        (is (= ["my-english-label"] (first result)))))))

(deftest assert-concept-part-test
  (testing "Create (assert) a new partial concept in the database"
    (let [[_result new-concept]
          (#'c/assert-concept-part
           cfg
           "my-user-id"
           {:id "gjd2_JSR_AeG"  ;; Valid concept ID
            :type "my-type"
            :definition "My definition."
            :preferred-label "my label"
            :comment "A comment"
            :quality-level 7
            :alternative-labels "my other label"
            :hidden-labels "sort by me"})]
      (is (= {:concept/id "gjd2_JSR_AeG"
              :concept/alternative-labels #{"my other label"}
              :concept/definition "My definition."
              :concept/hidden-labels #{"sort by me"}
              :concept/preferred-label "my label"
              :concept/quality-level 7
              :concept/type "my-type"}
             new-concept))
      (let [result (dc/q '[:find ?preferred-label
                           :in $ ?id
                           :where
                           [?e :concept/id ?id]
                           [?e :concept/preferred-label ?preferred-label]]
                         (dc/get-db cfg :next)
                         "gjd2_JSR_AeG")]
        (is (= 1 (count result)))
        (is (= ["my label"] (first result)))))))

(deftest assert-concept-test
  (testing "Create (assert) a new concept in the database"
    (assert-type "my-type")
    (let [[_result _ts new-concept]
          (c/assert-concept
           cfg
           "my-user-id"
           {:id (nano-id/generate-new-id-with-underscore)
            :type "my-type"
            :preferred-label "my label"
            :definition "My definition."})]
      (is (= {:concept/definition "My definition."
              :concept/preferred-label "my label"
              :concept/type "my-type"}
             (dissoc new-concept :concept/id)))))
  (testing "Fail when creating duplicate concept in the database"
    (let [[result new-concept]
          (c/assert-concept
           cfg
           "my-user-id"
           {:type "my-type"
            :preferred-label "my label"
            :definition "My definition."})]
      (is (not result))
      (is (nil? new-concept))))
  (testing "Fail when creating a concept with non-existing type"
    (let [[result new-concept]
          (c/assert-concept
           cfg
           "my-user-id"
           {:type "my-non-existing-type"
            :preferred-label "my label"
            :definition "My definition."})]
      (is (not result))
      (is (nil? new-concept)))))

(deftest find-concepts-test
  (testing "Find concepts in the database"
    (let [result
          (c/find-concepts
           cfg
           {:id "1111_111_111"
            :version :next})]
      (is (= [{:concept/alternative-labels ["another-alternative-label" "my-alternative-label"],
               :concept/definition "my-definition",
               :concept/hidden-labels ["another-hidden-label" "my-hidden-label"],
               :concept/id "1111_111_111",
               :concept/preferred-label "my-label",
               :concept/quality-level 7,
               :concept/type "my-type"}] result)))
    (let [result
          (c/find-concepts
           cfg
           {:id "2222_222_222"
            :version :next})]
      (is (= [{:concept/alternative-labels ["another-alternative-label1" "my-alternative-label1"],
               :concept/definition "my-definition1",
               :concept/hidden-labels ["another-hidden-label1" "my-hidden-label1"],
               :concept/id "2222_222_222",
               :concept/preferred-label "my-label1",
               :concept/quality-level 7,
               :concept/type "my-type"}] result)))
    (let [result
          (c/find-concepts
           cfg
           {:id "3333_333_333"
            :version :next})]
      (is (= [{:concept/alternative-labels ["another-alternative-label2" "my-alternative-label2"],
               :concept/definition "my-definition2",
               :concept/hidden-labels ["another-hidden-label2" "my-hidden-label2"],
               :concept/id "3333_333_333",
               :concept/preferred-label "my-label2",
               :concept/quality-level 7,
               :concept/type "my-type"}] result)))))

(deftest get-total-concepts-count-test
  (testing "Get total concepts count in the database"
    (is (= 4 (c/get-total-concepts-count
              cfg
              :next)))))

(deftest assert-relation-transaction-data-test
  (testing "Assert relation transaction in the database"
    (let [new-rel
          (#'c/assert-relation-transaction-data
           "2222_222_222"
           "3333_333_333"
           "substitutability"
           "my-description"
           33)]
      (is (some? new-rel))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33,
              :relation/type "substitutability"} new-rel)))))

(deftest assert-relation-part-test
  (testing "Assert relation part in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
             cfg
             "my-user-id"
             {:comment "my-comment"
              :concept-1 "2222_222_222"
              :concept-2 "3333_333_333"
              :type "substitutability"
              :description "my-description"
              :substitutability-percentage 33})]
      (is (some? result))
      (is (some? new-rel))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel)))))

(deftest assert-relation-test
  (testing "Assert relation in the database"
    (let [[result new-rel]
          (c/assert-relation
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})]
      (is (some? result))
      (is (some? new-rel))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel)))))

(deftest does-relation-exist-test
  (testing "Does relation exist in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})
          relation-exist
          (#'c/does-relation-exist?
           cfg
           "2222_222_222:substitutability:3333_333_333")]
      (is (some? result))
      (is (some? relation-exist))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel))
      (is (= true relation-exist)))))

(deftest id-test
  (testing "Calculate relation id for a given concept-1/type/concept-2 tuple"
    (let [result
          (#'r/id
           "2222_222_222"
           "substitutability"
           "3333_333_333")]
      (is (some? result))
      (is (= "2222_222_222:substitutability:3333_333_333" result)))))

(deftest change-relation-type-test
  (testing "Change relation type in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})
          relation-type
          (#'c/change-relation-type?
           "substitutability"
           "broader")]
      (is (some? new-rel))
      (is (some? result))
      (is (some? relation-type))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel))
      (is (= true relation-type)))))

;;TODO fetch-relation-data-by-id function not work
(deftest accumulate-relation-test
  (testing "Accumulate relation in the database"
    (let [[result new-rel]
          (c/assert-relation
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})

          accumulate
          (c/accumulate-relation
           cfg
           "my-user-id"
           {:concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :relation-type "substitutability"
            :substitutability-percentage 33
            :description "my-description"
            :comment "my-comment"
            :new-relation-type "broader"})]
      (is (some? new-rel))
      (is (some? result))
      (is (some? accumulate))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel)))))

(deftest retract-relation-test
  (testing "Retract relation in the database"
    (let [[result new-rel]
          (#'c/assert-relation-part
           cfg
           "my-user-id"
           {:comment "my-comment"
            :concept-1 "2222_222_222"
            :concept-2 "3333_333_333"
            :type "substitutability"
            :description "my-description"
            :substitutability-percentage 33})
          relation-retract
          (#'c/retract-relation
           cfg
           "my-user-id"
           "my-comment"
           "2222_222_222"
           "3333_333_333"
           "substitutability")]
      (is (some? new-rel))
      (is (some? result))
      (is (some? relation-retract))
      (is (= {:relation/concept-1 [:concept/id "2222_222_222"],
              :relation/concept-2 [:concept/id "3333_333_333"],
              :relation/description "my-description",
              :relation/id "2222_222_222:substitutability:3333_333_333",
              :relation/substitutability-percentage 33 ,
              :relation/type "substitutability"}
             new-rel)))))

(deftest fetch-simple-concept-test
  (testing "Fetch simple concept in the database"
    (let [fetch-concept
          (c/fetch-simple-concept
           cfg
           "1111_111_111")]
      (is (some? fetch-concept))
      (is (= {:concept/definition "my-definition",
              :concept/id "1111_111_111",
              :concept/preferred-label "my-label",
              :concept/type "my-type"}
             fetch-concept)))))

(deftest duplicate-concept-exists-test
  (testing "Duplicate concept exists in the database"
    (let [duplicate-concept
          (c/duplicate-concept-exists?
           cfg
           {:concept/id "1111_111_111"
            :concept/preferred-label "my-label"
            :concept/definition "my-definition"
            :concept/type "my-type"})]
      (is (some? duplicate-concept))
      (is (= {:duplicates (), :result false}
             duplicate-concept)))))

(deftest accumulate-concept-test
  (testing "Accumulate concept in the database"
    (let [accumulate
          (c/accumulate-concept
           cfg
           "my-user-id"
           {:id "4444_444_444"
            :type "my-type"
            :definition "my-definition4"
            :preferred-label "my-label4"
            :comment "my-comment"
            :quality-level 7
            :deprecated false
            :alternative-labels "my-alternative-label4|another-alternative-label4"
            :hidden-labels "my-hidden-label4|another-hidden-label4"
            :no-esco-relation true})]
      (is (some? accumulate)))))

(defn put-query-value [dst k v]
  (if (#{:offset :limit} k)
    (assoc dst k v)
    (update dst k #(conj (or % []) v))))

(deftest extend-query-test
  (let [ks [:find :keys :in :args :where :offset :limit]]
    (is (= {:offset 10
            :in [20]}
           (-> {}
               (put-query-value :offset 10)
               (put-query-value :in 20))))
    (doseq [x ks
            y ks
            :let [A (put-query-value {} x 10)
                  B (put-query-value {} y 20)
                  expected (-> {}
                               (put-query-value x 10)
                               (put-query-value y 20))]]
      (is (= expected (c/extend-query A B)))))

  (is (thrown? Exception (c/extend-query {} {:cup-of-coffee 119}))))




























(ns unit.jobtech-taxonomy.common.math-utils-test
  (:require [clojure.test :refer [deftest is testing]]
            [java-time.api :as jt]
            [jobtech-taxonomy.common.math-utils :as mu]))

(defn- jt-duration [seconds nanos]
  (jt/+
   (jt/duration seconds :seconds)
   (jt/duration nanos :nanos)))

(deftest duration->ms-double-test
  (testing "Converting duration to milliseconds as double"
    (is (= 0.0
           (mu/duration->ms-double (jt-duration 0 0))))
    (is (= 1000.0
           (mu/duration->ms-double (jt-duration 1 0))))
    (is (= 0.001
           (mu/duration->ms-double (jt-duration 0 1000))))
    (is (= 1.0
           (mu/duration->ms-double (jt-duration 0 1000000))))
    (is (= 500.0
           (mu/duration->ms-double (jt-duration 0 500000000))))
    (is (= 1123.456789
           (mu/duration->ms-double (jt-duration 1 123456789)))))
  (testing "Invalid input throws"
    (is (thrown? IllegalArgumentException
                 (mu/duration->ms-double :not-a-duration)))))

(deftest mean-test
  (testing "Calculating the mean of a list of values"
    (is (= 0 (mu/mean [0 0 0])))
    (is (= 2 (mu/mean [1 2 3])))
    (is (= 17/2 (mu/mean [7 10])))
    (is (= 10 (mu/mean [10])))
    (is (= 1/5 (mu/mean [1 0 0 0 0]))))
  (testing "Invalid input throws"
    (is (thrown? IllegalArgumentException (mu/mean [])))
    (is (thrown? IllegalArgumentException (mu/mean nil)))
    (is (thrown? IllegalArgumentException (mu/mean :not-a-list)))
    (is (thrown? IllegalArgumentException (mu/mean [:wrong-type])))
    (is (thrown? IllegalArgumentException (mu/mean "A string is obviously not a list of numbers")))))

(deftest population-variance-test
  (testing "Calculating the variance"
    (is (= 0.0 (mu/population-variance [0 0 0])))
    (is (= 0.6666666666666666 (mu/population-variance [1 2 3])))
    (is (= 2.25 (mu/population-variance [7 10])))
    (is (= 0.0 (mu/population-variance [10])))
    (is (= 0.16000000000000006 (mu/population-variance [1 0 0 0 0]))))
  (testing "Invalid input throws"
    (is (thrown? IllegalArgumentException (mu/population-variance :not-a-list)))
    (is (thrown? IllegalArgumentException (mu/population-variance [:wrong-type])))
    (is (thrown? IllegalArgumentException (mu/population-variance "A string is obviously not a list of numbers")))))

(deftest population-standard-deviation-test
  (testing "Calculating the standard deviation"
    (is (= 0.0 (mu/population-standard-deviation [0 0 0])))
    (is (= 0.816496580927726 (mu/population-standard-deviation [1 2 3])))
    (is (= 1.5 (mu/population-standard-deviation [7 10])))
    (is (= 0.0 (mu/population-standard-deviation [10])))
    (is (= 0.4000000000000001 (mu/population-standard-deviation [1 0 0 0 0]))))
  (testing "Invalid input throws"
    (is (thrown? IllegalArgumentException (mu/population-standard-deviation :not-a-list)))
    (is (thrown? IllegalArgumentException (mu/population-standard-deviation [:wrong-type])))
    (is (thrown? IllegalArgumentException (mu/population-standard-deviation "A string is obviously not a list of numbers")))))

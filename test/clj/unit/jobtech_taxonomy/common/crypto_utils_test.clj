(ns unit.jobtech-taxonomy.common.crypto-utils-test
  (:require [clojure.test :refer [deftest is testing]]
            [jobtech-taxonomy.common.crypto-utils :as ccu]
            [utils.helpers :as uh]))

(set! *warn-on-reflection* true)

(deftest utf-8->bytes-test
  (testing "Test utf-8->bytes"
    (is (= "class [B" (str (type (ccu/utf-8->bytes "abc")))))
    (is (= [97 98 99] (into [] (ccu/utf-8->bytes "abc")))))
  (testing "Nil throws exception"
    (is (= :null-pointer-exception
           (try (ccu/utf-8->bytes nil)
                (catch java.lang.NullPointerException _
                  :null-pointer-exception))))))

(deftest sha-3-digest-test
  (testing "Default is SHA3-256"
    (is (= "SHA3-256" (ccu/digest->get-algorithm (#'ccu/sha-3-digest)))))
  (testing "Test sha-3-digest with key"
    (is (= "SHA3-224" (ccu/digest->get-algorithm (#'ccu/sha-3-digest :SHA3-224))))
    (is (= "SHA3-256" (ccu/digest->get-algorithm (#'ccu/sha-3-digest :SHA3-256))))
    (is (= "SHA3-384" (ccu/digest->get-algorithm (#'ccu/sha-3-digest :SHA3-384))))
    (is (= "SHA3-512" (ccu/digest->get-algorithm (#'ccu/sha-3-digest :SHA3-512)))))
  (testing "Test sha-3-digest with invalid key"
    (is (thrown?
         IllegalArgumentException
         (#'ccu/sha-3-digest :not-a-sha-3-version)))))

(deftest bytes->hex-str-test
  (testing "Test converting bytes to hex string"
    (let [string-bytes (ccu/utf-8->bytes "abc")]
      (is (= "616263" (ccu/bytes->hex-str string-bytes)))))
  (testing "Test converting bytes to hex string"
    (is (= "000102030405060708090a0b0c0d0e0f10"
           (ccu/bytes->hex-str
            (byte-array (map byte [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16])))))
    (is (= "7f" (ccu/bytes->hex-str
                 (byte-array (map byte [127]))))))
  (testing "Nil throws exception"
    (is (nil? (ccu/bytes->hex-str nil)))))

(deftest digest-input-streamable-test
  (let [string-bytes (ccu/utf-8->bytes "abc")
        default-sha-key ccu/sha-3-default
        default-opts {:sha-key default-sha-key
                      :buffer-size @#'ccu/reader-buffer-size-default}]
    (testing "Calling digest-input-streamable without options should use the default SHA-3"
      (is (= (name default-sha-key)
             (ccu/digest->get-algorithm
              (ccu/digest-input-streamable string-bytes)))))
    (testing "Calling digest-input-streamable without options should be the same as calling with default options"
      (is (= (-> (ccu/digest-input-streamable string-bytes) ccu/digest->digest vec)
             (-> (ccu/digest-input-streamable string-bytes default-opts)
                 ccu/digest->digest vec))))))

(deftest utf-8->sha-3-test
  (testing "Default SHA-3 hashing"
    (let [sha-3 (ccu/utf-8->sha-3 "abc")]
      (is (= 32 (count sha-3)))
      (is (= "3a985da74fe225b2045c172d6bd390bd855f086e3e9d525b46bfe24511431532"
             (ccu/bytes->hex-str sha-3))))
    (let [sha-3 (ccu/utf-8->sha-3 "This is just a bit longer than thirty two bytes.")]
      (is (= 32 (count sha-3)))
      (is (= "674a05b76893c3bb6b9c55d415174eb6243adcb93f03638bc0b957f9b8247d2e"
             (ccu/bytes->hex-str sha-3)))))
  (testing "SHA-3 hashing for SHA3-224"
    (let [sha-3 (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-224})]
      (is (= 28 (count sha-3)))
      (is (= "e642824c3f8cf24ad09234ee7d3c766fc9a3a5168d0c94ad73b46fdf"
             (ccu/bytes->hex-str sha-3))))
    (let [sha-3 (ccu/utf-8->sha-3 "This is just a bit longer than thirty two bytes."
                                  {:sha-key :SHA3-224})]
      (is (= 28 (count sha-3)))
      (is (= "363c07483c9b38fd5ec72a84e248755b919c0d5097d7f3f8286b2c50"
             (ccu/bytes->hex-str sha-3)))))
  (testing "SHA-3 hashing for SHA3-256"
    (let [sha-3 (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-256})]
      (is (= 32 (count sha-3)))
      (is (= "3a985da74fe225b2045c172d6bd390bd855f086e3e9d525b46bfe24511431532"
             (ccu/bytes->hex-str sha-3))))
    (let [sha-3 (ccu/utf-8->sha-3 "This is just a bit longer than thirty two bytes."
                                  {:sha-key :SHA3-256})]
      (is (= 32 (count sha-3)))
      (is (= "674a05b76893c3bb6b9c55d415174eb6243adcb93f03638bc0b957f9b8247d2e"
             (ccu/bytes->hex-str sha-3)))))
  (testing "SHA-3 hashing for SHA3-384"
    (let [sha-3 (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-384})]
      (is (= 48 (count sha-3)))
      (is (= "ec01498288516fc926459f58e2c6ad8df9b473cb0fc08c2596da7cf0e49be4b298d88cea927ac7f539f1edf228376d25"
             (ccu/bytes->hex-str sha-3))))
    (let [sha-3 (ccu/utf-8->sha-3
                 "This is just a bit longer than thirty two bytes."
                 {:sha-key :SHA3-384})]
      (is (= 48 (count sha-3)))
      (is (= "7c7fe3c9e2300b5da9cf4c60f1a6c0cb6dfb956c85416361f36b21182f642b8bf365e20077d0ef8e8a5ed7384748b0dc"
             (ccu/bytes->hex-str sha-3)))))
  (testing "SHA-3 hashing for SHA3-512"
    (let [sha-3 (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-512})]
      (is (= 64 (count sha-3)))
      (is (= "b751850b1a57168a5693cd924b6b096e08f621827444f70d884f5d0240d2712e10e116e9192af3c91a7ec57647e3934057340b4cf408d5a56592f8274eec53f0"
             (ccu/bytes->hex-str sha-3))))
    (let [sha-3 (ccu/utf-8->sha-3 "This is just a bit longer than thirty two bytes."
                                  {:sha-key :SHA3-512})]
      (is (= 64 (count sha-3)))
      (is (= "cd6094ee0cc4599142cbb90aa985686469aa319b53bcdc2e4e8a2e396ba05325a44fad6abfa741cb1bd58b6cb88df478adf6971b019d14dcb530928d68cceebf"
             (ccu/bytes->hex-str sha-3)))))
  (testing "SHA-3 should equal for same string"
    (is (= (ccu/utf-8->sha-3 "abc")
           (ccu/utf-8->sha-3 "abc")))
    (is (= (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-224})
           (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-224})))
    (is (= (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-256})
           (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-256})))
    (is (= (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-384})
           (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-384})))
    (is (= (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-512})
           (ccu/utf-8->sha-3 "abc" {:sha-key :SHA3-512}))))
  (testing "Nil throws exception"
    (is (= {:data {:input nil}
            :message "Input must be a string"}
           (uh/catch-ex-info
            (ccu/utf-8->sha-3 nil))))))

(deftest input-streamable->sha-3-test
  (testing "Test SHA-3 hashing"
    (let [string-bytes (ccu/utf-8->bytes "abc")
          sha-3 (ccu/input-streamable->sha-3 string-bytes)]
      (is (= 32 (count sha-3)))
      (is (= "3a985da74fe225b2045c172d6bd390bd855f086e3e9d525b46bfe24511431532"
             (ccu/bytes->hex-str sha-3)))))
  (testing "Test SHA-3 hashing with buffer size"
    (let [string-bytes (ccu/utf-8->bytes "This is just a bit longer than thirty two bytes.")
          sha-3 (ccu/input-streamable->sha-3 string-bytes {:buffer-size 10})]
      (is (= 32 (count sha-3)))
      (is (= "674a05b76893c3bb6b9c55d415174eb6243adcb93f03638bc0b957f9b8247d2e"
             (ccu/bytes->hex-str sha-3)))))
  (testing "Nil throws exception"
    (is (= :null-pointer-exception
           (try (ccu/input-streamable->sha-3 nil)
                (catch IllegalArgumentException _
                  :null-pointer-exception))))))

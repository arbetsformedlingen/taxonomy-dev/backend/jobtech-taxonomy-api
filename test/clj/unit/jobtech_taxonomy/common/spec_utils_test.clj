(ns unit.jobtech-taxonomy.common.spec-utils-test
  (:require [clojure.spec.alpha :as spec]
            [clojure.test :refer [deftest is testing]]
            [jobtech-taxonomy.common.spec-utils :as spec-utils]))

(spec/def ::number number?)
(spec/def ::real ::number)
(spec/def ::imag ::number)
(spec/def ::complex-number (spec/keys :req-un [::real ::imag]))
(spec/def ::complex-numbers (spec/coll-of ::complex-number))

(deftest complex-number-test
  (testing "Collection of maps with required keys"
    (is (spec/valid? ::complex-numbers [{:real 9 :imag 4}]))
    (is (not (spec/valid? ::complex-numbers [{:real 9}])))
    (let [complex-numbers? (spec-utils/validator ::complex-numbers)]
      (doseq [[validity sample-value] [[false 9]
                                       [true []]
                                       [false [{}]]
                                       [true [{:real 9 :imag 4}]]
                                       [false [{:real :nine :imag "four"}]]]]
        (is (= validity (complex-numbers? sample-value)))
        (is (= validity (spec/valid? ::complex-numbers sample-value)))))))

(spec/def ::first-name string?)
(spec/def ::last-name string?)
(spec/def ::phone-number string?)
(spec/def ::email-address string?)
(spec/def ::age-in-years int?)
(spec/def ::has-insurance? boolean?)
(spec/def ::person (spec/keys :req-un [::first-name ::last-name]
                              :req [::email-address]
                              :opt-un [::phone-number]
                              :opt [::age-in-years ::has-insurance?]))

(deftest person-test
  (testing "Map with required and optional keys, qualified and unqualified"
    (let [person? (spec-utils/validator ::person)]
      (doseq [[validity sample-value]
              [[true {:first-name "Gabriel"
                      :last-name "Svensson"
                      ::email-address "gabriel.sv3nne@mjao.katt.edu"
                      :phone-number "0041 77 234333333333"
                      ::age-in-years 19
                      ::has-insurance? true}]
               [true {:first-name "Gabriel"
                      :last-name "Svensson"
                      ::email-address "gabriel.sv3nne@mjao.katt.edu"
                      :phone-number "0041 77 234333333333"
                      :age-in-years 19}]
               [true {:first-name "Gabriel"
                      :last-name "Svensson"
                      ::email-address "gabriel.sv3nne@mjao.katt.edu"
                      :phone-number "0041 77 234333333333"}]
               [true {:first-name "Gabriel"
                      :last-name "Svensson"
                      ::email-address "gabriel.sv3nne@mjao.katt.edu"}]
               [false {:first-name "Gabriel"
                       :last-name "Svensson"
                       :email-address "gabriel.sv3nne@mjao.katt.edu"}]
               [false {:first-name "Gabriel"
                       ::email-address "gabriel.sv3nne@mjao.katt.edu"}]
               [false {}]
               [false {:first-name "Gabriel"
                       :last-name "Svensson"
                       ::email-address "gabriel.sv3nne@mjao.katt.edu"
                       :phone-number 234
                       ::age-in-years 19
                       ::has-insurance? true}]
               [false {:first-name "Gabriel"
                       :last-name "Svensson"
                       ::email-address "gabriel.sv3nne@mjao.katt.edu"
                       :phone-number "0041 77 234333333333"
                       ::age-in-years 19
                       ::has-insurance? "Yes!"}]
               [false {:first-name "Gabriel"
                       :last-name 5
                       ::email-address "gabriel.sv3nne@mjao.katt.edu"
                       :phone-number "0041 77 234333333333"
                       ::age-in-years 19
                       ::has-insurance? true}]]]
        (is (= validity (person? sample-value)))
        (is (= validity (spec/valid? ::person sample-value)))))))

(deftest unsupported-feature-test
  (testing "nilable not supported"
    (is (thrown? AssertionError (spec-utils/validator (spec/nilable number?))))))

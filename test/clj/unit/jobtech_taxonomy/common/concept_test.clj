(ns unit.jobtech-taxonomy.common.concept-test
  (:require [clojure.test :refer [deftest is]]
            [jobtech-taxonomy.common.concept :as c]))

(deftest id-test
  (is (c/id? "abcd_xyz_123"))
  (is (not (c/id? "abcd_xyz_123d")))
  (is (not (c/id? "abcd_xyzy_123")))
  (is (not (c/id? "abc99_xyz_123")))
  (is (not (c/id? 9))))

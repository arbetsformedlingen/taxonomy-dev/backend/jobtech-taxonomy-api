(ns unit.jobtech-taxonomy.common.io-utils-test
  (:require [babashka.fs :as fs]
            [clojure.test :refer [deftest is testing]]
            [jobtech-taxonomy.common.io-utils :as iou]
            [utils.helpers :as uh]))

(set! *warn-on-reflection* true)

(defn- drop-ex-info [ex-data & keys]
  (update ex-data :data #(apply dissoc % keys)))

(deftest read-edn-string-test
  (testing "Succeed in reading good EDN"
    (is (= {:a :b}
           (#'iou/read-edn-string "{:a :b}"))))
  (testing "Reading a string"
    (is (= "a string"
           (#'iou/read-edn-string "\"a string\""))))
  (testing "Fail for invalid EDN"
    (is (= {:message "EDN: EOF while reading"
            :data {}}
           (drop-ex-info
            (uh/catch-ex-info
             (#'iou/read-edn-string "{:broken"))
            :exception)))
    (is (= {:message "EDN: Map literal must contain an even number of forms"
            :data {}}
           (drop-ex-info
            (uh/catch-ex-info
             (#'iou/read-edn-string "{:invalid-map}"))
            :exception))))
  (testing "Return nil if there is nothing to be read"
    (is (nil? (iou/read-edn-string "")))
    (is (nil? (iou/read-edn-string "    ")))
    (is (nil? (iou/read-edn-string " \n \t   ")))))

(deftest validate-edn-for-test
  (testing "Validate valid EDN with valid schema"
    (is (= {:one :two}
           ((iou/validate-for [:map-of :keyword :keyword])
            {:one :two}))))
  (testing "Valid EDN not matching valid schema"
    (is (= {:message "Invalid EDN data, cause: {:one [\"should be a keyword\"]}"
            :data {:schema [:map-of :keyword :keyword]}}
           (uh/catch-ex-info
            ((iou/validate-for [:map-of :keyword :keyword])
             {:one "bob"})))))
  (testing "Invalid validator"
    (is (= {:data
            {:data {:schema :my-invalid-schema
                    :form :my-invalid-schema}
             :message :malli.core/invalid-schema
             :type :malli.core/invalid-schema}
            :message ":malli.core/invalid-schema"}
           (uh/catch-ex-info
            (iou/validate-for :my-invalid-schema))))))

(deftest get-env-test
    ;; We cannot set environment variables from within Clojure.
    ;; So we use a subset of whatever environment variables that
    ;; are defined and check that we can read them.
  (is (nil? (#'iou/get-env "NONEXISTENT_ENV_VAR")))
  (doseq [[env-var-name env-var-value] (take 3 (System/getenv))]
    (is (= env-var-value (#'iou/get-env env-var-name)))))

(deftest read-json-resource-test
  (testing "Read JSON resource"
    (is (= {"name" "dummy-data.json" "purpose" "dummy data for testing"}
           (iou/read-json-resource [:map ["name" :string] ["purpose" :string]] "common/dummy-data.json"))))
  (testing "Resource not found"
    (testing "File not found"
      (is (= {:message "JSON: Cannot open <nil> as a Reader."
              :data {:resource-name "nonexistent.edn"}}
             (drop-ex-info
              (uh/catch-ex-info
               (iou/read-json-resource :int "nonexistent.edn"))
              :exception))))))

(deftest read-json-file-test
  (testing "Read JSON file"
    (is (= {"name" "dummy-data.json" "purpose" "dummy data for testing"}
           (iou/read-json-file [:map ["name" :string] ["purpose" :string]] "test/resources/common/dummy-data.json"))))
  (testing "File not found"
    (is (= {:message "JSON: nonexistent.json (No such file or directory)"
            :data {:file-name "nonexistent.json"}}
           (drop-ex-info
            (uh/catch-ex-info
             (iou/read-json-file :int "nonexistent.json"))
            :exception)))))

(deftest read-edn-file-test
  (testing "Read EDN file"
    (is (= {:name "dummy-data.json" :purpose "dummy data for testing"}
           (iou/read-edn-file [:map [:name :string] [:purpose :string]] "test/resources/common/dummy-data.edn"))))
  (testing "File not found"
    (is (= {:message "EDN: nonexistent.edn (No such file or directory)"
            :data {:file-name "nonexistent.edn"}}
           (drop-ex-info
            (uh/catch-ex-info
             (iou/read-edn-file :int "nonexistent.edn"))
            :exception)))))

(deftest read-edn-resource-test
  (testing "Read EDN resource"
    (is (= {:name "dummy-data.json"
            :purpose "dummy data for testing"}
           (iou/read-edn-resource [:map [:name :string] [:purpose :string]] "common/dummy-data.edn"))))
  (testing "Resource not found"
    (is (= {:message "EDN: Cannot open <nil> as a Reader."
            :data {:resource-name "nonexistent.edn"}}
           (drop-ex-info
            (uh/catch-ex-info
             (iou/read-edn-resource :int "nonexistent.edn"))
            :exception)))))

(deftest get-env-var-value-test
  (testing "Get existing environment variable"
    (with-redefs [iou/get-env (fn [_env-var-name] "value")]
      (is (= "value" (#'iou/get-env-var-value "ENV_VAR"))))))
(testing "Get missing environment variable"
  (with-redefs [iou/get-env (fn [_env-var-name] nil)]
    (is (nil? (#'iou/get-env-var-value "MISSING_VAR")))))

(deftest validate-env-value-test
  (testing "Validate environment variable with valid schema"
    (is (= "value" (#'iou/validate-env-value :string "\"value\""))))
  (testing "Validate environment variable with invalid schema"
    (is (= {:message "Invalid EDN data, cause: [\"should be an integer\"]" :data {:schema :int}}
           (uh/catch-ex-info
            (#'iou/validate-env-value :int "\"value\""))))))

(deftest get-env-var-test
  (testing "Get existing environment variable"
    (with-redefs [iou/get-env (fn [_env-var-name] "\"value\"")]
      (is (= "value" (iou/get-env-var :string "ENV_VAR")))))
  (testing "Get missing required environment variable"
    (with-redefs [iou/get-env (fn [_env-var-name] nil)]
      (is (= {:message "Invalid EDN data, cause: [\"should be a string\"]"
              :data {:schema :string}}
             (uh/catch-ex-info
              (iou/get-env-var :string "MISSING_VAR"))))))
  (testing "Get missing optional environment variable"
    (with-redefs [iou/get-env (fn [_env-var-name] nil)]
      (is (nil? (iou/get-env-var [:or :nil :int] "MISSING_VAR"))))))

(deftest get-files-from-test
  (testing "Missing directory"
    (is (= {:message "File not found" :data {:file-name "nonexistent"}}
           (uh/catch-ex-info
            (iou/pattern-file-seq-from "nonexistent")))))
  (testing "Get files from directory with pattern"
    (let [directory "test/resources/common"
          pattern #".+\.txt$"
          expected-files #{"test/resources/common/dummy-data.txt"
                           "test/resources/common/other/dummy-data.txt"}
          result (iou/pattern-file-seq-from directory pattern)]
      (is (instance? java.io.File (first result)))
      (is (= expected-files (into #{} (map str) result)))))
  (testing "Get all files from directory"
    (let [directory "test/resources/common"
          expected-files #{"test/resources/common/dummy-data.txt"
                           "test/resources/common/dummy-data.json"
                           "test/resources/common/dummy-data.edn"
                           "test/resources/common"
                           "test/resources/common/other"
                           "test/resources/common/other/dummy-data.txt"}
          result (iou/pattern-file-seq-from directory)]
      (is (instance? java.io.File (first result)))
      (is (= expected-files (into #{} (map str) result))))))

(deftest existing-file-test
  (testing "Existing file"
    (is (instance? java.io.File (iou/existing-file "README.md"))))
  (testing "Non-existing file"
    (is (= {:data {:file-name "nonexistent-file.txt"}
            :message "File not found"}
           (uh/catch-ex-info
            (iou/existing-file "nonexistent-file.txt"))))))

(deftest keyword-from-x-test
  (testing "keyword-from-x"
    (is (= :a (iou/keyword-from-x "a")))
    (is (= :a (iou/keyword-from-x :a)))
    (is (= :a (iou/keyword-from-x 'a)))
    (is (= :b (iou/keyword-from-x 'b)))
    (is (nil? (iou/keyword-from-x nil)))
    (is (thrown? Exception (iou/keyword-from-x 119)))))

(deftest delete-directory-contents-test
  (testing "Directory with files and nested directories"
    (let [root (fs/create-temp-dir)
          a (doto (fs/file root "a.txt")
              (spit "A"))
          b (doto (fs/file root "b.txt")
              (spit "B"))
          c (doto (fs/path root "c")
              (fs/create-dir))
          d (doto (fs/file c "d.txt")
              (spit  "D"))]
      (is (fs/directory? root))
      (is (= "A" (slurp a)))
      (is (= "B" (slurp b)))
      (is (= "D" (slurp d)))
      (iou/delete-directory-contents root)
      (is (fs/directory? root))
      (doseq [x [a b c d]]
        (is (not (fs/exists? x))))))
  (testing "Fail to delete directory contents in non-existing directory"
    (let [root (fs/path (fs/create-temp-dir) "not_created")]
      (is (thrown? clojure.lang.ExceptionInfo
                   (iou/delete-directory-contents root)))))
  (testing "Fail to delete directory contents inside a file"
    (let [root (doto (fs/file (fs/create-temp-file))
                 (spit "X"))]
      (is (= "X" (slurp root)))
      (is (thrown? clojure.lang.ExceptionInfo
                   (iou/delete-directory-contents root)))
      (is (= "X" (slurp root))))))

(defn- render-file-tree
  ([tree] (let [dst (fs/create-temp-dir)]
            (render-file-tree dst tree)
            dst))
  ([root tree]
   (cond
     (nil? tree) nil
     (string? tree) (spit (fs/file root) tree)
     :else (let [[sub & children] tree
                 root (fs/path root sub)]
             (when-not (some string? children)
               (fs/create-dir root))
             (doseq [child children]
               (render-file-tree root child))))))

(deftest non-merging-copy-directory-contents-test
  (testing "Existing target directory with some contents"
    (let [src-root (render-file-tree ["src"
                                      ["a.txt" "ABC"]
                                      ["b"
                                       ["c.txt" "XYZ"]]])
          dst-root (render-file-tree ["dst"
                                      ["d.txt" "123"]])

          src-path (fs/path src-root "src")
          dst-path (fs/path dst-root "dst")
          dst-time (fs/creation-time dst-path)]
      (is (= "ABC" (-> src-root
                       (fs/file "src/a.txt")
                       slurp)))
      (is (= "123" (-> dst-root
                       (fs/file "dst/d.txt")
                       slurp)))
      (iou/non-merging-copy-directory-contents src-path dst-path)
      (is (= dst-time (fs/creation-time dst-path)))
      (is (not (fs/exists? (fs/file dst-root "dst/d.txt"))))
      (is (= "ABC" (slurp (fs/file dst-root "dst/a.txt"))))
      (is (= "XYZ" (slurp (fs/file dst-root "dst/b/c.txt"))))))
  (testing "Non-existing target directory"
    (let [src-root (render-file-tree ["src"
                                      ["a.txt" "ABC"]
                                      ["b"
                                       ["c.txt" "XYZ"]]])
          dst-root (render-file-tree nil)]
      (is (= "ABC" (-> src-root
                       (fs/file "src/a.txt")
                       slurp)))
      (iou/non-merging-copy-directory-contents
       (fs/path src-root "src")
       (fs/path dst-root "dst"))
      (is (= "ABC" (slurp (fs/file dst-root "dst/a.txt"))))
      (is (= "XYZ" (slurp (fs/file dst-root "dst/b/c.txt")))))))

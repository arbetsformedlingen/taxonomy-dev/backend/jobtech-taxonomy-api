(ns unit.jobtech-taxonomy.common.malli-utils-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.common.malli-utils :as mu]
            [utils.helpers :as uh]))

(deftest test-malli-utils-schema-part
  (testing "Request validator for simple value under key"
    (let [vc (mu/config-validator-for [:k :int])]
      (is (= 42
             (vc {:k 42})))
      (is (= {:message "Invalid configuration under `:k`"
              :data {:k ["should be an integer"]}}
             (uh/catch-ex-info
              (vc {:k "not an integer"}))))
      (is (= nil
             (vc {:other "some other config"})))))
  (testing "Request validator for complex value under key"
    (let [vc (mu/config-validator-for
              [:k [:map {:closed true}
                   [:a {:optional true} :int] [:b :string]]])]
      (is (= {:a 42 :b "a string"}
             (vc {:k {:a 42 :b "a string"}})))
      (is (= {:message "Invalid configuration under `:k`"
              :data {:k {:a ["should be an integer"]}}}
             (uh/catch-ex-info
              (vc {:k {:a nil :b "a string"}}))))
      (is (= {:message "Invalid configuration under `:k`"
              :data {:k {:b ["should be a string"]}}}
             (uh/catch-ex-info
              (vc {:k {:b 42}}))))
      (is (= {:message "Invalid configuration under `:k`"
              :data {:k {:b ["missing required key"]}}}
             (uh/catch-ex-info
              (vc {:k {:a 42}}))))
      (is (= {:message "Invalid configuration under `:k`"
              :data {:k {:c ["disallowed key"]}}}
             (uh/catch-ex-info
              (vc {:k {:a 42 :b "a string" :c :not-a-valid-key}}))))
      (is (= nil
             (vc {:other "some other config"})))))
  (testing "Request validator for invalid schemas"
    (is (= {:message "Failed validating `schema-part`"
            :data {:error ["invalid tuple size 0, expected 2"]}}
           (uh/catch-ex-info
            (mu/config-validator-for []))))
    (is (= {:message "Failed validating `schema-part`"
            :data {:error ["invalid tuple size 1, expected 2"]}}
           (uh/catch-ex-info
            (mu/config-validator-for [1]))))
    (is (= {:message "Failed validating `schema-part`"
            :data {:error [["should be a keyword"]
                           ["`1` is not a valid malli schema"]]}}
           (uh/catch-ex-info
            (mu/config-validator-for [1 1]))))
    (is (= {:message "Failed validating `schema-part`"
            :data {:error [nil ["`1` is not a valid malli schema"]]}}
           (uh/catch-ex-info
            (mu/config-validator-for [:k 1]))))
    (is (= {:message "Failed validating `schema-part`"
            :data {:error [["should be a keyword"]]}}
           (uh/catch-ex-info
            (mu/config-validator-for [1 :int]))))))

(deftest test-coerce-string
  (testing "Coercing with invalid schema"
    (is (= {:data {:data {:schema :not-a-malli-schema
                          :form :not-a-malli-schema}
                   :message :malli.core/invalid-schema
                   :type :malli.core/invalid-schema}
            :message ":malli.core/invalid-schema"}
           (uh/catch-ex-info
            (mu/coerce-string :not-a-malli-schema)))))
  (testing "Coercing string with valid schema"
    (let [coercer (mu/coerce-string :string)]
      (is (= "hello" (coercer "hello")))
      (is (= ":malli.core/coercion"
             (:message
              (uh/catch-ex-info
               (coercer 42)))))
      (is (= "" (coercer "")))))
  (testing "Coercing string with invalid schema"
    (let [coercer (mu/coerce-string [:or :int :nil])]
      (is (= 42 (coercer "42")))
      (is (nil? (coercer nil)))
      (is (= ":malli.core/coercion"
             (:message
              (uh/catch-ex-info
               (coercer "hello"))))))))

(deftest test-throwing-validator-for
  (testing "Validator for invalid schema"
    (is (= {:data {:data {:schema :not-a-malli-schema
                          :form :not-a-malli-schema}
                   :message :malli.core/invalid-schema
                   :type :malli.core/invalid-schema}
            :message ":malli.core/invalid-schema"}
           (uh/catch-ex-info
            (mu/throwing-validator-for :not-a-malli-schema)))))
  (testing "Valid data"
    (let [validator (mu/throwing-validator-for [:int])]
      (is (= 42 (validator 42)))))
  (testing "Invalid data"
    (let [validator (mu/throwing-validator-for [:int])]
      (is (= "Invalid data"
             (:message
              (uh/catch-ex-info
               (validator "hello"))))))))

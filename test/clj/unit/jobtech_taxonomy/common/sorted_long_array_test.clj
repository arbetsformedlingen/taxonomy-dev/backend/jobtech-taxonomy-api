(ns unit.jobtech-taxonomy.common.sorted-long-array-test
  (:require [clojure.test :refer [deftest is testing]]
            [jobtech-taxonomy.common.sorted-long-array :as sla]))

(deftest construction-test
  (testing "Constructing sorted long array from vector"
    (let [arr (sla/distinct-sorted-long-array [9 4 4 7 3 3])]
      (is (= 4 (alength arr)))
      (is (= 3 (aget arr 0)))
      (is (= 4 (aget arr 1)))
      (is (= 7 (aget arr 2)))
      (is (= 9 (aget arr 3)))))
  (testing "Constructing sorted long array from a set"
    (let [arr (sla/distinct-sorted-long-array #{3 5 9})]
      (is (= 3 (alength arr)))
      (is (= 3 (aget arr 0)))
      (is (= 5 (aget arr 1)))
      (is (= 9 (aget arr 2)))))
  (testing "Constructing a sorted long array from an empty set"
    (is (= 0 (alength (sla/distinct-sorted-long-array #{})))))
  (testing "Constructing a sorted long array from an empty vector"
    (is (= 0 (alength (sla/distinct-sorted-long-array []))))))

(deftest lookup-fn-test
  (testing "non-empty lookup"
    (let [f (sla/lookup-fn {3 :a 5 :b})]
      (is (nil? (f :not-a-number)))
      (is (= :x (f :not-a-number :x)))

      (is (nil? (f 2)))
      (is (= :a (f 3)))
      (is (nil? (f 4)))
      (is (= :b (f 5)))
      (is (nil? (f 6)))

      (is (= :x (f 2 :x)))
      (is (= :a (f 3 :x)))
      (is (= :x (f 4 :x)))
      (is (= :b (f 5 :x)))
      (is (= :x (f 6 :x)))))
  (testing "empty lookup"
    (let [f (sla/lookup-fn {})]
      (is (nil? (f 9)))
      (is (nil? (f :y)))
      (is (= :x (f 9 :x)))
      (is (= :x (f :y :x))))))

(deftest memoize-test
  (testing "Memoization"
    (let [counter (atom 0)
          square (fn [x]
                   (swap! counter inc)
                   (* x x))
          f (sla/memoize square (sla/distinct-sorted-long-array [3 10 11]))]
      (is (= 0 (deref counter)))
      (is (= 9 (f 3)))
      (is (= 1 (deref counter)))
      (is (= 9 (f 3)))
      (is (= 1 (deref counter)))
      (is (= 100 (f 10)))
      (is (= 2 (deref counter)))
      (is (= 100 (f 10)))
      (is (= 2 (deref counter)))
      (is (= 9 (f 3)))
      (is (= 2 (deref counter))))))

(deftest contains-test
  (testing "Test contains? for a range of integers"
    (let [arr (sla/distinct-sorted-long-array [3 9])]
      (dotimes [i 20]
        (is (= (sla/contains? arr i)
               (contains? #{3 9} i)))))))

(deftest stateful-predicate-test
  (testing "Filtering by stateful-predicate"
    (is (= [3 3 3 3 3 5 5 5 6 6]
           (into []
                 (filter (sla/stateful-predicate
                          (sla/distinct-sorted-long-array [3 5 6])))
                 [1 1 1 2 3 3 3 3 3 4 5 5 5 6 6 7])))
    (is (= [6 6]
           (into []
                 (filter (sla/stateful-predicate
                          (sla/distinct-sorted-long-array [3 5 6])))
                 [0 6 6 20])))
    (is (= [5]
           (into []
                 (filter (sla/stateful-predicate
                          (sla/distinct-sorted-long-array [3 5 6])))
                 [0 5 20])))
    (is (= [3 6 6]
           (into []
                 (filter (sla/stateful-predicate
                          (sla/distinct-sorted-long-array [3 5 6])))
                 [0 3 4 6 6 20])))))

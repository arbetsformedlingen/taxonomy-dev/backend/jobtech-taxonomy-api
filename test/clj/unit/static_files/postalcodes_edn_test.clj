(ns unit.static-files.postalcodes-edn-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy.api.db.postalcodes :as pc]))

(deftest postalcodes-edn-size-test
  (testing "Number of postalcodes as of latest change"
    (is (= 17505 (count pc/postal-codes)))))

(deftest winter-2024-added-code-54938-test
  (testing "Regression test for postal code 54938"
    (let [postal-code "54938"
          result (filter #(= (:postal_code %) postal-code) pc/postal-codes)]
      (is (= [{:lau_2_code_2015 "1496"
               :locality_preferred_label "Skövde"
               :municipality_deprecated_legacy_id "287"
               :municipality_id "fqAy_4ji_Lz2"
               :municipality_preferred_label "Skövde"
               :national_nuts_level_3_code_2019 "14"
               :postal_code "54938"
               :region_deprecated_legacy_id "204"
               :region_id "zdoY_6u5_Krt"
               :region_term "Västra Götalands län"}]
             result)))))

(deftest updated-2023-added-code-54951-test
  (testing "Regression test for postal code 54951"
    (let [postal-code "54951"
          result (filter #(= (:postal_code %) postal-code) pc/postal-codes)]
      (is (= [{:lau_2_code_2015 "1496"
               :locality_preferred_label "Skövde"
               :municipality_deprecated_legacy_id "287"
               :municipality_id "fqAy_4ji_Lz2"
               :municipality_preferred_label "Skövde"
               :national_nuts_level_3_code_2019 "14"
               :postal_code "54951"
               :region_deprecated_legacy_id "204"
               :region_id "zdoY_6u5_Krt"
               :region_term "Västra Götalands län"}]
             result)))))
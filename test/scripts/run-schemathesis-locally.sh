#!/bin/sh
#
# Run with:
# $ ./test/scripts/run-schemathesis-locally.sh

time st run --junit-xml api-junit.xml --checks all http://localhost:8080/v1/taxonomy/openapi.json 2>&1 | tee ./test/scripts/schemathesis.log

#!/bin/bash
set -e
cobertura="/tmp/lcov_cobertura.py"
wget https://raw.githubusercontent.com/eriwen/lcov-to-cobertura-xml/028da3798355d0260c6c6491b39347d84ca7a02d/lcov_cobertura/lcov_cobertura.py -O "$cobertura"
actual=$(shasum -a 256 "$cobertura")
expected="7006f44480711c005c9c9081fa46f4f53db02626ef29562d9891e0d71e157875  /tmp/lcov_cobertura.py"
if [ "$actual" != "$expected" ]; then
    echo "FAILED!"
    echo "Actual SHA:     '$actual'"
    echo "Expected SHA:   '$expected'"
    exit 1
fi
python3 "$cobertura" "$@"

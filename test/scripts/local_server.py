import atexit
from subprocess import Popen, run, PIPE
import signal
import os
import psutil
import time

# File that wget writes to
wgetOutputFile = "wget-output.html"


def terminate_children(pid):
    try:
        parent = psutil.Process(pid)
    except psutil.NoSuchProcess:
        print("no such process")
        return
    children = parent.children(recursive=True)
    for process in children:
        process.send_signal(signal.SIGKILL)
        terminate_children(process.pid)
    parent.send_signal(signal.SIGKILL)


def ping_server(p, nbr=10):
    print('pinging server:')
    for i in range(nbr):
        print('.', end='', flush=True)
        returnCode = 0
        try:
            res = run(
                [
                    "wget",
                    "-q",
                    "http://localhost:3000/v1/taxonomy/main/versions",
                    "-O",
                    wgetOutputFile,
                    "--header",
                    "accept: application/json",
                    "--header",
                    "api-key: 111",
                ],
                stdout=PIPE,
            )
            returnCode = res.returncode
        except CalledProcessError as e:
            output = e.output
            returncode = e.returncode

        if returnCode == 0:
            print("\n")
            return 0
        ret = p.poll()
        if ret != None:
            print("\n")
            return ret
        time.sleep(1)
    # Failed reaching server
    print("\n")
    return 1


api_proc = None

# TODO: This function is unlikely to work after moving the `db-backend` config
#       into the global config.


def start_server(config_build_fail_is_failure=False, backend="datomic", patch_config=True):
    global api_proc
    if patch_config:
        api_proc = Popen([
            "clj", "-A:dev", "-M", "-e", "(require 'jobtech-taxonomy-api.config)",
            "-e", "(in-ns 'jobtech-taxonomy-api.config)", "-e",
            "(def db-backend :" + backend + ")", "-e",
            "((requiring-resolve 'jobtech-taxonomy-api.core/start) {:port 3000})"
        ], stdout=PIPE, stderr=PIPE)
    else:
        my_env = {**os.environ, 'DATABASE_BACKEND': backend}
        api_proc = Popen(
            ["clj", "-A:dev", "-X", "jobtech-taxonomy-api.core/start", ":port", "3000"], stdout=PIPE, stderr=PIPE, env=my_env)

    # Register this process for termination at exit
    atexit.register(terminate_children, api_proc.pid)

    res = ping_server(api_proc, 60)
    if res != 0:
        print("Failed to start server")
        if config_build_fail_is_failure:
            return 1
        else:
            print("Server failed, but skipping commit")
            return 125  # defined in git bisect
    else:
        print("Server started")
        return 0


def stop_server():
    global api_proc
    if api_proc != None:
        api_proc.kill()
        api_proc = None
    else:
        print("stop_server: server was already stopped")


def run_query(test_endpoint, api_key):
    returnCode = 0
    try:
        res = run(
            [
                "wget",
                "-q",
                "-O",
                wgetOutputFile,
                test_endpoint,
                "--header",
                "accept: application/json",
                "--header",
                "api-key: " + api_key,
            ],
            stdout=PIPE,
            timeout=30
        )
        returnCode = res.returncode
    except TimeoutExpired as e:
        returncode = e.returncode
        print("Got TimeoutExpired with " + str(returnCode))
    except CalledProcessError as e:
        output = e.output
        returncode = e.returncode
        print("Got exception with " + str(returnCode))

    if returnCode != 0:
        print("test failed with code " + str(returnCode))
    return returnCode

#!/bin/bash

mkdir -p target

# Run the openapi validation and capture the output
swagger-cli validate openapi.json &> target/openapi-validate.log
# Check the exit status of the validation
TEST_EXIT_STATUS=$?

# Extract relevant data from the test results
FAILURES=$(grep "must" target/openapi-validate.log | wc -l | tr -d ' ')

# Create the JUnit XML report
cat <<EOF > target/openapi-validate.xml
<?xml version="1.0" encoding="UTF-8"?>
<testsuite name="ValidateOpenAPI" failures="$FAILURES">
  <properties>
    <property name="failures" value="$FAILURES"/>
  </properties>
  <testcase classname="ValidateOpenAPI" name="Validate"/>
</testsuite>
EOF

# Print the test results as text
echo "Test Summary:"
echo "Failures: $FAILURES"

# Check the exit status of the API tests
if [ $TEST_EXIT_STATUS -ne 0 ]; then
    echo "Test failed."
    exit 1  # Return non-zero exit status if the test fails
fi

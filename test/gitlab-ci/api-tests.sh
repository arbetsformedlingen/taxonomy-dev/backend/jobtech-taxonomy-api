#!/bin/bash

# Set host to the first argument if provided, otherwise use the default value
config_edn="${1:-config.edn}"
echo "Using config: $config_edn"

if grep -q 'datahike' $config_edn; then
  backend="datahike"
elif grep -q 'datomic' $config_edn; then
  backend="datomic"
else
  error "Unknown backend type in '$config_edn'. Expected 'datahike' or 'datomic'."
fi
echo "Using backend: $backend"

# Declare server_pid as a global variable
server_pid=""

# Start the server
start_server() {
    # Download all required libs in advance,
    # to not let downloading affect the timeout.
    clj -M:dev -e "(. System exit 0)"

    # Start server
    clj -M:dev -m dev --config $config_edn &

    # Get the process ID of the server
    server_pid=$!

    echo "Server started with PID: $server_pid"
}

# Wait for the server with timeout
wait_for_server_with_timeout() {
    local timeout=$1  # Timeout in seconds
    local interval=1  # Check interval in seconds
    local elapsed_time=0

    if [ -z "$timeout" ]; then
        echo "Error: Timeout value required"
        return 1
    fi

    while ! server_ready; do
        sleep $interval
        elapsed_time=$((elapsed_time + interval))
        if [ $elapsed_time -ge $timeout ]; then
            echo "Server startup timeout reached after ${elapsed_time} seconds."
            stop_server
            exit 1
        fi
    done

    echo "Server started successfully after ${elapsed_time} seconds."
}

# Check if the server is ready
server_ready() {
    local server_url="http://localhost:8080/v1/taxonomy/status/ready"
    local response=$(curl -s -o /dev/null -w "%{http_code}" $server_url)

    if [ $response -eq 200 ]; then
        return 0  # Server is ready
    else
        return 1  # Server is not ready
    fi
}

# Run the API tests
run_test() {
    mkdir -p target

    # Run the API tests and capture the test output
    clj -X:test run-real-server-tests-for-host :host "\"http://localhost:8080\"" &> target/api-tests-$backend.log
    # Check the exit status of the API tests
    TEST_EXIT_STATUS=$?

    # Extract relevant data from the test results
    RES=$(grep -E "tests.*assertions.*failures" target/api-tests-$backend.log)
    TESTS=$(echo $RES | awk '{print $1}' | sed 's/^.....//')
    ASSERTIONS=$(echo $RES | awk '{print $3}')
    FAILURES=$(echo $RES | awk '{print $5}')
    TIME=$(grep "Elapsed time:" target/api-tests-$backend.log | awk '{print $3 / 1000}')

    # Create the JUnit XML report
    cat <<EOF > target/junit-api-test-report-$backend.xml
<?xml version="1.0" encoding="UTF-8"?>
<testsuite name="ServerAPITests" tests="$TESTS" failures="$FAILURES" time="$TIME">
  <properties>
    <property name="tests" value="$TESTS"/>
    <property name="assertions" value="$ASSERTIONS"/>
    <property name="failures" value="$FAILURES"/>
    <property name="time" value="$TIME"/>
  </properties>
  <testcase classname="ServerAPITests" name="TestSuite" time="$TIME"/>
</testsuite>
EOF

    # Print the test results as text
    echo "Test Summary:"
    echo "Tests run: $TESTS"
    echo "Assertions: $ASSERTIONS"
    echo "Failures: $FAILURES"
    echo "Elapsed time: $TIME seconds"

    # Check the exit status of the API tests
    if [ $TEST_EXIT_STATUS -ne 0 ]; then
        echo "Test failed."
        stop_server
        exit 1  # Return non-zero exit status if the test fails
    fi
}

# Stop server function
stop_server() {
    echo "Stopping server..."
    kill $server_pid  # Send SIGTERM to the server process
}

# Cleanup function to stop the server
cleanup() {
    stop_server
}

# Trap Ctrl+C (SIGINT) signal to execute cleanup function before exiting
trap cleanup SIGINT

# Main script
start_server
wait_for_server_with_timeout 150
run_test
stop_server

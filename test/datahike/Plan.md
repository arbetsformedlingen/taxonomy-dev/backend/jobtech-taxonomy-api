# Datahike Plan

The end goal is to get Datahike in production before the end of this procurement.

## Visibility
We want complete visibility on the following things:
- track progress in git
- who works on what
- when are things estimated to be done

## Developers
- Konrad: Mo, We, Fr
- Judith: Th, Fr
- Yee Fay: Tu-Fri

## Prio
These are the steps needed to reach the goal:
1. __Done.__ ~~Fix all functional errors, defined by a pass of the kaocha tests~~
    ```
    DATABASE_BACKEND=datahike clj -A:test -M:kaocha
    ```
    * [attribute access support](https://github.com/replikativ/datahike/issues/470), [PR](https://github.com/replikativ/datahike/pull/472), merged
    * [keyword keys in queries](https://github.com/replikativ/datahike/issues/471), [PR in datalog-parser](https://github.com/lambdaforge/datalog-parser/pull/23), merged
1. ~~All dependencies should be versioned~~, __Done__
    ```
    io.replikativ/konserve
    io.replikativ/datahike
    io.replikativ/sendandi
    io.lambdaforge/wanderung
    ```
    * ~~https://github.com/replikativ/datahike/pull/496~~, __Done__
    * [~~move to new release cycle~~](https://github.c~m/replikativ/konserve/issues/69), __Done__
    * [~~[Bug]: konserve versions between release and testing are different~~](https://github.com/replikativ/datahike/issues/467), __Done__
1. ~~Deploy in U1 test environment. U1 is the least important of the test environments.~~
    __Done__
    * [~~Datahike performance issue in main/concepts~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/410), __Done__
    * [~~Datahike performance issue in graphql~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/411), __Done__
1. ~~Check for functional errors triggered by U1~~, __Done, but U1 provides very little test coverage__
1. Check if performance is good enough, within 30% of Datomic. See Datomic baseline in milestone https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/milestones/8#tab-issues~
    * [~~Transient load entities fix (Speed up wanderung migration~~](https://github.com/replikativ/datahike/pull/499), __merged__
    * [~~Datahike performance scripts~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/merge_requests/216), __merged__
    * [~~477 history migration not working~~](https://github.com/replikativ/datahike/pull/496), __merged__, May have some effect on performance.
    * [~~migration with attr refs error~~](https://github.com/replikativ/datahike/pull/501), __merged__
    * [~~re-order concepts datalog query~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/merge_requests/220), __merged__
    * [~~Datahike performance issue in main/concepts~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/410), __merged__
    * [~~Datahike performance issue in graphql~~](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/411), __done__
    * [The graphql endpoint it slow for some queries](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/437)
    * [In-memory caching for indexes](https://github.com/replikativ/datahike/discussions/505), __This might improve our performance as well__
1. ~~Deploy in I1 test environment, where the traffic is more similar to the production environmen~~, __Done__
1. ~~Check for functional errors triggered by I1~~, __None found so far__
1. ~~Deploy to Production~~, __Done__
1. ~~Evalutate deployment~~, __Done__
    The deployment to PROD was reverted due to
    * ~~[BUG: SSYK relation to Occupation-Names doesn't get deleted](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/448)~~, __Solved by https://github.com/replikativ/datahike/issues/558__
    * ~~[~UG: Missing alternative labels from GraphQL API](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/447)~~, __Solved by https://github.com/replikativ/datahike/pull/561__
1. ~~Deploy Datahike to test env~~
1. Evaluate test deployment
    * ~~Create a way to compare results from Datomic and Datahike, [Create a Datahike middleware that queries Datomic](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/475)~~
    * ~~[Uncoupled attribute still present in I1](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/473), corresponding [Datahike issue](https://github.com/replikativ/datahike/issues/565~~
    * Check logs in https://opensearch-test.jobtechdev.se for functional errors and performance problems.
    * ~~[Fix sys attrs in history](https://github.com/replikativ/datahike/pull/597), merged~~
    * ~~Some of the diffs between datahike and datomic seems to be from comparing lists of answers with different orders but including the same elements. A Fix for this will be included in [I/521/parallel db test](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/merge_requests/239~~
    * [Query stats](https://github.com/replikativ/datahike/pull/601)
    * [Inconsistent labels for 2d-grafiker](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/544), the datomic middleware created some functional problems due to mappings of tx values. This implementation will be moved to the api middleware in handler.clj instead.
1. Deploy Datahike to prod env
1. Evaluate prod deployment
1. Remove Datomic
    * Datahike needs to run in the editor environment
    * The migration process needs to include Datahike
      * ~~[Performance problems migrating a database with wanderung](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/issues/406), __Waiting for durable persistant set,~~ Update: Use nippy instead.__
      * ~~[Durable persistent set](https://github.com/replikativ/datahike/pull/503)~~, needed by our database migrations in the gitops infra, ~~__Waiting for a patch in external dependency https://github.com/tonsky/persistent-sorted-set/pull/7.~~ This will be included in release in end of September, and in beginning of October the MR will be merged in Datahike. The performance testing is not completely finished, but many problems have already been fixed.__
      * ~~[[Bug]: Temporal upsert and retract not working for persistent set index](https://github.com/replikativ/datahike/issues/519), __Will be included in PR for Durable persistent set_~~

## Branch
All tests need to pass on the develop branch for both Datomic and Datahike, https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/tree/develop
```
clj -A:test -M:kaocha # Datomic
DATABASE_BACKEND=datahike clj -A:test -M:kaocha # Datahike
```

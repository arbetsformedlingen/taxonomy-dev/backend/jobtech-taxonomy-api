(ns profiler
  (:require [clj-async-profiler.core :as prof]
            [jobtech-taxonomy.api.config :refer [get-config]]
            [jobtech-taxonomy.api.db.database-connection :as dc]))

(set! *warn-on-reflection* true)

(defn my-postwalk
  [f form]
  (let [inner (partial my-postwalk f)]
    (cond
      (list? form) (f form (apply list (map inner form)))
      (instance? clojure.lang.IMapEntry form)
      (f form (clojure.lang.MapEntry/create (inner (key form)) (inner (val form))))
      (seq? form) (f form (doall (map inner form)))
      (instance? clojure.lang.IRecord form)
      (f form (reduce (fn [r x] (conj r (inner x))) form form))
      (coll? form) (f form (into (empty form) (map inner form)))
      :else (f form form))))

(defn is-fn-call? [x]
  (and (seq? x)
       (symbol? (first x))))

(defmacro decorate-calls [f expr]
  (my-postwalk
   (fn [src x]
     (if (is-fn-call? x)
       `(let [result# ~x]
          (~f (quote ~src) result#)
          result#)
       x))
   expr))

(def call-store (atom []))

(decorate-calls
 (fn [e r] (swap! call-store conj {:expr e :result r}))
 ((fn [x] (let [d (+ x x)] d)) 3))

; Prova det här
(decorate-calls println (+ (* 3 4) (* 9 10)))

(comment
  (* (+ 1 2) (- 2 3)))

(defn ^:export time-rec [expr]
  (let [start (.now)
        res (expr)
        end (.now)]
    (tap>  (str expr (- end start)))
    res))
(comment
  ;; Run these commands in the terminal:
  ;; sudo sysctl -w kernel.perf_event_paranoid=1
  ;; sudo sysctl -w kernel.kptr_restrict=0

  ;; Load a config 
  (def cfg (get-config []))
  ;; Init the DB
  (dc/init-db cfg)

  ;; Start the web server that shows results!
  (prof/serve-ui "0.0.0.0" 8070)

  ;; Profile something!
  ;; But how? Perhaps some more interesting, understandable,
  ;; and certainly useful examples should be added here.
  #_{:clj-kondo/ignore [:unused-namespace]}
  #_(require '[jobtech-taxonomy.api.db.daynotes :as daynotes])
  #_(let [after-version 15
        to-version-inclusive nil
        offset nil
        limit nil]
    #_(decorate-calls
       (daynotes/get-relation-changes-with-pagination-public cfg after-version to-version-inclusive offset limit)))

  'bye)

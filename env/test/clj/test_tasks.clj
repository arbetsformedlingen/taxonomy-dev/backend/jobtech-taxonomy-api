(ns test-tasks
  "This file contains tasks related to the unit and integration tests. Run them with `clj -X:test <function-name>`"
  (:require [integration.jobtech-taxonomy.api.api-test :as api-test]
            [utils.integration-helpers :as integration-helpers]))

;; Command line: clj -X:test rebuild-test-data
(defn rebuild-test-data
  [_]
  (api-test/rebuild-test-data))

;; Command line: clj -X:test run-real-server-tests-for-host :host '"https://taxonomy.api.jobtechdev.se"'
(defn run-real-server-tests-for-host
  [{:keys [host]}]
  {:pre [(string? host)]}
  (integration-helpers/run-real-server-tests-with-host host))

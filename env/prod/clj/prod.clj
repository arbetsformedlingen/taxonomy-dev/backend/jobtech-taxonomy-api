(ns prod
  (:require
          [jobtech-taxonomy.api.config :refer [get-config]]
          [jobtech-taxonomy.api.core :as jtac]
          [taoensso.timbre :as log])
  (:gen-class))

(defn instrument-config [cfg]
  (log/info "Loading config" (:path cfg) (:database-backend cfg) (:compare-backends cfg))
  cfg)

(def ^:export defaults
  {:init
   (fn []
     (log/info "\n-=[jobtech-taxonomy.api started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[jobtech-taxonomy.api has shut down successfully]=-"))
   :middleware identity})

(defn -main [& args]
  (jtac/start-http-server
   (instrument-config (get-config args))))

(ns dev-mapping 
  (:require [clojure.pprint :as pp]
            [jobtech-taxonomy.api.config :refer [get-config]]
            [user]))

(comment

  (require '[jobtech-taxonomy.api.db.database-connection :as dc]
           '[mapping.config] '[mapping.create]
           '[mapping.edge] '[mapping.external]
           '[mapping.mapping])

  (get-config [])
  (def conn (dc/connect @user/dev-cfg))

  ;; This uses the unpublished version of the taxonomy
  (def db (dc/get-db @user/dev-cfg :next))

  (pp/pprint
   (dc/q '[:find (pull ?e [:db/ident])
           :in $
           :where
           [?e :db/ident _]] db))

  (dc/transact conn mapping.mapping/schema)
  (dc/transact conn mapping.edge/schema)
  (dc/transact conn mapping.external/schema)
  (doseq [mapping-data mapping-data-collection]
    (mapping.create/create conn mapping-data))

  (def mapping-data-collection [{:mapping
                      {:type :broad-match
                       :organisation "ESCO"
                       :family "ESCO"
                       :description "Occupation mappings between Taxonomy and ESCO."
                       :uri "https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_yrken.md?ref_type=heads"
                       :name "ESCO-occupations"
                       :version "1.0.8"
                       :taxonomy-version "16"}
                      :config
                      {:source
                       {:id :concept/id
                        :include
                        #:concept{:type :type :preferred-label :preferred-label}}
                       :target
                       {:id :concept/id
                        :mapping :concept.external-standard/esco-uri
                        :include
                        {:concept/type :type
                         :concept/preferred-label :preferred-label
                         :concept.external-standard/isco-code-08 :isco}}}
                      :data
                      {:edges
                       [["ghs4_JXU_BYt" "XAcf_SQ9_fJw"]
                        ["Jx4V_6tm_fUH" "DmTx_t6n_jbW"]
                        ["YcvM_Gqk_6U7" "3eya_Cn2_Bt1"]
                        ["P2bU_f8o_Lkw" "TMGH_QHL_aE3"]
                        ["v5nC_xX1_Y7U" "TMGH_QHL_aE3"]
                        ["6ZJd_HTR_Bki" "tvYo_sjZ_JZW"]
                        ["Qzzb_67o_n2P" "fAjK_TWk_kpj"]
                        ["xcd7_WRf_3Ed" "fAjK_TWk_kpj"]
                        ["XMbW_XR6_Zeh" "fAjK_TWk_kpj"]
                        ["JQYZ_oUR_LEq" "fAjK_TWk_kpj"]
                        ["Fy1A_qaK_t5R" "fAjK_TWk_kpj"]
                        ["XGUw_uqV_gq6" "fAjK_TWk_kpj"]]}}
                     {:mapping
                      {:type :close-match
                       :organisation "ESCO"
                       :family "ESCO"
                       :description "Occupation mappings between Taxonomy and ESCO."
                       :uri
                       "https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_yrken.md?ref_type=heads"
                       :name "ESCO-occupations"
                       :version "1.0.8"
                       :taxonomy-version "16"}
                      :config
                      {:source
                       {:id :concept/id
                        :include
                        #:concept{:type :type :preferred-label :preferred-label}}
                       :target
                       {:id :concept/id
                        :mapping :concept.external-standard/esco-uri
                        :include
                        {:concept/type :type
                         :concept/preferred-label :preferred-label
                         :concept.external-standard/isco-code-08 :isco}}}
                      :data
                      {:edges
                       [["JQYZ_oUR_LEq" "N2D8_PTA_tYn"]
                        ["JQYZ_oUR_LEq" "aE6g_Moa_R3y"]
                        ["TRVV_bXp_BG9" "5imw_7zt_qEf"]
                        ["T6yh_MNf_CKv" "116C_d8K_W8e"]
                        ["6VUH_a4V_VdQ" "XL6M_S3b_KmM"]
                        ["RnSZ_Ub1_DiV" "NnB8_wbV_qwn"]
                        ["SsZw_eeZ_ZHF" "Gwkq_9Ao_rud"]
                        ["BK7A_kh5_xWc" "HjzM_9FE_Dsy"]
                        ["h2ta_5hn_hAf" "R3qt_KF6_FLp"]
                        ["nH7B_1c1_aMC" "Ts8R_hQs_MNN"]
                        ["amZE_UqJ_ACn" "emoh_kPz_Djs"]
                        ["pdKf_yyX_vWc" "z2jE_YcB_bAA"]
                        ["z5Dw_B8T_qu2" "4fzB_Zb1_5sN"]
                        ["bsth_VZF_chf" "4fzB_Zb1_5sN"]]}}
                     {:mapping
                      {:type :narrow-match
                       :organisation "ESCO"
                       :family "ESCO"
                       :description "Occupation mappings between Taxonomy and ESCO."
                       :uri
                       "https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_yrken.md?ref_type=heads"
                       :name "ESCO-occupations"
                       :version "1.0.8"
                       :taxonomy-version "16"}
                      :config
                      {:source
                       {:id :concept/id
                        :include
                        #:concept{:type :type :preferred-label :preferred-label}}
                       :target
                       {:id :concept/id
                        :mapping :concept.external-standard/esco-uri
                        :include
                        {:concept/type :type
                         :concept/preferred-label :preferred-label
                         :concept.external-standard/isco-code-08 :isco}}}}
                     {:mapping
                      {:type :exact-match
                       :organisation "ESCO"
                       :family "ESCO"
                       :description "Occupation mappings between Taxonomy and ESCO."
                       :uri
                       "https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_yrken.md?ref_type=heads"
                       :name "ESCO-occupations"
                       :version "1.0.8"
                       :taxonomy-version "16"}
                      :config
                      {:source
                       {:id :concept/id
                        :include
                        #:concept{:type :type :preferred-label :preferred-label}}
                       :target
                       {:id :concept/id
                        :mapping :concept.external-standard/esco-uri
                        :include
                        {:concept/type :type
                         :concept/preferred-label :preferred-label
                         :concept.external-standard/isco-code-08 :isco}}}
                      :data
                      {:edges
                       [["GPNi_fJR_B2B" "iCXb_uUw_Fye"]
                        ["GPNi_fJR_B2B" "TYat_S2W_WHT"]
                        ["rQds_YGd_quU" "ApaR_Uzn_nd2"]
                        ["YqiW_QLd_qHo" "GyLz_ubY_Akf"]
                        ["cAJj_uJb_s9C" "zwaU_Qv3_kf3"]
                        ["QrW9_R6M_QRK" "iYgH_F5a_4JA"]
                        ["Da9t_kGe_uFD" "BidP_vWQ_4c5"]
                        ["T6yh_MNf_CKv" "Zhzv_6xp_Nmi"]]}}]))
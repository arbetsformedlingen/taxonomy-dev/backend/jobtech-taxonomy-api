(ns swagger-generator
  (:require
   [clj-http.client :as client]
   [dev]
   [jobtech-taxonomy.api.config :as config]
   [jobtech-taxonomy.common.io-utils :as iou]
   [taoensso.timbre :as log]))

;; This namespace is only used by CI to get the openapi.json file.

(defn generate-openapi-json
  "This function writes openapi.json to disk"
  [cfg]
  (dev/with-http-server-fn cfg
    (fn [{:keys [url]}]
      (let [swagger-url (str url "/v1/taxonomy/openapi.json")
            {:keys [body status]} (client/get swagger-url {:accept :json})]
        (if (and (= 200 status) body)
          (spit "openapi.json" body)
          (println "Failed to generate Swagger spec!"))))))

(defn- instrument-config [cfg]
  (log/info "Loading config" (config/->backend-name cfg))
  (let [backend-storage-dir (config/->backend-local-storage-dir cfg)]
    (log/info "Storage dir" backend-storage-dir)
    (if (and backend-storage-dir (iou/is-relative-path? backend-storage-dir))
      (config/update-storage-dir-path cfg backend-storage-dir)
      cfg)))

;; Define the -main function that calls generate-openapi-json
(defn -main [& args]
  (generate-openapi-json (instrument-config (config/get-config args))))

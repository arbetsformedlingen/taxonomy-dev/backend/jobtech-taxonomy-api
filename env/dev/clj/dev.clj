(ns dev
  (:require [dev-middleware :refer [wrap-dev]]
            [jobtech-taxonomy.api.config :as config]
            [jobtech-taxonomy.api.core :as jtac]
            [jobtech-taxonomy.common.io-utils :as iou]
            [mount.core :as mount]
            [nrepl]
            [selmer.parser :as parser]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)

(defn- instrument-config [cfg]
  (log/info "Loading config" (config/->backend-name cfg))
  (let [backend-storage-dir (config/->backend-local-storage-dir cfg)]
    (log/info "Storage dir" backend-storage-dir)
    (if (and backend-storage-dir (iou/is-relative-path? backend-storage-dir))
      (config/update-storage-dir-path cfg backend-storage-dir)
      cfg)))

(def ^:export defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[jobtech-taxonomy.api started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[jobtech-taxonomy.api has shut down successfully]=-"))
   :middleware wrap-dev})

(mount/defstate ^{:on-reload :noop :export true} http-server
  :start
  (jtac/start-http-server (mount/args))
  :stop
  (when http-server (.stop http-server)))

(mount/defstate ^{:on-reload :noop} repl-server
  :start
  (let [cfg (mount/args)]
    (when (:nrepl-port cfg)
      (nrepl/start {:bind (:nrepl-bind cfg)
                    :port (:nrepl-port cfg)})))
  :stop
  (when repl-server
    (nrepl/stop repl-server)))

(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info :dev/stopping component "stopped"))
  (shutdown-agents))

(defn start [cfg]
  (log/debug :dev/start :cfg cfg)
  (doseq [component (:started (mount/start-with-args cfg))]
    (log/info :dev/starting component "started"))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))

(defn with-http-server-fn
  "This function will start a server with configuration `cfg`, pass a map with the server configuration to `body-fn` and execute that function, and finally stop the server."
  [cfg body-fn]
  (let [server (jtac/start-http-server cfg)
        connectors (.getConnectors server)
        port (if (seq connectors)
               (.getLocalPort (first connectors))
               8080) ;; Default to port 8080 if no connectors found
        url (format "http://localhost:%d" port)]
    (try
      (body-fn {:server server :url url})
      (finally
        (jtac/stop-http-server cfg server)))))

(defmacro ^:export with-http-server
  "This macro is used to run the API http server for a given configuration `cfg` and will bind `server-data` to a map with the keys `:server` and `:url`. It will then execute the `body` forms and turn off the server."
  [[server-data cfg] & body]
  `(with-http-server-fn ~cfg (fn [~server-data] ~@body)))

(defn -main [& args]
  (log/debug :dev/main :args args)
  (-> (config/get-config args)
      instrument-config
      start))

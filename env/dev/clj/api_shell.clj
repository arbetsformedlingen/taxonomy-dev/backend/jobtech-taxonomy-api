(ns api-shell
  (:require
   [clojure.java.shell :as sh]
   [dev]
   [jobtech-taxonomy.api.config :as config]
   [jobtech-taxonomy.common.io-utils :as iou]
   [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)

(defn run-shell-command
  "Runs a shell command and returns the exit code"
  [command]
  (try
    (let [_ (println "Running shell command:" command)
          result (sh/sh "sh" "-c" command)
          _ (println "Result from shell command:" result)]
      ;; Return the exit code of the command
      (:exit result))
    (catch Exception e
      (println "Error running command:" (.getMessage e))
      1))) ;; Return exit code 1 on exception

(defn api-run-shell
  "This function runs the provided shell command after the HTTP server starts."
  [cfg command]
  (dev/with-http-server-fn cfg
    (fn [{:keys [url]}]
      ;; Run the shell command once the server has started
      (run-shell-command command))))

(defn- instrument-config [cfg]
  (log/info "Loading config" (config/->backend-name cfg))
  (let [backend-storage-dir (config/->backend-local-storage-dir cfg)]
    (log/info "Storage dir" backend-storage-dir)
    (if (and backend-storage-dir (iou/is-relative-path? backend-storage-dir))
      (config/update-storage-dir-path cfg backend-storage-dir)
      cfg)))

(defn -main
  [& args]
  (if (empty? args)
    (do
      (println "No shell command provided.")
      (System/exit 1))
    (let [command (last args) ;; The last argument is the shell command
          config-args (butlast args)
          config (instrument-config (config/get-config config-args))
          exit-code (api-run-shell config command)]
      (println "Shell command exit code:" exit-code)
      (System/exit exit-code))))

# This is a multi-stage build.
# The first stage is used to prepare the runtime image.
# The second stage is used to build the application.
# The third stage is used to run the application.

# The image used to run the application
# Here we add the helper software that do not change often.
# Also setup the user that will run the application and
# give it permissions for the application folder.
# This caches the layer and speeds up the build.
FROM eclipse-temurin:23_37-jre-noble as runtime

RUN apt-get update && apt-get install -y --no-install-recommends \
    graphviz=2.42.2-9ubuntu0.1 \
    && rm -rf /var/lib/apt/lists/* \
    && useradd -ms /bin/bash runner

# The image used to build the application.
# We copy the entire directory so that we get the benefit
# of caching the dependencies and prepared libraries.
FROM clojure:temurin-23-tools-deps-noble as builder

WORKDIR /build

COPY . .

# If built in a clean repo, this step is needed
# to prepare the clojure libraries.
RUN clojure -A:prod:build -P

# Build the application uber-jar.
RUN clojure -T:build uber

# The final image that will run the application.
# We copy the built jar and the resources,
# expose the default port and run the application.
FROM runtime

WORKDIR /app

COPY --from=builder /build/target/app.jar taxonomy-api.jar
COPY --from=builder /build/resources/taxonomy.zip resources/taxonomy.zip
COPY --from=builder /build/resources/mappings resources/mappings

RUN chown -R runner:runner /app

USER runner

EXPOSE 3000

CMD [ "java", "-jar", "/app/taxonomy-api.jar" ]